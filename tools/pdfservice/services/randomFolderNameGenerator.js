/**
 * Finds a random folder name that does not exist under given directory
 *
 * @param {string} under path of given directory
 *
 * @returns {string} a random folder name
 */
exports.getRandomFolderName = (under) => {
    const Fs = require('fs-extra');
    const PathHelper = require('path');
    const uuidv4 = require('uuid/v4');

    let randomFolderName = null;
    do {
        randomFolderName = uuidv4();
    }
    while (Fs.existsSync(PathHelper.join(under, randomFolderName)));

    return randomFolderName;
}