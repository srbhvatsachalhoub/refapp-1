const Router = require('koa-router');
const PdfGenerator = require('../services/pdfGenerator');
const DirectoryZipper = require('../services/directoryZipper');
const router = Router({ prefix: '/pdf' });

router.post('/', async function (ctx) {
    // generate all pdfs
    const workingDirectoryPath = await PdfGenerator.generateAllPdfs(ctx.request.body);

    // collects files from disk and archives them directly to output stream
    ctx.body = DirectoryZipper.zipEverything(workingDirectoryPath);
});

module.exports = router;