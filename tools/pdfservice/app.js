const Koa = require('koa');
const Logger = require('koa-logger')
const bodyParser = require('koa-body');
const Auth = require('koa-basic-auth');
const AuthMiddleware = require('./auth/middleware');
const App = new Koa();


App.use(Logger());
App.use(bodyParser());
App.use(AuthMiddleware.auth);
App.use(Auth(require('./auth/cred.json').current));

var pdfController = require('./controllers/pdf');
App.use(pdfController.routes());

App.listen(8150);