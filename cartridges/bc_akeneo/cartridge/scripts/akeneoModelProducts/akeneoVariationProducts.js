'use strict';

var StringUtils = require('dw/util/StringUtils');
var HashSet = require('dw/util/HashSet');

var generalUtils = require('~/cartridge/scripts/utils/generalUtils');
var config = generalUtils.config;
var StringUtilsExt = require('~/cartridge/scripts/utils/libStringUtilsExt');
var akeneoVariationProducts = {};

/**
 * @desc Find variation attribute options
 * @param {string} akeneoAttributeCode -akeneoAttribute
 * @param {string} getOptionValue -attributeOptionValue
 * @returns {Object} - the set of attribute options
 */
function getAttributeOptions(akeneoAttributeCode, getOptionValue) {
    var akeneoServicesHandler = require('~/cartridge/scripts/utils/akeneoServicesHandler');
    var akeneoService = require('~/cartridge/scripts/akeneoServices/initAkeneoServices');
    var AkeneoAttributesService = akeneoService.getGeneralService();
    var attributeOptions = {};
    var attrOptionsUrl = config.serviceGeneralUrl + config.APIURL.endpoints.attributes + '/' + akeneoAttributeCode + '/options/' + getOptionValue;

    attributeOptions = akeneoServicesHandler.serviceAttributeOptionsValues(AkeneoAttributesService, attrOptionsUrl);

    return attributeOptions;
}

/**
 * @desc Writes variation values and attributes XML
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - xml stream writer
 * @param {Object} modelProduct - model product object
 * @returns {dw.util.HashSet} - the set of values written to XML
 */
function writeVariationAttributes(xswHandle, modelProduct) {
    var productCustomAttrsMapping = config.customAttrsMapping;
    var variationProducts = modelProduct.variationProducts;
    var variationValuesSet = new HashSet();
    var getSortingIdentifiers = new HashSet();
    var getSortingValues = new HashSet();
    var variationProductsNew = [];

    if (variationProducts && variationProducts.length > 0) {
        var variantAttributeSets = variationProducts[0].variant_attribute_sets;
        for (var i = 0; i < variantAttributeSets.length; i++) {
            var axes = variantAttributeSets[i].axes;
            for (var j = 0; j < axes.length; j++) {
                xswHandle.writeStartElement('variation-attribute');

                var modelValues = modelProduct.values;
                var variationAttr = axes[j];
                var variationValues = modelValues[variationAttr];
                var camelizedAttrKey = 'akeneo_' + StringUtilsExt.camelize(variationAttr);
                var displayVariationAttr;
                var attributedefinitionlabels = {};
                var sortIdentifiersNew = '';
                var getOptionData = [];
                var attributeOptions = [];
                var varArr = [];
                for (var varProductIndex = 0; varProductIndex < variationProducts.length; varProductIndex++) {
                    var varValue = variationProducts[varProductIndex].values[variationAttr];
                    var varProductKey = Object.keys(variationProducts[varProductIndex].values);
                    getOptionData.push(variationProducts[varProductIndex].values[varProductKey][0].data);

                    for (var optionDataIndex = 0; optionDataIndex < getOptionData.length; optionDataIndex++) {
                        var optionValue = getOptionData[optionDataIndex];
                        var hasToBeAdded = true;
                        for (var attrOptInd = 0; attrOptInd < attributeOptions.length; attrOptInd++) {
                            if (attributeOptions[attrOptInd].code === optionValue) {
                                hasToBeAdded = false;
                                break;
                            }
                        }
                        if (hasToBeAdded) {
                            var newAttrOption = getAttributeOptions(variationAttr, getOptionData[optionDataIndex]);
                            attributeOptions.push(newAttrOption);
                        }
                    }

                    var sortorder = '';
                    for (var attrOptIndex = 0; attrOptIndex < attributeOptions.length; attrOptIndex++) {
                        if (attributeOptions[attrOptIndex].code === varValue[0].data) {
                            sortorder = attributeOptions[attrOptIndex].sort_order;
                        }
                    }
                    if (sortorder) {
                        getSortingValues.add(variationProducts[varProductIndex].identifier + '+' + sortorder);
                        getSortingIdentifiers.add(variationProducts[varProductIndex].identifier + '+' + sortorder);
                    }
                }

                if (getSortingValues.length > 0) {
                    for (var f = 0; f < getSortingValues.length; f++) {
                        var getIdentifierMapping = getSortingValues[f].toString();
                        sortIdentifiersNew = getIdentifierMapping.split('+');
                        varArr.push({ sort: sortIdentifiersNew[1], identifier: sortIdentifiersNew[0] });
                    }
                }
                varArr.sort(function (a, b) {
                    return a.sort > b.sort ? 1 : -1;
                });
                if (varArr.length > 0) {
                    for (var varArrIndex = 0; varArrIndex < varArr.length; varArrIndex++) {
                        for (var varProdIndex = 0; varProdIndex < variationProducts.length; varProdIndex++) {
                            if (varArr[varArrIndex].identifier === variationProducts[varProdIndex].identifier) {
                                variationProductsNew.push(variationProducts[varProdIndex]);
                            }
                        }
                    }
                }
                if (camelizedAttrKey in productCustomAttrsMapping.matching) {
                    displayVariationAttr = productCustomAttrsMapping.matching[camelizedAttrKey];
                } else {
                    displayVariationAttr = camelizedAttrKey;
                }
                xswHandle.writeAttribute('attribute-id', displayVariationAttr);
                xswHandle.writeAttribute('variation-attribute-id', displayVariationAttr);

                if (attributeOptions.length > 0) {
                    for (var r = 0; r < attributeOptions.length; r++) {
                        attributedefinitionlabels = Object.keys(attributeOptions[r].labels);
                    }
                }
                if (Object.keys(attributedefinitionlabels).length > 1) {
                    for (var p = 0; p < attributedefinitionlabels.length; p++) {
                        xswHandle.writeStartElement('display-name');
                        xswHandle.writeAttribute('xml:lang', attributedefinitionlabels[p].replace('_', '-'));
                        xswHandle.writeCharacters(StringUtilsExt.capitalize(displayVariationAttr));
                        xswHandle.writeEndElement(); // display-name
                    }
                } else if (variationValues && variationValues[0].locale) {
                    for (var y = 0; y < variationValues.length; y++) {
                        xswHandle.writeStartElement('display-name');
                        xswHandle.writeAttribute('xml:lang', variationValues[y].locale.replace('_', '-'));
                        xswHandle.writeCharacters(StringUtilsExt.capitalize(displayVariationAttr));
                        xswHandle.writeEndElement(); // display-name
                    }
                } else {
                    xswHandle.writeStartElement('display-name');
                    xswHandle.writeAttribute('xml:lang', 'x-default');
                    xswHandle.writeCharacters(StringUtilsExt.capitalize(displayVariationAttr));
                    xswHandle.writeEndElement(); // display-name
                }

                xswHandle.writeStartElement('variation-attribute-values');
                var enteredValues = [];

                for (var l = 0; l < variationProductsNew.length; l++) {
                    var variationValue = variationProductsNew[l].values[variationAttr];
                    if (enteredValues.indexOf(variationAttr + '-' + variationValue[0].data) === -1) { // if same value was not entered earlier
                        xswHandle.writeStartElement('variation-attribute-value');
                        xswHandle.writeAttribute('value', variationValue[0].data);
                        xswHandle.writeStartElement('display-value');
                        xswHandle.writeAttribute('xml:lang', 'x-default');
                        xswHandle.writeCharacters(variationValue[0].data);
                        xswHandle.writeEndElement(); // display-value
                        for (var optionIndex = 0; optionIndex < attributeOptions.length; optionIndex++) {
                            var attributeOption = attributeOptions[optionIndex];
                            if (attributeOption.code === variationValue[0].data) {
                                Object.keys(attributeOption.labels).forEach(function (property) {// eslint-disable-line 
                                    xswHandle.writeStartElement('display-value');
                                    xswHandle.writeAttribute('xml:lang', property.replace('_', '-').toString());
                                    xswHandle.writeCharacters(attributeOption.labels[property].toString());
                                    xswHandle.writeEndElement();
                                });
                            }
                        }
                        xswHandle.writeEndElement(); // variation-attribute-value
                        variationValuesSet.add(variationValue[0].data);
                        enteredValues.push(variationAttr + '-' + variationValue[0].data);
                    }
                }
                xswHandle.writeEndElement(); // variation-attribute-values

                xswHandle.writeEndElement(); // variation-attribute
            }
        }
    }

    return {
        variationValuesSet: variationValuesSet,
        sortingIdentifiers: getSortingIdentifiers
    };
}

/**
 * @desc Writes variation group tags
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML stream writer
 * @param {Object} akeneoProduct - the product object
 * @param {dw.util.HashSet} variationValues - the values for variation attributes
 * @returns {void}
 */
function writeVariationGroupXML(xswHandle, akeneoProduct, variationValues) {
    var modelList = akeneoProduct.modelList || [];

    if (modelList && modelList.length > 0) {
        var customCacheWebdav = require('~/cartridge/scripts/io/customCacheWebdav');
        xswHandle.writeStartElement('variation-groups');

        for (var i = 0; i < modelList.length; i++) {
            var modelFileName = StringUtils.format(config.cacheDirectory.modelProducts.endPoint, modelList[i]);
            var modelProduct = customCacheWebdav.getCache(modelFileName);

            if (modelProduct) {
                var modelValues = modelProduct.values;
                var attrKeys = Object.keys(modelValues);

                for (var j = 0; j < attrKeys.length; j++) {
                    var productAttr = modelValues[attrKeys[j]];

                    if (variationValues.contains(productAttr[0].data)) {
                        xswHandle.writeEmptyElement('variation-group');
                        xswHandle.writeAttribute('product-id', modelProduct.code);
                        break;
                    }
                }
            }
        }
        xswHandle.writeEndElement(); // variation-groups
    }
}

/**
 * @desc Writes variation attributes XML
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML stream writer
 * @param {Object} modelProduct - model product object
 * @returns {void}
 */
akeneoVariationProducts.variationProducts = function (xswHandle, modelProduct) {
    var variationProducts = modelProduct.variationProducts;
    var varArr = [];
    var sortIdentifiers = '';

    try {
        if (variationProducts && variationProducts.length > 0) {
            xswHandle.writeStartElement('product');
            xswHandle.writeAttribute('product-id', modelProduct.code);
            xswHandle.writeStartElement('variations');

            xswHandle.writeStartElement('attributes');
            var variationValues = writeVariationAttributes(xswHandle, modelProduct);
            xswHandle.writeEndElement(); // attributes

            xswHandle.writeStartElement('variants');

            if (variationValues.sortingIdentifiers.length > 0) {
                for (var f = 0; f < variationValues.sortingIdentifiers.length; f++) {
                    var getIdentifierMapping = variationValues.sortingIdentifiers[f].toString();
                    sortIdentifiers = getIdentifierMapping.split('+');
                    varArr.push({ sort: sortIdentifiers[1], identifier: sortIdentifiers[0] });
                }
            }
            varArr.sort(function (a, b) {
                return a.sort > b.sort ? 1 : -1;
            });

            if (varArr.length > 0) {
                for (var ind = 0; ind < varArr.length; ind++) {
                    xswHandle.writeEmptyElement('variant');
                    xswHandle.writeAttribute('product-id', varArr[ind].identifier);
                }
            } else {
                for (var i = 0; i < variationProducts.length; i++) {
                    xswHandle.writeEmptyElement('variant');
                    xswHandle.writeAttribute('product-id', variationProducts[i].identifier);
                }
            }
            xswHandle.writeEndElement(); // variants

            if (config.modelImport.type === 'master-group-variation') {
                writeVariationGroupXML(xswHandle, modelProduct, variationValues.variationValuesSet);
            }
            xswHandle.writeEndElement(); // variations
            xswHandle.writeEndElement(); // product
        }
    } catch (e) {
        throw new Error('ERROR: while writing variation products for product:' + modelProduct.code + ', stack: ' + e.stack + ', message: ' + e.message);
    }
};

module.exports = akeneoVariationProducts;
