'use strict';

// API Includes
var File = require('dw/io/File');
var StringUtils = require('dw/util/StringUtils');
var FileWriter = require('dw/io/FileWriter');
var XMLIndentingStreamWriter = require('dw/io/XMLIndentingStreamWriter');
var Calendar = require('dw/util/Calendar');

// Lib includes
var fileUtils = require('~/cartridge/scripts/io/libFileUtils').FileUtils;
var stringUtilsExt = require('~/cartridge/scripts/utils/libStringUtilsExt');
var generalUtils = require('~/cartridge/scripts/utils/generalUtils');
var config = generalUtils.config;

var akeneoCreateProductsXML = {};

/**
 * @desc Write Header & Static part of akeneo catalog
 *
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - the XML writer
 *
 * @returns {void}
 */
function writeAkeneoCatalogHeader(xswHandle) {
    var catalogID = config.sfccMasterCatalogID;
    var viewTypes = config.imageViewTypes;
    var akeneoImageVariationValue = config.imageVariationValue;
    var customAttrsMapping = config.customAttrsMapping;
    var matchingKey = 'akeneo_' + stringUtilsExt.camelize(akeneoImageVariationValue);

    if (matchingKey in customAttrsMapping.matching) {
        akeneoImageVariationValue = customAttrsMapping.matching[matchingKey];
    }

    // XML definition & first node
    xswHandle.writeStartDocument('UTF-8', '1.0');
    xswHandle.writeStartElement('catalog');
    xswHandle.writeAttribute('xmlns', 'http://www.demandware.com/xml/impex/catalog/2006-10-31');
    xswHandle.writeAttribute('catalog-id', catalogID);

    xswHandle.writeStartElement('header');
    xswHandle.writeStartElement('image-settings');

    generalUtils.writeElement(xswHandle, 'internal-location', '', 'base-path', '/');

    if (viewTypes && viewTypes['view-types'].length > 0) {
        xswHandle.writeStartElement('view-types');

        for (var i = 0; i < viewTypes['view-types'].length; i++) {
            generalUtils.writeElement(xswHandle, 'view-type', viewTypes['view-types'][i]);
        }

        // close xml view-types
        xswHandle.writeEndElement();
    }
    generalUtils.writeElement(xswHandle, 'alt-pattern', '${productname}');
    generalUtils.writeElement(xswHandle, 'title-pattern', '${productname}');

    // close xml image-settings
    xswHandle.writeEndElement();
    // close xml header
    xswHandle.writeEndElement();

    xswHandle.writeStartElement('category');
    xswHandle.writeAttribute('category-id', 'root');

    generalUtils.writeElement(xswHandle, 'display-name', catalogID, 'xml:lang', 'x-default');
    generalUtils.writeElement(xswHandle, 'online-flag', 'true');
    generalUtils.writeElement(xswHandle, 'template', '');
    generalUtils.writeElement(xswHandle, 'page-attributes', '');

    // close xml root catalog
    xswHandle.writeEndElement();
}

/**
 * @desc Creates XML file and returns file Writers
 *
 * @param {number} index - the index in the file name
 * @param {string} fileName - the part of file name to describe its contents
 *
 * @returns {Object} - The file writers
 */
akeneoCreateProductsXML.createCatalogHeaderXML = function (index, fileName) {
    var catalogID = config.sfccMasterCatalogID;
    var AKENEO_CATALOG_FLUX_DIR = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'akeneo' + File.SEPARATOR + 'catalog' + File.SEPARATOR;
    var AKENEO_CATALOG_FILE_PATH = 'catalog-akeneo-' + index + '-' + fileName + '-' + catalogID + '-' + StringUtils.formatCalendar(new Calendar(), 'yyyyMMddHHmmss') + '.xml';
    var file = new File(AKENEO_CATALOG_FLUX_DIR + AKENEO_CATALOG_FILE_PATH);
    fileUtils.createFileAndFolders(file);

    var fwHandle = new FileWriter(file);
    var xswHandle = new XMLIndentingStreamWriter(fwHandle);

    try {
        writeAkeneoCatalogHeader(xswHandle);
    } catch (e) {
        throw new Error('ERROR : While writing XML Catalog file : ' + e.stack + ' with Error: ' + e.message);
    }

    return {
        fwHandle: fwHandle,
        xswHandle: xswHandle
    };
};

/**
 * @desc Write Footer & Static part of akeneo catalog
 *
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - the XML writer
 *
 * @returns {void}
 */
function writeAkeneoCatalogFooter(xswHandle) {
    // XML definition & close first node
    xswHandle.writeEndElement();
    xswHandle.writeEndDocument();
    xswHandle.flush();
}

/**
 * @desc Write Footer Akeneo Catalog Products XML part
 *
 * @param {Object} productWriter - The object containing the file writers
 *
 * @returns {void}
 */
akeneoCreateProductsXML.createCatalogFooterXML = function (productWriter) {
    var xswHandle = productWriter.xswHandle;
    var fwHandle = productWriter.fwHandle;

    try {
        writeAkeneoCatalogFooter(xswHandle);
    } catch (e) {
        throw new Error('ERROR : While writing XML Catalog file : ' + e.stack + ' with Error: ' + e.message);
    }

    if (xswHandle != null) {
        xswHandle.close();
    }
    if (fwHandle != null) {
        fwHandle.close();
    }
};

/**
 * @desc Return an object which contains mapping for product nodes
 *
 * @returns {Object} - which contains mapping for product nodes
 */
function getStaticProductNodes() {
    var productNodes = {
        EAN: {
            nodeName: 'ean',
            localizable: 'false',
            nodeValue: ''
        },
        UPC: {
            nodeName: 'upc',
            localizable: 'false',
            nodeValue: ''
        },
        unit: {
            nodeName: 'unit',
            localizable: 'false',
            nodeValue: ''
        },
        minOrderQuantity: {
            nodeName: 'min-order-quantity',
            localizable: 'false',
            nodeValue: 1
        },
        stepQuantity: {
            nodeName: 'step-quantity',
            localizable: 'false',
            nodeValue: 1
        },
        name: {
            nodeName: 'display-name',
            localizable: 'true',
            nodeValue: []
        },
        shortDescription: {
            nodeName: 'short-description',
            localizable: 'true',
            nodeValue: []
        },
        longDescription: {
            nodeName: 'long-description',
            localizable: 'true',
            nodeValue: []
        },
        storeForcePriceEnabled: {
            nodeName: 'store-force-price-flag',
            localizable: 'false',
            nodeValue: 'false'
        },
        storeNonInventoryEnabled: {
            nodeName: 'store-non-inventory-flag',
            localizable: 'false',
            nodeValue: 'false'
        },
        storeNonRevenueEnabled: {
            nodeName: 'store-non-revenue-flag',
            localizable: 'false',
            nodeValue: 'false'
        },
        storeNonDiscountableEnabled: {
            nodeName: 'store-non-discountable-flag',
            localizable: 'false',
            nodeValue: 'false'
        },
        onlineFlag: {
            nodeName: 'online-flag',
            localizable: 'false',
            sitespecific: [],
            flag: 'false',
            sitespecificVal: [],
            nodeValue: 'true'
        },
        onlineFrom: {
            nodeName: 'online-from',
            writable: 'false',
            localizable: 'false',
            nodeValue: ''
        },
        onlineTo: {
            nodeName: 'online-to',
            writable: 'false',
            localizable: 'false',
            nodeValue: ''
        },
        brand: {
            nodeName: 'brand',
            localizable: 'false',
            nodeValue: ''
        }
    };
    return productNodes;
}

/**
 * @desc Fill the productNodes for create sorted product xml part
 *
 * @param {Object} akeneoProduct - the product to be written
 * @param {Object} productNodes - the SFCC product nodes
 *
 * @returns {void}
 */
function setMatchingFromConfiguration(akeneoProduct, productNodes) {
    var productAttrsMapping = config.systemAttrsMapping;
    // loop n1 on all akeneo's product attributes for checking if we catch a correspondence
    Object.keys(akeneoProduct.values).forEach(function (attrKey) {
        var productAttr = akeneoProduct.values[attrKey];
        var camelizeAttrKey = 'akeneo_' + stringUtilsExt.camelize(attrKey);

        // if we find a match, we stock akeneo value in productNodes
        if (camelizeAttrKey in productAttrsMapping.matching) {
            var salesforcesKey = productAttrsMapping.matching[camelizeAttrKey];
            var salesforceMatching = productNodes[salesforcesKey]; // salesforce product Node
            if (camelizeAttrKey.indexOf('onlinefrom') > -1) {
                salesforceMatching = productNodes.onlineFrom;
            } else if (camelizeAttrKey.indexOf('onlineto') > -1) {
                salesforceMatching = productNodes.onlineTo;
            } else if (camelizeAttrKey.indexOf('uaeonline') > -1 || camelizeAttrKey.indexOf('ksaonline') > -1) {
                salesforceMatching = productNodes.onlineFlag;
            }
            // if multiple values, it means this is a localizable attr
            // EDIT : if there is multiple entrie in one attrs, it does not mean that it's a localizable attr.
            //        It could mean that it have multiple 'scope'
            if (productAttr.length > 1 || productAttr[0].locale) {
                // loop on localizable attributes
                for (var i = 0; i < productAttr.length; i++) {
                    var localeAttrValue = productAttr[i];
                    // we fill the salesforce value only if attr is localizable from both side (SalesForce & Akeneo)
                    var channelMatches = '';
                    if (localeAttrValue.scope !== 'undefined') {
                        channelMatches = generalUtils.checkScope(localeAttrValue.scope);
                        if (channelMatches && typeof salesforceMatching !== 'undefined' && salesforceMatching.localizable === 'true') {
                            // getting akeneo locale
                            var akeneoLocale = localeAttrValue.locale.replace('_', '-').toString();
                            // build a localizable object
                            salesforceMatching.nodeValue[akeneoLocale] = localeAttrValue.data;
                        }
                    }
                }
            } else if (typeof productAttr[0].data === 'string') { // Sometimes, akeneo attr is not localizable but it is in salesForce, we retrieve value (Ex : name is not localizable in akeneo)
                if (typeof salesforceMatching !== 'undefined') {
                    if (typeof salesforceMatching.nodeValue === 'object') {
                        salesforceMatching.nodeValue.push(productAttr[0].data); // push multiple values allowed
                    } else {
                        salesforceMatching.nodeValue = productAttr[0].data; // single value allowed
                        if (salesforceMatching.nodeName.indexOf('online-from') > -1 || salesforceMatching.nodeName.indexOf('online-to') > -1) {
                            var changedDateFormat = productAttr[0].data.split('+')[0] + '.000Z';
                            salesforceMatching.nodeValue = changedDateFormat;
                        }
                    }
                }
            } else if (typeof productAttr[0].data === 'boolean') {
                if (salesforceMatching.nodeName.indexOf('online-flag') > -1) {
                    if (camelizeAttrKey.indexOf('uaeonline') > -1) {
                        salesforceMatching.sitespecific.push('Faces_AE');
                        salesforceMatching.sitespecificVal.push(productAttr[0].data);
                        salesforceMatching.flag = true;
                    } else if (camelizeAttrKey.indexOf('ksaonline') > -1) {
                        salesforceMatching.sitespecific.push('Faces_SA');
                        salesforceMatching.sitespecificVal.push(productAttr[0].data);
                        salesforceMatching.flag = true;
                    }
                }
            }
        }
    });
}

/**
 * @desc Writing the product nodes based on matching found
 *
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - the XML writer
 * @param {Object} productNodes - the SFCC product nodes
 *
 * @returns {void}
 */
function writeProductNodesBasedOnMatching(xswHandle, productNodes) {
    Object.keys(productNodes).forEach(function (productNodeKey) {
        var productNode = productNodes[productNodeKey];

        // if the current product is empty, we set her nodeValue to simple string to avoid writing wrong data.
        if (productNode.localizable === 'true' && typeof productNode.nodeValue === 'object' && Object.keys(productNode.nodeValue).length === 0) {
            productNode.nodeValue = '';
        } else if (productNode.localizable === 'true' && typeof productNode.nodeValue === 'object' && (Object.keys(productNode.nodeValue).length === 1 && Object.keys(productNode.nodeValue)[0].match(new RegExp('[a-zA-Z]{2}-[a-zA-Z]{2}')) === null)) {
            productNode.nodeValue['x-default'] = productNode.nodeValue.shift();
        }

        // handle case where salesforce system attrs from productNode have parent node
        if (productNode.parentNode === true) {
            Object.keys(productNode).forEach(function (subNodeKey) {
                if (subNodeKey === 'parentNode') { // if parent node, we write the current parent node
                    xswHandle.writeStartElement(productNodeKey);
                    return;
                }
                var subNode = productNode[subNodeKey];
                // write the sub node
                generalUtils.writeElement(xswHandle, subNode.nodeName.toString(), subNode.nodeValue.toString());
            });
            // close parentNode
            xswHandle.writeEndElement();
        } else if (productNode.localizable === 'true') {
            // if salesforce attr is localizable, but not in akeneo side, we retrieve value
            if (typeof productNode.nodeValue === 'object') {
                Object.keys(productNode.nodeValue).forEach(function (locale) {
                    generalUtils.writeElement(xswHandle, productNode.nodeName.toString(), productNode.nodeValue[locale], 'xml:lang', locale.toString());
                });
            } else { // if salesforce attr is localizable but not in akeneo, we getting value
                generalUtils.writeElement(xswHandle, productNode.nodeName.toString(), productNode.nodeValue.toString(), 'xml:lang', 'x-default');
            }
        } else if (productNode.writable) {
            if (productNode.nodeValue.length > 0) {// eslint-disable-line
                generalUtils.writeElement(xswHandle, productNode.nodeName.toString(), productNode.nodeValue.toString());
            }
        } else {
            generalUtils.writeElement(xswHandle, productNode.nodeName.toString(), productNode.nodeValue.toString());
            if (productNode.sitespecific && productNode.flag === true) {
                if (productNode.sitespecific.length > 0) {
                    for (var i = 0; i < productNode.sitespecific.length; i++) {
                        generalUtils.writeElement(xswHandle, productNode.nodeName.toString(), productNode.sitespecificVal[i], 'site-id', productNode.sitespecific[i]);
                    }
                }
            }
        }
    });
}

/**
 * @desc returns config object for writing store nodes
 * @returns {Object} - config object
 */
function getStaticProductNodesStores() {
    var productNodes = {
        'store-attributes': {
            parentNode: true,
            storeForcePriceEnabled: {
                nodeName: 'force-price-flag',
                localizable: 'false',
                nodeValue: 'false'
            },
            storeNonInventoryEnabled: {
                nodeName: 'non-inventory-flag',
                localizable: 'false',
                nodeValue: 'false'
            },
            storeNonRevenueEnabled: {
                nodeName: 'non-revenue-flag',
                localizable: 'false',
                nodeValue: 'false'
            },
            storeNonDiscountableEnabled: {
                nodeName: 'non-discountable-flag',
                localizable: 'false',
                nodeValue: 'false'
            }
        }
    };
    return productNodes;
}

/**
 * @desc writes custom product nodes to XML
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML writer
 * @returns {void}
 */
function writeCustomProductNodes(xswHandle) {
    var productNodes = getStaticProductNodesStores();
    Object.keys(productNodes).forEach(function (productNodeKey) {
        var productNode = productNodes[productNodeKey];

        // handle case where salesforce system attrs from productNode have parent nodes
        if (productNode.parentNode === true) {
            Object.keys(productNode).forEach(function (subNodeKey) {
                if (subNodeKey === 'parentNode') { // if parent node, we write the current parent node
                    xswHandle.writeStartElement(productNodeKey);
                } else {
                    var subNode = productNode[subNodeKey];

                    generalUtils.writeElement(xswHandle, subNode.nodeName.toString(), subNode.nodeValue.toString());
                }
            });
            // close parentNode
            xswHandle.writeEndElement();
        }
    });
}

/**
 * @desc Writing the product-set-products nodes based product recommendations from API
 * @param {dw.io.XSWIndentingStreamWriter} xswHandle - XML writer
 * @param {Object} association - The product association object for product set
 * @returns {void}
 */
function writeAkeneoProductsSet(xswHandle, association) {
    if (association && (association.products.length > 0 || association.product_models.length > 0)) {
        var targetID;
        xswHandle.writeStartElement('product-set-products');

        for (var i = 0; i < association.products.length; i++) {
            targetID = association.products[i];

            if (typeof targetID === 'string') {
                generalUtils.writeElement(xswHandle, 'product-set-product', '', 'product-id', targetID);
            } else if (typeof targetID === 'object') {
                for (var j = 0; j < targetID.length; j++) {
                    generalUtils.writeElement(xswHandle, 'product-set-product', '', 'product-id', targetID[j]);
                }
            }
        }

        for (var k = 0; k < association.product_models.length; k++) {
            targetID = association.product_models[k];

            if (typeof targetID === 'string') {
                generalUtils.writeElement(xswHandle, 'product-set-product', '', 'product-id', targetID);
            } else if (typeof targetID === 'object') {
                for (var l = 0; l < targetID.length; l++) {
                    generalUtils.writeElement(xswHandle, 'product-set-product', '', 'product-id', targetID[l]);
                }
            }
        }
        xswHandle.writeEndElement();
    }
}

/**
 * Writes the tags for product bundle
 * @param {dw.io.XSWIndentingStreamWriter} xswHandle - XML writer
 * @param {string} productID - product ID
 * @returns {void}
 */
function writeProductBundleXML(xswHandle, productID) {
    xswHandle.writeStartElement('bundled-product');
    xswHandle.writeAttribute('product-id', productID);
    generalUtils.writeElement(xswHandle, 'quantity', '1');
    xswHandle.writeEndElement();
}

/**
 * @desc Writing the bundled-products nodes based product recommendations from API
 * @param {dw.io.XSWIndentingStreamWriter} xswHandle - XML writer
 * @param {Object} association - The product association object for product bundle
 * @returns {void}
 */
function writeAkeneoProductsBundle(xswHandle, association) {
    if (association && (association.products.length > 0 || association.product_models.length > 0)) {
        var targetID;
        xswHandle.writeStartElement('bundled-products');

        for (var i = 0; i < association.products.length; i++) {
            targetID = association.products[i];

            if (typeof targetID === 'string') {
                writeProductBundleXML(xswHandle, targetID);
            } else if (typeof targetID === 'object') {
                for (var j = 0; j < targetID.length; j++) {
                    writeProductBundleXML(xswHandle, targetID[j]);
                }
            }
        }

        for (var k = 0; k < association.product_models.length; k++) {
            targetID = association.product_models[k];

            if (typeof targetID === 'string') {
                writeProductBundleXML(xswHandle, targetID);
            } else if (typeof targetID === 'object') {
                for (var l = 0; l < targetID.length; l++) {
                    writeProductBundleXML(xswHandle, targetID[l]);
                }
            }
        }
        xswHandle.writeEndElement();
    }
}

/**
 * @desc Write XML for single product
 *
 * @param {Object} akeneoProduct - The product to be written
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - xml writer
 *
 * @returns {void}
 */
akeneoCreateProductsXML.writeAkeneoProducts = function (akeneoProduct, xswHandle) {
    var prodSetFamily = config.productSetFamily;
    var prodBundleFamily = config.productBundleFamily;
    var prodSetAssociationType = config.productSetAssociationType;
    var prodBundleAssociationType = config.productBundleAssociationType;

    if (akeneoProduct) {
        // getting salesforce static xml product nodes
        var productNodes = getStaticProductNodes();

        var akeneoProductID = typeof akeneoProduct.identifier !== 'undefined' ? akeneoProduct.identifier : akeneoProduct.code;
        var productID = StringUtils.trim(akeneoProductID);

        xswHandle.writeStartElement('product');

        xswHandle.writeAttribute('product-id', productID);

        setMatchingFromConfiguration(akeneoProduct, productNodes);

        writeProductNodesBasedOnMatching(xswHandle, productNodes);

        // SEO nodes
        generalUtils.writeElement(xswHandle, 'page-attributes', '');

        // write product-set-products nodes
        if (prodSetFamily && akeneoProduct.family === prodSetFamily) {
            writeAkeneoProductsSet(xswHandle, akeneoProduct.associations[prodSetAssociationType]);
        }

        // write bundled-product nodes
        if (prodBundleFamily && akeneoProduct.family === prodBundleFamily) {
            writeAkeneoProductsBundle(xswHandle, akeneoProduct.associations[prodBundleAssociationType]);
        }

        // write product custom nodes at specific place
        writeCustomProductNodes(xswHandle);

        // close xml product
        xswHandle.writeEndElement();
    }
};

module.exports = akeneoCreateProductsXML;
