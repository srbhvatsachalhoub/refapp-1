'use strict';

var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var generalUtils = require('~/cartridge/scripts/utils/generalUtils');
var config = generalUtils.config;
var logUtils = require('~/cartridge/scripts/utils/logUtils');
var logger = logUtils.getLogger('importedTime');

/**
 * @desc Fetches the runtime custom object
 * @returns {dw.object.CustomObject} - runtime custom object
 */
function getRuntimeObject() {
    var customObjectType = config.customObjectType.RunTime;
    var customObject;

    try {
        customObject = CustomObjectMgr.getCustomObject(customObjectType, customObjectType);
    } catch (e) {
        logger.error('Error ocurred while fetching runtime custom object: ' + e.message + ', stack: ' + e.stack);
        return getRuntimeObject();
    }
    return customObject;
}

/**
 * @desc Creates the runtime custom object
 * @returns {dw.object.CustomObject} - runtime custom object
 */
function createRuntimeObject() {
    var customObjectType = config.customObjectType.RunTime;
    var customObject;

    try {
        Transaction.begin();
        customObject = CustomObjectMgr.createCustomObject(customObjectType, customObjectType);
        Transaction.commit();
    } catch (e) {
        logger.error('Error ocurred while creating runtime custom object: ' + e.message + ', stack: ' + e.stack);
        return createRuntimeObject();
    }
    return customObject;
}

/**
 * @desc Sets last imported time to custom object
 */
function setLastImportedTime() {
    var customObject = getRuntimeObject();

    if (!customObject) {
        customObject = createRuntimeObject();
    }

    try {
        Transaction.begin();
        customObject.custom.lastImportedTime = StringUtils.formatCalendar(new Calendar(), 'yyyy-MM-dd HH:mm:ss');
        Transaction.commit();
    } catch (e) {
        logger.error('Error occurred while setting lastImportedTime: ' + e.message + ', stack: ' + e.stack);
        setLastImportedTime();
    }
}

/**
 * @desc Fetches last imported time from runtime custom object
 * @returns {string} - last imported time
 */
function getLastImportedTime() {
    var customObject = getRuntimeObject();

    if (customObject && customObject.custom.lastImportedTime) {
        return customObject.custom.lastImportedTime;
    }
    return '';
}

/**
 * @desc Clears last imported time from runtime custom object
 */
function clearImportedTime() {
    generalUtils.adjustLocale();
    var customObject = getRuntimeObject();

    if (customObject) {
        try {
            Transaction.begin();
            customObject.custom.lastImportedTime = '';
            Transaction.commit();
        } catch (e) {
            logger.error('Error ocurred while clearing lastImportedTime: ' + e.message + ', stack: ' + e.stack);
            clearImportedTime();
        }
    }
}

/* Exported functions */
module.exports = {
    setLastImportedTime: setLastImportedTime,
    getLastImportedTime: getLastImportedTime,
    clearImportedTime: clearImportedTime
};
