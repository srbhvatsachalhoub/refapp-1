'use strict';

var StringUtils = require('dw/util/StringUtils');
var config = require('~/cartridge/scripts/utils/generalUtils').config;
var akeneoCreateProductsXML = require('~/cartridge/scripts/akeneoCatalog/akeneoCreateProductsXML');
var productCustomAttributes = require('~/cartridge/scripts/akeneoAttributes/productCustomAttributes');

/**
 * @desc Saves ID of second level model product to its parent
 * @param {Object} modelProduct - 2nd level model
 */
function saveModelProductToParent(modelProduct) {
    var customCacheWebdav = require('~/cartridge/scripts/io/customCacheWebdav');
    var masterFileName = StringUtils.format(config.cacheDirectory.modelProducts.endPoint, modelProduct.parent);
    var masterProduct = customCacheWebdav.getCache(masterFileName);

    if (masterProduct) {
        var modelList = masterProduct.modelList || [];
        modelList.push(modelProduct.code);
        masterProduct.modelList = modelList;
        customCacheWebdav.clearCache(masterFileName);
        customCacheWebdav.setCache(masterFileName, masterProduct);
    }
}

/**
 * @desc gets the Model Products list from custom cache
 * @param {Object} modelProductSystemAttrWriter - XML writer for product system attributes
 * @param {Object} modelProductCustomAttrWriter - XML writer for product custom attributes
 */
function getModelProducts(modelProductSystemAttrWriter, modelProductCustomAttrWriter) {
    var modelImportType = config.modelImport.type ? config.modelImport.type : 'master-variation';
    var customCacheWebdav = require('~/cartridge/scripts/io/customCacheWebdav');
    var filesList = customCacheWebdav.listFilesInCache(config.cacheDirectory.modelProducts.baseLocation);

    for (var index = 0; index < filesList.length; index++) {
        var file = filesList[index];
        var fileName = StringUtils.format(config.cacheDirectory.modelProducts.endPoint, file.substring(0, file.lastIndexOf('.')));
        var modelProduct = customCacheWebdav.getCache(fileName);

        if (modelProduct) {
            if (modelImportType === 'master-group-variation' && modelProduct.parent) {
                saveModelProductToParent(modelProduct);
            }
            if ((modelImportType === 'master-variation' && !modelProduct.parent) || modelImportType === 'master-group-variation') {
                akeneoCreateProductsXML.writeAkeneoProducts(modelProduct, modelProductSystemAttrWriter.xswHandle);
                productCustomAttributes.getCustomAttributes(modelProduct, modelProductCustomAttrWriter.xswHandle);
            }
        }
    }
}

/**
 * @desc creates system and custom attributes XMLs for model products
 */
function createModelCatalog() {
    try {
        var modelProductSystemAttrWriter = akeneoCreateProductsXML.createCatalogHeaderXML(4, 'product-model');
        var modelProductCustomAttrWriter = akeneoCreateProductsXML.createCatalogHeaderXML(3.1, 'model-product-custom-attributes');

        getModelProducts(modelProductSystemAttrWriter, modelProductCustomAttrWriter);

        akeneoCreateProductsXML.createCatalogFooterXML(modelProductSystemAttrWriter);
        akeneoCreateProductsXML.createCatalogFooterXML(modelProductCustomAttrWriter);
    } catch (e) {
        throw new Error('Error occured due to ' + e.stack + ' with Error: ' + e.message);
    }
}

/* Exported functions */
module.exports = {
    createModelCatalog: createModelCatalog
};
