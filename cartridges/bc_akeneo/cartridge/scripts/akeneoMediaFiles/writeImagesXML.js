'use strict';

var File = require('dw/io/File');
var generalUtils = require('~/cartridge/scripts/utils/generalUtils');
var config = generalUtils.config;

var writeImagesXML = {};

/**
 * @desc gets list of image paths
 * @param {Object} productValues - the product image values
 * @param {Array} imageCodeList - the list of image codes
 * @returns {Array} - the list of image paths
 */
function getImagePathList(productValues, imageCodeList) {
    var imagePathList = [];

    if (config.imageType === 'images' || config.imageType === 'both') {
        if (imageCodeList && imageCodeList.length > 0) {
            for (var idx = 0; idx < imageCodeList.length; idx++) {
                var imageAttr = imageCodeList[idx];

                if (Object.prototype.hasOwnProperty.call(productValues, imageAttr)) {
                    var imageAttrObject = productValues[imageAttr];
                    if (imageAttrObject) {
                        imagePathList.push(imageAttrObject[0].data);
                    }
                }
            }
        }
    }
    return imagePathList;
}

/**
 * @desc gets list of asset paths
 * @param {Object} productValues - the product image values
 * @param {Array} assetCodeList - the list of asset codes
 * @returns {Array} - the list of asset paths
 */
function getAssetPathList(productValues, assetCodeList) {
    var assetPathList = [];

    if (config.imageType === 'assets' || config.imageType === 'both') {
        var AKENEO_CATALOG_PATH = File.CATALOGS + File.SEPARATOR + config.sfccMasterCatalogID + File.SEPARATOR + 'default' + File.SEPARATOR;
        var AKENEO_CATALOG_ASSETS_DIR = AKENEO_CATALOG_PATH + 'product' + File.SEPARATOR;

        if (assetCodeList && assetCodeList.length > 0) {
            for (var i = 0; i < assetCodeList.length; i++) {
                var assetAttr = assetCodeList[i];

                if (assetAttr === 'shadethumbnail') {
                    AKENEO_CATALOG_ASSETS_DIR = AKENEO_CATALOG_PATH + 'shadethumbnail' + File.SEPARATOR;
                }
                if (Object.prototype.hasOwnProperty.call(productValues, assetAttr)) {
                    var productAssetList = productValues[assetAttr][0].data;

                    if (productAssetList && productAssetList.length > 0) {
                        for (var j = 0; j < productAssetList.length; j++) {
                            var asset = productAssetList[j];
                            var createAssetDir = AKENEO_CATALOG_ASSETS_DIR + asset;
                            var AKENEO_CATALOG_ASSET_UNIQUE_DIR = new File(createAssetDir);
                            var folderExists = AKENEO_CATALOG_ASSET_UNIQUE_DIR.isDirectory();
                            var setImagePath = '';

                            if (folderExists) {
                                var assetFileFullPath = createAssetDir; // CATALOG/default/Assets/sample_asset
                                var ASSET_FULL_PATH_FILE = new File(assetFileFullPath);

                                while (ASSET_FULL_PATH_FILE.isDirectory()) {
                                    var NewFileList = ASSET_FULL_PATH_FILE.list();
                                    createAssetDir += File.SEPARATOR + NewFileList[0];
                                    assetFileFullPath = createAssetDir;
                                    ASSET_FULL_PATH_FILE = new File(assetFileFullPath);
                                }
                                if (ASSET_FULL_PATH_FILE.isFile()) {
                                    if (ASSET_FULL_PATH_FILE.path.indexOf('default/') > -1) {
                                        setImagePath = ASSET_FULL_PATH_FILE.path.replace(/default\//g, '');
                                    }
                                    assetPathList.push({ key: assetAttr, assetID: asset, images: setImagePath });
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return assetPathList;
}

/**
 * @desc Writes XML image tags
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML stream writer
 * @param {Object} masterProduct - The object of product
 * @param {Object} mediaAssets - The object of media assets
 * @returns {void}
 */
function createMasterProductImageXML(xswHandle, masterProduct, mediaAssets) {
    var variationProducts = masterProduct.variationProducts ? masterProduct.variationProducts : {};
    var productModelArr = [];
    var productImagesArr = [];

    xswHandle.writeStartElement('product');
    xswHandle.writeAttribute('product-id', masterProduct.code);
    xswHandle.writeStartElement('images');

    if (!('product_model_images' in masterProduct.values)) {
        if (mediaAssets.length > 0) {
            // if master images not exist , then we take first variant images as master images
            var getFirstVariantImages = mediaAssets[0];
            xswHandle.writeStartElement('image-group');
            xswHandle.writeAttribute('view-type', 'hi-res');
            for (var indxes = 0; indxes < getFirstVariantImages.length; indxes++) {
                xswHandle.writeEmptyElement('image');
                xswHandle.writeAttribute('path', getFirstVariantImages[indxes].images);
            }
            xswHandle.writeEndElement(); // image-group
        }
    } else {
        xswHandle.writeStartElement('image-group');
        xswHandle.writeAttribute('view-type', 'hi-res');
        productModelArr = mediaAssets.filter(function (value) { // eslint-disable-line
            if (typeof value.key !== undefined) {
                return value.key === 'product_model_images';
            }
        });
        for (var img = 0; img < productModelArr.length; img++) {
            xswHandle.writeEmptyElement('image');
            xswHandle.writeAttribute('path', productModelArr[img].images);
        }
        xswHandle.writeEndElement(); // image-group
    }

    for (var l = 0; l < variationProducts.length; l++) {
        // if model image not exists put first image of variations
        productImagesArr = mediaAssets.filter(function (value, index, arr) { // eslint-disable-line
            if (typeof arr[index].key !== undefined) {
                return value.key !== 'product_model_images';
            }
            arr[index].filter(function (v) {
                return v.key !== 'product_model_images';
            });
        });

        if ('size' in variationProducts[l].values) {
            var getVariationAssets = 'productimages' in variationProducts[l].mediaValues ? variationProducts[l].mediaValues.productimages[0].data : [];
            if (getVariationAssets.length > 0) {
                xswHandle.writeStartElement('image-group');
                xswHandle.writeAttribute('view-type', 'hi-res');
                xswHandle.writeEmptyElement('variation');
                xswHandle.writeAttribute('attribute-id', 'size');
                xswHandle.writeAttribute('value', variationProducts[l].values.size[0].data);
                for (var el = 0; el < getVariationAssets.length; el++) {
                    var checkisWrittenSize = false;
                    for (var s = 0; s < productImagesArr.length; s++) {
                        for (var v = 0; v < productImagesArr[s].length; v++) {
                            if (getVariationAssets[el] === productImagesArr[s][v].assetID && checkisWrittenSize !== true) {
                                xswHandle.writeEmptyElement('image');
                                xswHandle.writeAttribute('path', productImagesArr[s][v].images);
                                checkisWrittenSize = true;
                            }
                        }
                    }
                }
                xswHandle.writeEndElement(); // image-group
            }
        }
        if ('shade' in variationProducts[l].values) {
            var getVariationAssetShade = 'productimages' in variationProducts[l].mediaValues ? variationProducts[l].mediaValues.productimages[0].data : [];
            if (getVariationAssetShade.length > 0) { // eslint-disable-line
                xswHandle.writeStartElement('image-group');
                xswHandle.writeAttribute('view-type', 'hi-res');
                xswHandle.writeEmptyElement('variation');
                xswHandle.writeAttribute('attribute-id', 'color');
                xswHandle.writeAttribute('value', variationProducts[l].values.shade[0].data);
                for (var elm = 0; elm < getVariationAssetShade.length; elm++) {
                    var checkisWritten = false;
                    for (var t = 0; t < productImagesArr.length; t++) {
                        for (var i = 0; i < productImagesArr[t].length; i++) {
                            if (typeof productImagesArr[t][i] !== undefined) {
                                if (getVariationAssetShade[elm] === productImagesArr[t][i].assetID && checkisWritten !== true) {
                                    xswHandle.writeEmptyElement('image');
                                    xswHandle.writeAttribute('path', productImagesArr[t][i].images);
                                    checkisWritten = true;
                                }
                            }
                        }
                    }
                }
                xswHandle.writeEndElement(); // image-group
            }
        }
        if ('shadethumbnail' in variationProducts[l].mediaValues) {
            var getVariationAssetShadeThumb = 'shadethumbnail' in variationProducts[l].mediaValues ? variationProducts[l].mediaValues.shadethumbnail[0].data : [];
            if (getVariationAssetShadeThumb.length > 0) {
                var variationValue = 'size' in variationProducts[l].values ? 'size' : 'color';
                xswHandle.writeStartElement('image-group');
                xswHandle.writeAttribute('view-type', 'swatch');
                xswHandle.writeEmptyElement('variation');
                xswHandle.writeAttribute('attribute-id', variationValue);
                xswHandle.writeAttribute('value', 'size' in variationProducts[l].values ? variationProducts[l].values.size[0].data : variationProducts[l].values.shade[0].data);
                for (var elemt = 0; elemt < getVariationAssetShadeThumb.length; elemt++) { // eslint disa
                    var checkisWrittenThumbSize = false;
                    for (var idz = 0; idz < productImagesArr.length; idz++) {
                        for (var g = 0; g < productImagesArr[idz].length; g++) {
                            if (typeof productImagesArr[idz][g] !== undefined) {
                                if (getVariationAssetShadeThumb[elemt] === productImagesArr[idz][g].assetID && checkisWrittenThumbSize !== true) {
                                    xswHandle.writeEmptyElement('image');
                                    xswHandle.writeAttribute('path', productImagesArr[idz][g].images);
                                    checkisWrittenThumbSize = true;
                                }
                            }
                        }
                    }
                }
                xswHandle.writeEndElement(); // image-group
            }
        }
    }
    xswHandle.writeEndElement(); // images
    xswHandle.writeEndElement(); // product
}

/**
 * @desc Writes XML image tags
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML stream writer
 * @param {Object} getProduct - The object of product
 * @param {Object} getImagesPaths - The object containing variation image paths
 * @returns {void}
 */
function writeProductImagesXML(xswHandle, getProduct, getImagesPaths) {
    var productID = getProduct.identifier;
    xswHandle.writeStartElement('product');
    xswHandle.writeAttribute('product-id', productID);
    xswHandle.writeStartElement('images');
    if (Object.keys(getImagesPaths).length > 0) {
        xswHandle.writeStartElement('image-group');
        xswHandle.writeAttribute('view-type', 'hi-res');
        for (var ind = 0; ind < getImagesPaths.length; ind++) {
            xswHandle.writeEmptyElement('image');
            xswHandle.writeAttribute('path', getImagesPaths[ind].images);
        }
        xswHandle.writeEndElement(); // image-group
    }
    xswHandle.writeEndElement(); // images
    xswHandle.writeEndElement(); // product
}

/**
 * @desc Creates image XML for master products
 * @param {dw.io.XMLIndentingStreamWriter} xswHandle - XML stream writer
 * @param {Object} getProduct - the product object
 * @param {boolean} isMaster - the check product is master
 * @param {Array} imageCodeList - list of image codes
 * @param {Array} assetCodeList - list of asset codes
 * @returns {void}
 */
writeImagesXML.createImageXML = function (xswHandle, getProduct, isMaster, imageCodeList, assetCodeList) {
    var masterAssetPathList = [];
    var imagePathList = getImagePathList(getProduct.values, imageCodeList);
    masterAssetPathList = getAssetPathList(getProduct.values, assetCodeList);
    if (masterAssetPathList && masterAssetPathList.length) {
        imagePathList = imagePathList.concat(masterAssetPathList);
    }
    if (isMaster) {
        if (getProduct.variationProducts) {
            for (var i = 0; i < getProduct.variationProducts.length; i++) {
                masterAssetPathList.push(getAssetPathList(getProduct.variationProducts[i].mediaValues, assetCodeList));
            }
            var filteredMaster = masterAssetPathList.filter(function (el) { return el != null; });
            if (filteredMaster.length > 0) {
                createMasterProductImageXML(xswHandle, getProduct, masterAssetPathList);
            }
        }
    } else {
        var filteredSingle = imagePathList.filter(function (el) { return el != null; });
        if (filteredSingle.length > 0) {
            writeProductImagesXML(xswHandle, getProduct, imagePathList);
        }
    }
};

module.exports = writeImagesXML;
