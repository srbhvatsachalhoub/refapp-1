'use strict';

/**
 * @module controllers/BMCustomFeeds
 */

var boguard = require('bc_library/cartridge/scripts/boguard');
var ISML = require('dw/template/ISML');

function start() {
    ISML.renderTemplate('feeds/configurefeeds.isml');
}

function preview() {
    var feedPreview = require('~/cartridge/scripts/customobject/FeedPreviews');
    ISML.renderTemplate('data/preview.isml', {Preview : feedPreview.GeneratePreview()});
}

function getAllSites() {
    var Site = require('dw/system/Site');
    var Response = require('bc_library/cartridge/scripts/util/Response');
    
    var allSites = Site.getAllSites();
    
    var sites = [];
    for each(var availableSite in allSites) {
        var site = {};
        site['id'] = availableSite.ID;
        site['name'] = availableSite.name;
        sites.push(site);
    }
    
    Response.renderJSON(sites);
}

function getAllLocales() {
    var Site = require('dw/system/Site');
    var Response = require('bc_library/cartridge/scripts/util/Response');

    var locales = [];
    var allSites = Site.getAllSites();
    for each(var availableSite in allSites) {
        var allowedLocales = availableSite.getAllowedLocales();
        for each(var allowedlocale in allowedLocales) {
        	if(locales.indexOf(allowedlocale) === -1) {
        		locales.push(allowedlocale);
        	}
        }
    }

    Response.renderJSON(locales.sort().map(function(locale){
        return {'id': locale};
    }));
}

exports.Start = boguard.ensure(['get'], start);

exports.Preview = boguard.ensure(['post'], preview);

exports.GetAllSites = boguard.ensure(['get'], getAllSites);

exports.GetAllLocales = boguard.ensure(['get'], getAllLocales);