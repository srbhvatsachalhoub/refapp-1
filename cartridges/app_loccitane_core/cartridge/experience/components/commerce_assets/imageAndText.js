'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();
    var image = content.image && ImageTransformation.getScaledImage(content.image);

    model.header = content.header || '';
    model.description = content.description || '';
    model.imagePosition = content.imagePosition === 'right' ? 'rtl' : '';
    model.image = image;
    model.imageAlt = content.imageAlt;
    model.imageTitle = content.imageTitle || '';
    model.buttonName = content.buttonName || '';
    model.link = content.link;

    return new Template('experience/components/commerce_assets/imageAndText').render(model).text;
};
