'use strict';

module.exports = function () {
    $(function () {
        $('.review-count ').on('click', function () {
            $('.product-cta').css('display', 'none');
            $('html, body').animate({
                scrollTop: $('#review-scroll').offset().top
            }, 1000);
        });
    });
};
