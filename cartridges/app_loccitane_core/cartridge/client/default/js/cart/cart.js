'use strict';

var productCarousel = require('../product/productCarousel');
var quantitySelector = require('../product/quantitySelector');
var countCharacters = require('brand_core/helpers/countCharacters');
var StickySidebar = require('sticky-sidebar/dist/sticky-sidebar');
var throttle = require('lodash/throttle');
var counter = 0;

/**
 * Initializes sticky sidebar
 * @returns {Object} sticky sidebar object
 */
function initStickySidebar() {
    return new StickySidebar('.js-sticky-sidebar', { // eslint-disable-line no-unused-vars
        topSpacing: 100,
        bottomSpacing: 20,
        containerSelector: '.js-sticky-sidebar-container',
        innerWrapperSelector: '.js-sticky-sidebar-inner'
    });
}

module.exports = function () {
    var $sidebar = $('.js-sticky-sidebar');
    var $sidebarContainer = $('.js-sticky-sidebar-container');
    var $sidebarPlaceholderMobile = $('.js-sidebar-placeholder-mobile');
    var $win = $(window);
    var promoCount = 0;

    // Init quantity selector
    quantitySelector.init();

    // Init char counter
    countCharacters();

    // Init sticky sidebar
    var sidebar = initStickySidebar();

    // Update sticky calculations on accordion collapse/expand actions
    $('.js-acc-container').on('state:toggle:open', function () {
        sidebar.destroy();
        sidebar = initStickySidebar();
    });

    // Toggle gift wrapping textarea
    $('.js-gift-toggle').change(function () {
        if ($(this).is(':checked')) {
            $('.js-gift-form-item').removeClass('d-none');
        } else {
            $('.js-gift-form-item').addClass('d-none');
        }
        sidebar.destroy();
        sidebar = initStickySidebar();
    });

    // Update discount totals on cart update
    $('body').on('cart:updateTotals', function (e, data) {
        $('.js-error-message').empty();
        if (data.totals.orderLevelDiscountTotal.value > 0) {
            $('.sub-total').addClass('line-through');
            $('.js-discounted-sub-total')
                .removeClass('d-none')
                .html(data.totals.orderLevelDiscountTotal.discountedSubTotal.formatted);
            $('.js-discount-value-line').removeClass('d-none');
            $('.js-discount-value').html(data.totals.orderLevelDiscountTotal.formatted);
        } else {
            $('.js-discounted-sub-total').addClass('d-none');
            $('.js-discount-value-line').addClass('d-none');
            $('.sub-total').removeClass('line-through');
        }
        // Hide promo applied label after removing a promo code
        var NewPromoCount = $('.coupons-and-promos').children().length;
        if (NewPromoCount < promoCount) {
            $('.js-have-promo-label').addClass('d-none');
        }
        promoCount = NewPromoCount;
    });

    // Hide promo applied label on error
    $('body').on('cart:errorMessageUpdate', function () {
        $('.js-have-promo-label').addClass('d-none');
    });

    // Show promo applied label when valid coupon code applied
    $('body').on('cart:promoApplied', function () {
        $('.js-have-promo-label').removeClass('d-none');
    });

    // Reinit free shipping products slider after cart total price update
    $('body').on('cart:updateFreeShippingApproachingProducts', function () {
        productCarousel('.js-free-shipping-approaching-carousel');
    });

    /**
     * Moves sidebar to different places on mobile and desktop views
     */
    function moveSidebar() {
        if ($('input').is(':focus') === false) {
            if ($win.width() < window.RA_BREAKPOINTS.md) {
                $sidebarPlaceholderMobile.append($sidebar);
            } else {
                $sidebarContainer.append($sidebar);
            }
        }
    }

    // Change sidebar location on mobile and desktop versions
    moveSidebar();
    $win.on('resize', throttle(moveSidebar, 100));
};

/**
 * Get it's position in the viewport
 * @param {Object} dom jquery dom element
 * @returns {boolean} in the viewport or not
 */
function checkInViewPort(dom) {
    var bounding = dom[0].getBoundingClientRect();
    if (
        bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth) &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)
    ) {
        // In the viewport!
        return true;
    }
    return false;
}

$('.js-sample-control').on('click', function (e) {
    e.preventDefault();
    var $accContainer = $('.js-acc-container');
    if ($accContainer.length > 0) {
        var sampleProducts = $accContainer.find('.js-bonus-discount-product');
        var allSelectedProducts = $accContainer.find('.js-bonus-discount-product.selected');

        if (sampleProducts.length > 0 && allSelectedProducts.length === 0 && counter === 0 && !checkInViewPort($accContainer)) {
            counter++;
            $('html, body').animate({
                scrollTop: $accContainer.offset().top - $('.header-top').outerHeight()
            });
            return;
        }
    }

    // continue payment
    location.href = $('.js-sample-control').data('next');
});
