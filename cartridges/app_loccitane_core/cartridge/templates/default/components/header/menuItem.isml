<div class="dropdown-menu ${'nav-cat-level-' + menuItem.level}" role="menu" aria-hidden="true" aria-label="${category.id}">
    <ul class="container mb-0 px-0">
        <isif condition="${menuItem.level === 0}">
            <li class="dropdown-item flex-full-width d-xl-none">
                <a href="${menuItem.url}" id="${menuItem.id}" role="menuitem" class="nav-link dropdown-link nav-link-only-name link">
                    ${Resource.msgf('link.view.all.x', 'common', null, menuItem.name)}
                </a>
            </li>
            <li class="dropdown-item d-none d-xl-block js-main-menu-slot">
                <isslot id="cat-main-menu-product" description="Product slot for mega menu" context="category" context-object="${menuItem.category}" />
            </li>
            <li class="dropdown-item d-none d-xl-block js-main-menu-slot">
                <isslot id="cat-main-menu-banner" description="Category banner slot for mega menu" context="category" context-object="${menuItem.category}" />
            </li>
        </isif>
        <isloop items="${menuItem.subCategories.slice(0, menuItem.subcategoryItemLimit)}" var="category" status="loopState">
            <isif condition="${category.subCategories}">
                <isif condition="${menuItem.level === 0}">
                    <li class="dropdown-item flex-full-width hidden-lg-down js-link-main-menu-view-all">
                        <a href="${menuItem.url}" id="${menuItem.id}" role="menuitem" class="link">
                            ${Resource.msgf('link.view.all.x', 'common', null, menuItem.name)}
                        </a>
                    </li>
                </isif>
                <li class="dropdown-item dropdown js-dropdown-click" role="presentation">
                    <a href="${category.url}" id="${category.id}"
                        class="nav-link dropdown-link dropdown-toggle js-dropdown-toggle-btn ${category.thumbnailUrl ? 'nav-link-with-thumbnail' : 'nav-link-only-name'}"
                        role="menuitem"
                        aria-haspopup="true"
                        aria-expanded="false">
                        <isif condition="${category.thumbnailUrl}">
                            <img src="${category.thumbnailUrl}" alt="${category.name}" class="d-none d-xl-block" />
                        </isif>
                        ${category.name}
                        <i class="sc-icon-plus d-xl-none"></i>
                        <i class="sc-icon-minus d-xl-none"></i>
                    </a>
                    <isset name="menuItem" value="${category}" scope="page" />
                    <isinclude template="components/header/menuItem" />
                </li>
            <iselse/>
                <isif condition="${category.level > 1 && loopState.first}">
                    <li class="dropdown-item d-xl-none" role="presentation">
                        <a href="${menuItem.url}" id="${menuItem.id}" role="menuitem"
                            class="nav-link dropdown-link nav-link-only-name link">
                            ${Resource.msgf('link.view.all.x', 'common', null, menuItem.name)}
                        </a>
                    </li>
                </isif>
                <li class="dropdown-item" role="presentation">
                    <isif condition="${category.level === 1 && category.thumbnailUrl === null}">
                        <a href="${category.url}" id="${category.id}" role="menuitem" class="nav-link dropdown-link nav-link-only-name">
                            ${category.name}
                        </a>
                    </isif>
                    <isif condition="${category.level === 1 && category.thumbnailUrl}">
                        <a href="${category.url}" id="${category.id}" role="menuitem" class="nav-link dropdown-link nav-link-with-thumbnail">
                            <img src="${category.thumbnailUrl}" alt="${category.name}" class="d-none d-xl-block" />
                            <span class="d-block mt-xl-3">${category.name}</span>
                        </a>
                    </isif>
                    <isif condition="${category.level > 1 && category.thumbnailUrl === null}">
                        <a href="${category.url}" id="${category.id}" role="menuitem" class="nav-link dropdown-link nav-link-with-count">
                            ${category.name} (${category.productCount})
                        </a>
                    </isif>
                    <isif condition="${category.level > 1 && category.thumbnailUrl}">
                        <a href="${category.url}" id="${category.id}" role="menuitem"
                            class="nav-link dropdown-link nav-link-with-thumbnail nav-link-with-count">
                            <img src="${category.thumbnailUrl}" alt="${category.name}" class="d-none d-xl-block" />
                            ${category.name} (${category.productCount})
                        </a>
                    </isif>
                </li>
                <isif condition="${category.level > 1 && loopState.last && menuItem.subCategories.length > menuItem.subcategoryItemLimit}">
                    <li class="dropdown-item hidden-lg-down" role="presentation">
                        <a href="${menuItem.url}" id="${menuItem.id}" role="menuitem"
                            class="nav-link dropdown-link nav-link-only-name link">
                            ${Resource.msgf('link.view.all.x', 'common', null, menuItem.name)}
                        </a>
                    </li>
                </isif>
                <isif condition="${category.level === 1 && loopState.last && menuItem.subCategories.length > menuItem.subcategoryItemLimit}">
                    <li class="dropdown-item flex-full-width hidden-lg-down js-link-main-menu-view-all">
                        <a href="${menuItem.url}" id="${menuItem.id}" role="menuitem" class="link">
                            ${Resource.msgf('link.view.all.x', 'common', null, menuItem.name)}
                        </a>
                    </li>
                </isif>
            </isif>
        </isloop>
    </ul>
</div>
