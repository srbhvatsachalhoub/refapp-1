<isset name="hasEmbeddedBonusProducts" value="${lineItem.bonusProductLineItemUUID === 'bonus'}" scope="page" />
<isset name="bonusproductlineitem" value="${hasEmbeddedBonusProducts ? 'bonus-product-line-item' : ''}" scope="page" />
<isset name="bonuslineitemrow" value="${hasEmbeddedBonusProducts ? 'bonus-line-item-row' : ''}" scope="page" />
<isset name="bonusproduct" value="${lineItem.isBonusProductLineItem ? 'bonus-product' : ''}" scope="page" />

<isif condition="${!lineItem.isBonusDiscountProductLineItem}">
    <div class="row cart-product product-info ${bonusproductlineitem} uuid-${lineItem.UUID} js-product-info">
        <isinclude template="cart/productCard/quantitySelect" />
        <div class="col-12">
            <div class="d-xl-none">
                <isinclude template="cart/productCard/cartProductCardHeader" />
            </div>
            <div class="row ${bonuslineitemrow} ${bonusproduct}">
                <div class="col-xl-7 d-flex">
                    <div class="cart-product-image">
                        <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                            <a  href="${URLUtils.url('Product-Show', 'pid', lineItem.id)}"
                                <isif condition="${lineItem.gtmEnhancedEcommerce && lineItem.gtmEnhancedEcommerce.productClick}">
                                    data-gtm-enhancedecommerce-onclick="${JSON.stringify(lineItem.gtmEnhancedEcommerce.productClick)}"
                                </isif>>
                                <img class="product-image" src="${lineItem.images.small[0].url}" alt="${lineItem.images.small[0].alt}" title="${lineItem.images.small[0].title}" />
                            </a>
                        <iselseif condition="${lineItem.isBonusProductLineItem || lineItem.isGiftWrappingProduct}" />
                            <img class="product-image" src="${lineItem.images.small[0].url}" alt="${lineItem.images.small[0].alt}" title="${lineItem.images.small[0].title}" />
                        </isif>
                        <isif condition="${pdict.CurrentCustomer.authenticated && !lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                            <div class="d-xl-none mt-2">
                                <isinclude url="${URLUtils.url('Wishlist-AddToWishlistButton', 'pid', lineItem.id)}" />
                            </div>
                        </isif>
                    </div>
                    <div class="cart-product-details-container">
                        <div class="cart-product-name">
                            <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                                <a  href="${URLUtils.url('Product-Show', 'pid', lineItem.id)}"
                                    <isif condition="${lineItem.gtmEnhancedEcommerce && lineItem.gtmEnhancedEcommerce.productClick}">
                                        data-gtm-enhancedecommerce-onclick="${JSON.stringify(lineItem.gtmEnhancedEcommerce.productClick)}"
                                    </isif>>
                                    ${lineItem.productName}
                                </a>
                            <iselseif condition="${lineItem.isBonusProductLineItem || lineItem.isGiftWrappingProduct}" />
                                ${lineItem.productName}
                            </isif>
                        </div>
                        <div class="cart-product-details">
                            <isif condition="${(lineItem.variationAttributes && lineItem.variationAttributes.length > 0) || (lineItem.options && lineItem.options.length > 0)}">
                                <isloop items="${lineItem.variationAttributes}" var="attribute">
                                    <div class="cart-product-attribute">
                                        <div class="cart-product-attribute-title d-xl-none">${attribute.displayName}</div>
                                        <p class="line-item-attributes ${attribute.displayName}-${lineItem.UUID}">
                                            ${attribute.displayValue}
                                        </p>
                                    </div>
                                </isloop>
                                <isloop items="${lineItem.options}" var="option">
                                    <isif condition="${!!option}">
                                        <div class="cart-product-attribute">
                                            <div class="lineItem-options-values" data-option-id="${option.optionId}"
                                                data-value-id="${option.selectedValueId}">
                                                <p class="line-item-attributes">${option.displayName}</p>
                                            </div>
                                        </div>
                                    </isif>
                                </isloop>
                            </isif>
                            <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                                <div class="d-xl-none line-item-quantity">
                                    <div class="simple-quantity quantity-form">
                                        <div class="cart-product-attribute-title">${Resource.msg('label.quantity', 'common', null)}</div>
                                        <isinclude template="cart/productCard/quantityInput" />
                                    </div>
                                </div>
                            </isif>
                            <div class="cart-product-actions">
                                <isif condition="${pdict.CurrentCustomer.authenticated && !lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                                    <div class="d-none d-xl-block">
                                        <isinclude url="${URLUtils.url('Wishlist-AddToWishlistButton', 'pid', lineItem.id)}" />
                                    </div>
                                </isif>
                                <div class="card-product-remove-desktop">
                                    <isinclude template="cart/productCard/cartProductCardHeader" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                    <div class="col-xl-3">
                        <div class="simple-quantity d-none d-xl-block quantity-form">
                            <isinclude template="cart/productCard/quantityInput" />
                        </div>
                    </div>
                    <div class="col-xl-2 px-xl-0">
                        <div class="cart-product-price">
                            <div class="item-total-${lineItem.UUID} price">
                                <isprint value="${lineItem.priceTotal.renderedPrice}" encoding="off" />
                            </div>
                        </div>
                    </div>
                <iselse/>
                    <div class="col-xl-3"></div>
                    <div class="col-xl-2 px-xl-0">
                        <div class="cart-bonus-product-no-price">
                            ${Resource.msg('label.free', 'cart', null)}
                        </div>
                    </div>
                </isif>
                <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                    <div class="line-item-promo item-${lineItem.UUID}">
                        <isinclude template="checkout/productCard/productCardProductPromotions" />
                    </div>
                </isif>
            </div>
        </div>
    </div>
</isif>
