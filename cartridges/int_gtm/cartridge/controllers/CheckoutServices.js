'use strict';

var server = require('server');
server.extend(module.superModule);

var gtmEnhancedEcommerceDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmEnhancedEcommerceDataLayerHelpers');

/**
 * Extend viewdata by setting page data layer object
 */
server.append('SubmitPayment', function (req, res, next) {
    this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
        // attach payment method checkoutoption
        gtmEnhancedEcommerceDataLayerHelpers.checkoutOption(req, res, 'payment');
    });
    next();
});

module.exports = server.exports();
