<isscript>
    var Site = require('dw/system/Site');
    var Locale = require('dw/util/Locale');

    var requestLocale = Locale.getLocale(request.locale);
    var googleTagManagerId;
    var googleAnalysticsId;
    var requestLocaleCountry;
    if (requestLocale) {
        requestLocaleCountry = requestLocale.country;
        googleTagManagerId = Site.current.getCustomPreferenceValue('GoogleTagManagerId');
        googleAnalysticsId = Site.current.getCustomPreferenceValue('GoogleAnalysticsId');
    }
</isscript>

<isif condition="${googleTagManagerId && requestLocaleCountry}">
    <isscript>
        var gtmId;
        try {
            var gtmPref = JSON.parse(googleTagManagerId) || {};
            if (requestLocaleCountry in gtmPref) {
                gtmId = gtmPref[requestLocaleCountry];
            }
        } catch (e) {
            require('dw/system/Logger')
                .getLogger('int_gtm')
                .error('Google Tag Manager Id configuration is malformed!');
        }
    </isscript>
    <isif condition="${gtmId}">
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','${gtmId}');</script>
        <!-- End Google Tag Manager -->

        <script defer src="${URLUtils.staticURL('/js/gtm.js').toString()}"></script>
    </isif>
</isif>
<isif condition="${googleAnalysticsId && requestLocaleCountry}">
    <isscript>
        var gaId;
        try {
            var gaPref = JSON.parse(googleAnalysticsId) || {};
            if (requestLocaleCountry in gaPref) {
                gaId = gaPref[requestLocaleCountry];
            }
        } catch (e) {
            require('dw/system/Logger')
                .getLogger('int_gtm')
                .error('Google Analystics configuration is malformed!');
        }
    </isscript>
    <isif condition="${gaId}">
        <iscomment>Global site tag (gtag.js) - Google Analytics</iscomment>
        <script async src="https://www.googletagmanager.com/gtag/js?id=${gaId}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${gaId}');
        </script>
    </isif>
</isif>
