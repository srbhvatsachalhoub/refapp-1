'use strict';

var base = module.superModule;

var gtmEnhancedEcommerceAddToCart = require('*/cartridge/models/product/decorators/gtmEnhancedEcommerceAddToCart');

/**
 * Decorate product with gtm enhanced ecommerce event object
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {Object} options - Options passed in from the factory
 *
 * @returns {Object} - Decorated product model
 */
module.exports = function fullProduct(product, apiProduct, options) {
    base.call(this, product, apiProduct, options);
    gtmEnhancedEcommerceAddToCart(product, apiProduct);
    return product;
};
