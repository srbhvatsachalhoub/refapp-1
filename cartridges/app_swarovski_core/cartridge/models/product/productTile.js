'use strict';

var decorators = require('*/cartridge/models/product/decorators/index');

/**
 * Decorate product with product tile information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {string} productType - Product type information
 * @param {Object} options - Options passed in from the factory
 * @returns {Object} - Decorated product model
 */
module.exports = function productTile(product, apiProduct, productType, options) {
    decorators.base(product, apiProduct, productType);
    decorators.backInStock(product, apiProduct);
    decorators.sellable(product, apiProduct);
    decorators.availability(product, options.quantity, apiProduct.minOrderQuantity.value, apiProduct.availabilityModel);
    decorators.quantity(product, apiProduct, 1);
    decorators.price(product, apiProduct, options.promotions, false, options.optionModel);
    decorators.images(product, apiProduct, { types: ['medium'], quantity: 'single' });
    decorators.ratings(product);
    decorators.description(product, apiProduct);
    decorators.badges(product, apiProduct, options.promotions);
    if (productType === 'set') {
        decorators.setProductsCollection(product, apiProduct);
    }
    decorators.customBadge(product, apiProduct);
    decorators.readyToOrder(product, options.variationModel);
    decorators.variationAttributes(product, options.variationModel, {
        attributes: '*',
        endPoint: 'Variation'
    });

    decorators.promotions(product, options.promotions);
    decorators.category(product, apiProduct, options.productType);
    decorators.selectedVariationAttributes(product, apiProduct, options.variationModel);

    return product;
};
