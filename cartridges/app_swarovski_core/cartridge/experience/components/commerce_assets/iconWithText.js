'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');
/**
 * Render logic for storefront.iconWithText component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */


module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.iconText = content.iconText;
    model.iconAlt = content.iconAlt;
    model.icon = ImageTransformation.getScaledImage(content.icon);

    return new Template('experience/components/commerce_assets/iconWithText').render(model).text;
};
