'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');
/**
 * Render logic for storefront.alignedVideo component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */


module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.videoUrl = content.embedId ? 'https://www.youtube.com/embed/' + content.embedId + '?autoplay=0&rel=0' : '#';
    model.embedId = content.embedId;
    model.videoImage = ImageTransformation.getScaledImage(content.videoImage);

    return new Template('experience/components/commerce_assets/alignedVideo').render(model).text;
};
