<isinclude template="/components/modules" sf-toolkit="off" />

<div class="product-tile"
        data-pid="${product.id}"
        <isif condition="${product.gtmEnhancedEcommerce && product.gtmEnhancedEcommerce.productImpression}">
            data-gtm-enhancedecommerce-impression="${JSON.stringify(product.gtmEnhancedEcommerce.productImpression)}"
        </isif>>
    <!-- dwMarker="product" dwContentID="${product.uuid}" -->
    <a  href="${pdict.urls.product}"
        itemprop="url"
        class="product-tile-link"
        <isif condition="${product.gtmEnhancedEcommerce && product.gtmEnhancedEcommerce.productClick}">
            data-gtm-enhancedecommerce-onclick="${JSON.stringify(product.gtmEnhancedEcommerce.productClick)}"
        </isif>></a>
    <isinclude template="product/components/productTileImage" />

    <div class="tile-body">
        <div class="tile-promo-labels">
            <isinclude template="product/components/promotions" />
            <isloop items="${product.variationAttributes}" var="attr" status="loopstatus">
                <isif condition="${attr.attributeId === 'color'}">
                    <span class="tile-promo-label">${Resource.msgf('label.colors.count', 'search', null, attr.values.length.toFixed(0))}</span>
                </isif>
            </isloop>
        </div>

        <isif condition="${pdict.display.showBadges !== false && product.badges.length > 0}">
            <div class="badges-container">
                <isset name="badges" value="${product.badges}" scope="page" />
                <isinclude template="product/components/productTileBadges" />
            </div>
        </isif>

        <div class="swatches-container">
            <isif condition="${pdict.display.swatches !== false}">
                <isinclude template="product/components/productTileSwatch" />
            </isif>
        </div>

        <div class="name-container">
            <isinclude template="product/components/productTileName" />
        </div>

        <div class="price-container">
            <isset name="price" value="${product.price}" scope="page" />
            <isif condition="${product.productType === 'set'}">
                <isinclude template="product/components/pricing/setPrice" />
            <iselse/>
                <isinclude template="product/components/pricing/mainWithAttributes" />
            </isif>
        </div>

        <isif condition="${product.ratingSummary}">
            <div class="ratings-container d-none">
                <isif condition="${pdict.display.ratings !== false && product.ratingSummary.ratingCount > 0}">
                    <isratingstar
                        ratingstar_show_summary="false"
                        ratingstar_count="${product.ratingSummary.ratingCount}"
                        ratingstar_value="${product.ratingSummary.averageRating}"
                        ratingstar_filled_class="sc-icon-star"
                        ratingstar_half_filled_class="sc-icon-star-fill-half"
                        ratingstar_empty_class="sc-icon-star-empty" />
                </isif>
            </div>
        </isif>

        <isif condition="${pdict.display.showShopNowButton !== false}">
            <isinclude template="product/components/productTileButtonShopNow" />
        </isif>

        <isif condition="${pdict.display.showAddToBagButton}">
            <isinclude template="product/components/productTileButtonAddToBag" />
        </isif>
    </div>
    <!-- END_dwmarker -->
</div>
