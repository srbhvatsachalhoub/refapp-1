<isset name="hasEmbeddedBonusProducts" value="${lineItem.bonusProductLineItemUUID === 'bonus'}" scope="page" />
<isset name="bonusProductLineItem" value="${hasEmbeddedBonusProducts ? 'bonus-product-line-item' : ''}" scope="page" />
<isset name="bonusLineItemRow" value="${hasEmbeddedBonusProducts ? 'bonus-line-item-row' : ''}" scope="page" />
<isset name="bonusProduct" value="${lineItem.isBonusProductLineItem ? 'bonus-product' : ''}" scope="page" />

<isif condition="${!lineItem.isBonusDiscountProductLineItem}">
    <div class="row cart-product product-info ${bonusProductLineItem} uuid-${lineItem.UUID} ${bonusLineItemRow} ${bonusProduct} js-product-info">
        <isinclude template="cart/productCard/quantitySelect" />
        <div class="col-10 offset-1 offset-md-0 col-md-4">
            <div class="cart-product-image">
                <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                    <a  href="${URLUtils.url('Product-Show', 'pid', lineItem.id)}"
                        <isif condition="${lineItem.gtmEnhancedEcommerce && lineItem.gtmEnhancedEcommerce.productClick}">
                            data-gtm-enhancedecommerce-onclick="${JSON.stringify(lineItem.gtmEnhancedEcommerce.productClick)}"
                        </isif>>
                        <img class="product-image" src="${lineItem.images.small[0].url}" alt="${lineItem.images.small[0].alt}" title="${lineItem.images.small[0].title}" />
                    </a>
                <iselseif condition="${lineItem.isBonusProductLineItem || lineItem.isGiftWrappingProduct}" />
                    <img class="product-image" src="${lineItem.images.small[0].url}" alt="${lineItem.images.small[0].alt}" title="${lineItem.images.small[0].title}" />
                </isif>
            </div>
        </div>
    
        <div class="col-10 offset-1 offset-md-0 col-md-8">
            <div class="row">
                <div class="col-md-8">
                    <div class="cart-product-details-container">
                        <div class="cart-product-name">
                            <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                                <a  href="${URLUtils.url('Product-Show', 'pid', lineItem.id)}"
                                    <isif condition="${lineItem.gtmEnhancedEcommerce && lineItem.gtmEnhancedEcommerce.productClick}">
                                        data-gtm-enhancedecommerce-onclick="${JSON.stringify(lineItem.gtmEnhancedEcommerce.productClick)}"
                                    </isif>>
                                    ${lineItem.productName}
                                </a>
                            <iselseif condition="${lineItem.isBonusProductLineItem}" />
                                ${lineItem.productName}
                            </isif>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="cart-product-price">
                        <div class="item-total-${lineItem.UUID} price justify-content-md-end">
                            <isprint value="${lineItem.priceTotal.renderedPrice}" encoding="off" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="cart-product-details">
                        <isif condition="${(lineItem.variationAttributes && lineItem.variationAttributes.length > 0) || (lineItem.options && lineItem.options.length > 0)}">
                            <isloop items="${lineItem.variationAttributes}" var="attribute">
                                <div class="cart-product-attribute">
                                    <div class="cart-product-attribute-title">${attribute.displayName}</div>
                                    <p class="line-item-attributes ${attribute.displayName}-${lineItem.UUID}">
                                        ${attribute.displayValue}
                                    </p>
                                </div>
                            </isloop>
                            <isloop items="${lineItem.options}" var="option">
                                <isif condition="${!!option}">
                                    <div class="cart-product-attribute">
                                        <div class="lineItem-options-values" data-option-id="${option.optionId}"
                                            data-value-id="${option.selectedValueId}">
                                            <p class="line-item-attributes">${option.displayName}</p>
                                        </div>
                                    </div>
                                </isif>
                            </isloop>
                        </isif>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6 col-lg-5">
                    <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                        <div class="simple-quantity quantity-form">
                            <isinclude template="cart/productCard/quantityInput" />
                        </div>
                    <iselse/>
                        <div class="cart-bonus-product-no-price">
                            ${Resource.msg('label.free', 'cart', null)}
                        </div>
                    </isif>
                </div>
                <div class="col-6 col-lg-7">
                    <div class="cart-product-actions justify-content-lg-end">
                        <isinclude url="${URLUtils.url('Wishlist-AddToWishlistButton', 'pid', lineItem.id)}" />
                        <isinclude template="cart/productCard/cartProductCardHeader" />
                    </div>                       
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <isif condition="${!lineItem.isBonusProductLineItem && !lineItem.isGiftWrappingProduct}">
                        <div class="line-item-promo item-${lineItem.UUID}">
                            <isinclude template="checkout/productCard/productCardProductPromotions" />
                        </div>
                    </isif>
                </div>
            </div>
        </div>
    </div>
</isif>
