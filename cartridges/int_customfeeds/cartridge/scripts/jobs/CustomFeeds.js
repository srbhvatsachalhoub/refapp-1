'use strict';

/**
 * Script module version of the int_customfeeds/cartridge/pipelines/CustomFeeds.xml
 * 
 * Creates and registers export handlers from the CustomFeedConfig custom object definitions
 * 
 */

importScript('export/CustomFeedsExportMgr.ds');
importScript('export/handlers/CSVExportHandler.ds');
importScript('export/handlers/TemplateExportHandler.ds');
var CustomFeedsExportMgr = getCustomFeedsExportMgr();
var CSVExportHandler = getCSVExportHandler();
var TemplateExportHandler = getTemplateExportHandler();

var Status = require('dw/system/Status');
var Site = require('dw/system/Site');
var Locale = require('dw/util/Locale');
var StringUtils = require('dw/util/StringUtils');


/**
 * Generate the Feeds
 * @param parameters
 * @param stepExecution
 * @returns {Object} status code
 */
function generate(parameters, stepExecution) {
    var Logger = require('dw/system/Logger');
	try {		
		var executionContexts = {
	        ExportCatalogs : false, 
	        ExportOrders : false, 
	        ExportCustomers : false, 
	        DaysToExportCustomers : 1, 
	        DaysToExportOrders : 1
	    };
		
		executionContexts.ExportCatalogs = parameters.ExportCatalogs;
		executionContexts.ExportOrders = parameters.ExportOrders;
		executionContexts.ExportCustomers = parameters.ExportCustomers;
		executionContexts.DaysToExportCustomers = parameters.DaysToExportCustomers;
        executionContexts.DaysToExportOrders = parameters.DaysToExportOrders;

		var currentSite = Site.getCurrent();
		for each(var locale in currentSite.getAllowedLocales()) {			
			// check if the job locale and the current locale identical
			// if not we should set the new locale to request to get the localized values for the attributes 
			if(locale != request.getLocale()) {
				if(!request.setLocale(locale)) {
					// locale could not set successfully
					throw new Error(StringUtils.format('Could not set locale, locale: {0}', locale));
				}
            }
            // set currency based on countries.json
            var countries = require('*/cartridge/config/countries');
            var reqLocale = request.getLocale();
            if (countries && reqLocale) {
                var targetLocale = countries.filter(function (country) {
                    return reqLocale === country.id;
                });
                targetLocale = targetLocale.length > 0 ? targetLocale[0] : null;

                if (targetLocale && Object.keys(targetLocale).length && !request.session.getCurrency().getCurrencyCode().equalsIgnoreCase(targetLocale.currencyCode)) {
                    var Currency = require('dw/util/Currency');
                    request.session.setCurrency(Currency.getCurrency(targetLocale.currencyCode));
                }
            }

			var exportMgr = new CustomFeedsExportMgr(executionContexts);
			registerConfigurableHandlers(exportMgr, currentSite.getID(), executionContexts, locale);
			
			// check if any handler registered for the current site and locale
			if(empty(exportMgr.handlers)) {
				continue;
			}

			// run exports for registered handlers
			exportMgr.runExport();
		}
	} catch (e) {
		var errorMessage = StringUtils.format('Error occurred during feed generation: {0}', e.message);
	    Logger.error(errorMessage);
	    return new Status(Status.ERROR, 'ERROR', errorMessage);
	}
	
	return new Status(Status.OK, 'OK');
}

/**
 * Gets the default shipping method for the current locale (currency)
 */
function getDefaultShippingMethod() {
    var allShippingMethods = dw.order.ShippingMgr.getAllShippingMethods();
    var defaultShipping = null;
    for(var i = 0; i < allShippingMethods.length; i++) {
        var shippingMethod = allShippingMethods[i];
        if (shippingMethod.currencyCode === session.currency.currencyCode && shippingMethod.defaultMethod) {
            defaultShipping = shippingMethod;
            break;
        } 
    }
    return defaultShipping;
}

/**
 * Helper function which handles the custom objects
 * @param {Object} exportMgr CustomFeedsExportMgr
 * @param {Object} currentSite currentSiteId
 * @param {Object} executionContexts executionContexts object
 * @param {Object} locale current locale
 * returns{void}
 */
function registerConfigurableHandlers(exportMgr, currentSite, executionContexts, currentLocale) {
	var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var customFeedObjects = CustomObjectMgr.getAllCustomObjects('CustomFeedConfig');
    
    var defaultShipping = getDefaultShippingMethod();
	
	while (customFeedObjects.hasNext()) {
		var customFeedObject = customFeedObjects.next();
        
        // we should not register handlers, if current site is not configured
        // for this custom feeds
        var assignedSites = customFeedObject.custom.assignedSites;
        if(assignedSites.indexOf(currentSite) === -1) {
            continue;
        }
        
        // we should not register handlers, if current locale is not configured
        // for this custom feeds
        var assignedLocales = customFeedObject.custom.assignedLocales;
        if(assignedLocales.indexOf(currentLocale) === -1) {
            continue;
        }
        
        // skip the execution of current export handler, if job does not enable it with a parameter
        switch(customFeedObject.custom.feedContext.value) {
            case 'Catalog': 
                if (!executionContexts.ExportCatalogs) { 
                    continue;
                }
                break;
            case 'Order': 
                if (!executionContexts.ExportOrders) { 
                    continue;
                }
                break;
            case 'Customer': 
                if (!executionContexts.ExportCustomers) { 
                    continue;
                } 
                break;
        }
        
        var File = require('dw/io/File');
        
        var folder = new File(customFeedObject.custom.folderName);
        if(!folder.exists() && !folder.mkdirs()) {
            throw new Error(StringUtils.format('Could not create folder {0}', customFeedObject.custom.folderName));
        }
        
        var fileName = customFeedObject.custom.fileName.replace(/\{\{[^}]*\}\}/g, function(a) {
            var parts = a.split(/(?:\{\{| |\}\})/g);
            var variable = parts[1];
	        if(variable == 'timestamp'){
	            var format = 'yyyyMMddhhmmss';
	            parts.forEach(function(part) {
	                if(part.indexOf('format=') == 0) {
	                    format = part.substring(0, part.length-1).substring(8);
	                }
	            });
	            return StringUtils.formatCalendar(Site.getCalendar(), format);
	        }
	        if(variable == 'countrycode') {
	            return Locale.getLocale(Site.getCurrent().defaultLocale).country;
	        }
	        if(variable == 'locale') {
	            return currentLocale;
	        }
	        return '';
        });
        
        var file = new File(folder, fileName);
        var encoding = customFeedObject.custom.fileEncoding || 'UTF-8';
        if(!file.exists() && !file.createNewFile()) {
            throw new Error(StringUtils.format('Could not create export file, folder: {0}, fileName: {1}', customFeedObject.custom.folderName, fileName));
        }
        
        var FileWriter = require('dw/io/FileWriter');
        
        if(customFeedObject.custom.type == 'XML') {
            exportMgr.registerExportHandler(new TemplateExportHandler(new FileWriter(file, encoding), customFeedObject.custom.configuration, customFeedObject.custom.feedContext.value, defaultShipping));
        } else if(customFeedObject.custom.type == 'CSV') {
        	var Reader = require('dw/io/Reader');
        	
            var lines = new Reader(customFeedObject.custom.configuration);
            var config = {separator : ','};
            var line;
            while((line = lines.readLine()) !=null) {
                if(line.indexOf('separator ') == 0){
                    config.separator = line.substring(10);
                } else if(!config.fields) {
                    // use first line as fields
                    config.fields = line.split(config.separator);
                } else if(!config.header){
                    // if there are more lines, we previously read the header
                    config.header = config.fields;
                    config.fields = line.split(config.separator);
                }
            }
            exportMgr.registerExportHandler(new CSVExportHandler(new FileWriter(file, encoding), config.separator, config.fields, config.header, customFeedObject.custom.feedContext.value, defaultShipping));
        }
    }
}

module.exports = {
    generate: generate
};
