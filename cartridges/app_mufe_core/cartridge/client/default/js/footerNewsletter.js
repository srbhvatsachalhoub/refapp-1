'use strict';
var formValidation = require('base/components/formValidation');
var throttle = require('lodash/throttle');

$(document).ready(function () {
    /* eslint-disable */
    var $win = $(window);
    function checkWidth() {
        if ($win.width() <= window.RA_BREAKPOINTS.sm) {
            var bg = $('#formBackgroundImageMobile').val();
            ('.js-form-background').css('background-image', 'url(' + bg + ')');
        } else if ($win.width() <= window.RA_BREAKPOINTS.lg) {
            var bg = $('#formBackgroundImageTablet').val();
            $('.js-form-background').css('background-image', 'url(' + bg + ')');
        } else if ($win.width() >= window.RA_BREAKPOINTS.xl) {
            var bg = $('#formBackgroundImageDesktop').val();
            $('.js-form-background').css('background-image', 'url(' + bg + ')');
        }
    }

    checkWidth();
    $win.on('resize', throttle(checkWidth, 100));
    /* eslint-enable */
});


$('body').on('click', '.js-subbscribe-modal-button', function (e) {
    var $form = $('.subscribe-newsletter-form');
    e.preventDefault();
    var url = $form.attr('action');
    $('.js-error, .js-success', $form).addClass('d-none');
    $form.spinner().start();
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: $form.serialize(),
        success: function (data) {
            $form.spinner().stop();
            if (!data.success) {
                if (data.fieldErrors) {
                    formValidation($form, data);
                } else {
                    $('.js-error', $form).removeClass('d-none');
                    $('.js-error').fadeIn('slow');
                }
            } else {
                if (data.redirectUrl) { //eslint-disable-line
                    window.location = data.redirectUrl;
                } else {
                    $('.js-success', $form).removeClass('d-none');
                    $('.js-success').fadeIn('slow');
                }
            }
        },
        error: function () {
            $('.js-error', $form).removeClass('d-none');
            $form.spinner().stop();
        }
    });
    setTimeout(function () {
        $('.js-error,.js-success').fadeOut('slow');
    }, 8000);

    return false;
});
