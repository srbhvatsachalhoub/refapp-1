'use strict';

$(document).ready(function () {
    var $section = $('.home-video-background');
    var $videoPlayBtn = $('.chevron-right-arrow-btn');

    if ($section.length) {
        var bg = $('#videoBackgroundImage').val();
        if (bg.length) {
            $section.css('background-image', 'url(' + bg + ')');
        }
    }

    $videoPlayBtn.on('click', function () {
        $('.home-video-player-thumbnail').css('display', 'none');
    });
});
