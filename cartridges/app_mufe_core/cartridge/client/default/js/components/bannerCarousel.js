'use strict';

var nextArrow = require('../templates/slickArrowNext');
var prevArrow = require('../templates/slickArrowPrev');
var carousel = require('brand_core/components/carousel');

module.exports = function (selector) {
    $(selector || '.js-banner-carousel').each(function () {
        carousel($(this), {
            autoplay: true,
            autoplaySpeed: 7000,
            dots: true,
            nextArrow: nextArrow,
            prevArrow: prevArrow
        });
    });
};
