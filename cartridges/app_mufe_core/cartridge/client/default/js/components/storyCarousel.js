'use strict';

var nextArrow = require('../templates/storyNextArrow');
var prevArrow = require('../templates/storyPrevArrow');
var carousel = require('brand_core/components/carousel');

module.exports = function (selector) {
    $(selector || '.js-story-carousel').each(function () {
        carousel($(this), {
            nextArrow: nextArrow,
            prevArrow: prevArrow,
            dots: true
        });
    });
};
