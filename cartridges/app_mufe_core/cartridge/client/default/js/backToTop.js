'use strict';

var backToTopBtn = $('.back-to-top-button');
$(window).scroll(function () {
    if (backToTopBtn.length > 0) {
        if ($(window).scrollTop() > 300) {
            backToTopBtn.fadeIn('slow');
        } else {
            backToTopBtn.fadeOut('slow');
        }
    }
});

$('.back-to-top-button').on('click', function () {
    $('html, body').animate({ scrollTop: 0 }, 500);
});
