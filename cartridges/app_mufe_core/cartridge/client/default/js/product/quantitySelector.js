'user strict';

/**
 * Calculates new quantity within the range of min and max available quantities
 * @param {number} step - Value to be added to current quantity
 * @param {number} current - Current quantity
 * @param {number} min - Min available quantity
 * @param {number} max - Max available quantity
 * @returns {number} New quantity value
 */
function getQuantity(step, current, min, max) {
    /* eslint-disable no-param-reassign */
    min = parseInt(min, 10);
    max = parseInt(max, 10);
    step = parseInt(step || 0, 10);
    current = parseInt(current, 10);
    if (isNaN(current)) {
        current = min;
    }
    return Math.max(Math.min(current + step, max), min);
}

/**
 * Sets new quantity
 * @param {jQuery} $this - Button or input element
 * @param {number} timer - Timer
 * @returns {number} Returns timer
 */
function setQuantity($this, timer) {
    if ($this.hasClass('disable')) {
        return timer;
    }
    var uuid = $this.data('uuid');
    var $quantityInput = $('.js-quantity-input[data-uuid="' + uuid + '"]');
    var $quantitySelect = $('.js-quantity-select[data-uuid="' + uuid + '"]');
    var $quantityBtn = $('.js-quantity-btn[data-uuid="' + uuid + '"]');
    var step = $this.data('step');
    var min = $quantitySelect.find('option:first').val();
    var max = $quantitySelect.find('option:last').val();
    var current = $quantityInput.val();
    var newQuantity = getQuantity(step, current, min, max);
    if (isNaN(newQuantity)) {
        newQuantity = min;
    }
    $quantityInput.val(newQuantity);
    clearTimeout(timer);
    return setTimeout(function () {
        if (parseInt($quantitySelect.data('last'), 10) !== newQuantity) {
            $quantitySelect.data('last', newQuantity);
            $quantitySelect.val(newQuantity).trigger('change');
            $quantityBtn.filter('[data-step="-1"]')[min === newQuantity.toString() ? 'addClass' : 'removeClass']('disabled disabled-limit');
            $quantityBtn.filter('[data-step="1"]')[max === newQuantity.toString() ? 'addClass' : 'removeClass']('disabled disabled-limit');
        }
    }, 300);
}

/**
 * Initializes quantity selector
 */
function init() {
    var $quantityBtn = $('.js-quantity-btn');
    var $quantityInput = $('.js-quantity-input');
    var timer;

    $quantityBtn.on('click', function (e) {
        e.preventDefault();
        timer = setQuantity($(this), timer);
    });

    $quantityInput.on('input', function (e) {
        e.preventDefault();
        timer = setQuantity($(this), timer);
    });
}

module.exports = {
    getQuantity: getQuantity,
    init: init
};
