<div class="order-total-summary-container">
    <iscomment>Subtotal</iscomment>
    <div class="subtotal-item">
        <div class="row">
            <div class="col-6">
                <p class="order-receipt-label"><span>${Resource.msg('label.order.subtotal','confirmation', null)}</span></p>
            </div>
            <div class="col-6">
                <p class="text-right"><span class="sub-total">${pdict.order.totals.subTotal}</span></p>
            </div>
        </div>
    </div>

    <iscomment>Order Discount</iscomment>
    <div class="order-discount ${pdict.order.totals.orderLevelDiscountTotal.value === 0 ? 'hide-order-discount' : ''}">
        <div class="row">
            <div class="col-6">
                <p class="order-receipt-label"><span>${Resource.msg('label.order.discount', 'common', null)}</span></p>
            </div>
            <div class="col-6">
                <p class="text-right"><span class="order-discount-total">- ${pdict.order.totals.orderLevelDiscountTotal.formatted}</span></p>
            </div>
        </div>
    </div>

    <iscomment>Shipping Cost</iscomment>
    <div class="shipping-item">
        <div class="row">
            <div class="col-6">
                <p class="order-receipt-label"><span>${Resource.msg('label.order.delivery','confirmation', null)}</span></p>
            </div>
            <div class="col-6">
                <p class="text-right"><span class="shipping-total-cost text-uppercase">${pdict.order.totals.totalShippingCost}</span></p>
            </div>
        </div>
    </div>

    <iscomment>Shipping Discount</iscomment>
    <div class="shipping-discount ${pdict.order.totals.shippingLevelDiscountTotal.value === 0 || pdict.order.totals.shippingLevelDiscountTotal.isFree ? 'hide-shipping-discount' : ''}">
        <div class="row">
            <div class="col-6">
                <p class="order-receipt-label"><span>${Resource.msg('label.shipping.discount', 'common', null)}</span></p>
            </div>
            <div class="col-6">
                <p class="text-right"><span class="shipping-discount-total">- ${pdict.order.totals.shippingLevelDiscountTotal.formatted}</span></p>
            </div>
        </div>
    </div>

    <iscomment>Payment Service Fee</iscomment>
    <div class="js-payment-service-fee ${pdict.order.totals.serviceFee && pdict.order.totals.serviceFee.value > 0 ? '' : 'd-none'}">
        <div class="row">
            <div class="col-6">
                <p class="order-receipt-label"><span>${ Resource.msg('payment.service.fee', 'order', 'Payment Service Fee') }</span></p>
            </div>
            <div class="col-6">
                <p class="text-right"><span class="js-payment-service-fee-cost">${ pdict.order.totals.serviceFee && pdict.order.totals.serviceFee.formatted ? pdict.order.totals.serviceFee.formatted : '' }</span></p>
            </div>
        </div>
    </div>

    <iscomment>Grand Total</iscomment>
    <div class="grand-total">
        <div class="row">
            <div class="col-6">
                <p class="order-receipt-label">
                    <span>
                        ${Resource.msg('label.order.grand.total','confirmation', null)}
                    </span>
                    <span class="vat-label">
                        <isif condition="${'taxRate' in pdict.order.totals}">
                            (${Resource.msgf('label.including.x.percent.vat', 'account', null, pdict.order.totals.taxRate)})
                        <iselse/>
                            (${Resource.msg('label.including.vat', 'account', null)})
                        </isif>
                    </span>
                </p>
            </div>
            <div class="col-6">
                <p class="text-right"><span class="grand-total-sum">${pdict.order.totals.grandTotal}</span></p>
            </div>
        </div>
    </div>

    <iscomment>Gift Card/Amount Due Costs</iscomment>
    <isif condition="${dw.system.Site.getCurrent().getCustomPreferenceValue('isGiftCardEnabled') && pdict.order.billing.payment.hasOwnProperty('isGiftCardCodePaymentAvailable') && pdict.order.billing.payment.isGiftCardCodePaymentAvailable}">
        <isinclude template="giftCard/checkout/giftCardTotalSummary" />
    </isif>
</div>
