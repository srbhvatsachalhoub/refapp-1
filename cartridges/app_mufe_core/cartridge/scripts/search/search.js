'use strict';

var base = module.superModule;

/**
 * Sets the relevant product search model properties, depending on the parameters provided
 * Extended with setting PriceMin to -1 if pmin is not provided
 * Extended with setting PriceMax to (PriceMax+1) in order to list the products which priced same with pricemax
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {Object} httpParams - Query params
 * @param {dw.catalog.Category} selectedCategory - Selected category
 * @param {dw.catalog.SortingRule} sortingRule - Product grid sort rule
 */
function setProductProperties(productSearch, httpParams, selectedCategory, sortingRule) {
    base.setProductProperties.call(this, productSearch, httpParams, selectedCategory, sortingRule, true);
}

module.exports = {
    addRefinementValues: base.addRefinementValues,
    setProductProperties: setProductProperties
};
