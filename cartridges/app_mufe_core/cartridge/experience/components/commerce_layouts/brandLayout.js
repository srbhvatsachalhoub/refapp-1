'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var PageRenderHelper = require('*/cartridge/experience/utilities/PageRenderHelper.js');

/**
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var component = context.component;
    model.regions = PageRenderHelper.getRegionModelRegistry(component);
    var content = context.content;
    model.header = content.header;
    model.imageURL = '';
    if (content.image) {
        model.imageURL = content.image.file.URL;
    }
    model.showBackToTop = content.showBacktoTop;

    return new Template('experience/components/commerce_layouts/brandLayout').render(model).text;
};
