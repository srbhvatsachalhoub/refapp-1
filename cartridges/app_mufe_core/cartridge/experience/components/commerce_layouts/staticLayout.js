'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var PageRenderHelper = require('*/cartridge/experience/utilities/PageRenderHelper.js');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');

/**
 * Render logic for the storefront.2 Row x 2 Col (Mobile) 1 Row x 4 Col (Desktop) layout
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var component = context.component;
    model.regions = PageRenderHelper.getRegionModelRegistry(component);
    var content = context.content;
    model.header = content.header;

    var resource = '';
    switch (content.pageType) {
        case 'faq':
            model.className = 'faq-content';
            resource = 'title.faq';
            break;
        case 'privacy policy':
            model.className = 'privacy-policy-container';
            resource = 'title.privacy.policy';
            break;
        case 'terms and conditions':
            model.className = 'terms-container';
            resource = 'title.terms.and.conditions';
            break;
        case 'accessibility':
            model.className = 'accessibility-container';
            resource = 'title.accessibility';
            break;
        default:
            model.className = 'faq-content';
            resource = 'title.faq';
    }
    var breadcrumbs = [
        {
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg(resource, 'common', null)
        }
    ];
    model.breadcrumbs = breadcrumbs;
    model.showBackToTop = content.showBacktoTop;

    return new Template('experience/components/commerce_layouts/staticLayout').render(model).text;
};
