'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var URLUtils = require('dw/web/URLUtils');
/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var content = context.content;
    var model = new HashMap();
    model.imageURL = content.image.file.URL;
    model.title = content.title ? content.title : '';
    model.pageLink = URLUtils.url('Page-Show', 'cid', content.pageLink);

    return new Template('experience/components/commerce_assets/miniVideoSection').render(model).text;
};
