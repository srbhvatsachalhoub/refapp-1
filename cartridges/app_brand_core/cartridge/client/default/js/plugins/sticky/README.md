# SFRA Sticky Header jQuery Plugin

This plugin allows any given element to stick on top of the page while scrolling.

## Usage

```html
    <div class="js-some-selector">
        Some content
    </div>
```
```js
    $('.js-some-selector').sticky();
```

You can customize options by using data attributes. (see opitons table below)
```html
    <div class="js-some-selector" data-sticky-offset-top="200"></div>
```

If the input element has `js-sticky` class the plugin initializes itself automatically with supplied data attributes, otherwise you need to call `.sticky()` method manually.
```js
  $('.js-some-selector').sticky();
```

You can pass options object for customization.  (see opitons table below)
```js
  $('.js-some-selector').sticky({
    offsetTop: 200
  });
```

---

## Options

All options listed below are also available as HTML5 data attributes. In order to use them on search input element, you need to convert keys from camelCase to data-sws-hyphenated-text as follows. Let's say you want to change sticky status class, in this case *stickedStateClass* should be *data-sticky-sticked-state-class* as follows;
```html
    <div class="js-sticky" data-sticky-sticked-state-class="a-class-name"></div>
```
<br />

| Option | Description | Default |
|---|---|---|
| stickyClass | A class that will be added to sticky continer element | <span style="color:#ce8349;">fixed-top</span> |
| stickedStateClass | A class that will be added when element is sticked | <span style="color:#ce8349;">sticked</span> |
| offsetTop | Element will be sticked on top of the page after given offset in pixels is passed while scrolling | <span style="color:#93ce89;">100</span>  |
| throttleTime | Scrolling throttle time in milliseconds | <span style="color:#93ce89;">100</span> |

---

## Methods

You can call following methods after initializing the plugin.

| Method | Description |
|---|---|
| scroll | Sticks or unsticks the element depending on the scroll position |
| resize | Fills empty area with sticked element's height |

These methods can be used as follows;
```js
  $('.js-sticky').sticky('refresh', {
    // new options
  });
  $('.js-sticky').sticky('destroy');
  $('.js-sticky').sticky('scroll');
  $('.js-sticky').sticky('resize');
```

---

## Events

You can listen events as follows;

```js
  $('.js-sticky').on('sticky:stick', function() {
      // do something
  });
```

| Event | Description |
|---|---|
| <span style="color:#ce8349;">sticky:stick</span> | Triggers when the element is sticked on top of the page |
| <span style="color:#ce8349;">sticky:unstick</span> | Triggers when the element is released from top of the page |
| <span style="color:#ce8349;">sticky:resize</span> | Triggers when window is resized |
