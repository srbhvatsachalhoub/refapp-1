# SFRA Product Loader jQuery Plugin

This plugin allows you to load product by using html data attribute via ajax.

## Usage

```html
    <div data-load-pid="25413129M">
        Some content
        <!-- Product will be loaded here -->
    </div>
```

---

## Options

This plugin has no options.

---

## Methods

You can call destroy method after initializing the plugin as follows

```js
  $('[data-load-pid]').productLoader('destroy');
```

---

## Events

You can listen events as follows;

```js
  $('[data-load-pid]').on('productLoader:loaded', function() {
      // do something
  });
```

| Event | Description |
|---|---|
| <span style="color:#ce8349;">productLoader:loaded</span> | Triggers when the product is loaded in the DOM |
