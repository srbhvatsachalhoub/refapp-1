/* eslint no-param-reassign:0 */

'use strict';

window.jQuery = window.$ = require('jquery');
var debounce = require('lodash/debounce');
var pluginator = require('../pluginator');
var pluginPrefix = 'sws';
var $doc = $(document);

/**
 * Plugin constructor
 * @param {JQuery} $el - Search input element
 * @param {string} id - Id of the current plugin
 * @param {Object} options - Plugin opitons
 */
function SearchWithSuggestions($el, id, options) {
    this.$el = $el;
    this.id = id;
    this.options = options;
    this.lastKeyword = null;
    this.$container = null;
    this.$suggestionPlaceholder = null;
    this.endpoint = null;
    this.isMobileSearch = false;
    this.spinnerStarted = false;
}

SearchWithSuggestions.prototype.init = function () {
    var self = this;
    var options = this.options;

    this.$container = this.$el.parents(this.options.selContainer).first();
    this.$suggestionPlaceholder = this.$container.find(this.options.selSuggestionPlaceholder);
    this.endpoint = this.options.suggestionsUrl || this.$suggestionPlaceholder.data('url');
    this.isMobileSearch = !!this.$el.closest(this.options.selMobileContainer).length;

    if (options.showSuggestions) {
        var canBlur = true;
        /**
         * Use debounce to avoid making an Ajax call on every single key press by waiting a few
         * hundred milliseconds before making the request. Without debounce, the user sees the
         * browser blink with every key press.
         */
        var debounceSuggestions = debounce(this.getSuggestions, this.options.inputDebounceTime);
        this.$el.on('input', function () {
            debounceSuggestions.call(self);
            $(self.options.selInputSync).not(this).val(this.value);
        });
        this.$el.on('keydown', function (e) {
            self.navigation(e);
        });
        this.$el.on('focus', function () {
            self.$container.addClass('search-focus');
            self.getSuggestions();
        });
        this.$el.on('blur', function () {
            if (canBlur) {
                self.clearSuggestions();
                self.hideCloseButton();
                self.$container.removeClass('search-focus');
            }
            canBlur = true;
        });
        this.$suggestionPlaceholder.on('click', function (e) {
            if ($(e.target).parent().is(self.$suggestionPlaceholder)) {
                self.clearSuggestions();
            }
        });

        $(window).resize(function () {
            self.positionSuggestions();
        });


        // Hide suggestions on outside click
        if (this.isMobileSearch === false ||
            this.options.applyModalStylingOnMobile === false) {
            $doc.on('mousedown', function (e) {
                var $target = $(e.target);
                canBlur = $target.is(self.$el) ||
                    (!self.$container.has($target).length && !$target.is(self.$container));
            });
        }
    }

    if (options.showCloseButton) {
        // Add close button's functionality
        this.$container.find(options.selCloseButton).on('click', function () {
            self.clearSearch();
            self.$el.focus();
        });
    } else {
        // Hide close button
        this.$container.find(options.selCloseButton).hide();
    }

    // Hide search button
    if (!options.showSearchButton) {
        this.$container.find(options.selSearchButton).hide();
    }

    // Show spinner on search query submit
    this.$container.find('> form').on('submit', function () {
        $.spinner().start();
    });
};

SearchWithSuggestions.prototype.destroy = function () {
    this.clearSearch();
    this.$el.off('input');
    this.$el.off('click');
    this.$container.find(this.options.selCloseButton).off('click');
};

/**
 * Get suggestions container element
 * @return {JQuery} - Returns suggestions element
 */
SearchWithSuggestions.prototype.getSuggestionsContianer = function () {
    return this.$suggestionPlaceholder
        .find(this.options.selSuggestionsContainer);
};

/**
 * Shows close button and hides search button
 */
SearchWithSuggestions.prototype.showCloseButton = function () {
    if (this.options.showCloseButton) {
        this.$container.addClass('search-active');
        this.$container.find(this.options.selSearchButton).addClass('d-none');
        this.$container.find(this.options.selCloseButton).removeClass('d-none');
    }
};

/**
 * Hides close button and shows search button
 */
SearchWithSuggestions.prototype.hideCloseButton = function () {
    this.$container.removeClass('search-active');
    this.$container.find(this.options.selCloseButton).addClass('d-none');
    if (this.options.showSearchButton) {
        this.$container.find(this.options.selSearchButton).removeClass('d-none');
    }
};

/**
 * Navigates suggestions with keydown and keyup
 * @param {Object} e - Event object
 */
SearchWithSuggestions.prototype.navigation = function (e) {
    // Stop here if suggestions are not available
    if (this.$container.hasClass('search-found') === false) return;

    if (e.keyCode === 38 || e.keyCode === 40) e.preventDefault();

    if (e.keyCode === 38 || e.keyCode === 40 || e.keyCode === 13) {
        var $suggestions = this.getSuggestionsContianer();
        var $suggestionItems = $suggestions.find(this.options.selSuggestionsItem);
        var currentItemIndex = null;
        var nextItemIndex = null;
        var lastItemIndex = $suggestionItems.length - 1;
        $suggestionItems.each(function (index) {
            var $suggestionItem = $(this);
            if ($suggestionItem.hasClass('selected')) {
                $suggestionItem.removeClass('selected');
                currentItemIndex = index;
            }
        });

        if (currentItemIndex === null) {
            if (e.keyCode === 38) {
                // Up Arrow
                nextItemIndex = lastItemIndex;
            } else if (e.keyCode === 40) {
                // Down Arrow
                nextItemIndex = 0;
            }
        } else if (e.keyCode === 38) {
            // Up Arrow
            nextItemIndex = currentItemIndex - 1;
        } else if (e.keyCode === 40) {
            // Down Arrow
            nextItemIndex = currentItemIndex + 1;
        } else if (e.keyCode === 13) {
            // Enter
            e.preventDefault();
            $.spinner().start();
            window.location.href = $suggestionItems[currentItemIndex].href;
        }

        if (nextItemIndex >= 0 && nextItemIndex <= lastItemIndex) {
            $($suggestionItems[nextItemIndex]).addClass('selected');
        }
    }
};


/**
 * Retrieve suggestions
 */
SearchWithSuggestions.prototype.getSuggestions = function () {
    var keyword = this.$el.val();

    this.$el.trigger(pluginPrefix + ':keywordChange', keyword);

    // Toggle close button
    if (keyword.length) {
        this.showCloseButton();
    } else {
        this.hideCloseButton();
    }

    // Check if keyword has minimum characters
    // for a meaningful search result
    if (keyword.length < this.options.inputMinChars) {
        this.clearSuggestions();
        return;
    }

    // Show previous suggestions if the last
    // keyword is equal to current one
    if (this.lastKeyword === keyword) {
        var $suggestions = this.getSuggestionsContianer();
        if ($suggestions.length) {
            $suggestions.show();
            this.$container.addClass('search-found');
            this.positionSuggestions();
            return;
        }
    }

    // Keep last searched keyword
    this.lastKeyword = keyword;

    // Start ajax requst
    if (this.spinnerStarted === false) {
        $.spinner().start();
        this.spinnerStarted = true;
    }

    $.ajax({
        context: this,
        url: this.endpoint + encodeURIComponent(keyword),
        method: 'GET',
        success: this.processResponse,
        error: function () {
            $.spinner().stop();
            this.spinnerStarted = false;
        }
    });
};

/**
 * Process Ajax response for SearchServices-GetSuggestions
 * @param {Object|string} response - Empty object literal if null response or string with rendered
 *                                   suggestions template contents
 */
SearchWithSuggestions.prototype.processResponse = function (response) {
    // Remove previous suggestions
    this.$suggestionPlaceholder.empty().hide();

    // Stop spinner
    $.spinner().stop();
    this.spinnerStarted = false;

    // If response is an object instead of a string, don't go further
    if (typeof response === 'object') {
        return;
    }

    var $response = $(response);
    if ($response.filter('.suggestions').children().length === 0) {
        this.$container.removeClass('search-found');
        return;
    }

    // Append suggestions to placeholder
    this.$suggestionPlaceholder.append($response).show();

    // If search input is in mobile viewport,
    // show suggestions as if a modal is opened
    this.applyModalStyleOnMobile();

    this.$container.addClass('search-found');

    this.$el.trigger(pluginPrefix + ':showSuggestions', response);
};

/**
 * Apply modal classes needed for mobile suggestions
 * @param {Object} scope - Search input field DOM element
 */
SearchWithSuggestions.prototype.applyModalStyleOnMobile = function () {
    this.positionSuggestions();
    if (this.isMobileSearch && this.options.applyModalStylingOnMobile) {
        $('body').addClass('modal-open');
        this.getSuggestionsContianer().addClass('modal');
    }
};

/**
 * Positions Suggestions panel on page
 * @param {Object} scope - DOM element, usually the input.search-field element
 */
SearchWithSuggestions.prototype.positionSuggestions = function () {
    var $suggestions = this.getSuggestionsContianer();
    if (this.isMobileSearch) {
        var top = (this.$el.offset().top + this.$el.outerHeight()) - $(window).scrollTop();
        var height = 'calc(100% - ' + top + 'px)';
        $suggestions.css('top', top);
        $suggestions.css(
            this.options.applyModalStylingOnMobile ? 'height' : 'max-height', height);
    } else if ($suggestions && $suggestions.length) {
        $suggestions[0].style.top = null;
        $suggestions[0].style.height = null;
        $suggestions[0].style.maxHeight = null;
    }
};

/**
 * Clear suggestions and  search input field
 */
SearchWithSuggestions.prototype.clearSearch = function () {
    this.lastKeyword = null;
    this.$el.val('');
    this.hideCloseButton();
    this.clearModalStyle();
    this.getSuggestionsContianer().remove();
    this.$container.removeClass('search-found');
    this.$el.trigger(pluginPrefix + ':clearSearch');
};

/**
 * Clear suggestions
 */
SearchWithSuggestions.prototype.clearSuggestions = function () {
    this.clearModalStyle();
    this.getSuggestionsContianer().hide();
    this.$container.removeClass('search-found');
    this.$el.trigger(pluginPrefix + ':clearSuggestions');
};

/**
 * Remove modal classes needed for mobile suggestions
 */
SearchWithSuggestions.prototype.clearModalStyle = function () {
    $('body').removeClass('modal-open');
    this.getSuggestionsContianer().removeClass('modal');
};

module.exports = function () {
    pluginator({
        prefix: pluginPrefix,
        name: 'searchWithSuggestions',
        Constructor: SearchWithSuggestions,
        exports: [
            'clearSearch',
            'clearSuggestions'
        ],
        defaultOptions: {
            suggestionsUrl: null,
            selInputSync: '.js-sws-input',
            selContainer: '.js-sws-container',
            selMobileContainer: '.js-sws-mobile-container',
            selSearchButton: '.js-sws-search-btn',
            selCloseButton: '.js-sws-clear-search-btn',
            selSuggestionsItem: '.js-sws-suggestions-item',
            selSuggestionsContainer: '.js-sws-suggestions',
            selSuggestionPlaceholder: '.js-sws-suggestions-placeholder',
            inputDebounceTime: 300,
            inputMinChars: 3,
            showSuggestions: true,
            showSearchButton: true,
            showCloseButton: false,
            applyModalStylingOnMobile: true
        }
    });

    // Initialize plugin automatically
    $doc.ready(function () {
        $('.js-sws-input').searchWithSuggestions();
    });
};
