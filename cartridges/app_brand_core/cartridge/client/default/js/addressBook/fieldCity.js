'use strict';

module.exports = {
    init: function () {
        var $cities = $('select.js-selectbox-city');
        var $states = $('select.js-selectbox-state');

        $cities.each(function () {
            var $city = $(this);
            var $form = $(this.form);
            var $state = $form.find('select.js-selectbox-state');
            var hasSelectpicker = !!$city.selectpicker;

            if ($state.val() || $city.val()) {
                $city.prop('disabled', false);
                $form.find('.js-input-city').val($city.val());
                if (hasSelectpicker) {
                    $city.selectpicker('refresh');
                }
            }
        });

        if ($states.length) {
            $states.on('change', function () {
                var $state = $(this);
                var $form = $(this.form);
                var $city = $form.find('select.js-selectbox-city');
                var state = $state.val();
                var hasSelectpicker = !!$city.selectpicker;

                $city.prop('disabled', !state);

                var cityVal = $city.val();

                // update city dropdown
                var stateId = $state.find('option:selected').attr('id');
                $city.find('option:not(.bs-title-option)')
                    .prop('hidden', true)
                    .filter('option[id^="' + stateId + '"]')
                    .prop('hidden', false);

                if (cityVal && $city.find('option[value="' + cityVal + '"]').prop('hidden') === false) {
                    $city.val(cityVal);
                    $form.find('.js-input-city').val(cityVal);
                } else {
                    $city.val('');
                    $form.find('.js-input-city').val('');
                }

                if (hasSelectpicker) $city.selectpicker('refresh');
            });
        }

        $cities.on('change', function () {
            var $city = $(this);
            var $form = $(this.form);
            $form.find('.js-input-city').val($city.val());
        });

        $('.js-input-city').on('input', function () {
            var $form = $(this.form);
            var $cityInput = $(this);
            var $citySelect = $form.find('select.js-selectbox-city');
            var hasSelectpicker = !!$citySelect.selectpicker;

            $citySelect.val($cityInput.val());
            if (hasSelectpicker) $citySelect.selectpicker('refresh');
        });
    },

    clear: function ($form) {
        if (!$form) {
            return;
        }

        var $cities = $form.find('select.js-selectbox-city');
        var $states = $form.find('select.js-selectbox-state');

        // Clear state selection
        if ($states.length) {
            $states.each(function () {
                var $state = $(this);
                var hasSelectpicker = !!$state.selectpicker;
                $state.val('');
                if (hasSelectpicker) $state.selectpicker('refresh');
            });
        }

        // Clear city selection
        $cities.each(function () {
            var $city = $(this);
            var $state = $form.find('select.js-selectbox-state');
            var hasSelectpicker = !!$city.selectpicker;

            $('.js-input-city').val('');
            if ($city.length) {
                $city.val('');
                $city.prop('disabled', !!$state.length);
                if (hasSelectpicker) $city.selectpicker('refresh');
            }
        });
    }
};
