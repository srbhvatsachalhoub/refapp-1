'use strict';

var formHelpers = require('./formErrors');

var steps = [
    'js-smsverification-phone',
    'js-smsverification-code',
    'js-smsverification-success'
];

var formSelector = '.js-smsverification-form';
var $submitPayment = $('.js-submit-payment-btn');
var $container = $('.js-smsverification-container');
var $successMsg = $('.js-smsverification-success');
var $verificationModal = $('#smsVerificationModal');
var paymentMethod = $('[name$="_billing_paymentMethod"]').length ? $('[name$="_billing_paymentMethod"]').val() : 'CREDIT_CARD';

/**
 * Current submitted form
 * @param {jQuery} $form Submitted form
 */
function getNextStage($form) {
    var closestContainer = $form.closest('.' + steps.join(',.'));
    var currentStep = -1;
    for (var i = 0; i < steps.length; i++) {
        if (closestContainer.hasClass(steps[i])) {
            currentStep = i;
            break;
        }
    }
    if (currentStep !== -1 && currentStep + 1 < steps.length) {
        closestContainer.addClass('d-none');
        var nextContainer = $('.' + steps[currentStep + 1]);
        if (nextContainer.length) {
            if (nextContainer.hasClass('js-smsverification-success')) {
                $container.attr('data-is-verified', true);
                $submitPayment.prop('disabled', false);
                $verificationModal.modal('hide');
            }
            nextContainer.removeClass('d-none');
        }
    }
}

/**
 * Enables/disables submit payment method button by checking
 * the verification status of phone number
 */
function toggleSubmitPaymentButton() {
    if (paymentMethod === 'COD') {
        if (!$container.attr('data-is-verified')) {
            $submitPayment.prop('disabled', true);
        } else {
            $submitPayment.prop('disabled', false);
        }
    }
}

/**
 * Performs an ajax request for sending verification code and
 * @param {jQuery} $form - jQuery form element
 * @param {Function} successCallback - Callback function that will be called on success event
 */
function submit($form, successCallback) {
    $('.js-smsverification-msg').addClass('d-none');
    formHelpers.clearPreviousErrors(formSelector);
    $.spinner().start();

    $.ajax({
        method: 'POST',
        url: $form.attr('data-action'),
        data: $form.find(':input').serialize(),
        success: function (data) {
            if (data && data.success) {
                if (successCallback) {
                    successCallback($form);
                }
            } else {
                var $errorContainer = $('.js-smsverification-error-msg').empty();

                // Show server errors
                if (data.serverErrors && data.serverErrors.length) {
                    $errorContainer.show();
                    $errorContainer.removeClass('d-none');
                    $errorContainer.append(data.serverErrors.join('<br /><br />'));
                }

                // Show field errors
                if (data.fieldErrors && data.fieldErrors.length) {
                    data.fieldErrors.forEach(function (error) {
                        if (Object.keys(error).length) {
                            formHelpers.loadFormErrors(formSelector, error);
                        }
                    });
                }
            }
        },
        // error: function (err) {
        //     console.log(err);
        // },
        complete: function () {
            $.spinner().stop();
        }
    });
}

/**
 * Restarts sms verification flow
 */
function restart() {
    $container.removeClass('d-none').removeAttr('data-is-verified');
    $('.' + steps.join(',.')).addClass('d-none');
    $('.' + steps[0]).removeClass('d-none');
}

/**
 * Initialize events of verification process.
 */
function initialize() {
    toggleSubmitPaymentButton();

    $('.js-smsverification-submit-btn').on('click', function () {
        submit($(this).closest(formSelector), getNextStage);
    });

    $('.js-smsverification-resubmit-btn').click(function () {
        var $verifyPhoneForm = $('.js-smsverification-phone').find(formSelector);
        var $confirmCodeForm = $('.js-smsverification-code').find(formSelector);
        var $resubmitSuccessMsg = $confirmCodeForm.find('.js-smsverification-resubmit-success-msg');
        submit($verifyPhoneForm, function () {
            $resubmitSuccessMsg.removeClass('d-none');
        });
    });

    $('.js-smsverification-restart-btn').click(restart);

    $(document).on('psp:changed', function (event, methodID) {
        paymentMethod = methodID;
        if (paymentMethod === 'COD') {
            if (!$container.attr('data-is-verified')) {
                $submitPayment.prop('disabled', true);
                if (!($verificationModal.data('bs.modal') || {})._isShown) { // eslint-disable-line
                    $verificationModal.modal('show');
                }
            }
        } else {
            $submitPayment.prop('disabled', false);
        }
    });


    $(document).on('checkout:updateCheckoutView', function (event, data) {
        if (!data || !data.order) {
            return;
        }
        var order = data.order;

        if (data.order.smsIsVerificationRequired) {
            if ('smsVerificationStatus' in order && order.smsVerificationStatus) {
                $container.addClass('d-none').attr('data-is-verified', true);
                $successMsg.removeClass('d-none');
            } else {
                restart();
            }
            toggleSubmitPaymentButton();
        }

        if (order.totals.serviceFee && order.totals.serviceFee.value > 0) {
            $('.js-payment-service-fee').removeClass('d-none');
            $('.js-payment-service-fee-cost').text(order.totals.serviceFee.formatted);
        } else {
            $('.js-payment-service-fee').addClass('d-none');
            $('.js-payment-service-fee-cost').text('');
        }
    });
}

module.exports = {
    init: initialize
};
