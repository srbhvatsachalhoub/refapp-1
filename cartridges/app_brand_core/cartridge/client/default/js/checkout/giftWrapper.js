'use strict';

var addressHelpers = require('./address');
/**
 * Initializes this component
 */
function init() {
    var tickCrossIcons = {
        false: '<i class="fas fa fa-times"></i>',
        true: '<i class="fas fa fa-check"></i>'
    };

    // eslint-disable-next-line no-unused-vars
    $('body').on('shipping:updateShippingAddressFormValues', function (event, data) {
        $('input[value=' + data.shipping.UUID + ']').each(function (formIndex, el) {
            var form = el.form;
            if (!form) return;

            $('input[name$=_isGift]', form).prop('checked', data.shipping.isGift);
            $('input[name$=_isPriceHidden]', form).prop('checked', data.shipping.isGift && data.shipping.isPriceHidden);
            $('input[name$=_isGiftWrapped]', form).prop('checked', data.shipping.isGift && data.shipping.isGiftWrapped);
            $('textarea[name$=_giftMessage]', form).val(data.shipping.isGift && data.shipping.giftMessage ? data.shipping.giftMessage : '');
        });
    });

    // eslint-disable-next-line no-unused-vars
    $('body').on('shipping:updateShippingSummaryInformation', function (event, data) {
        $('[data-shipment-summary=' + data.shipping.UUID + ']').each(function (i, el) {
            var $container = $(el);
            var giftMessageSummary = $container.find('.gift-summary');

            var isGift = data.shipping.isGift;

            if (isGift) {
                // sets gift summary messages
                giftMessageSummary.find('.js-price-hidden-summary').html(tickCrossIcons[data.shipping.isPriceHidden]);
                giftMessageSummary.find('.js-gift-wrapped-summary').html(tickCrossIcons[data.shipping.isGiftWrapped]);
                giftMessageSummary.find('.gift-message-summary').html(data.shipping.giftMessage);
                giftMessageSummary.removeClass('d-none');
                if (data.shipping.giftMessage) {
                    giftMessageSummary.find('.gift-message-summary').removeClass('d-none');
                    giftMessageSummary.find('.js-gift-message-label').removeClass('d-none');
                } else {
                    giftMessageSummary.find('.gift-message-summary').addClass('d-none');
                    giftMessageSummary.find('.js-gift-message-label').addClass('d-none');
                }
            } else {
                giftMessageSummary.addClass('d-none');
            }
        });
    });

    // eslint-disable-next-line no-unused-vars
    $('body').on('shipping:updatePLIShippingSummaryInformation', function (event, data) {
        var $pli = $('input[value=' + data.productLineItem.UUID + ']');
        var form = $pli && $pli.length > 0 ? $pli[0].form : null;

        if (!form) return;

        var $viewBlock = $('.view-address-block', form);

        if (data.shipping.isGift) {
            $('.gift-message-summary', $viewBlock).text(data.shipping.giftMessage);
            $('.gift-message-' + data.shipping.UUID).val(data.shipping.giftMessage);

            $('.js-price-hidden-summary', $viewBlock).text(tickCrossIcons[data.shipping.isPriceHidden]);
            $('.js-gift-wrapped-summary', $viewBlock).text(tickCrossIcons[data.shipping.isGiftWrapped]);

            if (data.shipping.giftMessage) {
                $('.gift-message-summary', $viewBlock).removeClass('d-none');
                $('.js-gift-message-label', $viewBlock).find('.js-gift-message-label').removeClass('d-none');
            } else {
                $('.gift-message-summary', $viewBlock).addClass('d-none');
                $('.js-gift-message-label', $viewBlock).find('.js-gift-message-label').addClass('d-none');
            }
        } else {
            $('.gift-summary', $viewBlock).addClass('d-none');
        }
    });

    $('body').on('shipping:clearShippingForms', function (event, data) {
        data.order.shipping.forEach(function (shipping) {
            $('input[value=' + shipping.UUID + ']').each(function (formIndex, el) {
                var form = el.form;
                if (!form) return;

                $('input[name$=_isGift]', form).prop('checked', false);
                $('input[name$=_isPriceHidden]', form).prop('checked', false);
                $('input[name$=_isGiftWrapped]', form).prop('checked', false);
                $('textarea[name$=_giftMessage]', form).val('');
                $('.gift-attributes', form).addClass('d-none');
            });
        });
    });

    $('.gift').on('change', function (e) {
        e.preventDefault();
        var form = $(this).closest('form');

        if (this.checked) {
            $('.gift-attributes', form).removeClass('d-none');
            $('div.gift-message', form).removeClass('d-none');

            $('.price-hiding input', form).prop('checked', true);
        } else {
            $(form).find('.gift-attributes').addClass('d-none');
        }
    });
}

module.exports = {
    init: init,
    selectShippingMethod: function () {
        $('.shipping-method-list').off('change').on('change', function () {
            var $shippingForm = $(this).parents('form');
            var methodID = $(':checked', this).val();
            var shipmentUUID = $shippingForm.find('[name=shipmentUUID]').val();
            var urlParams = addressHelpers.methods.getAddressFieldsFromUI($shippingForm);
            urlParams.shipmentUUID = shipmentUUID;
            urlParams.methodID = methodID;
            urlParams.isGift = $shippingForm.find('.gift').prop('checked');
            urlParams.isGiftWrapped = $shippingForm.find('.gift-wrapping input').prop('checked');
            urlParams.isPriceHidden = $shippingForm.find('.price-hiding input').prop('checked');
            urlParams.giftMessage = $shippingForm.find('textarea[name$=_giftMessage]').val();

            var url = $(this).data('select-shipping-method-url');
            var shippingModule = require('./shipping');
            shippingModule.methods.selectShippingMethodAjax(url, urlParams, $(this));
        });
    }
};
