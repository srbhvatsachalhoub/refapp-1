'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./contactus/contactus'));
    processInclude(require('./helpers/countCharacters'));
});
