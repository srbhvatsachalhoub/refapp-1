/**
 * Attaches add/remove/toggle class events
 * @param {string} className Class name that will be added/removed/toggled
 * @param {string} methodName Method name (add, remove or toggle)
 */
function attachClassEvent(className, methodName) {
    var $doc = $(document);
    var attribute = methodName + '-class-' + className;
    var selector = '[data-' + attribute + ']:not([data-' + attribute + '=""])';

    $doc.on('click', selector, function (e) {
        e.preventDefault();

        var $this = $(this);
        var target = $this.data(attribute);
        var $target;

        if (target === 'self') {
            $target = $this;
        } else if (target === 'parent') {
            $target = $this.parent();
        } else {
            $target = $(target);
        }

        $target[methodName + 'Class'](className);
        $target.trigger('state:' + methodName + ':' + className);
    });
}

module.exports = function () {
    var classList = ['active', 'reveal', 'open', 'hidden', 'd-none'];
    for (var i = 0; i < classList.length; i++) {
        var className = classList[i];
        attachClassEvent(className, 'add');
        attachClassEvent(className, 'remove');
        attachClassEvent(className, 'toggle');
    }
};
