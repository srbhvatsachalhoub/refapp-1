'use strict';

var checks = [
    'min-length',
    'min-special-chars',
    'is-force-mixed-case',
    'is-force-letters',
    'is-force-numbers'
];

module.exports = function () {
    $('.js-validate-password').on('input', function () {
        var $this = $(this);
        var password = $this.val();

        for (var i = 0; i < checks.length; i++) {
            var check = checks[i];
            var rule = $this.data('constraint-' + check);
            var $rule = $('.js-validate-password-' + check);
            var passed = false;

            if (i === 0) {
                passed = password.length >= rule;
            } else if (i === 1) {
                var specialCharacterCount = password.length - password.replace(/\W/, '').length;
                passed = rule > 0 ? specialCharacterCount >= rule : true;
            } else if (i === 2) {
                passed = rule ? (/[a-z]/.test(password) && /[A-Z]/.test(password)) : true;
            } else if (i === 3) {
                passed = rule ? /[a-zA-Z]/.test(password) : true;
            } else if (i === 4) {
                passed = rule ? /\d/.test(password) : true;
            }

            if (password.length) {
                $rule.removeClass('d-none text-danger text-success');
                $rule.addClass(passed ? 'text-success' : 'text-danger');
                $rule.find('.js-validate-password-success')[passed ? 'removeClass' : 'addClass']('d-none');
                $rule.find('.js-validate-password-error')[passed ? 'addClass' : 'removeClass']('d-none');
            } else {
                $rule.addClass('d-none');
            }
        }
    });
};
