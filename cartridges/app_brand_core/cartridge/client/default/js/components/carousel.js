'use strict';

/**
 * Set responsive option by using breakpoints
 * @param {string} key - Option name
 * @param {Object} options - Options object
 * @param {Array|string} attribute - Options coming from data attributes
 */
function setResponsiveOption(key, options, attribute) {
    /* eslint-disable no-param-reassign */

    // Stop here if attribute parameter is not supplied
    if (!attribute) return;

    // Convert attribute to an array
    attribute = Array.isArray(attribute) ? attribute : [attribute, attribute, attribute, attribute, attribute];

    attribute = attribute.map(function (item) {
        var itemCount = parseInt(item, 10);
        var isItemCountNumber = isNaN(itemCount) === false && itemCount.toString() === item.toString();
        item = isItemCountNumber ? itemCount : item;
        return (isItemCountNumber && itemCount === 0) ? 'unslick' : item;
    });

    var lastAttribute;
    for (var i = 0; i < options.responsive.length; i++) {
        options.responsive[i].settings = options.responsive[i].settings || {};
        lastAttribute = attribute[i] || lastAttribute;
        if (lastAttribute === 'unslick') {
            options.responsive[i].settings = lastAttribute;
        } else if (options.responsive[i].settings !== 'unslick') {
            options.responsive[i].settings = options.responsive[i].settings || {};
            options.responsive[i].settings[key] = lastAttribute;
        }
    }
}

/**
 * Initializes slick carousels
 * @param {jQuery} $el - Element that holds carousel items
 * @param {Object} options - Options for slick carousel
 */
function init($el, options) {
    // Stop here, if slick plugin is available
    if (!$el.slick) return;

    // Stop here if breakpoints object is not availabe
    if (!window.RA_BREAKPOINTS) return;

    options.mobileFirst = true;
    options.responsive = options.responsive || [];
    options.centerMode = 'centerMode' in options ? options.centerMode : false;
    options.centerPadding = 'centerPadding' in options ? options.centerPadding : '50px';

    // Prepare responsive breakpoints
    Object.keys(window.RA_BREAKPOINTS).forEach(function (key) {
        options.responsive.push({
            breakpoint: Math.max(window.RA_BREAKPOINTS[key] - 1, 0)
        });
    });

    $el.each(function () {
        var $this = $(this);
        var slidesToShow = $this.data('carousel-slides-to-show');
        var slidesToScroll = $this.data('carousel-slides-to-scroll');
        var carouselCenterMode = $this.data('carousel-center-mode') || options.centerMode;
        var carouselCenterPadding = $this.data('carousel-center-padding') || options.centerPadding;

        var slickOptions = $.extend({}, options);

        setResponsiveOption('slidesToShow', slickOptions, slidesToShow);
        setResponsiveOption('slidesToScroll', slickOptions, slidesToScroll || slidesToShow);
        setResponsiveOption('centerMode', slickOptions, carouselCenterMode);
        setResponsiveOption('centerPadding', slickOptions, carouselCenterPadding);

        $this.off('destroy').on('destroy', function () {
            $this.addClass('slick-destroyed');
        });

        // Init slick carousel
        $this.slick(slickOptions);
    });
}

module.exports = init;
