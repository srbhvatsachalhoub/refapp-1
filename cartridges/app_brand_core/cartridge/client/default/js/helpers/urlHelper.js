'use strict';

/**
 * Searches the parameter in url and returns its value
 *
 * @param {string} key - Parameter that will be search in querystring
 * @param {string} url - Url that includes querystring
 * @return {string} value
 */
function getParameter(key, url) {
    key = key.replace(/[\[\]]/g, '\\$&'); // eslint-disable-line
    var regex = new RegExp('[?&]' + key + '(=([^&#]*)|&|#|$)');
    var results = regex.exec(url || window.location.href);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

/**
 * Sets the parameter in the url to the supplied value and returns new url
 *
 * @param {string} key - Parameter that will be search in querystring
 * @param {string} value - Value to be set
 * @param {string} url - Url that includes query string
 * @return {string} new url that includes new value
 */
function setParameter(key, value, url) {
    if (!url) url = window.location.href; // eslint-disable-line
    var re = new RegExp('([?&])' + key + '=.*?(&|$)', 'i');
    var separator = url.indexOf('?') !== -1 ? '&' : '?';
    if (url.match(re)) {
        return url.replace(re, '$1' + key + '=' + value + '$2');
    }
    if (value) {
        return url + separator + key + '=' + value;
    }
    return url;
}

/**
 * Removes the parameter from the url and returns new url
 *
 * @param {string} key - Parameter that will be search in querystring
 * @param {string} url - Url that includes query string
 * @return {string} new url
 */
function removeParameter(key, url) {
    if (!url) url = window.location.href; // eslint-disable-line

    // prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {
        var prefix = encodeURIComponent(key) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        // reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            // idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        return urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
    }
    return url;
}

/**
 * Update view more url
 * @param {string} url - current url
 * @param {number} productsCount - products Count
 * @return {string} url - url
 */
function updateViewMoreUrl(url, productsCount) {
    var szUrl = url.split('&').filter(function (element) {
        if (element.indexOf('sz=') >= 0) {
            return element;
        }
        return null;
    });
    var resultUrl = url.slice(0, url.indexOf(szUrl) - 1);
    resultUrl = setParameter('sz', productsCount, resultUrl);
    return resultUrl;
}

module.exports = {
    getParameter: getParameter,
    setParameter: setParameter,
    removeParameter: removeParameter,
    updateViewMoreUrl: updateViewMoreUrl
};
