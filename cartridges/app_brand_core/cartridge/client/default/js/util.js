var currentScrollTop = 0;

module.exports = {
    /**
     * @description Show and remove class with delay
     * @param {Object} $el - $el
     * @param {string} className - className
     * @param {number} duration - duration
     */
    addAndRemoveClass: function ($el, className, duration) {
        var timeout = duration || 5000;

        $el.addClass(className);

        setTimeout(function () {
            $el.removeClass(className);
        }, timeout);
    },

    saveScrollTop: function () {
        currentScrollTop = $(window).scrollTop();
    },

    restoreScrollTop: function () {
        $('html, body').scrollTop(currentScrollTop);
    },

    disableScroll: function () {
        this.saveScrollTop();
        $('html').addClass('disable-scroll');
        $('body').scrollTop(currentScrollTop);
        window.disableScroll = true;
    },

    enableScroll: function () {
        window.disableScroll = false;
        $('html').removeClass('disable-scroll');
        this.restoreScrollTop();
    },

    /**
     * @description Scroll page
     * @param {number} xLocation - position to scroll page to
     * @param {Object} $element - $element to scroll page to
     * @param {number} duration - scroll duration
     */
    scrollBrowser: function (xLocation, $element, duration) {
        var scrollTo = typeof xLocation === 'number' ? xLocation : 0;
        var scrollDuration = typeof duration === 'number' ? duration : 500;
        var stickyHeader = $('.js-sticky-header');

        if ($element && $element.length > 0) {
            scrollTo = $element.offset().top || 0;
        }

        if (stickyHeader.length > 0) {
            scrollTo -= stickyHeader.outerHeight();
        }

        $('html, body').animate({
            scrollTop: scrollTo
        }, scrollDuration);
    }
};
