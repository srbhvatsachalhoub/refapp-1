'use strict';

var server = require('server');
server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');

/**
 * Gets currency code of supplied locale
 * @param {Object} localLinks Object keeping locale information
 * @param {string} localID Locale
 * @returns {string} Currency code
 */
function getCurrencyCode(localLinks, localID) {
    var link = localLinks.filter(function (localLink) {
        return localLink.localID === localID;
    });
    link = (link && link.length > 0) ? link[0] : null;
    return (
        link || {}
    ).currencyCode;
}

/**
 * Adds distinct languages and countries to viewData.
 * With these array of objects having unique items, we can
 * render country and language selector separately.
 * "distinct" parameter in querystring should be "true",
 */
server.append('Locale', function (req, res, next) {
    // If distinct parameter is false or undefined,
    // we don't need to find distinct countries and languages
    if (req.querystring.distinct !== 'true') {
        return next();
    }

    var localeModel = (res.getViewData() || {}).localeModel;
    var isLocalLinksAvailable = localeModel &&
        localeModel.locale &&
        localeModel.locale.localLinks &&
        localeModel.locale.localLinks.length;

    // If localLinks isn't avalible on localeModel, don't go further
    if (!isLocalLinksAvailable) {
        return next();
    }

    var localLinks = localeModel.locale.localLinks;
    var languages = [];
    var countries = [];
    var languageCheck = {};
    var countryCheck = {};
    var i;

    // Loop through localLinks to find available countries and languages
    for (i = 0; i < localLinks.length; i++) {
        var localLink = localLinks[i];
        var languageCode = localLink.language;
        var countryCode = localLink.country;
        var localID;

        // Add language if it is not already added
        if (languageCode !== localeModel.locale.language && !languageCheck[languageCode]) {
            localID = languageCode + '_' + localeModel.locale.countryCode;
            languages.push({
                localID: localID,
                code: languageCode,
                displayName: localLink.displayLanguage,
                currencyCode: getCurrencyCode(localLinks, localID)
            });
            languageCheck[languageCode] = 1;
        }

        // Add country if it is not already added
        if (countryCode !== localeModel.locale.countryCode && !countryCheck[countryCode]) {
            localID = localeModel.locale.language + '_' + countryCode;
            countries.push({
                localID: localID,
                code: countryCode,
                displayName: localLink.displayCountry,
                currencyCode: getCurrencyCode(localLinks, localID)
            });
            countryCheck[countryCode] = 1;
        }
    }

    // Add languages to localeModel
    localeModel.locale.languages = languages;

    // Add countries to localeModel
    localeModel.locale.countries = countries;

    res.setViewData({ localeModel: localeModel });

    return next();
});

/**
 * Replcase SetLocate Action
 * To set the locale id value to cookie site_locale value
 * And to redirect with host change if the countries locale has host value
 */
server.replace('SetLocale', server.middleware.get, function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');
    var Currency = require('dw/util/Currency');
    var Site = require('dw/system/Site');
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var cookieHelpers = require('*/cartridge/scripts/helpers/cookieHelpers');
    var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');
    var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

    var QueryString = server.querystring;
    var currentSite = Site.getCurrent();
    var allowedCurrencies = currentSite.allowedCurrencies;
    var queryStringObj = new QueryString(req.querystring.queryString || '');

    if (Object.hasOwnProperty.call(queryStringObj, 'lang')) {
        delete queryStringObj.lang;
    }

    var localeId = req.querystring.code;
    if (req.setLocale(localeId)) {
        var currentBasket = BasketMgr.getCurrentBasket();
        var currency = Currency.getCurrency(req.querystring.CurrencyCode);
        var locale = localeHelpers.getLocale(localeId);

        // Update currency
        if (allowedCurrencies.indexOf(req.querystring.CurrencyCode) > -1
            && (req.querystring.CurrencyCode !== req.session.currency.currencyCode)) {
            req.session.setCurrency(currency);

            localeHelpers.setApplicablePriceBooks();

            if (currentBasket && currency && currentBasket.currencyCode !== currency.currencyCode) {
                Transaction.wrap(function () {
                    currentBasket.updateCurrency();
                });
            }
        }

        // Remove country specific data from shipping address
        COHelpers.removeShippingAddressFromBasket(locale.countryCode);

        inventoryHelpers.checkBasketProductInventoryList();

        var URLAction = require('dw/web/URLAction');
        var URLParameter = require('dw/web/URLParameter');

        var urlAction = new URLAction(req.querystring.action, currentSite.ID, localeId);
        var urlParameters = Object.keys(queryStringObj).map(function (parameterName) {
            return new URLParameter(parameterName, queryStringObj[parameterName]);
        });

        var url = URLUtils.url(urlAction, urlParameters);
        if (locale && locale.host) {
            url = URLUtils.sessionRedirect(locale.host, url);
            cookieHelpers.setSiteLocaleCookie(localeId, locale.host);
        } else {
            cookieHelpers.setSiteLocaleCookie(localeId);
        }

        res.json({
            success: true,
            redirectUrl: url.toString()
        });
    } else {
        res.json({ error: true });
    }
    next();
});

/**
 * Append breadcrumbs to Page-Show
 */
server.append('Show', function (req, res, next) {
    var Resource = require('dw/web/Resource');
    var URLUtils = require('dw/web/URLUtils');
    var viewData = res.getViewData();
    var breadcrumbs = [];
    if (viewData && viewData.content && viewData.content.raw) {
        breadcrumbs.push({
            htmlValue: viewData.content.name,
            url: URLUtils.url('Search-ShowContent', 'cid', viewData.content.ID)
        });
        var asset = viewData.content.raw;
        var folder = asset.getClassificationFolder();
        while (folder && folder.ID !== 'root') {
            breadcrumbs.unshift({
                htmlValue: folder.displayName,
                url: URLUtils.url('Page-Show', 'fdid', folder.ID)
            });
            folder = folder.parent;
        }
        breadcrumbs.unshift(
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            }
        );

        res.setViewData({
            breadcrumbs: breadcrumbs
        });
    }
    next();
});

server.replace(
    'IncludeHeaderMenu',
    server.middleware.include,
    server.middleware.get,
    cache.applyDefaultCache,
    function (req, res, next) {
        var catalogMgr = require('dw/catalog/CatalogMgr');
        var Categories = require('*/cartridge/models/categories');
        var siteRootCategory = catalogMgr.getSiteCatalog().getRoot();

        var topLevelCategories = siteRootCategory.getSubCategories();
        var categories = new Categories(topLevelCategories, true);
        categories.rurl = req.querystring.rurl || 1;
        categories.name = req.currentCustomer.profile ? req.currentCustomer.profile.firstName : null;
        res.render('/components/header/menu', categories);
        next();
    }
);

server.get(
    'IncludeSlidingCategoryMenu',
    server.middleware.include,
    server.middleware.get,
    cache.applyDefaultCache,
    function (req, res, next) {
        var catalogMgr = require('dw/catalog/CatalogMgr');
        var Categories = require('*/cartridge/models/categories');
        var siteRootCategory = catalogMgr.getSiteCatalog().getRoot();

        var topLevelCategories = siteRootCategory.getSubCategories();

        res.render('/components/header/slidingMenu', new Categories(topLevelCategories, true));
        next();
    }
);

module.exports = server.exports();
