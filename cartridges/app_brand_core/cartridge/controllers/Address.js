'use strict';
/* global customer */

var server = require('server');
server.extend(module.superModule);

var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var Transaction = require('dw/system/Transaction');
var CustomerMgr = require('dw/customer/CustomerMgr');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
var BreadcrumbsHelpers = require('*/cartridge/scripts/helpers/breadcrumbsHelpers');
var verificationHelpers = require('*/cartridge/scripts/helpers/verificationHelpers');
var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');

server.append('List', function (req, res, next) {
    var viewData = res.getViewData();

    viewData.countryCode = localeHelpers.getCurrentCountryCode();
    viewData.preferredAddress = accountHelpers.getPreferredAddress(req);
    res.setViewData(viewData);
    BreadcrumbsHelpers.addLastBreadcrumb(res, Resource.msg('label.addressbook', 'account', null));
    next();
});

server.append('AddAddress', function (req, res, next) {
    var viewData = res.getViewData();

    viewData.countryCode = localeHelpers.getCurrentCountryCode();
    viewData.preferredAddress = accountHelpers.getPreferredAddress(req);
    res.setViewData(viewData);
    BreadcrumbsHelpers.addLastBreadcrumb(res, Resource.msg('label.addressbook.addnewaddress', 'account', null));
    next();
});

server.append('EditAddress', function (req, res, next) {
    var viewData = res.getViewData();
    var addressId = req.querystring.addressId;
    var customer = CustomerMgr.getCustomerByCustomerNumber(
        req.currentCustomer.profile.customerNo
    );
    var addressBook = customer.getProfile().getAddressBook();
    var rawAddress = addressBook.getAddress(addressId);
    var preferredAddress = accountHelpers.getPreferredAddress(req);

    viewData.countryCode = localeHelpers.getCurrentCountryCode();
    viewData.preferredAddress = preferredAddress;
    viewData.addressForm.title.titleList.selectedOption = rawAddress.title || customer.profile.title;
    viewData.addressForm.setDefault.checked = preferredAddress ? preferredAddress.address.ID === addressId : false;

    res.setViewData(viewData);
    BreadcrumbsHelpers.addLastBreadcrumb(res, Resource.msg('label.addressbook.editaddress', 'account', null));
    next();
});

server.replace('SaveAddress', server.middleware.post, csrfProtection.validateAjaxRequest, function (req, res, next) {
    var formErrors = require('*/cartridge/scripts/formErrors');
    var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');

    var addressForm = server.forms.getForm('address');
    var addressFormObj = addressForm.toObject();
    addressFormObj.addressForm = addressForm;
    var customer = CustomerMgr.getCustomerByCustomerNumber(
        req.currentCustomer.profile.customerNo
    );
    var addressBook = customer.getProfile().getAddressBook();
    if (addressForm.valid) {
        res.setViewData(addressFormObj);
        this.on('route:BeforeComplete', function () { // eslint-disable-line no-shadow
            var formInfo = res.getViewData();
            Transaction.wrap(function () {
                var address = null;
                if (formInfo.addressId.equals(req.querystring.addressId) || !addressBook.getAddress(formInfo.addressId)) {
                    address = req.querystring.addressId
                        ? addressBook.getAddress(req.querystring.addressId)
                        : addressBook.createAddress(formInfo.addressId);
                }

                if (address) {
                    if (req.querystring.addressId) {
                        address.setID(formInfo.addressId);

                        if (address.custom.isSmsVerified &&
                            !verificationHelpers.compareAddresses(formInfo, address)) {
                            address.custom.isSmsVerified = false;
                        }
                    }

                    // Save form's address
                    addressHelpers.updateAddressFields(address, formInfo);

                    if (formInfo.setDefault) {
                        addressBook.setPreferredAddress(address);
                    }

                    // Send account edited email
                    accountHelpers.sendAccountEditedEmail(customer.profile);

                    res.json({
                        success: true,
                        redirectUrl: URLUtils.url('Address-List').toString()
                    });
                } else {
                    formInfo.addressForm.valid = false;
                    formInfo.addressForm.addressId.valid = false;
                    formInfo.addressForm.addressId.error =
                        Resource.msg('error.message.idalreadyexists', 'forms', null);
                    res.json({
                        success: false,
                        fields: formErrors.getFormErrors(addressForm)
                    });
                }
            });
            var viewData = res.getViewData();
            if (viewData.success) {
                require('dw/system/HookMgr').callHook('app.account.updated', 'updated', customer);
            }
        });
    } else {
        res.json({
            success: false,
            fields: formErrors.getFormErrors(addressForm)
        });
    }
    return next();
});

module.exports = server.exports();
