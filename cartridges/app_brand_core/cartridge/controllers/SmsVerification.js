'use strict';
/* global session */
var server = require('server');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var formErrors = require('*/cartridge/scripts/formErrors');
var System = require('dw/system/System');
var verificationHelpers = require('*/cartridge/scripts/helpers/verificationHelpers');
/**
 * Submits Phone and starts Sms Verification process.
 */
server.post('Start',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var smsVerificationForm = server.forms.getForm('smsverification');
        var fieldErrors = formErrors.getFormErrors(smsVerificationForm);
        if (smsVerificationForm.submitPhone.submitted && smsVerificationForm.valid && !Object.keys(fieldErrors).length) {
            // call SMS verification service and iniate verification process.

            var otpRequest = verificationHelpers.requestOTP(smsVerificationForm.phone.value);

            if (otpRequest.success) {
                res.json({
                    success: true,
                    fieldErrors: [],
                    serverErrors: []
                });
            } else {
                res.json({
                    success: false,
                    fieldErrors: [],
                    serverErrors: [otpRequest.message]
                });
            }
        } else {
            res.json({
                success: false,
                fieldErrors: [fieldErrors],
                serverErrors: []
            });
        }
        next();
    }
);

/**
 * Submits SMS Verification Code and confirms if it is valid or not.
 */
server.post('Confirm',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var smsVerificationForm = server.forms.getForm('smsverification');
        var errors = formErrors.getFormErrors(smsVerificationForm);
        if (smsVerificationForm.verifyCode.submitted && smsVerificationForm.valid && !Object.keys(errors).length) {
            // confirm code is correct.
            var verificationStatus = verificationHelpers.verifyOTP(smsVerificationForm.code.value);

            if (verificationStatus.success || (System.getInstanceType() !== System.PRODUCTION_SYSTEM && smsVerificationForm.code.value === '123456')) {
                verificationHelpers.updateSmsVerificationStatus(true);
                res.json({
                    success: true,
                    fieldErrors: [],
                    serverErrors: []
                });
                smsVerificationForm.clear();
            } else {
                res.json({
                    success: false,
                    fieldErrors: [],
                    serverErrors: [verificationStatus.message]
                });
            }
        } else {
            res.json({
                success: false,
                fieldErrors: [errors],
                serverErrors: []
            });
        }
        next();
    }
);

module.exports = server.exports();
