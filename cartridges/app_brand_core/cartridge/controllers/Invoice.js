var InvoiceHelper = require('*/cartridge/scripts/helpers/invoiceHelper');
var StringUtils = require('dw/util/StringUtils');
var FileReader = require('dw/io/FileReader');
var Logger = require('dw/system/Logger');
var Money = require('dw/value/Money');
var File = require('dw/io/File');
var server = require('server');
/* global response */

var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

/**
 * Accepts a total object and formats the value
 * @param {dw.value.Money} total - Total price of the cart
 * @returns {string} the formatted money value
 */
function getTotals(total) {
    if (total.available) {
        return renderTemplateHelper.getRenderedHtml({ price: total }, 'product/components/pricing/currencyFormatted');
    }
    return '-';
}

/**
 * Extracts order and locale from current request
 *
 * @param {Request} req current request
 *
 * @returns {Object} {order, locale}
 */
function extractOrderAndLocaleFromRequest(req) {
    var OrderMgr = require('dw/order/OrderMgr');
    var Locale = require('dw/util/Locale');

    var orderNo = req.querystring.orderNo;
    if (!orderNo) {
        Logger.error('orderNo missing from request');
        throw new Error('orderNo missing from request');
    }

    var order = OrderMgr.getOrder(orderNo);
    if (!order) {
        Logger.error('Order does not exist orderNo: {0}', orderNo);
        throw new Error('Order does not exist');
    }

    var orderToken = req.querystring.orderToken;
    if (order.getOrderToken() !== orderToken) {
        Logger.error('Order token does not match. orderNo: {0} orderToken: {1}', orderNo, orderToken);
        throw new Error('Order token does not match');
    }

    return {
        order: order,
        locale: Locale.getLocale(req.locale.id)
    };
}

/**
 * Creates a wrapper object containing the before/after tax service fee
 * @param {Object} serviceFee - serviceFee
 * @param {int} taxRate taxRate percentage
 * @param {string} currencyCode currencyCode
 * @returns {Object} wrapper
 */
function getServiceFeeForInvoice(serviceFee, taxRate, currencyCode) {
    if (!serviceFee) {
        return null;
    }

    return {
        withoutTax: getTotals(new Money((serviceFee.value / (100 + taxRate)) * 100, currencyCode)),
        taxAmount: getTotals(new Money((serviceFee.value / (100 + taxRate)) * taxRate, currencyCode))
    };
}

/**
 * Outputs the invoice of given order in a bootstrap 4 template.
 * This action is only supposed to be called by the pdf generation microservice.
 */
server.get('Show', server.middleware.https, function (req, res, next) {
    var OrderModel = require('*/cartridge/models/order');
    var collections = require('*/cartridge/scripts/util/collections');

    var orderAndLocale = extractOrderAndLocaleFromRequest(req);
    var order = orderAndLocale.order;
    var orderModel = new OrderModel(order, { countryCode: orderAndLocale.locale.country });

    var invoicePriceDecorator = require('*/cartridge/models/productLineItem/decorators/invoicePrice');
    orderModel.shipping.forEach(function (shipping) {
        shipping.productLineItems.items.forEach(function (lineItem) {
            var li = collections.find(orderAndLocale.order.allProductLineItems, function (pli) {
                return pli.UUID === lineItem.UUID;
            });
            invoicePriceDecorator(lineItem, li);
        });
    });

    res.render('/invoice/invoice', {
        order: orderModel,
        customerNo: orderAndLocale.order.customerNo,
        refNo: orderAndLocale.order.getUUID(),
        totalShippingCostExclVat: getTotals(order.shippingTotalNetPrice),
        totalShippingCostTax: getTotals(order.shippingTotalTax),
        totalExclTax: getTotals(new Money(order.totalGrossPrice.value - order.totalTax.value, order.currencyCode)),
        subtotalExclTax: getTotals(new Money(order.getAdjustedMerchandizeTotalPrice(false).value - order.totalTax.value, order.currencyCode)),
        serviceFeeForInvoice: getServiceFeeForInvoice(orderModel.totals.serviceFee, orderModel.totals.taxRate, order.currencyCode)
    });

    next();
});

/**
 * Gets reader to file to be downloaded
 *
 * @param {Object} orderAndLocale wrapper object for order and locale
 *
 * @returns {dw.io.FileReader} reader to file
 */
function getFileReaderFor(orderAndLocale) {
    var impex = File.getRootDirectory(File.IMPEX);

    var invoicePdfName = InvoiceHelper.getInvoiceName(orderAndLocale.order.orderNo);
    var nameWithPrefix = false;
    var file = new File(impex, InvoiceHelper.getInvoicePath(orderAndLocale.order.orderNo, invoicePdfName));
    if (!file.exists()) {
        invoicePdfName = InvoiceHelper.getInvoiceName(orderAndLocale.order.orderNo, orderAndLocale.locale.toString());
        file = new File(impex, InvoiceHelper.getInvoicePath(orderAndLocale.order.orderNo, invoicePdfName));
        if (!file.exists()) {
            return null;
        }
        nameWithPrefix = true;
    }
    return {
        reader: new FileReader(file, 'iso-8859-1'),
        fileName: invoicePdfName,
        nameWithPrefix: nameWithPrefix
    };
}

/**
 * Downloads the invoice pdf minding the locale. Will be used for attaching to SFMC or SFSC.
 */
server.get('Get', server.middleware.https, function (req, res, next) {
    var orderAndLocale = {};

    try {
        orderAndLocale = extractOrderAndLocaleFromRequest(req);
    } catch (ex) {
        next(ex);
        return;
    }

    var fileWrapper = getFileReaderFor(orderAndLocale);
    if (!fileWrapper) {
        Logger.error('Order with no: {0} does not have an available invoice', orderAndLocale.order.orderNo);
        throw new Error('Order with no: {0} does not have an available invoice', orderAndLocale.order.orderNo);
    }

    response.addHttpHeader('Content-Disposition',
        StringUtils.format('attachment; filename="{0}"', fileWrapper.nameWithPrefix ?
            fileWrapper.fileName.substr(1) :
            fileWrapper.fileName));
    response.setContentType('application/pdf; charset=iso-8859-1');

    var writer = response.getWriter();
    writer.write(fileWrapper.reader.readString());

    fileWrapper.reader.close();
});

module.exports = server.exports();
