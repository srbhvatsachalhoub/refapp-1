'use strict';

var server = require('server');
server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');

server.replace('Show', consentTracking.consent, cache.applyDefaultCache, function (req, res, next) {
    var PageMgr = require('dw/experience/PageMgr');
    var Site = require('dw/system/Site');
    var pdHomePageId = Site.current.getCustomPreferenceValue('pdHomePageId') || 'homepage';
    var page = PageMgr.getPage(pdHomePageId);
    if (page && page.isVisible()) {
        if (!page.hasVisibilityRules()) {
            res.cachePeriod = 168; // eslint-disable-line no-param-reassign
            res.cachePeriodUnit = 'hours'; // eslint-disable-line no-param-reassign
        }

        res.page(page.ID, {});
        return next();
    }

    var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
    pageMetaHelper.setPageMetaTags(req.pageMetaData, Site.current);
    res.render('/home/homePage');
    return next();
}, pageMetaData.computedPageMetaData);

/**
 * Extended by setting correct action
 */
server.append('Show', function (req, res, next) {
    res.setViewData({
        action: 'Home-Show'
    });
    next();
});

server.append('ErrorNotFound', function (req, res, next) {
    res.setStatusCode(410);
    next();
});

module.exports = server.exports();
