'use strict';

var server = require('server');
server.extend(module.superModule);

server.prepend('Show', function (req, res, next) {
    var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
    var showProductPageHelperResult = productHelper.showProductPage(req.querystring, req.pageMetaData);
    if (showProductPageHelperResult.product.onlineFrom && showProductPageHelperResult.product.onlineFrom > new Date() && showProductPageHelperResult.product.custom.countdownPageId) {
        var PageMgr = require('dw/experience/PageMgr');
        var page = PageMgr.getPage(showProductPageHelperResult.product.custom.countdownPageId);
        if (page && page.isVisible()) {
            if (!page.hasVisibilityRules()) {
                res.cachePeriod = 168; // eslint-disable-line no-param-reassign
                res.cachePeriodUnit = 'hours'; // eslint-disable-line no-param-reassign
            }
            res.page(page.ID, { params: { onlineFrom: showProductPageHelperResult.product.onlineFrom } });
            this.emit('route:Complete', req, res);
            return;
        }
    }

    if (!showProductPageHelperResult.product || !showProductPageHelperResult.product.assignedToSite) {
        res.setStatusCode(410);
        res.render('error/notFound');
    }

    // eslint-disable-next-line consistent-return
    return next();
});

server.append('Show', function (req, res, next) {
    var viewData = res.viewData;
    if (!viewData.product || !viewData.product.assignedToSite) {
        res.setStatusCode(410);
        res.render('error/notFound');
    }

    return next();
});

server.get('Module', function (req, res, next) {
    if ('mid' in req.querystring && 'pid' in req.querystring) {
        var ProductModulesFactory = require('*/cartridge/scripts/factories/productModules');
        var model = ProductModulesFactory.get(req.querystring.mid, req.querystring.pid);
        if (model) {
            res.render(model.template, model);
        } else {
            res.print('Undefined module id: ' + req.querystring.mid);
        }
    }

    next();
});

server.get('Price', function (req, res, next) {
    var Money = require('dw/value/Money');
    var DefaultPrice = require('*/cartridge/models/price/default');
    var sales = 'sales' in req.querystring ? parseFloat(req.querystring.sales) : 0;
    var list = 'list' in req.querystring ? parseFloat(req.querystring.list) : 0;
    var currencyCode = req.querystring.currency;
    var salesMoney = new Money(sales, currencyCode);
    var listMoney = sales < list ? new Money(list, currencyCode) : null;
    var totalPriceFormatted = new DefaultPrice(salesMoney, listMoney);

    res.json(totalPriceFormatted);

    next();
});

module.exports = server.exports();
