'use strict';

var server = require('server');
server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var BreadcrumbsHelpers = require('*/cartridge/scripts/helpers/breadcrumbsHelpers');
var Resource = require('dw/web/Resource');

server.append('List', function (req, res, next) {
    BreadcrumbsHelpers.addLastBreadcrumb(res, Resource.msg('label.payment', 'account', null));
    next();
});

server.append('AddPayment', function (req, res, next) {
    BreadcrumbsHelpers.addLastBreadcrumb(res, Resource.msg('button.add.payment', 'checkout', null));
    next();
});

server.prepend(
    'AddPayment',
    csrfProtection.generateToken,
    consentTracking.consent,
    userLoggedIn.validateLoggedIn,
    function (req, res, next) {
        var URLUtils = require('dw/web/URLUtils');

        // disable Add Payment from Account page
        res.redirect(URLUtils.url('Account-Show'));
        next();
    }
);

server.append(
    'DeletePayment',
    userLoggedIn.validateLoggedInAjax,
    function (req, res, next) {
        var oldHandler = this.listeners('route:BeforeComplete');
        this.off('route:BeforeComplete');

        this.on('route:BeforeComplete', function () { // eslint-disable-line no-shadow
            var PaymentInstrument = require('dw/order/PaymentInstrument');

            var payment = res.getViewData();
            if (payment.UUID
                && payment.raw
                && payment.raw.paymentMethod === PaymentInstrument.METHOD_CREDIT_CARD
                && payment.raw.custom.paymentProcessor
                && payment.raw.custom.referenceOrderNo
                && payment.raw.creditCardToken) {
                var OrderMgr = require('dw/order/OrderMgr');
                var HookMgr = require('dw/system/HookMgr');
                var StringUtils = require('dw/util/StringUtils');

                var order = OrderMgr.getOrder(payment.raw.custom.referenceOrderNo);
                if (order) {
                    var hookId = StringUtils.format('app.payment.processor.{0}', payment.raw.custom.paymentProcessor.toLowerCase());
                    if (HookMgr.hasHook(hookId)) {
                        HookMgr.callHook(hookId,
                            'DeleteToken',
                            order,
                            payment.raw
                        );
                    }
                }
            }

            oldHandler[0].call(this, req, res);
        });

        next();
    }
);

server.get('SetDefault', userLoggedIn.validateLoggedIn, function (req, res, next) {
    var AccountModel = require('*/cartridge/models/account');
    var URLUtils = require('dw/web/URLUtils');

    var paymentInstrumentUUID = req.querystring.uuid;
    AccountModel.setDefaultCustomerPaymentInstrument(req.currentCustomer.wallet.paymentInstruments, paymentInstrumentUUID);

    res.json({
        success: true,
        redirectUrl: URLUtils.url('PaymentInstruments-List').toString()
    });

    next();
});

module.exports = server.exports();
