'use strict';

/* global customer */
/* global request */
var server = require('server');
server.extend(module.superModule);

var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var productHelpers = require('*/cartridge/scripts/helpers/productHelpers');
var BreadcrumbsHelpers = require('*/cartridge/scripts/helpers/breadcrumbsHelpers');
var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');

/**
 * Converts comma separated product id list to an array
 * @param {string} pid - Comma separated list of product ids
 * @return {string[]} Array of product ids
 */
function prepareProductIds(pid) {
    return (pid || '').split(/[\s,]+/);
}

server.prepend('Show', userLoggedIn.validateLoggedIn, function (req, res, next) {
    next();
});

server.append('Show', function (req, res, next) {
    BreadcrumbsHelpers.addLastBreadcrumb(res, Resource.msg('wishlist.hero.image.header', 'wishlist', null));
    var itemIdsStr = [];
    var viewData = res.getViewData();
    var items = viewData.wishlist.items;
    for (var i = 0; i < items.length; i++) {
        itemIdsStr.push(items[i].pid);
    }
    viewData.itemIdsStr = itemIdsStr.join(',');
    res.setViewData(viewData);
    next();
});

server.get('AddToWishlistButton', function (req, res, next) {
    var isAlreadyAdded = false;
    var isVisible = true;
    var pidStr = req.querystring.pid;
    var pidList = prepareProductIds(req.querystring.pid);
    var isMultiple = pidList.length > 1;
    var templatePath = 'wishlist/components/addToWishListButton';
    var action = URLUtils.url(isMultiple ? 'Wishlist-AddProducts' : 'Wishlist-AddProduct');

    if (customer.authenticated && pidList.length > 0) {
        // get customer wishlist and check if the product is in list
        // show static text if they added the product before
        var list = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
        var addedItemCount = 0;

        pidList.forEach(function (pid) {
            var config = {
                qty: 1,
                req: req,
                type: 10
            };
            if (productListHelper.itemExists(list, pid, config)) {
                addedItemCount++;
            }
        });

        isAlreadyAdded = addedItemCount === pidList.length;
    } else {
        isVisible = false;
    }

    res.render(templatePath, {
        pid: pidStr,
        action: action,
        isVisible: isVisible,
        isMultiple: isMultiple,
        isAlreadyAdded: isAlreadyAdded
    });

    next();
});

/**
 * Add multiple products to wishlist
 */
server.post('AddProducts', function (req, res, next) {
    res.json(productHelpers.addProductsToWishlist(req.form.pid, req));
    next();
});

/**
 * Update quantity product to wishlist
 */
server.post('UpdateQuantityProduct', function (req, res, next) {
    var collections = require('*/cartridge/scripts/util/collections');
    var requestUuid = req.form.uuid;
    var productList = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
    var requestListItem = collections.find(productList.items, function (item) {
        return item.UUID === requestUuid;
    });

    try {
        var Transaction = require('dw/system/Transaction');
        Transaction.wrap(function () {
            requestListItem.quantityValue = req.form.qty;
        });
        res.json({
            success: true,
            error: false
        });
    } catch (error) {
        var logger = require('dw/system/Logger').getLogger('account');
        logger.error('An unpexted error ocurred {0}', JSON.stringify(error));
    }
    next();
});

server.get('GetCount', function (req, res, next) {
    var productList = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
    res.render('components/count', { count: productList && productList.items ? productList.items.length : 0 });
    next();
});

module.exports = server.exports();
