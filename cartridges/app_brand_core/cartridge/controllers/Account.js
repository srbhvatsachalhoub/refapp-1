'use strict';
/* global customer, request */

var server = require('server');
server.extend(module.superModule);

var Resource = require('dw/web/Resource');
var CustomerMgr = require('dw/customer/CustomerMgr');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var ProfileHelper = require('*/cartridge/scripts/helpers/profileHelper');
var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');
var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
var MobilePhoneHandler = require('*/cartridge/scripts/helpers/mobilePhoneHandler');
var BreadcrumbsHelpers = require('*/cartridge/scripts/helpers/breadcrumbsHelpers');
var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');

/**
 * Makes wishlist available before opening the account page
 */
server.prepend('Show', csrfProtection.generateToken, function (req, res, next) {
    // but in case of a OAuth login, redirections prevent reading viewData hence another strategy should be implemented.
    var viewData = res.getViewData();
    var listGuest = viewData.list;

    var currentCustomer = req.currentCustomer.raw;
    var session = req.session.raw.custom;

    // check to make sure wish list is only initialized after an external login (OAuth)
    // and hasn't been initialized before in this session
    if (currentCustomer && currentCustomer.externallyAuthenticated && (!session || !session.wishlistProcessed)) {
        var listLoggedIn = productListHelper.getList(currentCustomer, { type: 10 });
        productListHelper.mergelists(listLoggedIn, listGuest, req, { type: 10 });
        productListHelper.updateWishlistPrivacyCache(currentCustomer, req, { type: 10 });
        session.wishlistProcessed = true;
    }

    next();
});

/**
 * Adds current locale and preferred address to view data
 */
server.append('Show', function (req, res, next) {
    var viewData = res.getViewData();

    viewData.account.preferredAddress = accountHelpers.getPreferredAddress(req);
    viewData.countryCode = localeHelpers.getCurrentCountryCode();
    viewData.subscribeOption = accountHelpers.subscribeOption(req);
    res.setViewData(viewData);
    next();
});

/**
 * Logs in the user by using email from his/her profile
 * NOTE-1: This method is exact copy of 'Login' action in Account.js file in
 * app_storefront_base cartridge (one line is altered, check the method body).
 * NOTE-2: Copied because the same login procedure should be applied for mobilePhone as well.
 * NOTE-3: If that cartridge is updated don't forget to update this method too.
 * @param {Request} req req object from action
 * @param {Response} res res object from action
 * @param {Object} next next chain method from action
 * @param {dw.customer.Profile} profile profile of the user who tried to login via phone
 * @returns {Object} next()
 */
function login(req, res, next, profile) {
    var Site = require('dw/system/Site');
    var Transaction = require('dw/system/Transaction');
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
    var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');

    var email = profile.email; // this line is altered to get the email from the profile instead of request
    var password = req.form.loginPassword;
    var rememberMe = req.form.loginRememberMe
        ? (!!req.form.loginRememberMe)
        : false;

    var customerLoginResult = Transaction.wrap(function () {
        var authenticateCustomerResult = CustomerMgr.authenticateCustomer(email, password);

        if (authenticateCustomerResult.status !== 'AUTH_OK') {
            var errorCodes = {
                ERROR_CUSTOMER_DISABLED: 'error.message.account.disabled',
                ERROR_CUSTOMER_LOCKED: 'error.message.account.locked',
                ERROR_CUSTOMER_NOT_FOUND: 'error.message.login.form',
                ERROR_PASSWORD_EXPIRED: 'error.message.password.expired',
                ERROR_PASSWORD_MISMATCH: 'error.message.password.mismatch',
                ERROR_UNKNOWN: 'error.message.error.unknown',
                default: 'error.message.login.form'
            };

            var errorMessageKey = errorCodes[authenticateCustomerResult.status] || errorCodes.default;
            var errorMessage = Resource.msg(errorMessageKey, 'login', null);

            return {
                error: true,
                errorMessage: errorMessage,
                status: authenticateCustomerResult.status,
                authenticatedCustomer: null
            };
        }
        // Apply custom price books for customer
        localeHelpers.setApplicablePriceBooks();
        return {
            error: false,
            errorMessage: null,
            status: authenticateCustomerResult.status,
            authenticatedCustomer: CustomerMgr.loginCustomer(authenticateCustomerResult, rememberMe)
        };
    });

    if (customerLoginResult.error) {
        if (customerLoginResult.status === 'ERROR_CUSTOMER_LOCKED') {
            var context = {
                customer: CustomerMgr.getCustomerByLogin(email) || null
            };

            var emailObj = {
                to: email,
                subject: Resource.msg('subject.account.locked.email', 'login', null),
                from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
                type: emailHelpers.emailTypes.accountLocked
            };

            hooksHelper('app.customer.email', 'sendEmail', [emailObj, 'account/accountLockedEmail', context], function () { });
        }

        res.json({
            error: [customerLoginResult.errorMessage || Resource.msg('error.message.login.form', 'login', null)]
        });

        return next();
    }

    if (customerLoginResult.authenticatedCustomer) {
        res.setViewData({ authenticatedCustomer: customerLoginResult.authenticatedCustomer });
        res.json({
            success: true,
            redirectUrl: accountHelpers.getLoginRedirectURL(req.querystring.rurl, req.session.privacyCache, false)
        });

        req.session.privacyCache.set('args', null);
        // Apply custom price books for customer
        localeHelpers.setApplicablePriceBooks();
    } else {
        res.json({ error: [Resource.msg('error.message.login.form', 'login', null)] });
    }

    return next();
}

/**
 * Completes ajax request (emits route:Complete) with form errors included in the result
 * @param {Object} routeHandler 'this' of the calling action
 * @param {Object} form submitted form
 * @param {Request} req request object
 * @param {Response} res response object
 */
function completeAjaxRequestWithFormErrors(routeHandler, form, req, res) {
    var formErrors = require('*/cartridge/scripts/formErrors');
    res.json({
        success: false,
        fields: formErrors.getFormErrors(form)
    });

    routeHandler.emit('route:Complete', req, res);
}

/**
 * Runs before SubmitRegistration from app_storefront_base cartridge.
 * Checks validity of phone number, if phone number is not unique than fails
 * the request immediately. Otherwise passes request onto the next chain method.
 */
server.prepend('SubmitRegistration',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var form = server.forms.getForm('profile');
        form.customer.emailconfirm.value = form.customer.email.value;

        var emailOK = ProfileHelper.checkUniquenessOfField(this, req, res, form, 'email', false);
        var mobilePhoneOK = ProfileHelper.checkUniquenessOfField(this, req, res, form, 'phoneMobile', false);

        if (!emailOK || !mobilePhoneOK) {
            completeAjaxRequestWithFormErrors(this, form, req, res);
            return;
        }

        next();
    }
);

/**
 * Runs after SubmitRegistration from app_storefront_base cartridge.
 * Persists extra profile data into the system object.
 * Sets the PreferredLocale to the recently created customer's profile
 */
server.append('SubmitRegistration', function (req, res, next) {
    var profileForm = server.forms.getForm('profile');
    ProfileHelper.saveProfile(this, profileForm);
    this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
        var viewData = res.getViewData();
        if (viewData.success && viewData.authenticatedCustomer) {
            viewData.authenticatedCustomerID = viewData.authenticatedCustomer.ID;
            viewData.authenticatedCustomerProfileEmail = viewData.authenticatedCustomer.profile.email;
            ProfileHelper.updateNewsletter(viewData.authenticatedCustomer.profile, viewData.locale);
        }
    });
    // Apply custom price books for customer
    localeHelpers.setApplicablePriceBooks();
    next();
});

server.replace(
    'Login',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var Site = require('dw/system/Site');
        var Transaction = require('dw/system/Transaction');
        var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
        var hooksHelper = require('*/cartridge/scripts/helpers/hooks');

        var email = req.form.loginEmail;
        var password = req.form.loginPassword;
        var rememberMe = req.form.loginRememberMe
            ? (!!req.form.loginRememberMe)
            : false;

        var customerLoginResult = Transaction.wrap(function () {
            var authenticateCustomerResult = CustomerMgr.authenticateCustomer(email, password);

            if (authenticateCustomerResult.status !== 'AUTH_OK') {
                var errorCodes = {
                    ERROR_CUSTOMER_DISABLED: 'error.message.account.disabled',
                    ERROR_CUSTOMER_LOCKED: 'error.message.account.locked',
                    ERROR_CUSTOMER_NOT_FOUND: 'error.message.login.form',
                    ERROR_PASSWORD_EXPIRED: 'error.message.password.expired',
                    ERROR_PASSWORD_MISMATCH: 'error.message.password.mismatch',
                    ERROR_UNKNOWN: 'error.message.error.unknown',
                    default: 'error.message.login.form'
                };

                var errorMessageKey = errorCodes[authenticateCustomerResult.status] || errorCodes.default;
                var errorMessage = Resource.msg(errorMessageKey, 'login', null);

                return {
                    error: true,
                    errorMessage: errorMessage,
                    status: authenticateCustomerResult.status,
                    authenticatedCustomer: null
                };
            }

            return {
                error: false,
                errorMessage: null,
                originLocale: request.locale,
                status: authenticateCustomerResult.status,
                authenticatedCustomer: CustomerMgr.loginCustomer(authenticateCustomerResult, rememberMe)
            };
        });
        if (request.locale !== customerLoginResult.originLocale) {
            request.setLocale(customerLoginResult.originLocale);
        }
        if (customerLoginResult.error) {
            if (customerLoginResult.status === 'ERROR_CUSTOMER_LOCKED') {
                var context = {
                    customer: CustomerMgr.getCustomerByLogin(email) || null
                };

                var emailObj = {
                    to: email,
                    subject: Resource.msg('subject.account.locked.email', 'login', null),
                    from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
                    type: emailHelpers.emailTypes.accountLocked
                };

                hooksHelper('app.customer.email', 'sendEmail', [emailObj, 'account/accountLockedEmail', context], function () {});
            }

            res.json({
                error: [customerLoginResult.errorMessage || Resource.msg('error.message.login.form', 'login', null)]
            });

            return next();
        }

        if (customerLoginResult.authenticatedCustomer) {
            res.setViewData({ authenticatedCustomer: customerLoginResult.authenticatedCustomer });
            res.json({
                success: true,
                redirectUrl: accountHelpers.getLoginRedirectURL(req.querystring.rurl, req.session.privacyCache, false)
            });

            req.session.privacyCache.set('args', null);
        } else {
            res.json({ error: [Resource.msg('error.message.login.form', 'login', null)] });
        }

        return next();
    }
);

/**
 * Runs after 'Login' action from app_storefront_base cartridge is run.
 * If login via email is failed attempts to find the customer by his/her
 * mobile phone, then uses his/her email to authenticate.
 */
server.append('Login', function (req, res, next) {
    var loginViaPhone = MobilePhoneHandler.canLoginViaPhone(req, res);
    if (!loginViaPhone.possible) {
        // Apply custom price books for customer
        localeHelpers.setApplicablePriceBooks();
        return next();
    }

    // attempt to login with the email from found profile
    return login(req, res, next, loginViaPhone.profile);
});
/**
 * Guest wishlist copied from plugin_wishlists
 */
server.append('Login', function (req, res, next) {
    var viewData = res.getViewData();
    var listGuest = viewData.list;
    if (viewData.authenticatedCustomer) {
        var listLoggedIn = productListHelper.getList(viewData.authenticatedCustomer, { type: 10 });
        productListHelper.mergelists(listLoggedIn, listGuest, req, { type: 10 });
        productListHelper.updateWishlistPrivacyCache(req.currentCustomer.raw, req, { type: 10 });

        viewData.authenticatedCustomerID = viewData.authenticatedCustomer.ID;
        viewData.authenticatedCustomerProfileEmail = viewData.authenticatedCustomer.profile.email;
    }
    next();
});

/**
 * Runs after Show from app_storefront_base cartridge.
 * Adds extra customer data into the view data so that it can be shown.
 */
server.append('Show', function (req, res, next) {
    BreadcrumbsHelpers.addLastBreadcrumb(res, Resource.msg('page.title.myaccount', 'account', null));
    next();
});

/**
 * Runs after EditProfile from app_storefront_base cartridge.
 * Adds extra customer data into the view data so that it can be edited.
 */
server.append('EditProfile', function (req, res, next) {
    ProfileHelper.addCustomProfileDataToEditProfileAction(req, res, req.currentCustomer.raw);
    BreadcrumbsHelpers.addLastBreadcrumb(res, Resource.msg('label.profile.edit', 'account', null));
    next();
});

/**
 * Extended with checking validity of phone number, if phone number is not unique than fails
 * the request immediately. Otherwise passes request onto the next chain method.
 * Extended with checking emailconfirm is exist on form
 */
server.replace(
    'SaveProfile',
    server.middleware.https,
    server.middleware.post,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var Transaction = require('dw/system/Transaction');
        var URLUtils = require('dw/web/URLUtils');

        var formErrors = require('*/cartridge/scripts/formErrors');

        var profileForm = server.forms.getForm('profile');

        // check the email and mobile phone uniqueness
        ProfileHelper.checkUniquenessOfField(this, req, res, profileForm, 'email', true, req.currentCustomer.raw);
        ProfileHelper.checkUniquenessOfField(this, req, res, profileForm, 'phoneMobile', true, req.currentCustomer.raw);

        // form validation
        if (profileForm.customer.emailconfirm
            && profileForm.customer.emailconfirm.value
            && profileForm.customer.email.value.toLowerCase() !== profileForm.customer.emailconfirm.value.toLowerCase()) {
            profileForm.valid = false;
            profileForm.customer.email.valid = false;
            profileForm.customer.emailconfirm.valid = false;
            profileForm.customer.emailconfirm.error =
                Resource.msg('error.message.mismatch.email', 'forms', null);
        }

        var result = {
            firstName: profileForm.customer.firstname.value,
            lastName: profileForm.customer.lastname.value,
            phone: profileForm.customer.phone.value,
            email: profileForm.customer.email.value,
            confirmEmail: profileForm.customer.emailconfirm.value,
            password: profileForm.login.password.value,
            title: profileForm.title.titleList.value,
            birthday: profileForm.customer.birthday.value,
            phoneMobile: profileForm.customer.phoneMobile.value,
            profileForm: profileForm
        };

        if (profileForm.valid) {
            res.setViewData(result);
            this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
                var formInfo = res.getViewData();
                var customer = CustomerMgr.getCustomerByCustomerNumber(
                    req.currentCustomer.profile.customerNo
                );
                var profile = customer.getProfile();
                var customerLogin;
                var status;

                Transaction.wrap(function () {
                    status = profile.credentials.setPassword(
                        formInfo.password,
                        formInfo.password,
                        true
                    );
                    if (status.error) {
                        formInfo.profileForm.login.password.valid = false;
                        formInfo.profileForm.login.password.error =
                            Resource.msg('error.message.currentpasswordnomatch', 'forms', null);
                    } else {
                        customerLogin = profile.credentials.setLogin(
                            formInfo.email,
                            formInfo.password
                        );
                    }
                });

                delete formInfo.password;
                delete formInfo.confirmEmail;

                if (customerLogin) {
                    Transaction.wrap(function () {
                        profile.setFirstName(formInfo.firstName);
                        profile.setLastName(formInfo.lastName);
                        profile.setEmail(formInfo.email);
                        profile.setPhoneHome(formInfo.phone);

                        if (formInfo.title) {
                            profile.setTitle(formInfo.title);
                        }

                        if (formInfo.birthday) {
                            profile.setBirthday(formInfo.birthday);
                        }

                        if (formInfo.phoneMobile) {
                            profile.setPhoneMobile(formInfo.phoneMobile);
                        }

                        require('dw/system/HookMgr').callHook('app.account.updated', 'updated', customer);
                        if (formInfo.profileForm.customer.addtoemaillist.checked) {
                            ProfileHelper.updateNewsletter(customer.profile, req.locale.id);
                        }
                    });

                    // Send account edited email
                    accountHelpers.sendAccountEditedEmail(customer.profile);

                    delete formInfo.profileForm;
                    delete formInfo.email;

                    res.json({
                        success: true,
                        redirectUrl: URLUtils.url('Account-Show').toString()
                    });
                } else {
                    if (!status.error) {
                        formInfo.profileForm.customer.email.valid = false;
                        formInfo.profileForm.customer.email.error =
                            Resource.msg('error.message.username.invalid', 'forms', null);
                    }

                    delete formInfo.profileForm;
                    delete formInfo.email;

                    res.json({
                        success: false,
                        fields: formErrors.getFormErrors(profileForm)
                    });
                }
            });
        } else {
            res.json({
                success: false,
                fields: formErrors.getFormErrors(profileForm)
            });
        }
        return next();
    }
);

server.append('EditPassword', function (req, res, next) {
    BreadcrumbsHelpers.addLastBreadcrumb(res, Resource.msg('label.profile.changepassword', 'account', null));
    res.setViewData({
        passwordConstraints: ProfileHelper.getPasswordConstraints()
    });
    next();
});

server.append('SetNewPassword', server.middleware.https, function (req, res, next) {
    res.setViewData({
        passwordConstraints: ProfileHelper.getPasswordConstraints()
    });
    next();
});

/**
 * Account Preferences
 */
server.get('Preferences', server.middleware.https, csrfProtection.generateToken, userLoggedIn.validateLoggedIn, function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');

    var accountPreferencesForm = server.forms.getForm('accountpreferences');
    var currentCustomer = req.currentCustomer.raw;

    accountPreferencesForm.optinlist.optinemail.checked = !!currentCustomer.profile.custom.emailOptin;
    accountPreferencesForm.optinlist.optinsms.checked = !!currentCustomer.profile.custom.smsOptin;
    accountPreferencesForm.optinlist.optintelephone.checked = !!currentCustomer.profile.custom.telephoneOptin;
    accountPreferencesForm.optinlist.optinwhatsapp.checked = !!currentCustomer.profile.custom.whatsappOptin;

    var breadcrumbs = [
        {
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('page.title.myaccount', 'account', null),
            url: URLUtils.url('Account-Show').toString()
        },
        {
            htmlValue: Resource.msg('label.preferences', 'account', null)
        }
    ];

    res.render('account/preferences/preferences', {
        accountPreferencesForm: accountPreferencesForm,
        breadcrumbs: breadcrumbs
    });
    next();
});

/**
 * Account Save Preferences
 */
server.post('SavePreferences', server.middleware.https, csrfProtection.generateToken, userLoggedIn.validateLoggedIn, function (req, res, next) {
    var formErrors = require('*/cartridge/scripts/formErrors');
    var accountPreferencesForm = server.forms.getForm('accountpreferences');
    var errors = formErrors.getFormErrors(accountPreferencesForm);

    if (!accountPreferencesForm.save.submitted || !accountPreferencesForm.valid || Object.keys(errors).length) {
        res.json({
            success: false,
            fieldErrors: [errors],
            serverErrors: []
        });
    } else {
        try {
            var currentCustomer = req.currentCustomer.raw;

            if (!currentCustomer || !currentCustomer.profile) {
                var URLUtils = require('dw/web/URLUtils');
                res.redirect(URLUtils.url('Login-Show'));
            }

            ProfileHelper.saveOptinInformation(currentCustomer.profile, accountPreferencesForm.optinlist);
            var result = ProfileHelper.updateNewsletter(currentCustomer.profile, req.locale.id);
            require('dw/system/HookMgr').callHook('app.account.updated', 'updated', currentCustomer);

            if (result.isOk()) {
                res.json({
                    success: true,
                    message: Resource.msg('preferences.success.msg', 'account', null),
                    fieldErrors: [],
                    serverErrors: []
                });
            } else {
                res.json({
                    success: false,
                    fieldErrors: [],
                    serverErrors: []
                });
            }
        } catch (e) {
            var logger = require('dw/system/Logger').getLogger('account');
            logger.error('An unpexted error ocurred {0}', JSON.stringify(e));
            res.json({
                success: false,
                fieldErrors: [],
                serverErrors: [Resource.msg('account.preferences.general.error', 'account', null)]
            });
        }
    }
    next();
});

/**
 * Account Save Preferences
 */
server.append('SavePassword', function (req, res, next) {
    this.on('route:BeforeComplete', function () {
        var Site = require('dw/system/Site');
        var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
        var emailObj = {
            to: customer.profile.email,
            subject: Resource.msg('subject.profile.resetpassword.email', 'login', null),
            from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
            type: emailHelpers.emailTypes.passwordChanged
        };
        var objectForEmail = {
            firstName: customer.profile.firstName,
            lastName: customer.profile.lastName,
            url: require('dw/web/URLUtils').http('Account-EditPassword').toString(),
            resettingCustomer: customer
        };
        emailHelpers.sendEmail(emailObj, 'account/password/passwordChangedEmail', objectForEmail);
    });
    next();
});

/**
 * Extended to check if the given email has the account
 * Returns success if no account found from app_storefront_base
 */
server.append('PasswordResetDialogForm', server.middleware.https, function (req, res, next) {
    var viewData = res.getViewData();
    if (viewData.success) {
        var email = req.form.loginEmail;
        var resettingCustomer = CustomerMgr.getCustomerByLogin(email);

        // check if the account found, if not return error
        if (!resettingCustomer) {
            res.json({
                success: false,
                fields: {
                    loginEmail: Resource.msg('error.message.login.form', 'login', null)
                }
            });
        }
    }
    next();
});

server.replace('SaveNewPassword', server.middleware.https, function (req, res, next) {
    var Transaction = require('dw/system/Transaction');

    var passwordForm = server.forms.getForm('newPasswords');
    var token = req.querystring.token || req.querystring.Token;

    if (passwordForm.newpassword.value !== passwordForm.newpasswordconfirm.value) {
        passwordForm.valid = false;
        passwordForm.newpassword.valid = false;
        passwordForm.newpasswordconfirm.valid = false;
        passwordForm.newpasswordconfirm.error =
            Resource.msg('error.message.mismatch.newpassword', 'forms', null);
    }

    if (passwordForm.valid) {
        var result = {
            newPassword: passwordForm.newpassword.value,
            newPasswordConfirm: passwordForm.newpasswordconfirm.value,
            token: token,
            passwordForm: passwordForm
        };
        res.setViewData(result);
        this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
            var URLUtils = require('dw/web/URLUtils');
            var Site = require('dw/system/Site');
            var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');

            var formInfo = res.getViewData();
            var status;
            var resettingCustomer;
            Transaction.wrap(function () {
                resettingCustomer = CustomerMgr.getCustomerByToken(formInfo.token);
                status = resettingCustomer.profile.credentials.setPasswordWithToken(
                    formInfo.token,
                    formInfo.newPassword
                );
            });
            if (status.error) {
                passwordForm.newpassword.valid = false;
                passwordForm.newpasswordconfirm.valid = false;
                passwordForm.newpasswordconfirm.error =
                    Resource.msg('error.message.resetpassword.invalidformentry', 'forms', null);
                res.render('account/password/newPassword', {
                    passwordForm: passwordForm,
                    token: token
                });
            } else {
                var email = resettingCustomer.profile.email;
                var url = URLUtils.https('Login-Show');
                var objectForEmail = {
                    firstName: resettingCustomer.profile.firstName,
                    lastName: resettingCustomer.profile.lastName,
                    url: url
                };

                var emailObj = {
                    to: email,
                    subject: Resource.msg('subject.profile.resetpassword.email', 'login', null),
                    from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
                    type: emailHelpers.emailTypes.passwordChanged
                };

                emailHelpers.sendEmail(emailObj, 'account/password/passwordChangedEmail', objectForEmail);
                res.redirect(URLUtils.url('Login-Show'));
            }
        });
    } else {
        res.render('account/password/newPassword', { passwordForm: passwordForm, token: token });
    }
    next();
});

server.append('Header', server.middleware.include, csrfProtection.generateToken, function (req, res, next) {
    var subscribeForm = server.forms.getForm('subscribe');
    subscribeForm.clear();

    res.setViewData({
        subscribeForm: subscribeForm
    });
    return next();
});

module.exports = server.exports();
