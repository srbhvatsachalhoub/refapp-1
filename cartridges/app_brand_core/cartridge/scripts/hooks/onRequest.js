'use strict';

/* global request, response, session */

var urlHelpers = require('*/cartridge/scripts/helpers/urlHelpers');
var currentSite = require('dw/system/Site').current;

/**
 * RegEx used to identify system-generated requests that can be ignored
 * @type {RegExp}
 */
var systemRegEx = /__Analytics|__SYSTEM__/;

/**
 * Returns true if system request (Analytics, SYSTEM, etc)
 * @returns {boolean} true if current request is system-generated request
 */
function isSystemRequest() {
    return request.httpRequest
        && systemRegEx.test(request.httpURL.toString());
}

/**
 * Returns true if script executing in Business Manager context (Sites-Site)
 * @returns {boolean} true if script executed from Business Manager
 */
function isBM() {
    // if Sites-Site, we're in Business Manager
    return (require('dw/system/Site').current.ID === 'Sites-Site') || (request.httpPath && request.httpPath.indexOf('Sites-Site') > -1);
}

/**
 * Checks if the current request notified with ONREQUEST thread (real start of request)
 * @returns {boolean} true if current request is notified with ONREQUEST
 */
function isOnrequestNotified() {
    return request.httpHeaders
        && request.httpHeaders.get('x-is-notify') === 'ONREQUEST';
}

/**
 * Prepares and returns the ur to be redirected
 * @param {string} locale - the target locale
 * @param {string} host - the target host
 * @returns {string} - the prepared url to be redirected
 */
function getRedirectUrl(locale, host) {
    var controllerAction = urlHelpers.getControllerActionFromHttpPath();
    if (!controllerAction) {
        return '';
    }

    var URLUtils = require('dw/web/URLUtils');
    var URLAction = require('dw/web/URLAction');
    var URLParameter = require('dw/web/URLParameter');
    var collections = require('*/cartridge/scripts/util/collections');

    var urlAction = new URLAction(controllerAction, currentSite.ID, locale);
    var urlParameters = collections.map(request.httpParameterMap.parameterNames, function (parameterName) {
        return new URLParameter(parameterName, request.httpParameterMap[parameterName]);
    });

    var url = URLUtils.url(urlAction, urlParameters);
    if (host) {
        url = URLUtils.sessionRedirect(host, url);
    }

    return url.toString();
}

/**
 * Checks if the requst has the locale indicator in its url
 * If not then creates a new request url with locale indicator
 * and return created new url to be redirected
 * @returns {string} the url to be redirected, empty if redirect is not needed
 */
function checkSiteLocaleRedirect() {
    var setRedirectUrl = false;

    var requestLocale = request.locale;
    var localeHost = urlHelpers.getLocaleHost(requestLocale);
    if (localeHost && request.httpHost && localeHost !== request.httpHost) {
        setRedirectUrl = true;
    }

    if (!setRedirectUrl) {
        var localeMapping = currentSite.getCustomPreferenceValue('localeMapping');
        if (!localeMapping) {
            return '';
        }

        var currentLocale = session.custom.locale; // eslint-disable-line no-undef
        if (!currentLocale || (currentLocale === requestLocale)) {
            return '';
        }

        // check locale mapping settings
        switch (localeMapping.value) {
            case 'urlParameter':
                var localeParameterName = currentSite.getCustomPreferenceValue('localeParameterName');
                if (!request.httpParameterMap.get(localeParameterName).value) {
                    requestLocale = currentLocale;
                    setRedirectUrl = true;
                }
                break;
            default:
                break;
        }
    }

    if (setRedirectUrl) {
        return getRedirectUrl(requestLocale, localeHost);
    }

    return '';
}

/**
 * Checks the site locale from session and request locale
 * If not equal then set the request locale to the relevant places
 * sets correct currency
 * sets correct pricebooks
 * sets the locale cookie and session locale
 */
function checkSiteLocale() {
    var sessionLocale = session.custom.locale;
    if (!sessionLocale) {
        return;
    }

    var requestLocale = request.locale;
    if (requestLocale === sessionLocale) {
        return;
    }

    if (!request.setLocale(requestLocale)) {
        return;
    }

    var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');

    localeHelpers.setCountryLocale(requestLocale);
}

/**
 * onRequest Hook
 * Calls relevant methods on starting request
 */
function onRequest() {
    // check if the request is Business Manager request
    // if so, no check
    if (isBM()) {
        return;
    }

    // if it is system request then return
    if (isSystemRequest()) {
        return;
    }

    // here: call whatever you want for all requests excepts system generated requests

    if (!isOnrequestNotified()) {
        return;
    }

    // Avoid remote includes
    if (request.includeRequest) {
        return;
    }

    var redirectUrl = checkSiteLocaleRedirect();
    if (redirectUrl) {
        response.redirect(redirectUrl);
        return;
    }

    // check site locale and request locale
    checkSiteLocale();
}

module.exports = {
    onRequest: onRequest
};
