'use strict';

var Transaction = require('dw/system/Transaction');
var Resource = require('dw/web/Resource');
var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
var checkoutHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');


/**
 * Verifies that given basket is for PREPAID payment method/processor
 * The basket price should be 0 for this payment method/processor
 * @param {dw.order.Basket} basket Current users's basket
 * @return {Object} returns an error object
 */
function Handle(basket) {
    var errors = [];
    var nonComplimentaryPaymentAmount = basketCalculationHelpers.getNonComplimentaryPaymentAmount(basket);
    if (!basket || nonComplimentaryPaymentAmount.value > 0) {
        errors.push(Resource.msg('error.payment.processor.not.supported', 'checkout', null));

        return { fieldErrors: [], serverErrors: errors, error: true };
    }

    Transaction.wrap(function () {
        // remove existing payment instruments except complimentary payment methods
        checkoutHelpers.removeExistingPaymentInstruments(basket);

        basket.createPaymentInstrument(
            paymentMethodHelpers.getPrePaidPaymentMethodId(),
            nonComplimentaryPaymentAmount
        );
    });

    return { fieldErrors: [], serverErrors: errors, error: false };
}

/**
 * Authorizes a 0 price payment using PREPAID.
 * The basket price should be 0 for this payment method/processor
 * @param {dw.order.Order} order - The order object
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current payment method
 * @return {Object} returns an error object
 */
function Authorize(order, orderNumber, paymentInstrument, paymentProcessor) { // eslint-disable-line
    var serverErrors = [];
    var fieldErrors = {};
    var error = false;

    try {
        Transaction.wrap(function () {
            paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
            paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);
        });
    } catch (e) {
        error = true;
        serverErrors.push(
            Resource.msg('error.technical', 'checkout', null)
        );
    }

    return { fieldErrors: fieldErrors, serverErrors: serverErrors, error: error };
}

exports.Handle = Handle;
exports.Authorize = Authorize;
