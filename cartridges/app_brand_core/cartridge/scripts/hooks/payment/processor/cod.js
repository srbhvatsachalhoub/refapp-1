'use strict';
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var collections = require('*/cartridge/scripts/util/collections');
var verificationHelpers = require('*/cartridge/scripts/helpers/verificationHelpers');
var checkoutHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');

/**
 * @param {dw.order.Basket} basket Current users's basket
 * @return {Object} an object that contains error information
 */
function Handle(basket) {
    var errors = [];
    if (!basket) {
        // validate basket & confirm service fee is added & make sure customer phone number is verified.
        errors.push(Resource.msg('error.payment.processor.not.supported', 'checkout', null));
        return { fieldErrors: [], serverErrors: errors, error: true };
    }
    var currentBasket = basket;
    // run validation if verification is required for current locale/country.
    if (verificationHelpers.isVerificationRequired()) {
        if (currentBasket && currentBasket.getDefaultShipment() && currentBasket.getDefaultShipment().shippingAddress) {
            var address = currentBasket.getDefaultShipment().shippingAddress;
            if (!address.custom.isSmsVerified) {
                errors.push(Resource.msg('error.unidentified.smsverification', 'checkout', null));
                return { fieldErrors: [], serverErrors: errors, error: true };
            }
        } else {
            errors.push(Resource.msg('error.shipping.smsverification', 'checkout', null));
            return { fieldErrors: [], serverErrors: errors, error: true };
        }
    }

    // check the basket has no virtual gift card product
    if (collections.some(currentBasket.productLineItems, function (pli) {
        return (pli.product.custom.giftCardType.value === 'virtual');
    })) {
        errors.push(Resource.msg('error.cod.product.notsupported', 'checkout', null));
        return { fieldErrors: [], serverErrors: errors, error: true };
    }

    Transaction.wrap(function () {
        // remove existing payment instruments except complimentary payment methods
        checkoutHelpers.removeExistingPaymentInstruments(currentBasket);

        var totalToBePaid = basketCalculationHelpers.getNonComplimentaryPaymentAmount(currentBasket);

        if (totalToBePaid.value > 0) {
            currentBasket.createPaymentInstrument(
                paymentMethodHelpers.getCODPaymentMethodId(),
                totalToBePaid
            );
        }
    });

    return { fieldErrors: [], serverErrors: errors, error: false };
}

/**
 * Authorizes a payment using a credit card. Customizations may use other processors and custom
 *      logic to authorize credit card payment.
 * @param {dw.order.Order} order - The order object
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @return {Object} returns an error object
 */
function Authorize(order, orderNumber, paymentInstrument, paymentProcessor) { //eslint-disable-line
    var errors = [];
    var error = false;
    if (!orderNumber || !paymentInstrument || !paymentProcessor) {
        errors.push(Resource.msg('error.payment.processor.not.supported', 'checkout', null));
        return { fieldErrors: [], serverErrors: errors, error: true };
    }

    try {
        Transaction.wrap(function () {
            paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
            paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);
        });
    } catch (e) {
        error = true;
        errors.push(
            Resource.msg('error.technical', 'checkout', null)
        );
    }
    return { fieldErrors: [], serverErrors: errors, error: error };
}

module.exports = exports = {
    Handle: Handle,
    Authorize: Authorize
};
