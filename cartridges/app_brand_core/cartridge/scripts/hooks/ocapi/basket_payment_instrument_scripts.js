'use strict';

// dw.ocapi.shop.basket.payment_instrument.beforePOST
exports.beforePOST = function (basket, paymentInstrument) { // eslint-disable-line
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var collections = require('*/cartridge/scripts/util/collections');
    var Status = require('dw/system/Status');

    collections.forEach(basket.getPaymentInstruments(), function (item) {
        if (item.paymentMethod !== PaymentInstrument.METHOD_GIFT_CERTIFICATE) {
            basket.removePaymentInstrument(item);
        }
    });
    return new Status(Status.OK);
};
