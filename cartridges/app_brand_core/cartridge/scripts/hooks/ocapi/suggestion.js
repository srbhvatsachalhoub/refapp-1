'use strict';
var Status = require('dw/system/Status');
var ProductImageDIS = require('*/cartridge/scripts/helpers/productImageDIS');

/**
 * @param {SuggestionResult} doc SuggestionResult
 * @returns {dw.system.Status} status
 */
exports.modifyGETResponse = function (doc) {
    var ProductMgr = require('dw/catalog/ProductMgr');
    for (var i = 0; i < doc.productSuggestions.products.length; i++) {
        var product = doc.productSuggestions.products[i];
        var apiProduct = ProductMgr.getProduct(product.productId);
        var disImage = ProductImageDIS.getImage(apiProduct, 'small').getURL();
        product.c_image = disImage;
    }
    var ContentMgr = require('dw/content/ContentMgr');
    for (var c = 0; c < doc.contentSuggestions.content.length; c++) {
        var content = doc.contentSuggestions.content[c];
        var MediaFile = require('dw/content/MediaFile');
        var MarkupText = require('dw/content/MarkupText');
        var List = require('dw/util/List');
        var apiContent = ContentMgr.getContent(content.id);
        for (var attr in apiContent.custom) { // eslint-disable-line
            if (attr[0] === 'm') {
                var key = 'c_' + attr;
                if (apiContent.custom[attr] instanceof MediaFile) {
                    content[key] = apiContent.custom[attr].absURL.toString();
                } else if (apiContent.custom[attr] instanceof MarkupText) {
                    content[key] = apiContent.custom[attr].getMarkup();
                } else if (apiContent.custom[attr] instanceof List) {
                    content[key] = Array.prototype.slice.call(apiContent.custom[attr]).join(',');
                } else {
                    content[key] = apiContent.custom[attr];
                }
            }
        }
    }
    return new Status(Status.OK);
};
