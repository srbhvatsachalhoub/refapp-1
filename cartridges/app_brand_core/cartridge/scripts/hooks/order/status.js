'use strict';
/* global request */

var OrderMgr = require('dw/order/OrderMgr');
var Order = require('dw/order/Order');
var Status = require('dw/system/Status');
var Transaction = require('dw/system/Transaction');
var ArrayList = require('dw/util/ArrayList');
var StringUtils = require('dw/util/StringUtils');
var CurrentSite = require('dw/system/Site').current;

// constant value to indicate track history or not
// can be sitepref later
var trackHistoryEnabled = true;

// constant values for the type of the status
var statusTypes = {
    confirmation: 'confirmation',
    export: 'export',
    payment: 'payment',
    shipping: 'shipping',
    order: 'order',
    additionalShipping: 'additionalShipping'
};

/**
 * Tracks the status change into Order.custom.history SetofString field
 * trackHistoryEnabled to enable/disable status change history
 * @param {string} statusType - the type of the status
 * @param {dw.order.Order} order - the order to be historied
 * @param {dw.value.EnumValue} oldStatus - the old status before change
 * @param {dw.value.EnumValue} newStatus - the new status after change
 * @param {string} note - additional note
 */
function trackHistory(statusType, order, oldStatus, newStatus, note) {
    if (!trackHistoryEnabled || !order) {
        return;
    }

    var orderStatusHistory = new ArrayList(order.custom.history);
    var statusHistory = {
        type: statusType,
        time: StringUtils.formatCalendar(CurrentSite.calendar),
        user: request.session.userName,
        old: oldStatus.displayValue,
        new: newStatus.displayValue
    };
    if (note) {
        statusHistory.note = note;
    }

    orderStatusHistory.add1(JSON.stringify(statusHistory));
    Transaction.wrap(function () {
        order.custom.history = orderStatusHistory; // eslint-disable-line no-param-reassign
    });
}
/**
 * Adds Order Note if note is passed as object with subject & text
 * @param {dw.order.Order} order Order
 * @param {Object} note note object
 */
function addOrderNote(order, note) {
    // check if note is a object
    if (typeof note !== 'object') {
        return;
    }
    if (note.subject && note.text) {
        order.addNote(note.subject, note.text);
    }
}
/**
 * Sets the confirmation status value.
 * Possible values are Order.CONFIRMATION_STATUS_NOTCONFIRMED or Order.CONFIRMATION_STATUS_CONFIRMED.
 * @param {dw.order.Order} order - the order to be set
 * @param {number} status - the order confirmation status
 * @param {Object} note - additional note
 */
function setConfirmationStatus(order, status, note) {
    if (!order) {
        return;
    }

    var oldStatus = order.confirmationStatus;
    Transaction.wrap(function () {
        order.setConfirmationStatus(status);
        addOrderNote(order, note);
    });
    trackHistory(statusTypes.confirmation, order, oldStatus, order.confirmationStatus, note);
}

/**
 * Sets the export status of the order.
 * Possible values are: Order.EXPORT_STATUS_NOTEXPORTED, Order.EXPORT_STATUS_EXPORTED, Order.EXPORT_STATUS_READY, and Order.EXPORT_STATUS_FAILED.
 * Setting the status to EXPORT_STATUS_EXPORTED will also trigger the finalization of on order inventory transactions
 * for this order meaning that all inventory transactions with type on order will be moved into final inventory transactions.
 * This is only relevant when On Order Inventory is turned on for the inventory list ordered products are in.
 * In case of an exception the current transaction is marked as rollback only.
 * @param {dw.order.Order} order - the order to be set
 * @param {number} status - the order export status
 * @param {Object} note - additional note
 */
function setExportStatus(order, status, note) {
    if (!order) {
        return;
    }

    var oldStatus = order.exportStatus;
    Transaction.wrap(function () {
        order.setExportStatus(status);
        addOrderNote(order, note);
    });
    trackHistory(statusTypes.export, order, oldStatus, order.exportStatus, note);
}

/**
 * Sets the order payment status.
 * Possible values are Order.PAYMENT_STATUS_NOTPAID, Order.PAYMENT_STATUS_PARTPAID or Order.PAYMENT_STATUS_PAID.
 * @param {dw.order.Order} order - the order to be set
 * @param {number} status - the order payment status
 * @param {string} note - additional note
 */
function setPaymentStatus(order, status, note) {
    if (!order) {
        return;
    }

    var oldStatus = order.paymentStatus;
    Transaction.wrap(function () {
        order.setPaymentStatus(status);
        addOrderNote(order, note);
    });
    trackHistory(statusTypes.payment, order, oldStatus, order.paymentStatus, note);
}

/**
 * Sets the order shipping status value.
 * Possible values are Order.SHIPPING_STATUS_NOTSHIPPED, Order.SHIPPING_STATUS_PARTSHIPPED or Order.SHIPPING_STATUS_SHIPPED.
 * @param {dw.order.Order} order - the order to be set
 * @param {number} status - the order shipping status
 * @param {string} note - additional note
 */
function setShippingStatus(order, status, note) {
    if (!order) {
        return;
    }

    var oldStatus = order.shippingStatus;
    Transaction.wrap(function () {
        order.setShippingStatus(status);
        addOrderNote(order, note);
    });
    trackHistory(statusTypes.shipping, order, oldStatus, order.shippingStatus, note);
}

/**
 * Sets Order's additional shipping status
 * Possible values are packed, delivered
 * @param {dw.order.Order} order - the order to be set
 * @param {string} status - the additional shipping status
 * @param {string} note - additional note
 */
function setAdditionalShippingStatus(order, status, note) {
    if (!order) {
        return;
    }

    var oldStatus = order.custom.additionalOrderShipmentStatus;
    Transaction.wrap(function () {
        order.custom.additionalOrderShipmentStatus = status; // eslint-disable-line no-param-reassign
        addOrderNote(order, note);
    });
    trackHistory(statusTypes.additionalShipping, order, oldStatus, order.custom.additionalOrderShipmentStatus, note);
}

/**
 * Sets the status of the order.
 * Possible values are Order.ORDER_STATUS_NEW, Order.ORDER_STATUS_OPEN, Order.ORDER_STATUS_COMPLETED, Order.ORDER_STATUS_CANCELLED or Order.ORDER_STATUS_REPLACED
 * This method does not support order statuses ORDER_STATUS_CREATED or ORDER_STATUS_FAILED. Please use OrderMgr.placeOrder(Order) or OrderMgr.failOrder(Order).
 * Setting the order status to ORDER_STATUS_CANCELLED will have the same effect as calling OrderMgr.cancelOrder(Order).
 * Setting a canceled order to ORDER_STATUS_NEW, ORDER_STATUS_OPEN or ORDER_STATUS_COMPLETED will have the same effect as calling OrderMgr.undoCancelOrder(Order).
 * It is recommended to use the methods in OrderMgr directly to be able to do error processing with the return code.
 * @param {dw.order.Order} order - the order to be set
 * @param {number} status - the order status
 * @param {string} note - additional note
 */
function setStatus(order, status, note) {
    if (!order) {
        return;
    }

    var oldStatus = order.status;
    Transaction.wrap(function () {
        order.setStatus(status);
        addOrderNote(order, note);
    });
    trackHistory(statusTypes.order, order, oldStatus, order.status, note);
}

/**
 * This method cancels an order. Only orders in status OPEN, NEW, or COMPLETED can be cancelled.
 * Setting of cancel code and cancel description can be done by calling Order.setCancelCode(String) and Order.setCancelDescription(String).
 * If the order contains product or gift certificate line items associated with product list items, records of the purchase of the product list items will be removed.
 * Inventory transactions and coupon redemptions associated with the order will be rolled back.
 * It is important to consider that this method will cancel orders with gift certificate line items.
 * If an order has any active post-processing objects (e.g. shipping orders, returns, appeasements), then it cannot be cancelled directly. Its status is set automatically, based on the statuses of its post-processing objects.
 * To cancel such an order, you must cancel all related post-processing objects.
 * If your B2C Commerce instance is integrated with Order Management, then you manage order statuses in Order Management. Use Order Management API endpoints.
 * @param {dw.order.Order} order - the order to be cancelled
 * @param {string} cancelCode - the cancel code
 * @param {string} cancelDescription - the cancel description
 * @param {string} note - additional note
 * @returns {dw.system.Status} Status 'OK' or 'ERROR'
 */
function cancelOrder(order, cancelCode, cancelDescription, note) {
    var status = new Status(Status.ERROR);
    if (!order
        || order.status.value === Order.ORDER_STATUS_CANCELLED
        || order.status.value === Order.ORDER_STATUS_CREATED
        || order.status.value === Order.ORDER_STATUS_FAILED
        || order.status.value === Order.ORDER_STATUS_REPLACED) {
        return status;
    }

    var oldStatus = order.status;
    Transaction.wrap(function () {
        status = OrderMgr.cancelOrder(order);
        if (status.status === Status.ERROR) {
            return;
        }

        if (cancelCode) {
            order.setCancelCode(cancelCode);
        }

        if (cancelDescription) {
            order.setCancelDescription(cancelDescription);
        }
        addOrderNote(order, note);
    });
    trackHistory(statusTypes.order, order, oldStatus, order.status, note);

    return status;
}

/**
 * This method fails an unplaced order and is usually called if payment could not be authorized.
 * The specified Order must be in status CREATED, and will be set to status FAILED.
 * Inventory transactions and coupon redemptions associated with the Order will be rolled back.
 * @param {dw.order.Order} order - the order to be failed
 * @param {boolean} reopenBasketIfPossible - reopen the basket if it still exists and limit for number of baskets is not reached
 * @param {string} note - additional note
 * @returns {dw.system.Status} Status 'OK' or 'ERROR' with an error message. Status detail basket contains the reopened basket, if it has been reopened successfully.
 */
function failOrder(order, reopenBasketIfPossible, note) {
    var status = new Status(Status.ERROR);
    if (!order
        || order.status.value !== Order.ORDER_STATUS_CREATED) {
        return status;
    }

    var oldStatus = order.status;
    Transaction.wrap(function () {
        status = OrderMgr.failOrder(order, reopenBasketIfPossible);
        addOrderNote(order, note);
    });
    trackHistory(statusTypes.order, order, oldStatus, order.status, note);

    return status;
}

/**
 * This method is used to turn a CANCELLED order into an OPEN order.
 * The specified order must be a cancelled order (Order.ORDER_STATUS_CANCELLED).
 * The method will reserve inventory for all product line items, and create redemptions for all coupons.
 * If successful, the status of the order will be changed to Order.ORDER_STATUS_OPEN.
 * If the order contains product or gift certificate line items associated with product list items,
 * records of the purchase of the product list items will be recreated.
 * Possible error status codes are:
 * OrderProcessStatusCodes.COUPON_INVALID - coupon is not active anymore or maximum amount of redemptions is reached
 * OrderProcessStatusCodes.ORDER_NOT_CANCELLED - order is not in status Order.ORDER_STATUS_CANCELLED
 * OrderProcessStatusCodes.INVENTORY_RESERVATION_FAILED - inventory reservation failed
 * @param {dw.order.Order} order - the order on which to undo the cancel cancelOrder(Order)
 * @param {string} note - additional note
 * @returns {dw.system.Status} Status 'OK' or 'ERROR' with one of the error codes described above
 */
function undoCancelOrder(order, note) {
    var status = new Status(Status.ERROR);
    if (!order
        || order.status.value !== Order.ORDER_STATUS_CANCELLED) {
        return status;
    }

    var oldStatus = order.status;
    Transaction.wrap(function () {
        status = OrderMgr.undoCancelOrder(order);
        addOrderNote(order, note);
    });
    trackHistory(statusTypes.order, order, oldStatus, order.status, note);

    return status;
}

/**
 * This method is used to turn a FAILED order into a CREATED order.
 * The specified order must be a failed order (Order.ORDER_STATUS_FAILED).
 * The method will reserve inventory for all product line items, and create redemptions for all coupons.
 * If successful, the status of the order will be changed to Order.ORDER_STATUS_CREATED.
 * Possible error status codes are:
 * OrderProcessStatusCodes.COUPON_INVALID - coupon is not active anymore or maximum amount of redemptions is reached
 * OrderProcessStatusCodes.ORDER_NOT_FAILED - order is not in status Order.ORDER_STATUS_FAILED
 * OrderProcessStatusCodes.INVENTORY_RESERVATION_FAILED - inventory reservation failed
 * @param {dw.order.Order} order - the order on which to undo the fail failOrder(Order)
 * @param {string} note - additional note
 * @returns {dw.system.Status} Status 'OK' or 'ERROR' with one of the error codes described above
 */
function undoFailOrder(order, note) {
    var status = new Status(Status.ERROR);
    if (!order
        || order.status.value !== Order.ORDER_STATUS_FAILED) {
        return status;
    }

    var oldStatus = order.status;
    Transaction.wrap(function () {
        status = OrderMgr.undoFailOrder(order);
        addOrderNote(order, note);
    });
    trackHistory(statusTypes.order, order, oldStatus, order.status, note);

    return status;
}

module.exports = {
    setConfirmationStatus: setConfirmationStatus,
    setExportStatus: setExportStatus,
    setPaymentStatus: setPaymentStatus,
    setShippingStatus: setShippingStatus,
    setAdditionalShippingStatus: setAdditionalShippingStatus,
    setStatus: setStatus,
    cancelOrder: cancelOrder,
    failOrder: failOrder,
    undoCancelOrder: undoCancelOrder,
    undoFailOrder: undoFailOrder
};
