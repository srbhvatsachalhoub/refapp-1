var Status = require('dw/system/Status');
var Transaction = require('dw/system/Transaction');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');

/**
 * converts milliseconds to days
 * @param {long} ms - milliseconds
 * @returns {long} - number of days
 */
function millisecondsToDays(ms) {
    var seconds = ms / 1000;
    var minutes = seconds / 60;
    var hours = minutes / 60;
    return hours / 24;
}

/**
 * Converts the badges into a string array (value of each badge)
 * @param {dw.value.EnumValue[]} badges - collection of badges
 * @returns {int} - index of badge
 */
function getBadgeValuesArray(badges) {
    var badgeValuesArray = [];
    for (var i = 0; i < badges.length; i++) {
        badgeValuesArray.push(badges[i].value);
    }
    return badgeValuesArray;
}

/**
 * Checks the existing badges of the product, according to the new flag value, adds or removes the incoming flag if necessary
 * @param {dw.catalog.Product} product - product to set the flag onto
 * @param {boolean} shouldBeFlagged - expected existance of the flag
 * @param {string} badgeValue - string value of the flag to be added
 */
function setFlagIfNecessary(product, shouldBeFlagged, badgeValue) {
    var badges = getBadgeValuesArray(product.custom.badges);
    var initialLengthOfBadges = badges.length;

    var indexOfBadge = badges.indexOf(badgeValue);

    if (shouldBeFlagged && indexOfBadge === -1) {
        badges.push(badgeValue);
    } else if (!shouldBeFlagged && indexOfBadge !== -1) {
        badges.splice(indexOfBadge, 1);
    }

    if (initialLengthOfBadges !== badges.length) {
        Transaction.wrap(function () {
            product.custom.badges = badges; // eslint-disable-line no-param-reassign
        });
    }
}

/**
 * Sets or Unsets new flag
 * @param {dw.catalog.Product} product - product to process
 * @param {long} daysToBeNew - number of days a product is assumed to be NEW
 * @param {Date} now - date of now
 */
function handleNewFlag(product, daysToBeNew, now) {
    if (!product.onlineFrom || !daysToBeNew) {
        return;
    }

    var daysOnline = millisecondsToDays(now - product.onlineFrom);
    var isNew = daysOnline < daysToBeNew ? true : null;

    setFlagIfNecessary(product, isNew, 'new');
}

/**
 * Sets or Unsets new flag
 * @param {dw.catalog.Product} product - product to process
 * @param {long} daysToBeNew - number of days a product is assumed to be NEW
 * @param {Date} now - date of now
 */
function handleSaleFlag(product) {
    var pricingHelper = require('*/cartridge/scripts/helpers/pricing');
    if (!product.priceModel || !product.priceModel.priceInfo || !product.priceModel.priceInfo.priceBook) {
        return;
    }
    var rootPriceBook = pricingHelper.getRootPriceBook(product.priceModel.priceInfo.priceBook);

    var regularPrice = product.priceModel.getPriceBookPrice(rootPriceBook.ID);
    var salePrice = product.priceModel.price;
    var isSale = salePrice < regularPrice ? true : null;

    setFlagIfNecessary(product, isSale, 'sale');
}

/**
 * @param {dw.util.HashMap} args inputs from the BM
 * Entry function for the job: traverses every product and sets related flags
 * @returns {dw.system.Status} status of the job step
 */
function setAutomaticFlags(args) {
    var daysToBeNew = args.DaysToBeNew;
    var now = new Date();

    var psm = new ProductSearchModel();
    psm.setCategoryID('root');
    psm.orderableProductsOnly = false;
    psm.recursiveCategorySearch = true;
    psm.search();
    var products = psm.getProductSearchHits();

    var distinctProducts = [];

    while (products.hasNext()) {
        var productHit = products.next();

        var product = productHit.product;
        if (distinctProducts.indexOf(product.ID) === -1) {
            distinctProducts.push(product.ID);
            handleNewFlag(product, daysToBeNew, now);
            handleSaleFlag(product);
        }

        var representedProducts = productHit.representedProducts.iterator();
        while (representedProducts.hasNext()) {
            product = representedProducts.next();
            if (distinctProducts.indexOf(product.ID) === -1) {
                distinctProducts.push(product.ID);
                handleNewFlag(product, daysToBeNew, now);
                handleSaleFlag(product);
            }
        }
    }

    return new Status(Status.OK);
}

module.exports = {
    setAutomaticFlags: setAutomaticFlags
};
