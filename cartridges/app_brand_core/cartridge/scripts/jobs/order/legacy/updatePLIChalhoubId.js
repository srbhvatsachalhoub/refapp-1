'use strict';

/**
 * Loops all legacy orders and order's product line items
 * to set value to custom.chalhoub_id if map has the sku
 * if map has no value then use product.id as chalhoub_id
 * @returns {dw.system.Status} Status
 */
function execute() {
    var logger = require('dw/system/Logger').getLogger('updatePLIChalhoubId'); // eslint-disable-line no-unused-vars
    var map = require('*/cartridge/scripts/jobs/order/legacy/skuChaloubIdMap');
    var Transaction = require('dw/system/Transaction');
    var collections = require('*/cartridge/scripts/util/collections');
    var OrderMgr = require('dw/order/OrderMgr');
    var Status = require('dw/system/Status');

    OrderMgr.processOrders(function (order) {
        collections.forEach(order.productLineItems, function (pli) {
            if (pli.custom.chalhoub_id) {
                return;
            }

            var productId = pli.productID.toUpperCase();
            Transaction.wrap(function () {
                pli.custom.chalhoub_id = map[productId] || productId; // eslint-disable-line no-param-reassign
            });
        });
    }, 'createdBy = {0}', 'legacy');

    return new Status(Status.OK);
}

module.exports = {
    execute: execute
};
