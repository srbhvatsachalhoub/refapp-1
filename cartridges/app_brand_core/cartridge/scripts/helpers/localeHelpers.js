'use strict';

/* global session, request, response, customer */

var Site = require('dw/system/Site');
var PriceBookMgr = require('dw/catalog/PriceBookMgr');
/**
 * Applies customer group specific priceBooks to session
 */
function setApplicablePriceBooks() {
    var collections = require('*/cartridge/scripts/util/collections');
    var sitePriceBooks = PriceBookMgr.getSitePriceBooks();
    var priceBooks = [];
    collections.forEach(sitePriceBooks, function (priceBook) {
        if (priceBook.currencyCode === session.currency.currencyCode) {
            priceBooks.push(priceBook);
        }
    });
    var customerGroups = customer.customerGroups;

    if (!customerGroups) {
        PriceBookMgr.setApplicablePriceBooks(priceBooks);
        return;
    }

    var priceBookIds = [];
    collections.forEach(customerGroups, function (customerGroup) {
        if (customerGroup && 'applicablePriceBooks' in customerGroup.custom && customerGroup.custom.applicablePriceBooks) {
            Array.prototype.push.apply(priceBookIds, Array.prototype.slice.apply(customerGroup.custom.applicablePriceBooks));
        }
    });
    priceBookIds.forEach(function (priceBookId) {
        var priceBook = PriceBookMgr.getPriceBook(priceBookId);
        if (priceBook) {
            priceBooks.push(priceBook);
        }
    });

    if (priceBooks && priceBooks.length) {
        PriceBookMgr.setApplicablePriceBooks(priceBooks);
    }
}

/**
 * Tries to find the revelant locale in LocaleModel
 * @param {string} locale - The locale to be searched
 * @param {string} countryCode - The countryCode to be searched if no locale provided
 * @returns {Object} the countries locale object if found, null otherwise
 */
function getLocale(locale, countryCode) {
    var LocaleModel = require('*/cartridge/models/locale');
    var Locale = require('dw/util/Locale');

    var currentSite = Site.getCurrent();
    var siteId = currentSite.getID();
    var allowedLocales = currentSite.allowedLocales;
    var currentLocale = Locale.getLocale(request.locale);
    var localeModel = new LocaleModel(currentLocale, allowedLocales, siteId);
    if (!localeModel || !localeModel.locale) {
        return null;
    }

    // check if the localeModel.locale is the desired locale if locale is provided
    if (locale && locale === localeModel.locale.localID) {
        return localeModel.locale;
    }

    var targetLocale;
    // check if the localeModel.locale is the desired country locale if countryCode is provided
    if (countryCode && countryCode === localeModel.locale.countryCode) {
        // if the locale is default then return the locale
        if (localeModel.locale.default) {
            return localeModel.locale;
        }

        // set targetLocale as first, and try to find the default one
        targetLocale = localeModel.locale;
    }

    // try to find locale/countryCode countries locale object
    var localLinks = localeModel.locale.localLinks;

    // Loop through localLinks to find relevant localLink
    for (var i = 0; i < localLinks.length; i++) {
        var localLink = localLinks[i];

        if ((locale && locale === localLink.localID) || (countryCode && countryCode === localLink.country)) {
            if (locale) {
                // found for provided locale
                targetLocale = localLink;
                break;
            }

            if (localLink.default) {
                // default one is found for the country
                targetLocale = localLink;
                break;
            } else if (!targetLocale) {
                // set the first one in countries
                // loop through to find the default one
                targetLocale = localLink;
            }
        }
    }

    return targetLocale;
}

/**
 * Sets the locale if different than the request locale
 * If locale not provided then tries to find the given country default locale
 * @param {string} locale - The locale to be set
 * @param {string} countryCode - The countryCode if no locale found
 * @returns {Object} the object that has the process is succeeded or not, also has the redirectURL if need to be used
 */
function setNewLocale(locale, countryCode) {
    var result = {
        locale: request.locale,
        success: false
    };

    // check the parameters
    // also check if the given locale is same with request locale, if so no need any action
    if ((!locale && !countryCode) || (locale && locale === request.locale)) {
        return result;
    }

    var countryLocale = getLocale(locale, countryCode);

    if (!countryLocale) {
        return result;
    }

    // found the new locale, set it to relevant places
    if (request.setLocale(countryLocale.localID)) {
        var Currency = require('dw/util/Currency');
        var BasketMgr = require('dw/order/BasketMgr');
        var Transaction = require('dw/system/Transaction');
        var urlHelpers = require('*/cartridge/scripts/helpers/urlHelpers');
        var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

        var currentSite = Site.getCurrent();
        var allowedCurrencies = currentSite.allowedCurrencies;
        var currency = Currency.getCurrency(countryLocale.currencyCode);
        if (currency && allowedCurrencies.indexOf(countryLocale.currencyCode) > -1
            && (countryLocale.currencyCode !== request.session.currency.currencyCode)) {
            session.setCurrency(currency);

            setApplicablePriceBooks();

            var currentBasket = BasketMgr.getCurrentBasket();
            if (currentBasket && currency && currentBasket.currencyCode !== currency.currencyCode) {
                Transaction.wrap(function () {
                    currentBasket.updateCurrency();
                });
            }
        }

        inventoryHelpers.checkBasketProductInventoryList();

        result.locale = countryLocale.localID;

        // locale has been set
        // prepare the url to be redirected
        var controllerAction = urlHelpers.getControllerActionFromHttpPath();
        if (!controllerAction) {
            return result;
        }

        var URLUtils = require('dw/web/URLUtils');

        var redirectUrl;
        if (countryLocale.host) {
            redirectUrl = URLUtils.url(controllerAction).host(countryLocale.host).toString();
        } else {
            redirectUrl = URLUtils.url(controllerAction).toString();
        }

        result.success = true;
        result.redirectURL = urlHelpers.appendQueryParams(redirectUrl, (request.httpQueryString ? request.httpQueryString.split('&') : {}));
    }
    return result;
}


/**
 * Sets the given locale to the request
 * Sets the given locale to the site local cookie
 * Checks and correct the currency if needed and provided
 * @param {string} localeId - the locale Id to be set
 * @param {string} currencyCode - the currency code to be set, optional
 * @returns {boolean} true locale set is success, false otherwise
 */
function setLocale(localeId, currencyCode) {
    if (!localeId || !request.setLocale(localeId)) {
        return false;
    }

    var cookieHelpers = require('*/cartridge/scripts/helpers/cookieHelpers');

    // set the selected locale into the cookie
    cookieHelpers.setSiteLocaleCookie(localeId);

    if (!currencyCode) {
        return true;
    }

    var currentSite = require('dw/system/Site').current;

    var allowedCurrencies = currentSite.allowedCurrencies;
    if (allowedCurrencies.indexOf(currencyCode) > -1
        && (currencyCode !== session.currency.currencyCode)) {
        var Currency = require('dw/util/Currency');
        var BasketMgr = require('dw/order/BasketMgr');

        var currency = Currency.getCurrency(currencyCode);
        session.setCurrency(currency);

        var currentBasket = BasketMgr.getCurrentBasket();
        if (currentBasket && currentBasket.currencyCode !== currency.currencyCode) {
            var Transaction = require('dw/system/Transaction');

            Transaction.wrap(function () {
                currentBasket.updateCurrency();
            });
        }
    }


    return true;
}

/**
 * Setup site for the given locale id
 * Finds the country locale object that belongs to given locale id
 * Sets the currency to the session
 * Sets the applicable price books
 * Updates currency for the current basket if needed
 * Setup the inventory list
 * Sets the locale cookie
 * @param {string} localeId - The locale id to be set
 */
function setCountryLocale(localeId) {
    // try to get countries locale object for the selected
    var countryLocale = getLocale(localeId);
    if (!countryLocale) {
        return;
    }

    var BasketMgr = require('dw/order/BasketMgr');
    var Currency = require('dw/util/Currency');
    var cookieHelpers = require('*/cartridge/scripts/helpers/cookieHelpers');
    var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

    var currency = Currency.getCurrency(countryLocale.currencyCode);
    session.setCurrency(currency);
    setApplicablePriceBooks();

    var currentBasket = BasketMgr.getCurrentBasket();
    if (currentBasket && currency && currentBasket.currencyCode !== currency.currencyCode) {
        var Transaction = require('dw/system/Transaction');
        var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
        var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

        Transaction.wrap(function () {
            currentBasket.updateCurrency();

            cartHelper.ensureAllShipmentsHaveMethods(currentBasket);
            basketCalculationHelpers.calculateTotals(currentBasket);
        });
    }

    inventoryHelpers.checkBasketProductInventoryList();
    cookieHelpers.setSiteLocaleCookie(countryLocale.localID);
}

/**
 * Gets the current locale's country code
 * @returns {string} the current request locale's country code
 */
function getCurrentCountryCode() {
    var Locale = require('dw/util/Locale');

    var currentLocale = Locale.getLocale(request.locale);
    return currentLocale ? currentLocale.country : null;
}

module.exports = {
    setLocale: setLocale,
    setNewLocale: setNewLocale,
    getLocale: getLocale,
    setApplicablePriceBooks: setApplicablePriceBooks,
    setCountryLocale: setCountryLocale,
    getCurrentCountryCode: getCurrentCountryCode
};
