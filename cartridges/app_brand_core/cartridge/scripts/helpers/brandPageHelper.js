'use strict';
var CatalogMgr = require('dw/catalog/CatalogMgr');
var URLUtils = require('dw/web/URLUtils');
var PageMgr = require('dw/experience/PageMgr');

/**
 * Get Brand category for current category
 *
 * @param {dw.catalog.Category} category - category
 * @return {dw.catalog.Category} - brand category
 */
function getBrandCategory(category) {
    if (!category) {
        return null;
    }

    var currentCategory = category;
    var currentCategoryBrandPageType = currentCategory.getCustom().uniqueBrandPageType.value;
    var parent = category.getParent();
    var flag = false;

    if (currentCategoryBrandPageType) {
        return currentCategory;
    }

    while (parent && !parent.isRoot()) {
        var parentCategoryBrandPageType = parent.getCustom().uniqueBrandPageType;

        currentCategory = parent;

        if (parentCategoryBrandPageType && parentCategoryBrandPageType.value) {
            flag = true;
            break;
        }

        parent = parent.getParent();
    }

    return flag ? currentCategory : null;
}

/**
 * Get category url
 * @param {dw.catalog.Category} category - Current category
 * @returns {string} - Url of the category
 */
function getCategoryUrl(category) {
    return category.custom && 'alternativeUrl' in category.custom && category.custom.alternativeUrl
        ? category.custom.alternativeUrl
        : URLUtils.url('Search-Show', 'cgid', category.getID()).toString();
}

/**
 * Get Brand Header object (secondary navigation)
 *
 * @param {dw.catalog.Category} category - category
 * @param {string} pageType - pageType
 * @param {string} productId product Id
 * @return {Object} - info for brand Header
 */
function getUniqueBrandPageData(category, pageType, productId) {
    var Categories = require('*/cartridge/models/categories');

    var result = {
        brandHeader: null,
        brandID: null
    };
    var brandsRootCategory = null;
    var brandCategory = null;
    var uniqueBrandPageType = null;
    var brandPageTypesHasUniquePDP = ['type_2'];
    var brandPageTypesHasUniquePLP = ['type_2'];
    if (pageType === 'pdp' && productId) {
        var ProductMgr = require('dw/catalog/ProductMgr');
        var apiProduct = ProductMgr.getProduct(productId);
        if (apiProduct) {
            apiProduct = apiProduct.variant ? apiProduct.masterProduct : apiProduct;
            var categories = apiProduct.categories;
            var categoryItr = categories.iterator();
            while (categoryItr.hasNext()) {
                var assignedCategory = categoryItr.next();
                if (assignedCategory.custom.uniqueBrandPageType) {
                    brandCategory = assignedCategory;
                    break;
                }
            }
        }
    }
    // if brandCategory is not already assigned
    if (category && !brandCategory) {
        if (typeof category === 'string') {
            category = CatalogMgr.getCategory(category); // eslint-disable-line
        }

        brandCategory = getBrandCategory(category);
        if (brandCategory) {
            brandsRootCategory = brandCategory.getParent();
        }
    }

    if (brandCategory) {
        var uniqueBrandPageTypeAttribute = brandCategory.getCustom().uniqueBrandPageType;

        uniqueBrandPageType = uniqueBrandPageTypeAttribute ? uniqueBrandPageTypeAttribute.value : null;
    }

    if (!brandCategory || !uniqueBrandPageType) {
        return result;
    }

    result.isUniqueBrandProductPage = brandPageTypesHasUniquePDP.indexOf(uniqueBrandPageType) > -1;
    result.isUniqueBrandProductListingPage = brandPageTypesHasUniquePLP.indexOf(uniqueBrandPageType) > -1;

    if (pageType && pageType === 'pdp' && !result.isUniqueBrandProductPage) {
        return result;
    }

    var req = request; // eslint-disable-line

    result.brandID = brandCategory.ID;
    result.brandPageClassName = 'page-unique-brand page-unique-brand-' + uniqueBrandPageType;
    result.brandsRootCategory = brandsRootCategory;

    var topLevelCategories = brandCategory.getSubCategories();

    var brandMenuContentPagesList = brandCategory.getCustom().brandMenuContentPages;
    var brandMenuContentPages = null;
    var currentContentPageID = null;

    if (req.httpParameterMap.cid && req.httpParameterMap.cid.stringValue) {
        currentContentPageID = req.httpParameterMap.cid.stringValue;
    }

    if (brandMenuContentPagesList) {
        brandMenuContentPages = [];

        brandMenuContentPagesList.forEach(function (contentPageID) {
            var brandContentPage = PageMgr.getPage(contentPageID);

            if (brandContentPage != null && brandContentPage.isVisible()) {
                brandMenuContentPages.push({
                    url: URLUtils.url('Page-Show', 'cid', contentPageID),
                    name: brandContentPage.name,
                    active: contentPageID === currentContentPageID
                });
            }
        });
    }

    result.brandCategoryUrl = getCategoryUrl(brandCategory);

    result.brandHeader = {
        categories: new Categories(topLevelCategories, false, true).categories,
        brandContentPages: brandMenuContentPages,
        brandLogo: {
            alt: brandCategory.displayName,
            image: brandCategory.getCustom().brandLogoImage,
            link: result.brandCategoryUrl
        }
    };

    return result;
}

module.exports.getUniqueBrandPageData = getUniqueBrandPageData;
module.exports.getCategoryUrl = getCategoryUrl;
