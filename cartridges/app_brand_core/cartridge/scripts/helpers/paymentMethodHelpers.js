'use strict';

var PaymentInstrument = require('dw/order/PaymentInstrument');
var KNET_PAYMENT_METHOD = 'KNET';
var PAYPAL_PAYMENT_METHOD = 'PAYPAL';
var COD_PAYMENT_METHOD = 'COD';
var PREPAID_PAYMENT_METHOD = 'PREPAID';

/**
 * Checks if the payment method is complementary payment method
 * complementary methods:
 * GIFT_CERTIFICATE
 *
 * regular methods:
 * CREDIT_CARD
 * DW_APPLE_PAY
 * KNET
 * COD (Cash On Delivery)
 * PREPAID
 * @param {boolean} paymentMethod - the payment method to be checked
 * @returns {boolean} true if complementary, false otherwise
 */
function isComplementaryPaymentMethod(paymentMethod) {
    switch (paymentMethod) {
        case PaymentInstrument.METHOD_GIFT_CERTIFICATE:
            return true;
        case PaymentInstrument.METHOD_CREDIT_CARD:
        case PaymentInstrument.METHOD_DW_APPLE_PAY:
        case KNET_PAYMENT_METHOD:
        case PAYPAL_PAYMENT_METHOD:
        case COD_PAYMENT_METHOD:
        case PREPAID_PAYMENT_METHOD:
        default:
            return false;
    }
}

/**
 * Gets the priority of the payment method
 * is being used authorizing the payment instruments in order
 * complementary methods should be authorized first
 * GIFT_CERTIFICATE
 *
 * regular methods:
 * CREDIT_CARD
 * DW_APPLE_PAY
 * KNET
 * COD (Cash On Delivery)
 * PREPAID
 * @param {boolean} paymentMethod - the payment method to be checked
 * @returns {number} true if complementary, false otherwise
 */
function getPriority(paymentMethod) {
    switch (paymentMethod) {
        case PaymentInstrument.METHOD_GIFT_CERTIFICATE:
            return 1;
        case PaymentInstrument.METHOD_CREDIT_CARD:
        case PaymentInstrument.METHOD_DW_APPLE_PAY:
        case KNET_PAYMENT_METHOD:
        case PAYPAL_PAYMENT_METHOD:
        case COD_PAYMENT_METHOD:
        case PREPAID_PAYMENT_METHOD:
        default:
            return 2;
    }
}

/**
 * Calculates the given paymentMethod used amount in LineItemCtnr
 * @param {dw.order.LineItemCtnr} lineItemCtnr - the LineItemCtnr object
 * @param {string} paymentMethodId - the payment method Id to be calculated
 * @returns {dw.value.Money} the total used amount as money
 */
function getLineItemCtnrPaymentMethodAmount(lineItemCtnr, paymentMethodId) {
    if (!lineItemCtnr || !paymentMethodId) {
        return null;
    }

    var Money = require('dw/value/Money');

    var totalUsed = new Money(0.0, lineItemCtnr.getCurrencyCode());
    var paymentInstruments = lineItemCtnr.getPaymentInstruments(paymentMethodId).iterator();
    var paymentInstrument = null;

    // calculates the total used amount in current lineItemCtnr
    while (paymentInstruments.hasNext()) {
        paymentInstrument = paymentInstruments.next();
        totalUsed = totalUsed.add(paymentInstrument.paymentTransaction.amount);
    }

    return totalUsed;
}

/**
 * Sorts the payment instruments into Array by using LineItemCtnr
 * @param {dw.order.LineItemCtnr} lineItemCtnr - the LineItemCtnr object
 * @returns {Array} the sorted payment instruments
 */
function sortPaymentInstrumentsIntoArray(lineItemCtnr) {
    if (!lineItemCtnr) {
        return [];
    }

    var paymentInstrumentsArray = lineItemCtnr.paymentInstruments.toArray();
    return paymentInstrumentsArray.sort(function (pi1, pi2) {
        return getPriority(pi1.paymentMethod) - getPriority(pi2.paymentMethod);
    });
}


module.exports = {
    isComplementaryPaymentMethod: isComplementaryPaymentMethod,
    getKnetPaymentMethodId: function () {
        return KNET_PAYMENT_METHOD;
    },
    getPayPalPaymentMethodId: function () {
        return PAYPAL_PAYMENT_METHOD;
    },
    getCODPaymentMethodId: function () {
        return COD_PAYMENT_METHOD;
    },
    getPrePaidPaymentMethodId: function () {
        return PREPAID_PAYMENT_METHOD;
    },
    getLineItemCtnrPaymentMethodAmount: getLineItemCtnrPaymentMethodAmount,
    sortPaymentInstrumentsIntoArray: sortPaymentInstrumentsIntoArray
};
