var imageUtil = {};

/**
 * Return an object containing the scaled image for mobile, table and desktop.
 *
 * @param {Object} images - images
 *
 * @return {Object} pictureObj
 */
imageUtil.transformImagesToPicture = function (images) {
    var pictureObj = null;
    var deviceModes = ['mobile', 'tablet', 'desktop'];
    var sources = {};
    var objectHasImages = false;

    if (images) {
        pictureObj = {
            src: sources
        };

        Object.keys(images).forEach(function (key) {
            if (deviceModes.indexOf(key) > -1) {
                var image = images[key];

                if (image) {
                    objectHasImages = true;
                    sources[key] = image.getURL();
                }
            }
        });

        if (!objectHasImages) {
            pictureObj = null;
        }

        if (!Object.hasOwnProperty.call(sources, 'tablet') && Object.hasOwnProperty.call(sources, 'desktop')) {
            // If no tablet image provided, make it same as desktop
            sources.tablet = sources.desktop;
        }
    }

    return pictureObj;
};

module.exports = imageUtil;
