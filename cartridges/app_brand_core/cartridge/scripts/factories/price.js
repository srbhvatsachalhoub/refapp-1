'use strict';

var Money = require('dw/value/Money');
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
var base = module.superModule;
var worthPriceAvailableActions = {
    'Tile-Show': true,
    'Product-Show': true,
    'Product-ShowQuickView': true,
    'Product-Variation': true
};

/**
 * Convert API price to an object
 * @param {dw.value.Money} price - Price object returned from the API
 * @returns {Object} price formatted as a simple object
 */
function toPriceModel(price) {
    var value = price.available ? price.getDecimalValue().get() : null;
    var currency = price.available ? price.getCurrencyCode() : null;
    var formattedPrice = price.available ? renderTemplateHelper.getRenderedHtml({ price: price }, 'product/components/pricing/currencyFormatted') : null;
    var decimalPrice;

    if (formattedPrice) { decimalPrice = price.getDecimalValue().toString(); }

    return {
        value: value,
        currency: currency,
        formatted: formattedPrice,
        decimalPrice: decimalPrice
    };
}

/**
 * Adds worth price if requested from an appropriate page
 * @param {dw.catalog.Product|dw.catalog.productSearchHit} inputProduct API object for a product
 * @param {DefaultPrice} basePrice The product's price
 */
function addWorthPrice(inputProduct, basePrice) {
    var urlHelper = require('*/cartridge/scripts/helpers/urlHelpers');

    var action = urlHelper.getControllerActionFromHttpPath();
    if (inputProduct.custom.worth && worthPriceAvailableActions[action] && basePrice.sales.currency) {
        basePrice.worth = toPriceModel(new Money(inputProduct.custom.worth, basePrice.sales.currency)); // eslint-disable-line no-param-reassign
    }
}

/**
 * Retrieves Price instance
 *
 * @param {dw.catalog.Product|dw.catalog.productSearchHit} inputProduct - API object for a product
 * @param {string} currency - Current session currencyCode
 * @param {boolean} useSimplePrice - Flag as to whether a simple price should be used, used for
 *     product tiles and cart line items.
 * @param {dw.util.Collection<dw.campaign.Promotion>} promotions - Promotions that apply to this
 *                                                                 product
 * @param {dw.catalog.ProductOptionModel} currentOptionModel - The product's option model
 * @return {TieredPrice|RangePrice|DefaultPrice} - The product's price
 */
function getPrice(inputProduct, currency, useSimplePrice, promotions, currentOptionModel) {
    var basePrice = base.getPrice.call(this, inputProduct, currency, useSimplePrice, promotions, currentOptionModel);
    addWorthPrice(inputProduct, basePrice);
    return basePrice;
}

module.exports = {
    getPrice: getPrice
};
