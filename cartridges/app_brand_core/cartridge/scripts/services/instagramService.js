'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var StringUtils = require('dw/util/StringUtils');
var baseUrl;
var serviceImpl = LocalServiceRegistry.createService('instagram.api', {
    createRequest: function (svc, payload) { // eslint-disable-line consistent-return
        var igUser = payload.user;
        var credential = svc.getConfiguration().getCredential();
        var queryUrl = baseUrl || StringUtils.format(credential.URL, igUser);
        if (payload.max_id) {
            queryUrl += '&max_id=' + payload.max_id;
        }
        svc.setURL(queryUrl);
        svc.setRequestMethod('GET');
    },
    parseResponse: function (svc, client) {
        return JSON.parse(client.text);
    },
    mockCall: function () {
        return {
            statusCode: 200,
            statusMessage: 'Success',
            text: JSON.stringify({
                graphql: {
                    user: {
                        username: 'IG Username',
                        edge_owner_to_timeline_media: {
                            edges: []
                        }
                    }
                }
            })
        };
    }
});


module.exports = serviceImpl;

