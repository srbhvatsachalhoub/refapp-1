'use strict';

var decorators = require('*/cartridge/models/product/decorators/index');
var promotionCache = require('*/cartridge/scripts/util/promotionCache');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');
var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

/**
 * Get product search hit for a given product
 * @param {dw.catalog.Product} apiProduct - Product instance returned from the API
 * @returns {dw.catalog.ProductSearchHit} - product search hit for a given product
 */
function getProductSearchHit(apiProduct) {
    var searchModel = new ProductSearchModel();
    searchModel.setSearchPhrase(apiProduct.ID);
    searchModel.search();

    if (searchModel.count === 0) {
        searchModel.setSearchPhrase(apiProduct.ID.replace(/-/g, ' '));
        searchModel.search();
    }

    var hit = searchModel.getProductSearchHit(apiProduct);
    if ((!hit || !searchModel.getProductSearchHits()) && searchModel.getProductSearchHits().hasNext()) {
        var tempHit = searchModel.getProductSearchHits().next();
        if (tempHit.firstRepresentedProductID === apiProduct.ID) {
            hit = tempHit;
        }
    }
    return hit;
}

/**
 * Decorate product with product tile information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {string} productType - Product type information
 * @param {Object} options - Options passed in from the factory
 *
 * @returns {Object} - Decorated product model
 */
module.exports = function productTile(product, apiProduct, productType, options) {
    var productSearchHit = getProductSearchHit(apiProduct);
    decorators.description(product, apiProduct);
    decorators.sellable(product, apiProduct);
    decorators.availability(
        product,
        options.quantity,
        apiProduct.minOrderQuantity.value,
        inventoryHelpers.getProductAvailabilityModel(apiProduct)
    );
    decorators.base(product, apiProduct, productType);
    decorators.searchPrice(product, productSearchHit, promotionCache.promotions, getProductSearchHit);
    decorators.images(product, apiProduct, { types: ['medium'], quantity: 'single' });
    decorators.ratings(product);
    if (productType === 'set') {
        decorators.setProductsCollection(product, apiProduct);
    }
    decorators.searchVariationAttributes(product, productSearchHit);
    decorators.price(product, apiProduct, options.promotions, false, options.optionModel);
    decorators.badges(product, apiProduct, options.promotions);
    decorators.selectedVariationAttributes(product, apiProduct, options.variationModel);
    decorators.customBadge(product, apiProduct);
    decorators.customization(product, apiProduct);
    decorators.category(product, apiProduct, productType);
    decorators.custom(product, apiProduct);
    return product;
};
