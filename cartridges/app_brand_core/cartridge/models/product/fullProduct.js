'use strict';
var base = module.superModule;

var decorators = require('*/cartridge/models/product/decorators/index');
var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

/**
 * Decorate product with full product information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {Object} options - Options passed in from the factory
 *
 * @returns {Object} - Decorated product model
 */
module.exports = function fullProduct(product, apiProduct, options) {
    decorators.sellable(product, apiProduct);
    decorators.customization(product, apiProduct);
    decorators.availability(product, options.quantity, apiProduct.minOrderQuantity.value, inventoryHelpers.getProductAvailabilityModel(apiProduct));
    base.call(this, product, apiProduct, options);
    if (options.variationModel) {
        decorators.images(product, options.variationModel, { types: ['hi-res', 'large', 'small'], quantity: 'all' });
    } else {
        decorators.images(product, apiProduct, { types: ['hi-res', 'large', 'small'], quantity: 'all' });
        product.variationAttributes = decorators.standardVariationAttributes(product, apiProduct, options.variationModel); // eslint-disable-line no-param-reassign
    }
    decorators.recommendations(product, apiProduct);
    decorators.backInStock(product, apiProduct);
    decorators.badges(product, apiProduct, options.promotions);
    decorators.category(product, apiProduct, options.productType);
    decorators.custom(product, apiProduct);
    decorators.tabs(product, apiProduct);
    decorators.customBadge(product, apiProduct);
    decorators.backgroundImage(product, apiProduct);
    decorators.contentAssets(product, apiProduct);
    return product;
};
