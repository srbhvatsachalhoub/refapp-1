'use strict';

var base = module.superModule;

module.exports = function (object, product, promotions, useSimplePrice, currentOptions) {
    base.call(this, object, product, promotions, useSimplePrice, currentOptions);
    if (object && object.available && object.price && object.price.sales && !object.price.sales.value) {
        object.available = false; // eslint-disable-line no-param-reassign
    }
    if (object && object.price && object.price.list && object.price.list.value && object.price.sales && object.price.sales.value && object.price.list.value > object.price.sales.value) {
        var discountRate = ((1 - (object.price.sales.value / object.price.list.value)) * 100).toFixed(0);
        Object.defineProperty(object.price, 'discountRate', {
            enumerable: true,
            value: discountRate
        });
    }
};
