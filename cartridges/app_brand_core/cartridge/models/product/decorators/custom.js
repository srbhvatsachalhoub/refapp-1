
'use strict';
module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'custom', {
        enumerable: true,
        value: apiProduct.custom
    });
};
