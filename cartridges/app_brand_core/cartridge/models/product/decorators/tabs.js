

'use strict';

var CatalogMgr = require('dw/catalog/CatalogMgr');

/**
 * Checks if given category has an ancestor category with given ancestorCategoryId
 * @param {*} category Category object that needs to be checked if has an ancestor with given id
 * @param {string} ancestorCategoryId Ancestor category id
 * @returns {boolean} true if given category has ancestor category with given id
 */
function hasCategoryAsAncestor(category, ancestorCategoryId) {
    if (!category.parent) {
        return false;
    }

    if (category.parent.ID === ancestorCategoryId) {
        return true;
    }
    return hasCategoryAsAncestor(category.parent, ancestorCategoryId);
}

/**
 * Gets category responsible of holding the product tabs
 * @param {dw.catalog.Product} apiProduct product
 * @param {dw.catalog} catalog catalog
 * @returns {dw.catalog.Category} category
 */
function getTabsCategory(apiProduct, catalog) {
    if (!catalog || !catalog.custom || !catalog.custom.productTabsCategory) {
        return null;
    }

    var categoryId = catalog.custom.productTabsCategory;
    var categories = apiProduct.categories;

    if (categories && !categories.length && apiProduct.variant) {
        categories = apiProduct.masterProduct.categories;
    }

    for (var i = 0; i < categories.length; i++) {
        var category = categories[i];
        if (hasCategoryAsAncestor(category, categoryId)) {
            return category;
        }
    }
    return null;
}

/**
 * Gets tabs content from supplied source (category or product),
 * if cannot find and category has aparent then checks parent,
 * otherwise returns empty array
 * @param {*} sourceObj Category or product object that needs to be checked for tab definitions
 * @param {string} sourceType The type of source object ("category" or "product")
 * @param {string} tabsStr Comma separated values of tab definitions coming from source type
 * @param {dw.catalog} catalog catalog
 * @param {dw.catalog.Product} apiProduct apiProduct
 * @returns {Object[]} returns tab contents
 */
function getTabsFromSource(sourceObj, sourceType, tabsStr, catalog, apiProduct) {
    var productTabs = [];

    if (sourceObj && sourceObj.custom && tabsStr) {
        var propertyIds = tabsStr.trim().split(/[\s,]+/);
        var titleDefinitions = {};

        // Get localizable title definitions
        if (catalog && catalog.custom && catalog.custom.productTabsTitleDefinitions) {
            titleDefinitions = JSON.parse(catalog.custom.productTabsTitleDefinitions);
        }

        for (var i = 0; i < propertyIds.length; i++) {
            var propertyId = propertyIds[i];
            var content = null;
            if (propertyId in apiProduct.custom && apiProduct.custom[propertyId]) {
                content = apiProduct.custom[propertyId];
            } else if (propertyId in apiProduct && apiProduct[propertyId]) {
                content = apiProduct[propertyId];
            } else {
                continue;
            }

            if (content) {
                productTabs.push({
                    key: propertyId,
                    title: titleDefinitions['product.' + propertyId] || propertyId,
                    content: content,
                    type: sourceType
                });
            }
        }
    }

    return productTabs;
}

/**
 * Gets tabs content from product, if cannot find any tabs returns empty array
 * @param {dw.catalog.Product} apiProduct api product object
 * @param {dw.catalog} catalog catalog
 * @returns {Object[]} returns tab contents
 */
function getTabsFromProduct(apiProduct, catalog) {
    if (apiProduct && apiProduct.custom.tabs) {
        return getTabsFromSource(apiProduct, 'product', apiProduct.custom.tabs, catalog, apiProduct);
    }
    return [];
}

/**
 * Gets tabs content from category, if cannot find and category has aparent then checks parent, otherwise returns empty array
 * @param {*} category Category object that needs to be checked if has productTabs configured
 * @param {dw.catalog} catalog catalog
 * @param {dw.catalog.Product} apiProduct apiProduct
 * @returns {Object[]} returns tab contents
 */
function getTabsFromCategory(category, catalog, apiProduct) {
    if (category && category.custom.productTabs) {
        return getTabsFromSource(category, 'category', category.custom.productTabs, catalog, apiProduct);
    }
    return [];
}

/**
 * Gets tabs from category, if cannot find and category has aparent then checks parent, otherwise returns null
 * @param {dw.catalog.Category} category Category object that needs to be checked if has productTabs configured
 * @param {dw.catalog} catalog catalog
 * @param {dw.catalog.Product} apiProduct apiProduct
 * @returns {Object[]} returns tab contents
 */
function getTabsFromCategoryRecursively(category, catalog, apiProduct) {
    if (category && category.custom.productTabs) {
        return getTabsFromCategory(category, catalog, apiProduct);
    }

    if (category && category.parent) {
        return getTabsFromCategoryRecursively(category.parent, catalog, apiProduct);
    }
    return null;
}

/**
 * Returns tab definition list
 * @param {dw.catalog.Product} apiProduct api product object
 * @returns {string[]} returns an array of property ids
 */
function getTabs(apiProduct) {
    var result = [];
    var catalog = CatalogMgr.getSiteCatalog();
    if (!catalog || !catalog.custom || !catalog.custom.productTabsSourceOrder) {
        return result;
    }

    // Prepare tab definitions with respect to corresponding source
    var category = getTabsCategory(apiProduct, catalog);
    var tabSourceTypes = catalog.custom.productTabsSourceOrder.trim().split(/[\s,]+/);
    var tabs = {
        category: getTabsFromCategoryRecursively(category, catalog, apiProduct),
        product: getTabsFromProduct(apiProduct, catalog)
    };

    // Merge category and product tabs with respect to the order defined within "productTabsSourceOrder"
    for (var i = 0; i < tabSourceTypes.length; i++) {
        var type = tabSourceTypes[i];
        if (tabs[type]) {
            result = result.concat(tabs[type]);
        }
    }

    return result;
}

module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'tabs', {
        enumerable: true,
        value: getTabs(apiProduct)
    });
};
