'use strict';

var Money = require('dw/value/Money');
var DefaultPrice = require('*/cartridge/models/price/default');
var priceFactory = require('*/cartridge/scripts/factories/price');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
var collections = require('*/cartridge/scripts/util/collections');
var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

var ALTERNATIVE = 1;
var COMPLEMENTARY = 2;

/**
 * Returns an array of recommended product ids
 * @param {dw.catalog.Product} apiProduct api product object
 * @param {number} recommendationType - recommendation type
 * @returns {string[]} returns category of product
 */
function getProductsByRecommendationType(apiProduct, recommendationType) {
    var recommendations = [];
    var recommendationIds = [];
    var decimalPriceTotalSales = 0;
    var decimalPriceTotalList = 0;
    var currencyCode;

    // Prepare recommendation items and total prices
    collections.forEach(apiProduct.getRecommendations(recommendationType), function (recommendedProduct) {
        var apiRecommendedProduct = recommendedProduct.recommendedItem;
        var availabilityModel = inventoryHelpers.getProductAvailabilityModel(apiRecommendedProduct);
        if (!availabilityModel.orderable ||
            !apiRecommendedProduct.online ||
            (apiRecommendedProduct.master && apiRecommendedProduct.variants.length === 0)) {
            return;
        }

        recommendations.push(apiRecommendedProduct);
        recommendationIds.push(recommendedProduct.recommendedItemID);
        var options = productHelper.getConfig(apiRecommendedProduct, {});
        var price = priceFactory.getPrice(apiRecommendedProduct, null, false, options.promotions, options.optionModel);
        if (!price.sales) {
            if (price.min && price.min.sales) {
                decimalPriceTotalSales += price.min.sales.value;
                decimalPriceTotalList += (price.min.list || price.min.sales).value;
                return;
            }
            return;
        }
        if (price.sales.currency) {
            currencyCode = price.sales.currency;
        }
        decimalPriceTotalSales += price.sales.value;
        decimalPriceTotalList += (price.list || price.sales).value;
    });

    // Prepare formatted price
    var priceTotalSales = new Money(decimalPriceTotalSales, currencyCode);
    var priceTotalList = decimalPriceTotalSales < decimalPriceTotalList ? new Money(decimalPriceTotalList, currencyCode) : null;
    var totalPriceFormatted = new DefaultPrice(priceTotalSales, priceTotalList);

    // Return recommendation object
    return {
        items: recommendations,
        itemIdsArr: recommendationIds,
        itemIdsStr: recommendationIds.join(','),
        totalPrice: totalPriceFormatted
    };
}

module.exports = function (object, apiProduct) {
    // Appends id list of alternative products as a property to product model
    Object.defineProperty(object, 'recommendationsAlternative', {
        enumerable: true,
        value: getProductsByRecommendationType(apiProduct, ALTERNATIVE)
    });

    // Appends id list of complementary products as a property to product model
    Object.defineProperty(object, 'recommendationsComplementary', {
        enumerable: true,
        value: getProductsByRecommendationType(apiProduct, COMPLEMENTARY)
    });
};
