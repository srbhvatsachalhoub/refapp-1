'use strict';

module.exports = function (object, product) {
    var isEngravingEnabled =
        'enableProductEngraving' in product.custom &&
        product.custom.enableProductEngraving &&
        product.custom.enableProductEngraving.value === 'true';
    var isPersonalizationEnabled =
        'enableProductPersonalization' in product.custom &&
        product.custom.enableProductPersonalization &&
        product.custom.enableProductPersonalization.value === 'true';

    Object.defineProperty(object, 'customization', {
        enumerable: true,
        writable: true,
        value: {
            isEngravingEnabled: isEngravingEnabled,
            isPersonalizationEnabled: isPersonalizationEnabled,
            isCustomizable: isEngravingEnabled || isPersonalizationEnabled
        }
    });
};
