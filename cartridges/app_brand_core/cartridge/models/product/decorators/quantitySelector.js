'use strict';

var URLUtils = require('dw/web/URLUtils');
var urlHelper = require('*/cartridge/scripts/helpers/urlHelpers');

/**
 * Creates an array of numbers between min and max
 * @param {number} min min value of the array
 * @param {number} max max value of the array
 * @param {number} step incremental step value between each array item
 * @param {array} arr the array that needs to be filled with numbers
 * @param {function} callback callback function that will be called for each item added to array
 * @return {number[]} array of numbers
 */
function range(min, max, step, arr, callback) {
    if (min > max) return arr;
    arr.push(callback ? callback(min) : min);
    return range(min + (Math.abs(step) || 1), max, step, arr, callback);
}

/**
 * Compile quantity meta for pull-down menu selection
 *
 * @param {number} minOrderQty - Minimum order quantity
 * @param {number} maxOrderQty - Maximum order quantity
 * @param {number} stepQuantity - Quantity increment from one value to the next
 * @param {string} selectedQty - Quanity selected
 * @param {string} pid - Product ID
 * @param {Object} attributes - Variation attribute query params
 * @param {ProductOptions[]} options - Product options query params
 * @return {Array} - Quantity options for PDP pull-down menu
 */
function getQuantities(minOrderQty, maxOrderQty, stepQuantity, selectedQty, pid, attributes, options) {
    var baseUrl = URLUtils.url('Product-Variation', 'pid', pid).relative().toString();
    var params = {
        options: options || [],
        variables: attributes || {}
    };
    return range(minOrderQty, maxOrderQty, stepQuantity, [], function (quantity) {
        params.quantity = quantity.toString();
        return {
            value: params.quantity,
            selected: params.quantity === selectedQty.toString(),
            url: urlHelper.appendQueryParams(baseUrl, params)
        };
    });
}

module.exports = function (object, stepQuantity, attributes, options) {
    Object.defineProperty(object, 'quantities', {
        enumerable: true,
        value: getQuantities(
            object.minOrderQuantity,
            object.maxOrderQuantity,
            stepQuantity,
            object.selectedQuantity,
            object.id,
            attributes,
            options)
    });
};
