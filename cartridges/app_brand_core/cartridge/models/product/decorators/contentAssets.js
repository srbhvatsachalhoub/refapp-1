'use strict';

var getContentAsset = require('*/cartridge/scripts/helpers/productHelpers').getProductContentAsset;

module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'contentAssets', {
        enumerable: true,
        writable: true,
        value: {
            exclusiveOffer: getContentAsset(apiProduct.ID, 'exclusive-offer')
        }
    });
};
