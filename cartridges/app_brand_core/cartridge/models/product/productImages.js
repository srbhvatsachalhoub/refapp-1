'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var ProductImageDIS = require('*/cartridge/scripts/helpers/productImageDIS');

/**
 * Normalizes the text (alt and title values)
 * Delicate Cherry Blossom Gift Set, -> Delicate Cherry Blossom Gift Set
 * @param {string} title - the string value
 * @returns {string} normalized string
 */
function normalizeValue(title) {
    if (!title) {
        return title;
    }

    return title.split(',').filter(function (el) {
        return !!(el.trim());
    }).join(',');
}

/**
 * @constructor
 * @classdesc Returns images for a given product
 * @param {dw.catalog.Product} product - product to return images for
 * @param {Object} imageConfig - configuration object with image types
 */
function Images(product, imageConfig) {
    imageConfig.types.forEach(function (type) {
        var images = ProductImageDIS.getImages(product, type);
        var result = {};

        if (images.length) {
            if (imageConfig.quantity === 'single') {
                var firstImage = collections.first(images);
                if (firstImage) {
                    result = [{
                        alt: normalizeValue(firstImage.getAlt()),
                        url: firstImage.getURL() && firstImage.getURL().toString(),
                        title: normalizeValue(firstImage.getTitle()),
                        index: '0',
                        absUrl: firstImage.getURL() && firstImage.getURL().toString()
                    }];
                }
            } else {
                result = collections.map(images, function (image, index) {
                    return {
                        alt: normalizeValue(image.getAlt()),
                        url: image.getURL() && image.getURL().toString(),
                        index: index.toString(),
                        title: normalizeValue(image.getTitle()),
                        absUrl: image.getURL() && image.getURL().toString()
                    };
                });
            }
        } else {
            var URLUtils = require('dw/web/URLUtils');
            var Resource = require('dw/web/Resource');

            result = [{
                url: URLUtils.staticURL('/images/noimagelarge.png').toString(),
                alt: Resource.msgf('msg.no.image', 'common', null),
                title: Resource.msgf('msg.no.image', 'common', null),
                noimage: true
            }];
        }
        this[type] = result;
    }, this);
}

module.exports = Images;
