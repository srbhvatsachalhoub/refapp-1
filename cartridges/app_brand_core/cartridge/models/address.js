'use strict';
/* global request */
var base = module.superModule;
var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');

/**
 * Address class that represents an orderAddress
 * @param {dw.order.OrderAddress} addressObject - User's address
 * @constructor
 */
function address(addressObject) {
    base.call(this, addressObject);
    if (this.address && !!this.address.city) {
        this.address.city = addressHelpers.findCurrentCityValue(this.address.city, request.locale);
    }
    if (this.address && !!this.address.stateCode && this.address.stateCode.length) {
        this.address.stateCode = addressHelpers.findCurrentStateValue(this.address.stateCode, request.locale);
    }
}

module.exports = address;
