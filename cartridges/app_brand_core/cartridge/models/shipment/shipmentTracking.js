'use strict';

module.exports = function (object, shipment) {
    if (!shipment) {
        return;
    }

    Object.defineProperties(object, {
        trackingNumber: {
            enumerable: true,
            value: shipment.trackingNumber
        },
        trackingUrl: {
            enumerable: true,
            value: shipment.custom.trackingUrl
        }
    });
};
