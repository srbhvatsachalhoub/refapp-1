'use strict';

var BasketMgr = require('dw/order/BasketMgr');

/**
 * Sets the basket shipping prices for the applicableShippingMethods
 * @param {dw.order.Basket} currentBasket - the current basket
 * @param {dw.order.Shipment} shipment - a shipment of the current basket
 * @param {Array} applicableShippingMethods - applicableShippingMethods for the shipment
 */
function setBasketShippingCosts(currentBasket, shipment, applicableShippingMethods) {
    var Transaction = require('dw/system/Transaction');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

    applicableShippingMethods.forEach(function (applicableShippingMethod) {
        if (!applicableShippingMethod.shippingCost || !applicableShippingMethod.raw) {
            return;
        }

        var basketShippingPrice;

        if (shipment.shippingMethodID !== applicableShippingMethod.ID) {
            var currentShippingMethod = shipment.shippingMethod;

            Transaction.wrap(function () {
                // set the temporary shipping method
                shipment.setShippingMethod(applicableShippingMethod.raw);
                basketCalculationHelpers.calculateTotals(currentBasket);
                basketShippingPrice = currentBasket.adjustedShippingTotalPrice;

                // set the existing shipping method
                shipment.setShippingMethod(currentShippingMethod);
                basketCalculationHelpers.calculateTotals(currentBasket);
            });
        } else {
            basketShippingPrice = currentBasket.adjustedShippingTotalPrice;
        }

        if (basketShippingPrice) {
            var formatCurrency = require('*/cartridge/scripts/util/formatting').formatCurrency;

            var formattedBasketShippingPrice = formatCurrency(basketShippingPrice.value, basketShippingPrice.currencyCode);
            if (formattedBasketShippingPrice !== applicableShippingMethod.shippingCost) {
                applicableShippingMethod.shippingBasketCost = formattedBasketShippingPrice; // eslint-disable-line no-param-reassign
            }
        }

        // remove raw shippingmethod from the object
        delete applicableShippingMethod.raw; // eslint-disable-line no-param-reassign
    });
}


module.exports = function (object, shipment, containerView) {
    if (!object
        || !shipment
        || !object.applicableShippingMethods
        || !object.applicableShippingMethods.length
        || containerView !== 'basket') {
        return;
    }

    var currentBasket = BasketMgr.getCurrentBasket();
    if (!currentBasket) {
        return;
    }

    setBasketShippingCosts(currentBasket, shipment, object.applicableShippingMethods);
};
