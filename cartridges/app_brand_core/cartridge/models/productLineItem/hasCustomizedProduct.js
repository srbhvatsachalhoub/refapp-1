'use strict';

var checkoutHelper = require('*/cartridge/scripts/checkout/checkoutHelpers');

module.exports = function (object, productLineItems) {
    var hasEngravedProduct = checkoutHelper.hasEngravedProduct(productLineItems);
    var hasPersonalizedProduct = checkoutHelper.hasPersonalizedProduct(productLineItems);

    Object.defineProperty(object, 'hasEngravedProduct', {
        enumerable: true,
        writable: true,
        value: hasEngravedProduct
    });

    Object.defineProperty(object, 'hasPersonalizedProduct', {
        enumerable: true,
        writable: true,
        value: hasPersonalizedProduct
    });

    Object.defineProperty(object, 'hasCustomizedProduct', {
        enumerable: true,
        writable: true,
        value: hasEngravedProduct || hasPersonalizedProduct
    });
};
