'use strict';

var checkoutHelper = require('*/cartridge/scripts/checkout/checkoutHelpers');

module.exports = function (object, productLineItems) {
    Object.defineProperty(object, 'hasGiftCardProduct', {
        enumerable: true,
        writable: true,
        value: checkoutHelper.hasGiftCardProduct(productLineItems)
    });
};
