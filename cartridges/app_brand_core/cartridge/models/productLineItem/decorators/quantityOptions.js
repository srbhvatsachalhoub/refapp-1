'use strict';

var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
var preferences = require('*/cartridge/config/preferences');
var DEFAULT_MAX_ORDER_QUANTITY = preferences.maxOrderQty || 10;
var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

/**
 * get the min and max numbers to display in the quantity drop down.
 * @param {Object} productLineItem - a line item of the basket.
 * @param {number} quantity - number of items for this product
 * @returns {Object} The minOrderQuantity and maxOrderQuantity to display in the quantity drop down.
 */
function getMinMaxQuantityOptions(productLineItem, quantity) {
    var availabilityModel = inventoryHelpers.getProductAvailabilityModel(productLineItem.product);

    var availableToSell = DEFAULT_MAX_ORDER_QUANTITY;
    var perpetual = false;
    if (availabilityModel && availabilityModel.inventoryRecord && availabilityModel.inventoryRecord.ATS.value) {
        availableToSell = availabilityModel.inventoryRecord.ATS.value;
        perpetual = availabilityModel.inventoryRecord.perpetual;
        if (productLineItem.productInventoryListID
            && productLineItem.productInventoryListID !== inventoryHelpers.getInventoryList().ID) {
            var inventoryList = ProductInventoryMgr.getInventoryList(productLineItem.productInventoryListID);
            var inventoryRecord = inventoryList.getRecord(productLineItem.product.ID);
            availableToSell = inventoryRecord.ATS.value;
            perpetual = inventoryRecord.perpetual;
        }
    }


    // added maxOrderQuantity here
    var max = productLineItem.product.custom.maxOrderQuantity ?
        productLineItem.product.custom.maxOrderQuantity :
        Math.max(Math.min(perpetual ? DEFAULT_MAX_ORDER_QUANTITY : availableToSell, DEFAULT_MAX_ORDER_QUANTITY), quantity);

    return {
        minOrderQuantity: productLineItem.product.minOrderQuantity.value || 1,
        maxOrderQuantity: max
    };
}

module.exports = function (object, productLineItem, quantity) {
    Object.defineProperty(object, 'quantityOptions', {
        enumerable: true,
        value: getMinMaxQuantityOptions(productLineItem, quantity)
    });
};
