'use strict';

/* global session */

var base = module.superModule;
var formatCurrency = require('*/cartridge/scripts/util/formatting').formatCurrency;

/**
 * Returns a template that can be updated by using string interpolation.
 *
 * @param {string} valueFormatted - Money string based on locale format
 * @param {string} placeholder - Placeholder text for the decimal part of money
 * @return {string} money template with placeholder and locale artifacts
 */
function getMoneyTemplate(valueFormatted, placeholder) {
    var value = valueFormatted
        .replace(/[^\d\.,-]/g, '') // eslint-disable-line
        .replace(/^\.+|\.+$/g, '')
        .replace(/^,+|,+$/g, '');
    return valueFormatted.replace(value, placeholder);
}

/**
 * @constructor
 * @classdesc Price refinement value class
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - ProductSearchModel instance
 * @param {dw.catalog.ProductSearchRefinementDefinition} refinementDefinition - Refinement definition
 * @param {dw.catalog.ProductSearchRefinementValue} refinementValue - Raw DW refinement value
 */
function PriceRefinementValueWrapper(productSearch, refinementDefinition, refinementValue) {
    base.call(this, productSearch, refinementDefinition, refinementValue);

    var valueFromFormatted = formatCurrency(refinementValue.valueFrom, session.currency.currencyCode);
    var valueToFormatted = formatCurrency(refinementValue.valueTo, session.currency.currencyCode);
    var valueFromTemplate = getMoneyTemplate(valueFromFormatted, '{pmin}');
    var valueToTemplate = getMoneyTemplate(valueToFormatted, '{pmax}');
    this.displayValueTemplate = valueFromTemplate.trim() + ' - ' + valueToTemplate.trim();
    this.displayValue = valueFromFormatted.trim() + ' - ' + valueToFormatted.trim();
    this.valueFrom = refinementValue.valueFrom;
    this.valueTo = refinementValue.valueTo;
    this.hitCount = refinementValue.hitCount;
}

module.exports = PriceRefinementValueWrapper;
