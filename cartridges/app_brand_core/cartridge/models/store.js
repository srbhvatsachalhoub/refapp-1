'use strict';

var base = module.superModule;

var Resource = require('dw/web/Resource');
var StringUtils = require('dw/util/StringUtils');

/**
 * Gets the day hours object
 * {
 *      day: "Monday to Wednesday"
 *      time: "10:30 AM to 10:00 PM"
 * }
 * @param {Object} temp - temp helper object
 * @param {string} openToClose - open and closing time string
 * @returns {Object} the object that contains days and times
 */
function getDayHoursObject(temp, openToClose) {
    return {
        day: temp.start === temp.end
            ? Resource.msg(StringUtils.format('day.{0}', temp.start), 'storeLocator', null)
            : Resource.msgf('open.close.daytoday', 'storeLocator', null,
                Resource.msg(StringUtils.format('day.{0}', temp.start), 'storeLocator', null),
                Resource.msg(StringUtils.format('day.{0}', temp.end), 'storeLocator', null)),
        time: openToClose
    };
}

/**
 * Fills the store opening and closing times for all days
 * @param {Object} object - store object to be wrapped
 * @param {dw.catalog.Store} apiObject - the api store Object
 */
function storeHours(object, apiObject) {
    if (!apiObject.custom.jsonStoreHours) {
        return;
    }

    var storeHoursObject;
    try {
        storeHoursObject = JSON.parse(apiObject.custom.jsonStoreHours);
    } catch (e) {
        storeHoursObject = null;
    }

    if (!storeHoursObject || !(storeHoursObject instanceof Array)) {
        return;
    }


    var Calendar = require('dw/util/Calendar');
    var currentTimeZone = require('dw/system/Site').getCurrent().getTimezone();

    var currentCalendar = new Calendar();
    currentCalendar.setTimeZone(currentTimeZone);
    var todayFormat = StringUtils.formatCalendar(currentCalendar, 'ddMMyyyy');

    var today = currentCalendar.get(Calendar.DAY_OF_WEEK);

    storeHoursObject = storeHoursObject.sort(function (a, b) {
        return a.Day > b.Day;
    });

    var openingCalendar = new Calendar();
    openingCalendar.setTimeZone(currentTimeZone);

    var closingCalendar = new Calendar();
    closingCalendar.setTimeZone(currentTimeZone);

    object.dayHours = []; // eslint-disable-line no-param-reassign
    var tempDayHour = {
        start: 0,
        end: 0,
        time: ''
    };
    var openToClose;
    storeHoursObject.forEach(function (storeHourObject) {
        openingCalendar.parseByFormat(StringUtils.format('{0}{1}', todayFormat, storeHourObject.OpenTime), 'ddMMyyyyHH:mm');
        closingCalendar.parseByFormat(StringUtils.format('{0}{1}', todayFormat, storeHourObject.CloseTime), 'ddMMyyyyHH:mm');

        openToClose = Resource.msgf('open.close.timetotime', 'storeLocator', null,
            StringUtils.formatCalendar(openingCalendar, 'h:mm a'),
            StringUtils.formatCalendar(closingCalendar, 'h:mm a'));

        if (tempDayHour.time !== '' && tempDayHour.time !== openToClose) {
            object.dayHours.push(getDayHoursObject(tempDayHour, tempDayHour.time));
            // set temp
            tempDayHour = {
                start: storeHourObject.Day,
                end: storeHourObject.Day,
                time: openToClose
            };
        } else {
            tempDayHour.time = openToClose;
            tempDayHour.start = tempDayHour.start === 0 ? storeHourObject.Day : tempDayHour.start;
            tempDayHour.end = storeHourObject.Day;
        }

        if (storeHourObject.Day && storeHourObject.Day === today) {
            object.now = { // eslint-disable-line no-param-reassign
                isOpen: currentCalendar.after(openingCalendar) && currentCalendar.before(closingCalendar),
                openToClose: openToClose,
                openUntil: Resource.msgf('open.until', 'storeLocator', null,
                    StringUtils.formatCalendar(closingCalendar, 'h:mm a'))
            };
        }
    });
    // process last item
    object.dayHours.push(getDayHoursObject(tempDayHour, openToClose));
}

/**
 * Fills the store image if set
 * @param {Object} object - store object to be wrapped
 * @param {dw.catalog.Store} apiObject - the api store Object
 */
function storeImage(object, apiObject) {
    if (!apiObject.image) {
        return;
    }

    object.image = { // eslint-disable-line no-param-reassign
        title: apiObject.image.title || apiObject.name,
        url: apiObject.image.URL
    };
}

/**
 * @constructor
 * @classdesc The stores model
 * @param {dw.catalog.Store} storeObject - a Store objects
 */
function store(storeObject) {
    base.call(this, storeObject);
    if (!storeObject) {
        return;
    }

    storeHours(this, storeObject);
    storeImage(this, storeObject);
}

module.exports = store;
