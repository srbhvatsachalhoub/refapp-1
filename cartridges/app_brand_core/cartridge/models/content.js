'use strict';

var base = module.superModule;

/**
 * Represents content model
 * @param  {dw.content.Content} contentValue - result of ContentMgr.getContent call
 * @param  {string} renderingTemplate - rendering template for the given content
 * @return {void}
 * @constructor
 */
function content(contentValue, renderingTemplate) {
    if (!contentValue.online) {
        return null;
    }
    base.call(this, contentValue, renderingTemplate);
    this.raw = contentValue;
    return this;
}

module.exports = content;
