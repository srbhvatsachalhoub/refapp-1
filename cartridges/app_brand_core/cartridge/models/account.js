'use strict';

var base = module.superModule;

var AddressModel = require('*/cartridge/models/address');

/**
 * Creates a plain object that contains profile information
 * @param {Object} profile - model's profile object
 * @param {dw.customer.Customer} customer - the current customer api class
 */
function extendProfile(profile, customer) {
    if (!profile || !customer || !customer.profile) {
        return;
    }

    profile.phoneMobile = customer.profile.phoneMobile; // eslint-disable-line no-param-reassign
    profile.birthday = customer.profile.birthday; // eslint-disable-line no-param-reassign
    profile.title = customer.profile.title; // eslint-disable-line no-param-reassign
}

/**
 * Creates a plain object that contains payment instrument information
 * @param {Array} customerPaymentInstruments - current customer's PaymentInstruments
 * @returns {Object} object that contains info about the current customer's payment instrument
 */
function getPayment(customerPaymentInstruments) {
    if (customerPaymentInstruments && customerPaymentInstruments.length > 0) {
        var paymentInstrument = customerPaymentInstruments[0];

        if (paymentInstrument) {
            return {
                maskedCreditCardNumber: paymentInstrument.maskedCreditCardNumber,
                creditCardType: paymentInstrument.creditCardType,
                creditCardExpirationMonth: paymentInstrument.creditCardExpirationMonth,
                creditCardExpirationYear: paymentInstrument.creditCardExpirationYear
            };
        }
    }
    return null;
}

/**
 * Creates a plain object that contains payment instrument information
 * Filters the paymentinstruments by current active processor
 * @param {Object} userPaymentInstruments - current customer's paymentInstruments
 * @returns {Object} object that contains info about the current customer's payment instruments
 */
function getCustomerPaymentInstruments(userPaymentInstruments) {
    if (!userPaymentInstruments) {
        return null;
    }

    var URLUtils = require('dw/web/URLUtils');
    var PaymentMgr = require('dw/order/PaymentMgr');
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var StringUtils = require('dw/util/StringUtils');
    var ArrayList = require('dw/util/ArrayList');
    var HookMgr = require('dw/system/HookMgr');

    var creditCardPaymentMethod = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD);
    var paymentProcessorId;
    if (creditCardPaymentMethod && creditCardPaymentMethod.paymentProcessor) {
        paymentProcessorId = creditCardPaymentMethod.paymentProcessor.ID;
    }

    var paymentInstruments = new ArrayList();

    userPaymentInstruments.forEach(function (paymentInstrument) {
        // check if the saved paymentInstrument processor is same with current active processor
        // if not, eliminate it
        if (paymentProcessorId && paymentInstrument.raw.custom.paymentProcessor !== paymentProcessorId) {
            return;
        }

        // check if the saved card is applicable for the current locale
        // since the PSP merchant could be different for a locale or country
        // if the savedcard PSP merchant and current PSP merchant are not same
        // means the saved card is not applicable and CAN'T be used
        if (paymentInstrument.raw.custom.locale) {
            var isApplicableForCurrentLocale = true;
            var hookId = StringUtils.format('app.payment.processor.{0}', paymentInstrument.raw.custom.paymentProcessor.toLowerCase());
            if (HookMgr.hasHook(hookId)) {
                isApplicableForCurrentLocale = HookMgr.callHook(hookId,
                    'IsSavedCardApplicableForLocale',
                    paymentInstrument.raw.custom.locale,
                    request.locale // eslint-disable-line no-undef
                );
            }

            if (!isApplicableForCurrentLocale) {
                return;
            }
        }

        var result = {
            creditCardHolder: paymentInstrument.creditCardHolder,
            maskedCreditCardNumber: paymentInstrument.maskedCreditCardNumber,
            creditCardType: paymentInstrument.creditCardType,
            creditCardExpirationMonth: paymentInstrument.creditCardExpirationMonth,
            creditCardExpirationYear: paymentInstrument.creditCardExpirationYear,
            UUID: paymentInstrument.UUID,
            isDefault: !!paymentInstrument.raw.custom.isDefault
        };

        result.cardTypeImage = {
            src: URLUtils.staticURL(StringUtils.format('/images/{0}.svg', paymentInstrument.creditCardType.toLowerCase().replace(/\s/g, ''))),
            alt: paymentInstrument.creditCardType
        };

        if (result.isDefault) {
            paymentInstruments.addAt(0, result);
        } else {
            paymentInstruments.push(result);
        }
    });

    return paymentInstruments;
}

/**
 * Creates an array of plain object that contains address book addresses, if any exist
 * @param {dw.customer.Customer} addressBook - target customer
 * @returns {Array<Object>} an array of customer addresses
 */
function getAddresses(addressBook) {
    var result = [];
    if (addressBook) {
        for (var i = 0, ii = addressBook.addresses.length; i < ii; i++) {
            result.push(new AddressModel(addressBook.addresses[i]).address);
        }
    }

    return result;
}

/**
 * Sets the default customer payment instrument
 * @param {Object} userPaymentInstruments - current customer's paymentInstruments
 * @param {string} paymentInstrumentUUID - the new default payment instrument UUID
 */
function setDefaultCustomerPaymentInstrument(userPaymentInstruments, paymentInstrumentUUID) {
    if (!userPaymentInstruments) {
        return;
    }
    var Transaction = require('dw/system/Transaction');
    var collections = require('*/cartridge/scripts/util/collections');

    var customerApplicablePaymentInstruments = getCustomerPaymentInstruments(userPaymentInstruments);
    userPaymentInstruments.forEach(function (paymentInstrument) {
        // check if the paymentInstrument is active/applicable for current payment processor and locale
        // if not move on to next payment instrument
        if (!collections.some(customerApplicablePaymentInstruments, function (customerApplicablePaymentInstrument) {
            return customerApplicablePaymentInstrument.UUID === paymentInstrument.UUID;
        })) {
            return;
        }

        if (paymentInstrumentUUID === paymentInstrument.UUID) {
            Transaction.wrap(function () {
                paymentInstrument.raw.custom.isDefault = true; // eslint-disable-line no-param-reassign
            });
            return;
        }

        if (paymentInstrument.raw.custom.isDefault) {
            Transaction.wrap(function () {
                paymentInstrument.raw.custom.isDefault = null; // eslint-disable-line no-param-reassign
            });
        }
    });
}

/**
 * Account class that represents the current customer's profile dashboard
 * @param {dw.customer.Customer} currentCustomer - Current customer
 * @param {Object} addressModel - The current customer's preferred address
 * @param {Object} orderModel - The current customer's order history
 * @constructor
 */
function account(currentCustomer, addressModel, orderModel) {
    base.call(this, currentCustomer, addressModel, orderModel);

    // override customerPaymentInstruments
    this.customerPaymentInstruments =
        currentCustomer.wallet && currentCustomer.wallet.paymentInstruments
            ? getCustomerPaymentInstruments(currentCustomer.wallet.paymentInstruments)
            : null;

    // override payment object
    this.payment = getPayment(this.customerPaymentInstruments);

    // override addresses
    this.addresses = getAddresses(currentCustomer.raw.addressBook);

    // extend profile object
    extendProfile(this.profile, currentCustomer.raw);
}

base.account = account;
base.account.getCustomerPaymentInstruments = getCustomerPaymentInstruments;
base.account.setDefaultCustomerPaymentInstrument = setDefaultCustomerPaymentInstrument;

module.exports = base.account;
