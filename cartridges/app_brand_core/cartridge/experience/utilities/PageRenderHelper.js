'use strict';

var base = module.superModule;

/**
 * Parse Render Parameters
 *
 * @param {Object} renderParametersJson The json render parameters
 *
 * @returns {Object} render parameters
 */
function parseRenderParameters(renderParametersJson) {
    var renderParameters = {};
    if (renderParametersJson) {
        try {
            renderParameters = JSON.parse(renderParametersJson);
        } catch (e) {
            var Logger = require('dw.system.Logger');
            Logger.error('Unable to parse renderParameters: ' + renderParametersJson);
        }
    }
    return renderParameters;
}

base.parseRenderParameters = parseRenderParameters;
module.exports = base;
