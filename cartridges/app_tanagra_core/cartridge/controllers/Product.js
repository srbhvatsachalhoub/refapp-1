'use strict';

var server = require('server');
server.extend(module.superModule);

/**
 * Gets products randomly to show as recommendations
 */
server.get('RandomRecommendations', function (req, res, next) {
    var ProductSearchModel = require('dw/catalog/ProductSearchModel');
    var apiProductSearch = new ProductSearchModel();
    var recommendationsRandom = [];
    var productsArray = [];
    var index = 0;
    var products;

    apiProductSearch.setPriceMin(0);
    apiProductSearch.setPriceMax(100000);
    apiProductSearch.search();

    products = apiProductSearch.getProducts();

    // eslint-disable-next-line require-jsdoc
    function shuffle(array) {
        var ctr = array.length;
        var temp;
        var i;

        while (ctr > 0) {
            i = Math.floor(Math.random() * ctr);
            ctr--;
            temp = array[ctr];
            // eslint-disable-next-line no-param-reassign
            array[ctr] = array[i];
            // eslint-disable-next-line no-param-reassign
            array[i] = temp;
        }
        return array;
    }

    while (products.hasNext()) {
        productsArray.push(products.next().getID());
    }

    productsArray = shuffle(productsArray);

    for (index; index < 5; index++) {
        recommendationsRandom.push(productsArray[index]);
    }

    res.render('product/components/recommendationsRandom', {
        recommendations: {
            title: req.querystring.title,
            items: recommendationsRandom
        }
    });

    next();
});


module.exports = server.exports();
