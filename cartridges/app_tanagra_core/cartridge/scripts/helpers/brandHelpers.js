'use strict';

var alphabets = {
    en: { A: [], B: [], C: [], D: [], E: [], F: [], G: [], H: [], I: [], J: [], K: [], L: [], M: [], N: [], O: [], P: [], Q: [], R: [], S: [], T: [], U: [], V: [], W: [], X: [], Y: [], Z: [], '0-9': [] },
    ar: { أ: [], ب: [], ت: [], ث: [], ج: [], ح: [], خ: [], د: [], ذ: [], ر: [], ز: [], س: [], ش: [], ص: [], ض: [], ط: [], ظ: [], ع: [], غ: [], ف: [], ق: [], ك: [], ل: [], م: [], ن: [], ه: [], و: [], '0-9': [] }
};

var brands = alphabets[require('dw/util/Locale').getLocale(request.getLocale()).getLanguage()]; // eslint-disable-line no-undef

/**
 * Prepare data for all brands page
 * @param {Object} productSearch - the search productSearch object
 * @returns {array} array of brand objects
 */
function getSearchAllBrands(productSearch) {
    var collections = require('*/cartridge/scripts/util/collections');

    var subCategories = productSearch.refinementCategory.subCategories;

    if (subCategories) {
        collections.forEach(subCategories, function (subcategory) {
            var categoryId = subcategory.ID;
            // var brandStatus = null;
            // if (subcategory.custom && subcategory.custom.brandStatus && subcategory.custom.brandStatus.length > 0) {
            //     brandStatus = subcategory.custom.brandStatus[0].value;
            // }
            var brand = {
                id: categoryId,
                name: subcategory.displayName,
                online: subcategory.online,
                // onlineInStoreStatus: brandStatus,
                categories: 'all' // categories ? need to separate search for each brand
            };

            var firstLetter = subcategory.displayName.charAt(0).toUpperCase();

            if (brand.online) {
                if (brands[firstLetter]) {
                    brands[firstLetter].push(brand);
                } else if (firstLetter.match(/\d/)) {
                    brands['0-9'].push(brand);
                } else {
                    brands[firstLetter] = [brand];
                }
            }
        });

        // sort the values
        Object.keys(brands).forEach(function (key) {
            if (brands[key].length) {
                brands[key] = brands[key].sort(function (br1, br2) {
                    return br1.name - br2.name;
                });
            } else {
                brands[key] = false;
            }
        });
    }

    return {
        rootCategory: productSearch.refinementCategory,
        brands: brands,
        brandsLength: subCategories ? subCategories.length : 0
    };
}

module.exports = {
    getSearchAllBrands: getSearchAllBrands
};
