<isinclude template="/components/modules" sf-toolkit="off" />
<isdecorate template="common/layout/checkout">
    <isscript>
        var assets = require('*/cartridge/scripts/assets.js');
        assets.addJs('/js/checkout.js');
        assets.addCss('/css/checkout.css');
    </isscript>

    <isif condition="${pdict.reportingURLs && pdict.reportingURLs.length}">
        <isinclude template="reporting/reportingUrls" />
    </isif>

    <div id="checkout-main" class="data-checkout-stage checkout-main ${pdict.order.usingMultiShipping && pdict.order.shipping.length > 1 ? 'multi-ship' : ''}"
        data-customer-type="${pdict.customer.registeredUser ? 'registered' : 'guest'}"
        data-checkout-stage="${pdict.currentStage}"
        data-checkout-get-url="${URLUtils.https('CheckoutServices-Get')}">

        <div class="checkout-steps-background">
            <div class="checkout-steps-container container mb-0">
                <div class="row">
                    <div class="col-md-7 col-lg-8">
                        <div class="checkout-steps-container-inner">
                            <ul class="checkout-steps-wrapper">
                                <li>
                                    <isif condition="${pdict.customer.registeredUser}">
                                        <span>${Resource.msg('step.sign.in', 'checkout', null)}</span>
                                    <iselse/>
                                        <a href="${URLUtils.https('Checkout-Login')}">${Resource.msg('step.sign.in', 'checkout', null)}</a>
                                    </isif>
                                </li>
                                <li>
                                    <a href="${'#'}" class="checkout-step-shipping-title js-show-shipping-step">
                                        ${Resource.msg('step.shipping', 'checkout', null)}
                                    </a>
                                </li>
                                <li>
                                    <span class="checkout-step-payment-title">${Resource.msg('step.payment', 'checkout', null)}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-7 col-lg-8">
                    <iscomment>Checkout Forms: Shipping, Payment, Coupons, Billing, etc</iscomment>
                    <div class="alert alert-danger error-message mt-0 mb-4" role="alert" <isif condition="${pdict.pspErrorMessage}">style="display:block"</isif>>
                        <p class="error-message-text mb-0 js-error-message-text"
                            data-shipping-error="${Resource.msg('error.no.shipping.address', 'checkout', null)}"
                            data-billing-error="${Resource.msg('error.no.billing.address', 'checkout', null)}">
                            <isif condition="${pdict.pspErrorMessage}">
                                <isprint value="${pdict.pspErrorMessage}" />
                            </isif>
                        </p>
                    </div>

                    <iscomment>Step 1: Shipping</iscomment>
                    <isinclude template="checkout/shipping/shipping" />

                    <iscomment>Step 2: Payment and Billing</iscomment>
                    <isinclude template="checkout/billing/billing" />
                </div>

                <iscomment>Order Totals, Details and Summary</iscomment>
                <div class="col-md-5 col-lg-4 sticky-sidebar-container js-sticky-sidebar-container">
                    <div class="card js-sticky-sidebar">
                        <div class="js-sticky-sidebar-inner">
                            <div class="card-body order-total-summary">
                                <isinclude template="checkout/orderTotalSummary" />

                                <iscomment>Checkout Workflow Buttons</iscomment>
                                <div class="row">
                                    <div class="col-12 next-step-button">
                                        <div>
                                            <button class="btn btn-secondary btn-block submit-shipping js-submit-shipping-btn" type="submit" name="submit" value="submit-shipping"
                                                <isif condition="${pdict.order.usingMultiShipping && !pdict.order.shippable}">disabled</isif>>
                                                ${Resource.msg('button.next.payment', 'checkout', null)}
                                            </button>

                                            <button class="btn btn-secondary btn-block submit-payment mt-0 js-submit-payment-btn" type="submit" name="submit" value="submit-payment">
                                                ${Resource.msg('button.place.order', 'checkout', null)}
                                            </button>

                                            <button class="btn btn-secondary btn-block place-order mt-0 d-none js-place-order-btn" data-action="${URLUtils.url('CheckoutServices-PlaceOrder')}"
                                                    type="submit" name="submit" value="place-order">${Resource.msg('button.place.order', 'checkout', null)}
                                            </button>
                                            <div class="terms-and-conditions-warning">
                                                <iscontentasset aid="payment-agree-terms-conditions" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <isinclude template="checkout/shipping/shippingAddressTemplate" />
    <isinclude template="checkout/billing/termsAndConditionsModal" />
</isdecorate>
