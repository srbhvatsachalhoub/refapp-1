'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('brand_core/productTile'));
    processInclude(require('./cart/cart'));
});
