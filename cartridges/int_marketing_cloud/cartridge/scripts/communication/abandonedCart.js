'use strict';

/**
 * @module communication/abandonedCart
 */

const sendTrigger = require('./util/send').sendTrigger;
var hookPath = 'app.communication.abandonedCart.';

/**
 * Trigger an abandoned cart notification
 * @param {SynchronousPromise} promise sync promise of handler framework
 * @param {module:communication/util/trigger~AbandonedCart} data  abandoned cart data
 * @returns {SynchronousPromise} synch promise
 */
function send(promise, data) {
    data.OrderAsXML = data.params.AbandonedCart.OrderAsXML;
    return sendTrigger(hookPath + 'send', promise, data);
}

/**
 * Declares attributes available for data mapping configuration
 * @returns {Object} Map of hook function to an array of strings
 */
function triggerDefinitions() {
    return {
        send: {
            description: 'AbandonedCart Notification',
            attributes: [
                'AbandonedCart.email',
                'AbandonedCart.OrderAsXML',
                'AbandonedCart.Locale'
            ]
        }
    };
}

module.exports = require('dw/system/HookMgr').callHook(
    'app.communication.handler.initialize',
    'initialize',
    require('./handler').handlerID,
    'app.communication.abandonedCart',
    {
        send: send
    }
);

// non-hook exports
module.exports.triggerDefinitions = triggerDefinitions;
