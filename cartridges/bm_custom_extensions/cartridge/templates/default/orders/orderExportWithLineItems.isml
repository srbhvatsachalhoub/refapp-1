<isif condition="${ pdict.index === 0 }">
    <tr>
        <td>OrderNo</td>
        <td>CustomerNo</td>
        <td>Email</td>
        <td>First Name</td>
        <td>Last Name</td>
        <td>Birthdate</td>
        <td>Phone</td>
        <td>NbLine</td>
        <td>Total</td>
        <td>Shipping Fee</td>
        <td>Tax</td>
        <td>Order Status</td>
        <td>Confirmation Status</td>
        <td>Export Status</td>
        <td>Shipping Status</td>
        <td>Payment Status</td>
        <td>Date created</td>
        <td>Short Title</td>
        <td>OrderLineQuantity</td>
        <td>OrderLinePrice</td>
        <td>OrderLinePriceTotal</td>
        <td>Shipping Method</td>
        <td>Payment Method</td>
        <td>Transaction ID</td>
        <td>Card Type</td>
        <td>Date modified</td>
        <td>Discount Total</td>
        <td>Shipping FirstName</td>
        <td>Shipping LastName</td>
        <td>Shipping Phone</td>
        <td>Shipping Zip</td>
        <td>Shipping City</td>
        <td>Shipping State</td>
        <td>Shipping Address</td>
        <td>Shipping Country</td>
        <td>Customer Address</td>
        <td>Customer Country</td>
        <td>Is Gift</td>
        <td>Gift Message</td>
        <td>Gift Wrapping</td>
        <td>Price Hidden</td>
        <td>Status Updates</td>
    </tr>
</isif>
<isscript>
    var order = pdict.order;
    var customer = order.customer;
    var customerProfile = customer.profile;
    var lineItems = order.getAllProductLineItems();
    var firstPLI = lineItems.toArray()[0];
    var taxRate = firstPLI.taxRate;

    var totalPrice = Number(order.getMerchandizeTotalPrice().value.toPrecision(2));
    var adjustedTotalPrice = Number(order.getAdjustedMerchandizeTotalPrice().value.toPrecision(2));
    var priceAdjustment = order.getPriceAdjustmentByPromotionID('PAYMENT_SERVICE_FEE');
    var paymentServiceFee = Number(priceAdjustment ? priceAdjustment.getPriceValue().toPrecision(2) : 0);
    var discountAmount = totalPrice - adjustedTotalPrice + paymentServiceFee;
</isscript>

<isloop items="${order.getAllLineItems()}" var="pli">
    <tr class="${pdict.even ? 'even' : 'odd'}">
        <td style="vnd.ms-excel.numberformat:${pdict.orderNoFormatter}">${order.orderNo}</td>
        <td style="vnd.ms-excel.numberformat:${pdict.customerNoFormatter}">${order.customerNo ? order.customerNo : ''}</td>
        <td>${order.customerEmail}</td>
        <isif condition="${customerProfile}">
            <td>${customerProfile.firstName}</td>
            <td>${customerProfile.lastName}</td>
        <iselse/>
            <isset name="billingAddress" value="${order.billingAddress}" scope="page" />
            <td>${billingAddress.firstName}</td>
            <td>${billingAddress.lastName}</td>
        </isif>
        <td>${customerProfile && customerProfile.birthday ? customerProfile.birthday : ''}</td>
        <td>${customerProfile && customerProfile.phoneMobile ? customerProfile.phoneMobile : ''}</td>
        <td>${lineItems.length}</td>
        <td>${order.getAdjustedMerchandizeTotalPrice().value}</td>
        <td>${order.getAdjustedShippingTotalPrice().value}</td>
        <td>${pli.getTax().value}</td>
        <td>${order.getStatus()}</td>
        <td>${order.getConfirmationStatus()}</td>
        <td>${order.getExportStatus()}</td>
        <td>${order.getShippingStatus()}</td>
        <td>${order.getPaymentStatus()}</td>
        <td>
            <isprint value="${order.getCreationDate()}" timezone="SITE" formatter="dd.MM.yyyy HH:mm" />
        </td>
        <td>${pli.getLineItemText()}</td>
        <td>${pli instanceof dw.order.ProductLineItem ? pli.getQuantity().value.toFixed(0) : ''}</td>
        <td>${pli.getPriceValue()}</td>
        <td>${pli instanceof dw.order.ProductLineItem ? pli.getAdjustedPrice().value : ''}</td>
        <td>${order.getDefaultShipment().getShippingMethod().displayName}</td>
        <td>
            <isloop items="${order.getPaymentInstruments()}" var="paymentInstrument" status="piLoopStatus">
                ${paymentInstrument.getPaymentMethod()}${piLoopStatus.last ? '' : ','}
            </isloop>
        </td>
        <td>
            <isloop items="${order.getPaymentInstruments()}" var="paymentInstrument" status="piLoopStatus">
                ${paymentInstrument.getPaymentTransaction().getTransactionID()}${piLoopStatus.last ? '' : ','}
            </isloop>
        </td>
        <td>
            <isloop items="${order.getPaymentInstruments()}" var="paymentInstrument" status="piLoopStatus">
                ${paymentInstrument.getCreditCardType() ? paymentInstrument.getCreditCardType() : '-'}${piLoopStatus.last ? '' : ','}
            </isloop>
        </td>
        <td>
            <isloop items="${order.getPaymentInstruments()}" var="paymentInstrument" status="piLoopStatus">
                <isprint value="${paymentInstrument.getLastModified()}" timezone="SITE" formatter="dd.MM.yyyy HH:mm" />
                ${piLoopStatus.last ? '' : ','}
            </isloop>
        </td>
        <td>
            ${discountAmount}
        </td>
        <isset name="shipment" scope="page" value="${order.getDefaultShipment()}" />
        <isset name="shippingAddress" scope="page" value="${shipment.getShippingAddress()}" />
        <td>${shippingAddress.firstName}</td>
        <td>${shippingAddress.lastName}</td>
        <td>${shippingAddress.phone}</td>
        <td>${shippingAddress.postalCode}</td>
        <td>${shippingAddress.city}</td>
        <td>${shippingAddress.stateCode ? shippingAddress.stateCode : ''}</td>
        <td>${shippingAddress.address1}</td>
        <td>${shippingAddress.countryCode}</td>
        <isset name="customerAddress" scope="page"
            value="${order.getCustomer() && order.getCustomer().getAddressBook() && order.getCustomer().getAddressBook().getPreferredAddress() ? order.getCustomer().getAddressBook().getPreferredAddress() : null}" />
        <td>${customerAddress ? customerAddress.address1 : ''}</td>
        <td>${customerAddress ? customerAddress.countryCode : ''}</td>
        <td>${shipment.isGift()}</td>
        <td>${shipment.isGift() ? shipment.getGiftMessage() : ''}</td>
        <td>${shipment.isGift() ? (shipment.custom.isGiftWrapped ? shipment.custom.isGiftWrapped : 'false') : ''}</td>
        <td>${shipment.isGift() ? (shipment.custom.isPriceHidden ? shipment.custom.isPriceHidden : 'false') : ''}</td>
        <td>
            <table>
                <isloop items="${order.custom.history}" var="history" status="historyLoopStatus">
                    <isset name="history" scope="page" value="${JSON.parse(history)}" />
                    <tr class="${pdict.even ? 'even' : 'odd'}">
                        <td>${history.new}</td>
                        <td>${history.time}</td>
                    </tr>
                </isloop>
            </table>
        </td>
    </tr>
</isloop>
