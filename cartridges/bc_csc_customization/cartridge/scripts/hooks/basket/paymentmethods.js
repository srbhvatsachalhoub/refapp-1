'use strict';

var Status = require('dw/system/Status');
var collections = require('*/cartridge/scripts/util/collections');

/**
 * basket/order applicable payment methods modifyGETResponse hook
 * Is is only COD - Cash on Delivery payment method allowed in Customer Service Center ofor Order Creation
 *
 * @param {Object} paymentMethodResultResponse - The applicable payment methods.
 */
function modifyGETResponse(paymentMethodResultResponse) {
    if(request.clientId === "dw.csc") {
        // only cash on delivery payment mthod is available in Customer Service Center
        paymentMethodResultResponse.applicablePaymentMethods =
            collections.filter(paymentMethodResultResponse.applicablePaymentMethods, function (applicablePaymentMethod) {
                return applicablePaymentMethod.id === 'COD';
            });
    }

    return new Status(Status.OK);
}

module.exports = {
    modifyGETResponse: modifyGETResponse
};
