'use strict';

var Status = require('dw/system/Status');
var SynchronousPromise = require('synchronous-promise');

/**
 * Calls Password Reset Hook
 * @param {dw.customer.Customer} customer - Password Reset Customer
 * @param {string} resetToken - resetToken
 */
function afterPOST(customer, resetToken) {
    if(request.clientId === "dw.csc") {
        var HookMgr = require('dw/system/HookMgr');
        var promise = SynchronousPromise.unresolved()
            .then(function (data) {
                result = data;
            })
            .catch(function (data) {
                result = data;
            });

        var Site = require('dw/system/Site');
        var URLUtils = require('dw/web/URLUtils');
        var HashMap = require('dw/util/HashMap');
        var context = new HashMap();
        context.put('ResetPasswordToken', resetToken);
        var urlHelpers = require('*/cartridge/scripts/helpers/urlHelpers');
        var url = urlHelpers.setHostToUrl(URLUtils.https('Account-SetNewPassword', 'Token', resetToken), customer.profile.preferredLocale);
        context.put('url', url);
        context.put('Customer', customer);
        HookMgr.callHook(
            'app.communication.account.passwordReset',
            'passwordReset',
            promise,
            {
                fromEmail: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
                toEmail: customer.profile.email,
                params: context
            }
        );
    }

    return new Status(Status.OK);
}

module.exports = {
    afterPOST: afterPOST
};
