
var decorators = require('*/cartridge/models/speedbus/decorators/index');
/**
 * Builds Recognize Sale Order view model
 * @param {dw.order.Order} lineItemCtnr lineItemCtnr of Order
 * @returns {Object} orderExportModel
 */
module.exports = function (lineItemCtnr) {
    var orderExportModel = {
        Transaction_Type: 'SALE'
    };
    decorators.transactionDetail(orderExportModel, lineItemCtnr);
    decorators.generalDetail(orderExportModel, lineItemCtnr);
    decorators.saleDetail(orderExportModel, lineItemCtnr, 'P');
    decorators.paymentDetail(orderExportModel, lineItemCtnr);

    return {
        SaleMessage: {
            Head: orderExportModel
        }
    };
};
