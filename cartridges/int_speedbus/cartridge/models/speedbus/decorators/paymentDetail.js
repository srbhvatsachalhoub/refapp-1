/**
 * Builds recognizeSaleModel view model
 * @param {Object} recognizeSaleModel recognizeSaleModel
 * @param {dw.order.Order} lineItemCtnr lineItemCtnr of Order
 */
module.exports = function (recognizeSaleModel, lineItemCtnr) {
    var paymentDetail = [];
    var collections = require('*/cartridge/scripts/util/collections');
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var speedBusHelpers = require('*/cartridge/scripts/helpers/speedbusHelpers');
    var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');

    collections.forEach(lineItemCtnr.getPaymentInstruments(), function (paymentInstrument) {
        var tenderTypeGroupCode = speedBusHelpers.getTenderTypeGroupCode(paymentInstrument);
        var tenderTypeAmount = paymentInstrument.paymentTransaction.amount.value;

        var paymentItem = {};
        paymentItem.Original_Currency = lineItemCtnr.getCurrencyCode();
        paymentItem.Original_Currency_Amount = lineItemCtnr.getTotalGrossPrice().value;
        paymentItem.Tender_Type_Amount = tenderTypeAmount;
        paymentItem.Tender_Type_ID = tenderTypeGroupCode;
        switch (paymentInstrument.paymentMethod) {
            case PaymentInstrument.METHOD_CREDIT_CARD:
                paymentItem.CC_NO = paymentInstrument.maskedCreditCardNumber;
                paymentItem.Tender_Type_Group = 'CCARD';
                break;
            case PaymentInstrument.METHOD_DW_APPLE_PAY:
                paymentItem.Tender_Type_Group = 'CCARD';
                break;
            case paymentMethodHelpers.getCODPaymentMethodId():
                paymentItem.Tender_Type_Group = 'CASH';
                break;
            case PaymentInstrument.METHOD_GIFT_CERTIFICATE:
                paymentItem.Voucher_no = paymentInstrument.giftCertificateCode;
                paymentItem.Tender_Type_Group = 'VOUCH';
                break;
            case paymentMethodHelpers.getPrePaidPaymentMethodId():
                paymentItem.Tender_Type_Group = 'CCARD';
                paymentItem.CC_NO = '************1111';
                break;
            case paymentMethodHelpers.getKnetPaymentMethodId():
                paymentItem.Tender_Type_Group = 'DCARD';
                paymentItem.CC_NO = '************1111';
                break;
            default:
                paymentItem = null;
                break;
        }
        if (paymentItem) {
            paymentDetail.push(paymentItem);
        }
    });
    if (paymentDetail.length) {
        Object.defineProperty(recognizeSaleModel, 'PaymentDetail', {
            enumerable: true,
            value: paymentDetail
        });
    }
};
