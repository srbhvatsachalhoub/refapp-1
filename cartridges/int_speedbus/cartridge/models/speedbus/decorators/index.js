module.exports = {
    generalDetail: require('*/cartridge/models/speedbus/decorators/generalDetail'),
    saleDetail: require('*/cartridge/models/speedbus/decorators/saleDetail'),
    paymentDetail: require('*/cartridge/models/speedbus/decorators/paymentDetail'),
    transactionDetail: require('*/cartridge/models/speedbus/decorators/transactionDetail')
};
