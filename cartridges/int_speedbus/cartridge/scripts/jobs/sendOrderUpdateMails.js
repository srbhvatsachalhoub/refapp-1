'use strict';
/* eslint-disable no-loop-func */


/**
 * Send mails for updated orders
 * @returns {dw.system.Status} Status
 */
function execute() {
    var logger = require('dw/system/Logger').getLogger('orderupdatemail');
    var orderStatusHelpers = require('*/cartridge/scripts/helpers/orderStatusHelpers');
    var Transaction = require('dw/system/Transaction');
    var OrderMgr = require('dw/order/OrderMgr');
    var Status = require('dw/system/Status');

    OrderMgr.processOrders(function (order) {
        var mailSent = true;
        try {
            switch (order.custom.smcStatus.value) {
                case 'shipped':
                    if (order.custom.isInvoiceGenerated === true) {
                        orderStatusHelpers.sendShippingEmail(order);
                    } else {
                        logger.info('Order shipment e-mail for {0} will be delayed because invoice is not generated yet', order.orderNo);
                        mailSent = false;
                    }
                    break;
                case 'cancelled':
                    orderStatusHelpers.sendCancelledEmail(order);
                    break;
                case 'confirmed':
                    orderStatusHelpers.sendConfirmationEmail(order);
                    break;
                default:
                    mailSent = false;
                    break;
            }
            if (mailSent) {
                Transaction.wrap(function () {
                    order.custom.smcStatus = null; // eslint-disable-line no-param-reassign
                });
            }
        } catch (e) {
            logger.error('While sending update order mail {0}, an error occured {1}', order.orderNo, e.message);
        }
    }, 'custom.smcStatus != NULL', null);

    return new Status(Status.OK);
}

module.exports = {
    execute: execute
};
