'use strict';
/* global request */
var logger = require('dw/system/Logger').getLogger('speedbus');

/**
 * Pulls primary payment method from payment instrument
 * @param {dw.order.Order} lineItemCtnr Order
 * @returns {string} payment Method
 */
function getOrderPrimaryPaymentMethod(lineItemCtnr) {
    var collections = require('*/cartridge/scripts/util/collections');
    var paymentMethod = null;
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var paymentMethodHelpers = require('*/cartridge/scripts/helpers/paymentMethodHelpers');

    collections.forEach(lineItemCtnr.getPaymentInstruments(), function (paymentInstrument) {
        switch (paymentInstrument.paymentMethod) {
            case PaymentInstrument.METHOD_CREDIT_CARD:
                paymentMethod = 'CREDIT_CARD';
                break;
            case PaymentInstrument.METHOD_DW_APPLE_PAY:
                paymentMethod = 'DW_APPLE_PAY';
                break;
            case PaymentInstrument.METHOD_GIFT_CERTIFICATE:
                paymentMethod = 'GIFT_CERTIFICATE';
                break;
            case paymentMethodHelpers.getCODPaymentMethodId():
                paymentMethod = 'COD';
                break;
            case paymentMethodHelpers.getPrePaidPaymentMethodId():
                paymentMethod = 'PREPAID';
                break;
            case paymentMethodHelpers.getKnetPaymentMethodId():
                paymentMethod = 'KNET';
                break;
            default:
                paymentMethod = null;
                break;
        }
        if (paymentMethod) {
            return;
        }
    });
    return paymentMethod;
}
/**
 * updates order
 * @param {dw.order.Order} order Order
 * @returns {JSON} update result
 */
function updateOrder(order) {
    var Order = require('dw/order/Order');
    var paymentMethod = getOrderPrimaryPaymentMethod(order);
    var Transaction = require('dw/system/Transaction');

    // early quit if payment method is not found!
    if (!paymentMethod) {
        return {
            success: false,
            message: 'Payment method is not found for the order'
        };
    }
    if (paymentMethod === 'COD') {
        Transaction.wrap(function () { // eslint-disable-line
            order.setConfirmationStatus(Order.CONFIRMATION_STATUS_CONFIRMED);
            order.setExportStatus(Order.EXPORT_STATUS_READY);
        });

        return {
            success: true
        };
    }
    return {
        success: false,
        message: 'Payment method is not COD for the order'
    };
}

/**
 * Update COD Orders
 * @returns {dw.system.Status} Status
 */
function execute() {
    var OrderMgr = require('dw/order/OrderMgr');
    var Status = require('dw/system/Status');
    var Order = require('dw/order/Order');
    var allowedLocales = require('dw/system/Site').getCurrent().allowedLocales.toArray();

    // Latin numeric system is not working for ar locales, although it is set properly in BM. Temporary fix to set english locale in job.
    var reqLocale;
    for (var i = 0; i < allowedLocales.length; i++) {
        if (allowedLocales[i].indexOf('en') !== -1) {
            reqLocale = allowedLocales[i];
            break;
        }
    }
    request.setLocale(reqLocale);

    var orders = OrderMgr.searchOrders('(status = {0} OR status = {1}) AND paymentStatus = {2} AND shippingStatus = {3} AND confirmationStatus = {4} AND exportStatus = {5}',
        null,
        Order.ORDER_STATUS_NEW,
        Order.ORDER_STATUS_OPEN,
        Order.PAYMENT_STATUS_NOTPAID,
        Order.SHIPPING_STATUS_NOTSHIPPED,
        Order.CONFIRMATION_STATUS_NOTCONFIRMED,
        Order.EXPORT_STATUS_EXPORTED);
    if (!orders.hasNext()) {
        logger.info('No orders are found to update in this iteration');
        return new Status(Status.OK);
    }
    while (orders.hasNext()) {
        var order = orders.next();
        try {
            var updateStatus = updateOrder(order);
            if (!updateStatus.success) {
                logger.error('While updating order {0}, an error occured {1}', order.orderNo, updateStatus.message);
            }
        } catch (e) {
            logger.error('While updating order {0}, an error occured {1}', order.orderNo, e.message);
            continue;
        }
    }

    return new Status(Status.OK);
}

module.exports = {
    execute: execute
};
