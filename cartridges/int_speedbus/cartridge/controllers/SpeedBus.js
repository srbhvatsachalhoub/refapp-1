'use strict';
var sfccAuthHelpers = require('*/cartridge/scripts/helpers/sfccAuthHelpers');

var server = require('server');

server.post('Price', server.middleware.https, function (req, res, next) {
    var headers = req.httpHeaders;
    var authorizationHeader = headers.get('x-speedbus-authorization') || headers.get('X-SPEEDBUS-AUTHORIZATION');

    if (!authorizationHeader || !sfccAuthHelpers.isOCAPITokenValid(authorizationHeader)) {
        res.setStatusCode(403);
        res.json({
            message: 'Authorization failed'
        });
        return next();
    }
    try {
        var currentSite = require('dw/system/Site').getCurrent();
        var requestPayLoad = JSON.parse(req.body); // eslint-disable-line
        var CustomObjectMgr = require('dw/object/CustomObjectMgr');
        var StringUtils = require('dw/util/StringUtils');
        var UUIDUtils = require('dw/util/UUIDUtils');
        var objectId = StringUtils.format('{0}_{1}', currentSite.ID, UUIDUtils.createUUID());
        var Transaction = require('dw/system/Transaction');
        Transaction.begin();
        var priceObj = CustomObjectMgr.createCustomObject('SpeedBusPrice', objectId);
        priceObj.custom.listPriceBookId = currentSite.getCustomPreferenceValue('listPriceDefault');
        priceObj.custom.salePriceBookId = currentSite.getCustomPreferenceValue('salePriceDefault');
        priceObj.custom.value = JSON.stringify(requestPayLoad, null, 3);
        Transaction.commit();
        res.setStatusCode(200);
        res.json({
            status: 'ok'
        });
    } catch (e) {
        res.json({
            status: 'no',
            message: e.message
        });
    }
    return next();
});

server.get('Inspect', server.middleware.https, function (req, res, next) {
    var System = require('dw/system/System');
    if (System.instanceType === System.PRODUCTION_SYSTEM) {
        res.setStatusCode(410);
        res.render('error/notFound');
        return next();
    }
    var orderNo = req.querystring.orderNo;
    if (!orderNo) {
        res.setStatusCode(410);
        res.json({ message: 'Order is not found please provide order number' });
        return next();
    }
    var type = req.querystring.type;
    var OrderMgr = require('dw/order/OrderMgr');
    var order = OrderMgr.getOrder(orderNo);
    var resp = {};
    if (order) {
        switch (type) {
            case 'LAYCMP':
                var completeOrderModel = require('*/cartridge/models/speedbus/completeOrder');
                resp = completeOrderModel(order);
                break;
            case 'SALE':
                var recognizeSaleModel = require('*/cartridge/models/speedbus/recognizeSaleOrder');
                resp = recognizeSaleModel(order);
                break;
            case 'LAYDEL':
                var cancelOrderModel = require('*/cartridge/models/speedbus/cancelOrder');
                resp = cancelOrderModel(order);
                break;
            case 'RETURN':
                var returnOrderModel = require('*/cartridge/models/speedbus/returnOrder');
                resp = returnOrderModel(order);
                break;
            case 'LAYINT':
            default:
                var exportOrderModel = require('*/cartridge/models/speedbus/exportOrder');
                resp = exportOrderModel(order);
                break;
        }
    }
    res.json(resp);
    return next();
});

module.exports = server.exports();
