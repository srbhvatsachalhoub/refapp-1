var ISML = require('dw/template/ISML');
var Logger = require('dw/system/Logger');
var OrderMgr = require('dw/order/OrderMgr');
var Order = require('dw/order/Order');
var Transaction = require('dw/system/Transaction');
var SynchronousPromise = require('synchronous-promise');
var StringUtils = require('dw/util/StringUtils');


function sendEmail(type, order) {
	var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
	if (emailHelpers.isMCOrderEmailsDisabled()) {
		return;
	}
	var HookMgr = require('dw/system/HookMgr');
	var promise = SynchronousPromise.unresolved()
		.then(function (data) {
			result = data;
		})
		.catch(function (data) {
			result = data;
		});
	var Site = require('dw/system/Site');
	HookMgr.callHook(
		StringUtils.format('app.communication.order.{0}', type),
		type,
		promise,
		{
			fromEmail: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
			toEmail: order.getCustomerEmail(),
			params: { Order: order }
		}
	);
}

function sendCancelledEmail(order) {
	sendEmail('cancelled', order);
}

function sendConfirmationEmail(order) {
	sendEmail('confirmation', order);
}

/**
 * Provides a order details for COD fraud confirmation
 */
exports.Start = function () {
	this.order_no = request.httpParameterMap.order_no.stringValue;
	this.order = OrderMgr.getOrder(this.order_no);
	var server = require('server');
	var addressForm = server.forms.getForm('address');
	try {
		ISML.renderTemplate('order/payment/order_confirm_details.isml', { "order_no": order_no, "order": order, "address_definition": addressForm });
	} catch (e) {
		Logger.error('Error while rendering template ' + templateName);
		throw e;
	}
}

/**
 * Confirms order
 */
exports.Confirm = function () {
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');

	this.action = 'confirm';

	var orderNo = request.httpParameterMap.get('order_no');
	this.orderNo = orderNo;
	var success = true;
	if (orderNo) {
		try {
			var order = OrderMgr.getOrder(orderNo);
			if (order) {
				if (order.status.value !== Order.ORDER_STATUS_CANCELLED && order.confirmationStatus.value !== Order.CONFIRMATION_STATUS_CONFIRMED) {
					Transaction.wrap(function(){
                        hooksHelper('app.order.status.setConfirmationStatus', 'setConfirmationStatus', [order, Order.CONFIRMATION_STATUS_CONFIRMED], function () {
                            order.setConfirmationStatus(Order.CONFIRMATION_STATUS_CONFIRMED);
                        });
                        hooksHelper('app.order.status.setExportStatus', 'setExportStatus', [order, Order.EXPORT_STATUS_READY], function () {
                            order.setExportStatus(Order.EXPORT_STATUS_READY);
                        });

                        order.custom.sscSyncStatus = 'updated';
                    });
				}
			}
		}
		catch (e) {
			dw.system.Logger.error('Error while confirming Order ' + orderNo + ':' + e);
			success = false;
		}
	}
	else {
		success = false;
	}

	this.success = success;

	try {
		ISML.renderTemplate('order/payment/order_confirm_success.isml', this);
	} catch (e) {
		Logger.error('Error while rendering template ' + templateName);
		throw e;
	}

}

/**
 * Cancels order
 */
exports.Cancel = function () {

	this.action = 'cancel';

	var orderNo = request.httpParameterMap.get('order_no');
	this.orderNo = orderNo;
    var success = true;
	if (orderNo) {
		try {
			var order = OrderMgr.getOrder(orderNo);
			if (order) {
				if (order.status.value !== Order.ORDER_STATUS_CANCELLED) {
                    // use speedbus statushelper to cancel the order
                    // also sends relevant request to PSP for CREDIT_CARD payment
                    var statusUpdates = require('*/cartridge/scripts/order/statusUpdates');
                    statusUpdates.updateOrderStatus(order, {
                        status: 'cancelled',
                        c_cancel_description: 'Customer Service Center Cancelled'
                    });
                    if (order.status.value === Order.ORDER_STATUS_CANCELLED) {
                        sendCancelledEmail(order);
                    }
				}
			}
		}
		catch (e) {
			dw.system.Logger.error('Error while cancelling Order ' + orderNo + ':' + e);
			success = false;
		}
	}
	else {
		success = false;
	}

	this.success = success;

	try {
		ISML.renderTemplate('order/payment/order_confirm_success.isml', this);
	} catch (e) {
		Logger.error('Error while rendering template ' + templateName);
		throw e;
	}

}

/**
 * Confirmation Email Order
 */
exports.Confirmation = function () {

	this.action = 'confirmation';

	var orderNo = request.httpParameterMap.get('order_no');
	this.orderNo = orderNo;
	var success = true;
	if (orderNo) {
		try {
			var order = OrderMgr.getOrder(orderNo);
			if (order) {
				sendConfirmationEmail(order);
			}
		}
		catch (e) {
			dw.system.Logger.error('Error while sending confirmation e-mail for Order ' + orderNo + ':' + e);
			success = false;
		}
	}
	else {
		success = false;
	}

	this.success = success;

	try {
		ISML.renderTemplate('order/payment/order_confirm_success.isml', this);
	} catch (e) {
		Logger.error('Error while rendering template ' + templateName);
		throw e;
	}

}

function saveAddress(address, formData) {
	if (!address || !formData) {
		return;
	}

	if (formData.title && formData.title.stringValue && address.title !== formData.title.stringValue) {
		address.title = formData.title.stringValue;
	}

	if (formData.firstName && formData.firstName.stringValue && address.firstName !== formData.firstName.stringValue) {
		address.firstName = formData.firstName.stringValue;
	}

	if (formData.lastName && formData.lastName.stringValue && address.lastName !== formData.lastName.stringValue) {
		address.lastName = formData.lastName.stringValue;
	}

	if (formData.phone && formData.phone.stringValue && address.phone !== formData.phone.stringValue) {
		address.phone = formData.phone.stringValue;
	}

	if (formData.state && formData.state.stringValue && address.stateCode !== formData.state.stringValue) {
		address.stateCode = formData.state.stringValue;
	}

	if (formData.city && formData.city.stringValue && address.city !== formData.city.stringValue) {
		address.city = formData.city.stringValue;
	}

	if (formData.address1 && formData.address1.stringValue && address.address1 !== formData.address1.stringValue) {
		address.address1 = formData.address1.stringValue;
	}

	if (formData.address2 && address.address2 !== formData.address2.stringValue) {
		address.address2 = formData.address2.stringValue;
	}
}

/**
 * Saves submitted address details into shipping or billing address of order
 */
exports.SaveAddress = function () {
	this.order_no = request.httpParameterMap.order_no.stringValue;
	this.order = OrderMgr.getOrder(this.order_no);
	var order = this.order;
	var addressType = request.httpParameterMap.address_type.stringValue;
	if (addressType === 'shipping') {
		if (order.shipments.length > 0) {
			var shipment = order.shipments[0];
			if (shipment.shippingAddress) {
				Transaction.wrap(function () {
					saveAddress(shipment.shippingAddress, request.httpParameterMap);
				});
			}
		}
	} else if (addressType === 'billing') {
		Transaction.wrap(function () {
			saveAddress(order.billingAddress, request.httpParameterMap);
		});
	}

	var server = require('server');
	var addressForm = server.forms.getForm('address');
	try {
		ISML.renderTemplate('order/payment/order_confirm_details.isml', { "order_no": order_no, "order": order, "address_definition": addressForm });
	} catch (e) {
		Logger.error('Error while rendering template ' + templateName);
		throw e;
	}
}

/**
 * Saves return or cancel reason and the comment if provided
 */
exports.SaveReturnCancellation = function () {
	this.order_no = request.httpParameterMap.order_no.stringValue;
	this.order = OrderMgr.getOrder(this.order_no);
    var order = this.order;

	var returnReason = request.httpParameterMap.returnReason.stringValue;
    Transaction.wrap(function () {
        order.custom.returnReason = returnReason;
    });

    var cancellationReason = request.httpParameterMap.cancellationReason.stringValue;
    Transaction.wrap(function () {
        order.custom.cancellationReason = cancellationReason;
    });

    var returnCancellationComment = request.httpParameterMap.returnCancellationComment.stringValue;
    Transaction.wrap(function () {
        order.custom.returnCancellationComment = returnCancellationComment;
    });

	var server = require('server');
	var addressForm = server.forms.getForm('address');
	try {
		ISML.renderTemplate('order/payment/order_confirm_details.isml', { "order_no": this.order_no, "order": this.order, "address_definition": addressForm });
	} catch (e) {
		Logger.error('Error while rendering template ' + templateName);
		throw e;
	}
}

exports.Start.public = true;
exports.Cancel.public = true;
exports.Confirm.public = true;
exports.Confirmation.public = true;
exports.SaveAddress.public = true;
exports.SaveReturnCancellation.public = true;
