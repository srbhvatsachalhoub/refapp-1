'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');

/**
 * Render logic for storefront.imageAndText component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */

module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.image = ImageTransformation.getScaledImage(content.image);
    model.alt = content.alt || null;
    model.title = content.title || null;
    model.buttonText1 = content.buttonText1 || null;
    model.buttonLink1 = content.buttonLink1 || '#';
    model.buttonText2 = content.buttonText2 || null;
    model.buttonLink2 = content.buttonLink2 || '#';
    model.descriptionBelow = content.descriptionBelow || false;
    model.descriptionSmall = content.descriptionSmall || false;
    model.fullWidth = content.fullWidth || false;
    model.customClasses1 = context.content.classes1 || '';

    return new Template('experience/components/commerce_assets/mainBannerCustom').render(model).text;
};
