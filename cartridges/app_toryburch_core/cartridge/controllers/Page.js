'use strict';

var server = require('server');
server.extend(module.superModule);

/**
 * Update queryStringObj of variables
 * @param {Object} queryStringObj query string params
 */
function parseVariables(queryStringObj) {
    var newQueryString = queryStringObj;
    Object.keys(newQueryString.variables).forEach(function name(variant) {
        newQueryString['dwvar_' + newQueryString.variables[variant].id + '_' + variant] = newQueryString.variables[variant].value;
    });

    delete newQueryString.variables;
}

/**
 * Replcase SetLocate Action
 * To set the locale id value to cookie site_locale value
 * And to redirect with host change if the countries locale has host value
 */
server.replace('SetLocale', server.middleware.get, function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');
    var Currency = require('dw/util/Currency');
    var Site = require('dw/system/Site');
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var cookieHelpers = require('*/cartridge/scripts/helpers/cookieHelpers');
    var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');
    var inventoryHelpers = require('*/cartridge/scripts/helpers/inventoryHelpers');

    var QueryString = server.querystring;
    var currentSite = Site.getCurrent();
    var allowedCurrencies = currentSite.allowedCurrencies;
    var queryStringObj = new QueryString(req.querystring.queryString || '');

    if (Object.hasOwnProperty.call(queryStringObj, 'lang')) {
        delete queryStringObj.lang;
    }

    if (Object.hasOwnProperty.call(queryStringObj, 'variables')) {
        parseVariables(queryStringObj);
    }

    var localeId = req.querystring.code;
    if (req.setLocale(localeId)) {
        var currentBasket = BasketMgr.getCurrentBasket();
        var currency = Currency.getCurrency(req.querystring.CurrencyCode);
        var locale = localeHelpers.getLocale(localeId);

        // Update currency
        if (allowedCurrencies.indexOf(req.querystring.CurrencyCode) > -1
            && (req.querystring.CurrencyCode !== req.session.currency.currencyCode)) {
            req.session.setCurrency(currency);

            localeHelpers.setApplicablePriceBooks();

            if (currentBasket && currency && currentBasket.currencyCode !== currency.currencyCode) {
                Transaction.wrap(function () {
                    currentBasket.updateCurrency();
                });
            }
        }

        // Remove country specific data from shipping address
        COHelpers.removeShippingAddressFromBasket(locale.countryCode);

        inventoryHelpers.checkBasketProductInventoryList();

        var URLAction = require('dw/web/URLAction');
        var URLParameter = require('dw/web/URLParameter');

        var urlAction = new URLAction(req.querystring.action, currentSite.ID, localeId);
        var urlParameters = Object.keys(queryStringObj).map(function (parameterName) {
            return new URLParameter(parameterName, queryStringObj[parameterName]);
        });

        var url = URLUtils.url(urlAction, urlParameters);
        if (locale && locale.host) {
            url = URLUtils.sessionRedirect(locale.host, url);
            cookieHelpers.setSiteLocaleCookie(localeId, locale.host);
        } else {
            cookieHelpers.setSiteLocaleCookie(localeId);
        }

        res.json({
            success: true,
            redirectUrl: url.toString()
        });
    } else {
        res.json({ error: true });
    }
    next();
});

module.exports = server.exports();
