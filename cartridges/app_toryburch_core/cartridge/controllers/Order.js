'use strict';

var server = require('server');

server.extend(module.superModule);

var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var ProfileHelper = require('*/cartridge/scripts/helpers/profileHelper');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');

server.replace(
    'Confirm',
    server.middleware.get,
    consentTracking.consent,
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
        var OrderMgr = require('dw/order/OrderMgr');
        var OrderModel = require('*/cartridge/models/order');
        var Locale = require('dw/util/Locale');

        var order = OrderMgr.getOrder(req.querystring.ID);
        var token = req.querystring.token ? req.querystring.token : null;

        if (!order
            || !token
            || token !== order.orderToken
            || order.customer.ID !== req.currentCustomer.raw.ID
        ) {
            res.render('/error', {
                message: Resource.msg('error.confirmation.error', 'confirmation', null)
            });

            return next();
        }
        var lastOrderID = Object.prototype.hasOwnProperty.call(req.session.raw.custom, 'orderID') ? req.session.raw.custom.orderID : null;
        if (lastOrderID === req.querystring.ID) {
            res.redirect(URLUtils.url('Home-Show'));
            return next();
        }

        var config = {
            numberOfLineItems: '*'
        };

        var currentLocale = Locale.getLocale(req.locale.id);

        var orderModel = new OrderModel(
            order,
            { config: config, countryCode: currentLocale.country, containerView: 'order' }
        );
        var passwordForm;

        var reportingURLs = reportingUrlsHelper.getOrderReportingURLs(order);

        if (!req.currentCustomer.profile) {
            passwordForm = server.forms.getForm('confirmationRegister');
            passwordForm.clear();
            res.render('checkout/confirmation/confirmation', {
                order: orderModel,
                returningCustomer: false,
                passwordForm: passwordForm,
                reportingURLs: reportingURLs
            });
        } else {
            res.render('checkout/confirmation/confirmation', {
                order: orderModel,
                returningCustomer: true,
                reportingURLs: reportingURLs
            });
        }
        req.session.raw.custom.orderID = req.querystring.ID; // eslint-disable-line no-param-reassign
        return next();
    }
);

/**
 * Saves app_brand_core append
 */
server.append('Confirm', function (req, res, next) {
    res.setViewData({
        passwordConstraints: ProfileHelper.getPasswordConstraints()
    });
    next();
});

server.replace(
    'CreateAccount',
    server.middleware.post,
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var OrderMgr = require('dw/order/OrderMgr');

        var formErrors = require('*/cartridge/scripts/formErrors');

        var passwordForm = server.forms.getForm('confirmationRegister');
        var newPassword = passwordForm.newpassword.htmlValue;

        var order = OrderMgr.getOrder(req.querystring.ID);
        res.setViewData({ orderID: req.querystring.ID });
        var registrationObj = {
            firstName: order.billingAddress.firstName,
            lastName: order.billingAddress.lastName,
            phone: order.billingAddress.phone,
            email: order.customerEmail,
            password: newPassword
        };

        if (passwordForm.valid) {
            res.setViewData(registrationObj);

            this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
                var CustomerMgr = require('dw/customer/CustomerMgr');
                var Transaction = require('dw/system/Transaction');
                var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
                var addressHelpers = require('*/cartridge/scripts/helpers/addressHelpers');

                var registrationData = res.getViewData();

                var login = registrationData.email;
                var password = registrationData.password;
                var newCustomer;
                var authenticatedCustomer;
                var newCustomerProfile;
                var errorObj = {};

                delete registrationData.email;
                delete registrationData.password;

                // attempt to create a new user and log that user in.
                try {
                    Transaction.wrap(function () {
                        var error = {};
                        newCustomer = CustomerMgr.createCustomer(login, password);

                        var authenticateCustomerResult = CustomerMgr.authenticateCustomer(login, password);
                        if (authenticateCustomerResult.status !== 'AUTH_OK') {
                            error = { authError: true, status: authenticateCustomerResult.status };
                            throw error;
                        }

                        authenticatedCustomer = CustomerMgr.loginCustomer(authenticateCustomerResult, false);

                        if (!authenticatedCustomer) {
                            error = { authError: true, status: authenticateCustomerResult.status };
                            throw error;
                        } else {
                            // assign values to the profile
                            newCustomerProfile = newCustomer.getProfile();

                            newCustomerProfile.firstName = registrationData.firstName;
                            newCustomerProfile.lastName = registrationData.lastName;
                            newCustomerProfile.phoneHome = registrationData.phone;
                            newCustomerProfile.email = login;

                            order.setCustomer(newCustomer);

                            // save all used shipping addresses to address book of the logged in customer
                            var allAddresses = addressHelpers.gatherShippingAddresses(order);
                            allAddresses.forEach(function (address) {
                                addressHelpers.saveAddress(address, { raw: newCustomer }, addressHelpers.generateAddressName(address));
                            });
                        }
                    });
                } catch (e) {
                    errorObj.error = true;
                    errorObj.errorMessage = e.authError
                        ? Resource.msg('error.message.unable.to.create.account', 'login', null)
                        : Resource.msg('error.message.account.create.error', 'forms', null);
                }

                if (errorObj.error) {
                    res.json({ error: [errorObj.errorMessage] });

                    return;
                }

                accountHelpers.sendCreateAccountEmail(authenticatedCustomer.profile);

                res.json({
                    success: true,
                    redirectUrl: URLUtils.url('Account-Show', 'registration', 'submitted').toString()
                });
            });
        } else {
            res.json({
                fields: formErrors.getFormErrors(passwordForm)
            });
        }

        return next();
    }
);

/**
 * Saves app_brand_core append
 */
server.append(
    'CreateAccount',
    function (req, res, next) {
        var StringUtils = require('dw/util/StringUtils');
        var registrationData = res.getViewData();

        var emailOK = ProfileHelper.customerWithFieldValueDoesNotExists('email', registrationData.email);
        var mobilePhoneOK = ProfileHelper.customerWithFieldValueDoesNotExists('phoneMobile', registrationData.phone);
        var errorMessageType;
        if (!emailOK) {
            errorMessageType = 'email';
        } else if (!mobilePhoneOK) {
            errorMessageType = 'phoneMobile';
        }

        if (errorMessageType) {
            res.json({
                success: false,
                nonUniqueIdentifier: true,
                message: Resource.msg(StringUtils.format('error.message.{0}.alreadyexists', errorMessageType), 'forms', null)
            });

            this.emit('route:Complete', req, res);
            return;
        }
        var passwordForm = server.forms.getForm('newPasswords');
        this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
            var viewData = res.getViewData();

            // if success and customer authenticated
            // set the current locale into Profile PreferredLocale
            if (viewData.success) {
                var OrderMgr = require('dw/order/OrderMgr');

                var order = OrderMgr.getOrder(req.querystring.ID);
                if (order && order.customer && order.customer.authenticated) {
                    var Transaction = require('dw/system/Transaction');

                    Transaction.wrap(function () {
                        order.customer.profile.setPreferredLocale(req.locale.id);
                    });

                    // set recently authenticated customer to viewdata
                    viewData.authenticatedCustomer = order.customer;
                    res.setViewData(viewData);
                    if (passwordForm.addtoemaillist.checked) {
                        var currentCustomer = order.customer;
                        ProfileHelper.saveOptinInformation(currentCustomer.profile, passwordForm.accountpreferences.optinlist);
                        var serviceResult = ProfileHelper.updateNewsletter(currentCustomer.profile, req.locale.id);
                        if (!serviceResult.isOk()) {
                            require('dw/system/Logger')
                                .getLogger('speedbus')
                                .error('An unexpected error while subscribing to newsletter occured {0}', serviceResult.getErrorMessage());
                        }
                    }
                }
            }
        });

        next();
        return;
    }
);

module.exports = server.exports();
