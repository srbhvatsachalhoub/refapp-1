'use strict';


module.exports = function () {
    var $addToCartButton = $('button.add-to-cart, button.add-to-cart-global');

    $('body').on('product:beforeAddToCart', function () {
        $addToCartButton.spinner().start();
    });

    $('body').on('product:afterAddToCart', function () {
        $addToCartButton.spinner().stop();
    });
};
