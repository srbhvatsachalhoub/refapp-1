'use strict';

/**
 * Add event for prevent click on selected or disabled color swatch
 */
function colorAttribute() {
    $(document).on('click', '[data-attr="color"] button', function (event) {
        if ($(this).find('.swatch-value').hasClass('selected')) {
            event.stopImmediatePropagation();
        }
    });
}

module.exports = {
    colorAttribute: colorAttribute
};
