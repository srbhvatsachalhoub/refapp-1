'use strict';

/**
 * Process the attribute values for an attribute that has image swatches
 *
 * @param {Object} product - product
 * @param {jQuery} $productContainer - DOM container for a given product
 */
function processSwatchValues(product, $productContainer) {
    product.variationAttributes.forEach(function (attr) {
        if (attr.id === 'color') {
            var $selectedSwatch = $productContainer.find('.js-swatch-circle-wrapper.selected');

            attr.values.forEach(function (attrValue) {
                var $attrValue = $productContainer
                    .find('[data-attr-value="' + attrValue.value + '"]');
                var $swatchAnchor = $attrValue.parent();

                $attrValue.removeClass('unselectable disabled');
                $attrValue.addClass(attrValue.selectable ? '' : 'unselectable disabled');
                $swatchAnchor.attr('href', 'javascript:void(0)'); //eslint-disable-line

                if (attrValue.selected) {
                    $attrValue.addClass('selected');
                    $selectedSwatch.removeClass('selected');
                } else if (attrValue.url) {
                    $swatchAnchor.attr('href', attrValue.url);
                }
            });
        }
    });
}

/**
 * Updates carousel images on each attribute change
 * @param {Object} product - product object including images array
 * @param {jQuery} $container - product detail section element that contains carousel
 */
function updateCarouselAfterAttributeSelect(product, $container) {
    var smallImageUrls = product.images.medium;
    var $imageCarousel = $container.find('.js-plp-product-image-carousel');
    var $imageRef = $imageCarousel.find('.slick-slide:not(.slick-cloned) img').eq(0);
    var $linkRef = $imageCarousel.find('.image-container-link').eq(0);

    var $productCtaImage = $container.find('.js-product-cta-image-ph img');
    var imagesFragment = document.createDocumentFragment();

    // Prepare new carousel images
    if (smallImageUrls) {
        smallImageUrls.forEach(function (image) {
            var $image = $imageRef.clone();
            var $link = $linkRef.clone().empty();

            if ($image.length === 0) return;

            $image.attr('src', image.url);
            $image.attr('alt', image.alt);

            // Append new image to fragment
            $link.append($image.get(0));
            imagesFragment.appendChild($link.get(0));
        });
    }

    // Refresh image carousel content
    $imageCarousel
        .html(imagesFragment);

    $imageCarousel[0].slick = false;

    // Updata cta image
    if (smallImageUrls) {
        $productCtaImage.attr('src', smallImageUrls[0].url);
        $productCtaImage.attr('alt', smallImageUrls[0].alt);
    }

    $(document).trigger('carousel:reinit', { $container: $imageCarousel });
}

/**
 * Initialize swatch handler
 */
function initSwatchHandler() {
    $(document).delegate('.js-swatch-item-link', 'click', function (event) {
        event.preventDefault();

        var $currentSwatch = $(event.currentTarget);
        var url = $currentSwatch.attr('href');

        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json'
        }).done(function (response) {
            var product = response.product;

            var $productTile = $currentSwatch.closest('.js-product-tile');

            updateCarouselAfterAttributeSelect(product, $productTile);
            processSwatchValues(product, $productTile, $currentSwatch);

            $productTile.data('pid', product.id);
        });
    });
}

module.exports = function () {
    $(document).ready(function () {
        initSwatchHandler();
    });
};
