'use strict';

/**
 * Show a spinner inside a given element
 * @param {element} $target - Element to block by the veil and spinner.
 *                            Pass body to block the whole page.
 */
function addSpinner($target) {
    var $veil = $('<div class="veil"></div>');
    var spanTag = '<span class="preloader-element"></span>';
    var tagName = $target.get(0).tagName;

    if (tagName === 'BUTTON') {
        $veil.append('<div class="underlay-dark"></div><div class="preloader v2">' + spanTag.repeat(4) + '</div>');
    } else {
        $veil.append('<div class="underlay"></div><div class="preloader v2 circle-preloader dark">' + spanTag.repeat(10) + '</div>');
    }

    if (tagName === 'IMG') {
        $target.after($veil);
        $veil.css({ width: $target.width(), height: $target.height() });
        if ($target.parent().css('position') === 'static') {
            $target.parent().addClass('relative-preloader');
        }
    } else {
        $target.append($veil);
        if ($target.css('position') === 'static') {
            $target.parent().addClass('relative-preloader');
            $target.parent().addClass('veiled');
        }
        if (tagName === 'BODY') {
            $veil.find('.preloader').addClass('fixed-preloader');
        }
    }
    $veil.click(function (e) {
        e.stopPropagation();
    });
}

/**
 * Remove existing spinner
 * @param  {element} $veil - jQuery pointer to the veil element
 */
function removeSpinner($veil) {
    if ($veil.parent().hasClass('veiled')) {
        $veil.parent().css('position', '');
        $veil.parent().removeClass('veiled');
    }
    $veil.off('click');
    $veil.remove();
}

// element level spinner:
$.fn.spinner = function () {
    var $element = $(this);
    var Fn = function () {
        this.start = function () {
            if ($element.length) {
                addSpinner($element);
            }
        };
        this.stop = function () {
            if ($element.length) {
                var $veil = $('.veil');
                removeSpinner($veil);
            }
        };
    };
    return new Fn();
};

// page-level spinner:
$.spinner = function () {
    var Fn = function () {
        this.start = function () {
            addSpinner($('body'));
        };
        this.stop = function () {
            removeSpinner($('.veil'));
        };
    };
    return new Fn();
};
