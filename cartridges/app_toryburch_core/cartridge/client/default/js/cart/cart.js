'use strict';

var quantitySelector = require('../product/quantitySelector');
var countCharacters = require('brand_core/helpers/countCharacters');

module.exports = function () {
    $('body').on('shown.bs.modal', '#editProductModal', function () {
        quantitySelector.init();
    });

    // Init char counter
    countCharacters();

    // Toggle gift wrapping textarea
    $('.js-gift-toggle').click(function () {
        var $giftForm = $(this).closest('.js-update-cart-gift-form');
        var $giftWrapping = $giftForm.closest('.js-gift-wrapping');
        var $giftFormItem = $('.js-gift-form-item', $giftForm);
        var $isGiftInput = $('.js-isGift-input', $giftForm);
        var hasMessage = !!$giftForm.find('textarea').val();
        if ($giftWrapping.hasClass('gift-wrapping-active')) {
            $giftFormItem.addClass('d-none');
            $giftWrapping.removeClass('gift-wrapping-active');
        } else {
            $giftFormItem.removeClass('d-none');
            $giftWrapping.addClass('gift-wrapping-active');
        }

        if ((hasMessage && !$isGiftInput.prop('checked')) || (!hasMessage && $isGiftInput.prop('checked'))) {
            $isGiftInput.click();
        }
    });

    $('.js-coupon-code-field').on('input', function (event) {
        $('.js-apply-promo-code').show();
        $(this).off(event);
    });

    // make sure the gift message is one line
    $('body').on('keypress', '#id_dwfrm_gift_giftMessage', function (e) {
        return (e.keyCode || e.which || e.charCode || 0) !== 13;
    });

    $('body').on('product:afterUpdate', function (e, response) {
        response.cartModel.items.forEach(function (item) {
            var $quantity = $('.js-quantity[data-uuid="' + item.UUID + '"]');
            $quantity.text(item.quantity);
        });
    });

    $('body').on('product:afterAttributeSelect', function (e, response) {
        const $container = response.container;
        const product = response.data.product;

        if (!$container || !product) {
            return;
        }

        const images = product.images;
        if (images.large && images.large[0]) {
            const mainImage = images.large[0];
            const $imageElement = $container.find('.product-image img');

            $imageElement.attr('src', mainImage.url);
            $imageElement.attr('alt', mainImage.alt);
            $imageElement.attr('title', mainImage.title);
        }
    });
};
