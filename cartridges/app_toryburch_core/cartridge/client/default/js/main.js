window.jQuery = window.$ = require('jquery');
var processInclude = require('base/util');
window.RA_BREAKPOINTS = require('./config/breakpoints.json').breakpoints;

$(document).ready(function () {
    processInclude(require('./utils/componentsMgr'));
    processInclude(require('brand_core/dataStates'));
    processInclude(require('brand_core/components/tooltip'));
    processInclude(require('brand_core/components/login'));
    processInclude(require('brand_core/components/cookieWarning'));
    processInclude(require('./header'));
    processInclude(require('./product/addToCartNotification'));
    processInclude(require('./footer'));
    processInclude(require('./product/productAttributes'));
    processInclude(require('brand_core/components/miniCart'));
    processInclude(require('base/components/collapsibleItem'));
    processInclude(require('brand_core/components/clientSideValidation'));
    processInclude(require('base/components/countrySelector'));
    processInclude(require('brand_core/newsletterSubscribe/newsletter'));
    processInclude(require('brand_core/components/cleave'));
    processInclude(require('brand_core/components/selectdate'));
    processInclude(require('brand_core/components/password'));
    processInclude(require('brand_core/components/multilineEllipsis'));
    processInclude(require('./carousel/globalCarousel'));
    processInclude(require('./common/bootstrapCustom'));
    processInclude(require('./product/productCarousel'));
    processInclude(require('base/components/toolTip'));
    processInclude(require('./product/productTileAddToCart'));
    processInclude(require('./productTile'));
    processInclude(require('./footerNewsletter/footerNewsletterHandler'));
});

/* Third party scripts */
require('bootstrap/js/src/util.js');
require('bootstrap/js/src/alert.js');
require('bootstrap/js/src/carousel.js');
require('bootstrap/js/src/collapse.js');
require('bootstrap/js/src/modal.js');
require('bootstrap/js/src/scrollspy.js');
require('bootstrap/js/src/tab.js');
require('bootstrap/js/src/tooltip.js');
require('bootstrap/js/src/dropdown.js');
require('./components/spinner');
require('slick-carousel');

/* jQuery Plugins */
processInclude(require('brand_core/plugins/sticky'));
processInclude(require('brand_core/plugins/productLoader'));
processInclude(require('brand_core/plugins/consentTracking'));
processInclude(require('brand_core/plugins/searchWithSuggestions'));
