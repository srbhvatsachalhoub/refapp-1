'use strict';

var $configs = $('.js-stores-configs');
var storeLocator = {
    mainUrl: $configs.data('main-url'),
    radius: $configs.data('radius'),
    findStoresUrl: $configs.data('find-stores-url'),
    $form: $('.js-storelocator'),
    $countrySelect: $('.js-storelocator-country'),
    currentCountryCode: null,
    $modal: $('.js-store-modal'),
    $results: $('.js-results'),
    $noResults: $('.js-no-results')
};

storeLocator.updateStoresList = function () {
    storeLocator.$countrySelect.on('change', function () {
        var selectedCountryCode = $(this).find('option:selected').val();
        if (selectedCountryCode === storeLocator.currentCountryCode || selectedCountryCode === '') {
            return;
        }
        storeLocator.currentCountryCode = selectedCountryCode;
        var redirectURL = storeLocator.$form.attr('action');

        window.location.href = redirectURL + '?countryCode=' + selectedCountryCode;
    });
};

storeLocator.findStoresByLocation = function (pos) {
    $.spinner().start();
    $.ajax({
        url: storeLocator.findStoresUrl,
        type: 'get',
        dataType: 'json',
        data: {
            lat: pos.lat,
            long: pos.lng,
            radius: storeLocator.radius,
            country: 'allCountry'
        },
        success: function (data) {
            var hasResults = data.stores.length > 0;
            if (hasResults) {
                var selectedCountryCode = $(this).find('option:selected').val();
                storeLocator.currentCountryCode = selectedCountryCode;
                var redirectURL = storeLocator.$form.attr('action');

                // eslint-disable-next-line no-useless-concat
                window.location.href = redirectURL + '?countryCode=' + 'allCountry' + '&lat=' + data.searchKey.lat + '&long=' + data.searchKey.long;
            } else {
                storeLocator.$modal.modal('show');
            }
            $.spinner().stop();
        },
        error: function () {
            $.spinner().stop();
        }
    });
};

storeLocator.detectLocation = function () {
    // clicking on detect location.
    $('.js-use-my-current-location').on('click', function () {
        $.spinner().start();
        if (!navigator.geolocation) {
            $.spinner().stop();
            return;
        }

        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            storeLocator.findStoresByLocation(pos);
        });

        $.spinner().stop();
    });
};

module.exports = {
    init: function () {
        storeLocator.updateStoresList();
        storeLocator.detectLocation();

        if (!storeLocator.$results.data('has-results')) {
            storeLocator.$noResults.show();
        }
    }
};
