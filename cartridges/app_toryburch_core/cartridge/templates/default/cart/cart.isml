<isinclude template="/components/modules" sf-toolkit="off" />
<isdecorate template="common/layout/page">
    <isscript>
        var Site = require('dw/system/Site');
        var assets = require('*/cartridge/scripts/assets.js');
        assets.addCss('/css/cart.css');
        assets.addJs('/js/cart.js');
        if (Site.getCurrent().getCustomPreferenceValue('isGiftCardEnabled')) {
            assets.addJs('/js/giftCardDetail.js');
        }
    </isscript>

    <isif condition="${pdict.reportingURLs && pdict.reportingURLs.length}">
        <isinclude template="reporting/reportingUrls" />
    </isif>
    <isinclude template="cart/cartEmpty" />
    <isif condition="${pdict.items.length > 0}">
        <div class="container mt-4 cart cart-page">
            <div class="row">
                <div class="col">
                    <h2 class="cart-title text-center">${Resource.msg('label.your.bag', 'cart', null)}
                        <span class="js-item-count d-none"
                            data-singular-tpl="(${Resource.msg('label.one.item.in.cart', 'cart', null)})"
                            data-plural-tpl="(${Resource.msg('label.x.items.in.cart', 'cart', null)})">
                            (${Resource.msgf(pdict.numItems === 1 ? 'label.one.item.in.cart' : 'label.x.items.in.cart', 'cart', null, pdict.numItems)})
                        </span>
                    </h2>
                </div>
            </div>
            <div class="row cart-page-wrapper">
                <div class="cart-page-main col-lg-7 col-xl-6 offset-xl-1">
                    <div class="cart-error-messaging cart-error">
                        <isif condition="${pdict.valid && pdict.valid.error && pdict.items.length !== 0}">
                            <div class="alert alert-danger alert-dismissible valid-cart-error mb-4 fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                ${pdict.valid.message}
                            </div>
                        </isif>
                    </div>
                    <div class="row cart-header-row text-uppercase d-none">
                        <div class="col-xl-7">
                            ${Resource.msg('label.products', 'checkout', null)}
                        </div>
                        <div class="col-xl-3">
                            ${Resource.msg('label.quantity', 'checkout', null)}
                        </div>
                        <div class="col-xl-2 px-xl-0">
                            ${Resource.msg('label.price', 'checkout', null)}
                        </div>
                    </div>
                    <div class="cart-product-list">
                        <isloop items="${pdict.items}" var="lineItem">
                            <isif condition="${lineItem.productType === 'bundle'}">
                                <isinclude template="cart/productCard/cartBundleCard" />
                            <iselse/>
                                <isif condition="${lineItem.noProduct === true}">
                                    <isinclude template="cart/productCard/uncategorizedCartProductCard" />
                                <iselse/>
                                    <isinclude template="cart/productCard/cartProductCard" />
                                </isif>
                            </isif>
                        </isloop>
                    </div>
                    <div class="js-sidebar-placeholder-mobile"></div>
                    <div class="row cart-menu-vertical row">
                        <isif condition="${pdict.bonusDiscountItems && pdict.bonusDiscountItems.length > 0}">
                            <div class="col-12">
                                <div class="cart-section-wrapper js-acc-container open">
                                    <h3 data-toggle-class-open="parent">
                                        <i class="sc-icon-free-samples tab-icon"></i>
                                        ${Resource.msg('label.select.free.samples', 'cart', null)}
                                        <i class="sc-icon-plus"></i>
                                        <i class="sc-icon-minus"></i>
                                    </h3>
                                    <ul>
                                        <li>
                                            <isinclude template="cart/bonusDiscountLineItems" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </isif>
                        <isif condition="${!pdict.hasGiftCardProduct && dw.system.Site.getCurrent().getCustomPreferenceValue('enableSendAsGift')}">
                            <isinclude template="cart/components/isGift" />
                        </isif>
                    </div>
                    <div class="row cart-free-shipping-wrapper">
                        <div class="col-12 js-cart-free-shipping-approaching-products">
                            <isinclude template="cart/freeShippingApproachingProducts" />
                        </div>
                    </div>
                </div>
                <div class="cart-page-sidebar col-lg-5 totals js-sticky-sidebar-container">
                    <iscomment>totals, and checkout actions</iscomment>
                    <div class="js-sticky-sidebar">
                        <div class="js-sticky-sidebar-inner">
                            <div class="next-step-box">
                                <isinclude template="cart/cartPromoCode" />
                                <div class="row">
                                    <div class="col-12">
                                        <div class="next-step-box-totals">
                                            <div class="row totals-line">
                                                <div class="col-6 col-sm-8 sub-total-label">
                                                    <span>${Resource.msg('label.sub.total', 'cart', null)}
                                                    </span>
                                                    <span>(${Resource.msg('label.vat.included', 'cart', null)})
                                                    </span>
                                                </div>

                                                <div class="col-6 col-sm-4 totals-line-value order-sub-total price ">
                                                    <span
                                                        class='sub-total text-right'>
                                                        ${pdict.totals.subTotal}
                                                    </span>
                                                </div>
                                            </div>
                                            <div class='row shipping-discount js-discount-value-line <isif condition="
                                                ${!pdict.totals.orderLevelDiscountTotal.discountedSubTotal}">d-none</isif>'>
                                                <div class="col-6 discount-label">
                                                    ${Resource.msg('label.discount', 'checkout', null)}
                                                </div>
                                                <div class="col-6 text-right totals-line-value js-discount-value">
                                                    ${pdict.totals.orderLevelDiscountTotal.formatted}
                                                </div>
                                            </div>
                                            <div class="row shipping-item <isif condition="${pdict.totals.totalShippingCost.toLowerCase() === 'free'}">free-shipping</isif>">
                                                <div class="col-6 start-lines">
                                                    <p class="order-receipt-label"><span>${Resource.msg('label.order.shipping.cost','confirmation', null)}</span></p>
                                                </div>
                                                <div class="col-6 end-lines">
                                                    <p class="text-right"><span class="shipping-total-cost">${pdict.totals.totalShippingCost}</span></p>
                                                </div>
                                            </div>
                                            <div class="row shipping-discount ${pdict.totals.shippingLevelDiscountTotal.value === 0 ? 'hide-shipping-discount free-shipping' : ''}">
                                                <div class="col-6 start-lines">
                                                    <p class="order-receipt-label"><span>${Resource.msg('label.shipping.discount', 'common', null)}</span></p>
                                                </div>
                                                <div class="col-6 end-lines">
                                                    <p class="text-right"><span class="shipping-discount-total">${pdict.totals.shippingLevelDiscountTotal.formatted}</span></p>
                                                </div>
                                            </div>
                                            <div class="row sales-tax-item">
                                                <div class="col-6 start-lines">
                                                    <p class="order-receipt-label"><span>${Resource.msg('label.order.sales.tax','confirmation', null)}</span></p>
                                                </div>
                                                <div class="col-6 end-lines">
                                                    <p class="text-right"><span class="tax-total">${pdict.totals.totalTax}</span></p>
                                                </div>
                                            </div>
                                            <div class="row total-container">
                                                <div class="col-6 start-lines">
                                                    <p class="order-receipt-label"><span>${Resource.msg('label.order.grand.total','confirmation', null)}</span></p>
                                                </div>
                                                <div class="col-6 end-lines">
                                                    <p class="text-right"><span class="grand-total">${pdict.totals.grandTotal}</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="promo-applied-label js-have-promo-label d-none">
                                    ${Resource.msg('label.promo.applied', 'cart', null)}</div>
                                <div class="coupons-and-promos">
                                    <isinclude template="cart/cartCouponDisplay" />
                                </div>
                                <div class="row">
                                    <div class="col-12 checkout-continue">
                                        <a href="${URLUtils.https('Checkout-Login')}"
                                            class="h-100 btn btn-primary btn-large btn-block ${pdict.valid && pdict.valid.error ? 'disabled' : ''}"
                                            role="button" aria-pressed="true">
                                            ${Resource.msg('button.checkout', 'cart', null)}
                                        </a>
                                    </div>
                                </div>
                                <iscontentasset aid="cart-coupon-presented-image" />
                                <iscontentasset aid="checkout-supported-payment-methods" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cart-page-carousel product-carousel-wrapper">
                <isslot id="product-carousel-2" description="Product carousel slot" context="global" />
                <iscomment>
                TODO: Should be updated
                <ul class="nav nav-tabs justify-content-center" role="tablist">
                    <li class="nav-item recently-viewed-tab">
                        <a class="nav-link active" id="recently-viewed-tab" data-toggle="tab" href="#recently-viewed" role="tab" aria-controls="recently-viewed" aria-selected="true">${Resource.msg('label.recentlyviewed', 'cart', null)}</a>
                    </li>
                    <li class="nav-item also-like-tab">
                        <a class="nav-link" id="also-like-tab" data-toggle="tab" href="#also-like" role="tab" aria-controls="also-like" aria-selected="false">${Resource.msg('label.alsolike', 'cart', null)}</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade" id="also-like" role="tabpanel" aria-labelledby="also-like-tab">
                        <isslot id="product-carousel-2" description="Product carousel slot" context="global" />
                    </div>
                    <div class="tab-pane fade show active" id="recently-viewed" role="tabpanel" aria-labelledby="home-tab">
                        <isslot id="product-carousel-3" description="Product carousel slot" context="global" />
                    </div>
                </div>
                </iscomment>
            </div>

            <isinclude template="cart/cartRemoveProductModal" />
        </div>

        <isinclude template="cart/cartRemoveCouponModal" />
    </isif>
    <div class="container">
        <isslot id="cart-recommendations-m" description="Recommended products" context="global" />
    </div>
</isdecorate>