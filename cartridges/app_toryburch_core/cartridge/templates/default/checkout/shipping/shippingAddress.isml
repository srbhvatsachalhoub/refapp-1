<isinclude template="/components/modules" sf-toolkit="off" />
<isset name="addressFields" value="${pdict.forms.shippingForm.shippingAddress.addressFields}" scope="page"/>
<isif condition="${shippingModel.shippingAddress}">
    <isset name="shippingAddress" value="${shippingModel.shippingAddress}" scope="page" />
<iselse/>
    <isset name="shippingAddress" value="${{}}" scope="page" />
</isif>

<input type="hidden" value="${pdict.countryCode}" id="shippingCountry"
    <isprint value=${addressFields.country.attributes} encoding="off"/> />

<div class="row">
    <div class="col-12 shipping-address-title">
        <isselectbox    selectbox_formfield="${addressFields.title.titleList}"
                            selectbox_selected="${shippingAddress.title}"
                            selectbox_hidelabel="${true}"
                            selectbox_preselectvalue="${Resource.msg('label.profile.title', 'forms', null)}"
        />
    </div>
    <div class="col-lg-6">
        <isinputtext    inputtext_formfield="${addressFields.firstName}"
                        inputtext_value="${shippingAddress.firstName || ''}"
                        inputtext_autocomplete="given-name"
                        inputtext_missingerror="${Resource.msg('error.message.required.firstname', 'forms', null)}"
                        inputtext_rangeerror="${Resource.msg('error.message.range.firstname', 'forms', null)}"
                        inputtext_parseerror="${Resource.msg('error.message.parse.firstname', 'forms', null)}"
                        inputtext_placeholder="${addressFields.firstName}"
                        inputtext_labelafter="${true}"
        />
    </div>
    <div class="col-lg-6">
        <isinputtext    inputtext_formfield="${addressFields.lastName}"
                        inputtext_value="${shippingAddress.lastName || ''}"
                        inputtext_autocomplete="family-name"
                        inputtext_missingerror="${Resource.msg('error.message.required.lastname', 'forms', null)}"
                        inputtext_rangeerror="${Resource.msg('error.message.range.lastname', 'forms', null)}"
                        inputtext_parseerror="${Resource.msg('error.message.parse.lastname', 'forms', null)}"
                        inputtext_placeholder="${addressFields.lastName}"
                        inputtext_labelafter="${true}"
        />
    </div>
    <div class="col-lg-6">
        <isinputphone   inputphone_formfield="${addressFields.phone}"
                        inputphone_value="${shippingAddress.phone}"
                        inputphone_label="${Resource.msg('field.shipping.address.phone','address',null)}"
                        inputphone_missingerror="${Resource.msg('error.message.required.phonemobile', 'forms', null)}"
                        inputphone_parseerror="${Resource.msg('error.message.parse.phonemobile', 'forms', null)}"
                        inputphone_rangeerror="${Resource.msg('error.message.range.phonemobile', 'forms', null)}"
                        inputphone_hidelabel="${true}"
        />
    </div>
    <div class="col-lg-6">
        <isscript>
            var customerEmail = '';
            if (pdict.order.orderEmail) {
                customerEmail = pdict.order.orderEmail;
            } else if (customer.authenticated && customer.profile && customer.profile.email) {
                customerEmail = customer.profile.email;
            }
        </isscript>
        <isinputtext    inputtext_formfield="${addressFields.email}"
                        inputtext_type="email"
                        inputtext_value="${customerEmail}"
                        inputtext_autocomplete="email"
                        inputtext_missingerror="${Resource.msg('error.message.required.email', 'forms', null)}"
                        inputtext_parseerror="${Resource.msg('error.message.parse.email', 'forms', null)}"
                        inputtext_placeholder="${addressFields.email}"
                        inputtext_labelafter="${true}"
        />
    </div>
    <div class="col-lg-6">
        <isinputcity    inputcity_formfield="${addressFields.city}"
                        inputcity_formfield_cities="${addressFields.cities}"
                        inputcity_value="${shippingAddress.city}"
                        inputcity_missingerror="${Resource.msg('error.message.required.cityshipping', 'forms', null)}"
                        inputcity_hidelabel="${true}"
                        inputcity_preselectvalue="${Resource.msg('label.shipping.city', 'forms', null)}"
                        inputcity_context="shipping_address"
        />
    </div>
    <div class="col-lg-6">
        <isinputtext    inputtext_formfield="${addressFields.address2}"
                        inputtext_value="${shippingAddress.address2 || ''}"
                        inputtext_autocomplete="shipping address-line2"
                        inputtext_placeholder="${addressFields.address2}"
                        inputtext_labelafter="${true}" />
    </div>
    <div class="col-12">
        <isinputtext    inputtext_formfield="${addressFields.address1}"
                        inputtext_value="${shippingAddress.address1 || ''}"
                        inputtext_autocomplete="shipping address-line1"
                        inputtext_class="js-input-address-one"
                        inputtext_missingerror="${Resource.msg('error.message.required.address', 'forms', null)}"
                        inputtext_rangeerror="${Resource.msg('error.message.range.address', 'forms', null)}"
                        inputtext_placeholder="${addressFields.address1}"
                        inputtext_labelafter="${true}"
        />
    </div>
</div>

<isif condition="${customer.authenticated}">
    <div class="row shipping-form-actions">
        <div class="col-lg-6">
            <div class="form-group">
                <input type="checkbox"
                    id="shippingSetDefault"
                    class="js-set-address-default-checkbox"
                    <isprint value=${addressFields.setDefault.attributes} encoding="off"/> />
                <label class="form-control-label font-weight-light" for="shippingSetDefault">
                    ${Resource.msg('field.shipping.address.make.default', 'address', null)}
                </label>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group shipping-form-action-buttons">
                <button type="button" class="btn btn-outline-secondary mt-0 js-hide-shipping-address-form">
                    ${Resource.msg('button.cancel', 'common', null)}
                </button>
                <div class="mx-3"></div>
                <button type="button" class="btn btn-primary mt-0 js-save-address-btn"
                    data-type="shipping"
                    data-url="${URLUtils.url('Address-SaveAddress')}">
                    ${Resource.msg('button.save', 'common', null)}
                </button>
            </div>
        </div>
    </div>
</isif>
