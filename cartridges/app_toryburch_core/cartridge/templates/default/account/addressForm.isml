<form action="${URLUtils.url('Address-SaveAddress', 'addressId', pdict.addressId)}"
    method="POST"
    class="address-form js-address-form"
    name="address-form"
    <isprint value="${pdict.addressForm.attributes}" encoding="off" />>

    <input type="hidden"
        class="js-input-country-code"
        value="${pdict.countryCode}"
        <isprint value=${pdict.addressForm.country.attributes} encoding="off"/> />

    <div class="row">
        <div class="col-12">
            <isselectbox    selectbox_formfield="${pdict.addressForm.title.titleList}"
                            selectbox_selected="${pdict.addressForm.title.titleList.selectedOption}"
                            selectbox_hidelabel="${true}"
                            selectbox_preselectvalue="${Resource.msg('label.profile.title', 'forms', null)}"
            />
        </div>

        <div class="col-lg-12">
            <isinputtext    inputtext_formfield="${pdict.addressForm.firstName}"
                            inputtext_autocomplete="given-name"
                            inputtext_missingerror="${Resource.msg('error.message.required.firstname', 'forms', null)}"
                            inputtext_rangeerror="${Resource.msg('error.message.range.firstname', 'forms', null)}"
                            inputtext_parseerror="${Resource.msg('error.message.parse.firstname', 'forms', null)}"
                            inputtext_placeholder="${pdict.addressForm.firstName}"
                            inputtext_labelafter="${true}"
            />
        </div>

        <div class="col-lg-12">
            <isinputtext    inputtext_formfield="${pdict.addressForm.lastName}"
                            inputtext_autocomplete="family-name"
                            inputtext_missingerror="${Resource.msg('error.message.required.lastname', 'forms', null)}"
                            inputtext_rangeerror="${Resource.msg('error.message.range.lastname', 'forms', null)}"
                            inputtext_parseerror="${Resource.msg('error.message.parse.lastname', 'forms', null)}"
                            inputtext_placeholder="${pdict.addressForm.lastName}"
                            inputtext_labelafter="${true}"
            />
        </div>

        <div class="col-lg-12">
            <isinputphone   inputphone_formfield="${pdict.addressForm.phone}"
                            inputphone_value="${pdict.addressForm.phone.htmlValue}"
                            inputphone_label="${Resource.msg('label.input.phoneMobile.profile', 'forms', null)}"
                            inputphone_missingerror="${Resource.msg('error.message.required.phonemobile', 'forms', null)}"
                            inputphone_parseerror="${Resource.msg('error.message.parse.phonemobile', 'forms', null)}"
                            inputphone_rangeerror="${Resource.msg('error.message.range.phonemobile', 'forms', null)}"
                            inputtext_placeholder="${pdict.addressForm.phone}"
                            inputtext_labelafter="${true}"
            />
        </div>

        <div class="col-lg-12">
            <isinputcity    inputcity_formfield="${pdict.addressForm.city}"
                            inputcity_formfield_cities="${pdict.addressForm.cities}"
                            inputtext_placeholder="${pdict.addressForm.city}"
                            inputtext_labelafter="${true}" />
        </div>

        <div class="col-lg-12">
            <isinputtext    inputtext_formfield="${pdict.addressForm.address2}"
                            inputtext_autocomplete="address-line2"
                            inputtext_placeholder="${pdict.addressForm.address2}"
                            inputtext_labelafter="${true}" />
        </div>

        <div class="col-lg-12">
            <div class="form-group ${pdict.addressForm.address1.mandatory === true ? 'required' : ''}">
                <input  type="text"
                        class="form-control js-input-address-one"
                        id="address1"
                        autocomplete="address-line1"
                        placeholder="<isprint value="${pdict.addressForm.address1.label}" encoding="htmlcontent" />"
                        <isprint value="${pdict.addressForm.address1.attributes}" encoding="off" />
                        data-missing-error="${Resource.msg('error.message.required.address', 'forms', null)}"
                        data-range-error="${Resource.msg('error.message.range.address', 'forms', null)}"
                />
                <label class="form-control-label" for="address1">
                    <span><isprint value="${pdict.addressForm.address1.label}" encoding="htmlcontent" /></span>
                </label>
                <input  type="hidden"
                        class="form-control js-input-address-id"
                        id="addressId"
                        <isprint value="${pdict.addressForm.addressId.attributes}" encoding="off" />
                />
                <div class="invalid-feedback"></div>
            </div>
        </div>
        <iscomment>Make default address</iscomment>
        <isif condition="${pdict.preferredAddress !== null}">
            <div class="col-12">
                <div class="form-group">
                    <input type="checkbox"
                        id="setDefault"
                        class="js-set-address-default-checkbox"
                        ${pdict.addressForm.setDefault.checked ? 'disabled' : ''}
                        <isprint value=${pdict.addressForm.setDefault.attributes} encoding="off"/> />
                    <label class="form-control-label" for="setDefault">
                        <isif condition="${pdict.addressForm.setDefault.checked}">
                            ${Resource.msg('label.input.default.address', 'forms', null)}
                        <iselse/>
                            ${pdict.addressForm.setDefault.label}
                        </isif>
                    </label>
                </div>
            </div>
        </isif>
        <input type="hidden" name="${pdict.csrf.tokenName}" value="${pdict.csrf.token}" />
    </div>
    <div class="row mb-5">
        <isif condition="${pdict.addressForm.addressId.value}">
            <div class="col-6">
                <button type="button" class="btn btn-block btn-outline-primary js-button-delete-address"
                    data-toggle="modal"
                    data-target="#deleteAddressModal"
                    data-id="${pdict.addressForm.addressId.value}"
                    data-url="${URLUtils.url('Address-DeleteAddress')}"
                    data-address="${pdict.addressForm.address1.value}"
                    data-default="${pdict.addressForm.setDefault.checked}">
                    ${Resource.msg('button.delete','common',null)}
                </button>
            </div>
        </isif>
        <div class="${pdict.addressForm.addressId.value ? 'col-6' : 'col-lg-6'}">
            <button type="submit" name="save" class="btn btn-block btn-primary js-button-save-address">
                <isif condition="${pdict.addressForm.addressId.value}">
                    ${Resource.msg('button.update','common',null)}
                <iselse/>
                    <span class="d-md-none">${Resource.msg('button.addaddress','forms',null)}</span>
                    <span class="hidden-sm-down">${Resource.msg('button.save','common',null)}</span>
                </isif>
            </button>
        </div>
    </div>
</form>
