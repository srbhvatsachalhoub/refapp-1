<iscomment>

Template Notes:

- This template is intended to be referenced from an <isinclude> tag within an <isloop> in another
  source template.  The <isloop> is expected to have a "status" attribute, named "attributeStatus".
- ${attributeStatus.last} is checked to see whether a particular attribute row should include the
  Quantity drop-down menu

</iscomment>
<div class="attribute attribute-${attr.id}">
    <isif condition="${attr.swatchable}">
        <iscomment>Select color dropdown</iscomment>
        <isif condition="${attr.values.length > 1}">
            <div class="swatches">
                <iscomment>Selected color</iscomment>
                <isif condition="${!(isBundle && product.productType === 'variant' && !attr.selectedValue)}">
                    <isif condition="${attr.selectedValue}">
                        <button type="button" class="swatches-item"
                            data-url="javascript:void(0)"
                            title="${attr.selectedValue.displayValue}">
                            <span data-attr-value="${attr.selectedValue.value}"
                                class="${attr.id}-value swatch-circle swatch-value selected swatch-value-${attr.selectedValue.displayValue}"
                                style="background-image: url(${attr.selectedValue.images['swatch'][0].url})"
                            ></span>
                        </button>
                    </isif>
                </isif>

                <iscomment>Other color options</iscomment>
                <isloop items="${attr.values}" var="attrValue">
                    <isif condition="${!(isBundle && product.productType === 'variant' && !attrValue.selected)}">
                        <isif condition="${!attrValue.selected}">
                            <button type="button" class="swatches-item  ${!attrValue.selectable ? 'd-none' : '' }"
                                data-url="${attrValue.url || 'javscript:void(0)'}"
                                title="${attrValue.displayValue}"
                                ${!attrValue.selectable ? 'disabled' : '' }
                                >
                                <span data-attr-value="${attrValue.value}"
                                    class="${attr.id}-value swatch-circle swatch-value swatch-value-${attrValue.displayValue} ${attrValue.selectable ? 'selectable' : 'unselectable'}"
                                    style="background-image: url(${attrValue.images['swatch'][0].url})"
                                ></span>
                            </button>
                        </isif>
                    </isif>
                </isloop>
            </div>
        </isif>
        <isif condition="${!(isBundle && product.productType === 'variant' && !attr.selectedValue)}">
            <isif condition="${attr.selectedValue}">
                <div>
                    <span class="color-ladel">${Resource.msg('label.color', 'product', '')}</span>
                    <span class="js-selected-swatch-name">
                        ${attr.selectedValue.displayValue}
                    </span>
                </div>
            </isif>
        </isif>
    <iselse/>
        <isif condition="${attr.values.length > 1}">
            <isset name="unSelectableSize" value="${!attrValue.selectable && attr.id === 'size'}" scope="page" />
            <isset name="placeholderDataAtr" value="data-placeholder=${Resource.msgf('label.detail.size', 'forms', null)}" scope="page" />
            <isset name="isDisabled" value="${(product.productType === "variant" && isBundle) || attr.disabled}" scope="page" />

            <select id="${attr.id}"
                data-cmp="customSelect"
                data-placeholder= "${Resource.msgf('label.detail.size', 'forms', null)}"
                data-soldout="${Resource.msg('product.sold.out', 'product', null)}"

                class="form-control-select select select-${attr.id} ${attr.values.length > 1 ? '' : 'single-select-option'}"

                <isprint value="${unSelectableSize ? placeholderDataAtr : ''}" />
                <isprint value="${isDisabled ? 'disabled' : ''}" />
            >

                <isif condition="${!attr.selectedValue && attr.id === 'size'}">
                    <option></option>
                </isif>

                <isloop items="${attr.values}" var="attrValue">
                    <isset name="isSoldOut" value="${!attrValue.selectable && attr.id === 'size'}" scope="page" />
                    <option value="${attrValue.url}"
                            data-attr-value="${attrValue.value ? attrValue.value : ''}"
                            data-subtext="${attrValue.price ? attrValue.price : ''}"
                            data-option-label="${isSoldOut ? Resource.msg('product.sold.out', 'product', null) : ''}"
                            ${!attrValue.selectable ? 'disabled' : ''}
                            ${attrValue.selected ? 'selected' : ''}
                    >
                        <isprint value="${attrValue.displayValue}"/>
                    </option>
                </isloop>
            </select>
        </isif>
    </isif>
</div>
