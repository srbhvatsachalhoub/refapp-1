'use strict';

var Logger = require('dw/system/Logger'); // can be custom logger
var Status = require('dw/system/Status');

var imgTypeMap = null;

/**
 * Returns variation products from all products in the catalog
 * @param {string} catalogId Catalog ID
 * @returns {Array} Array of variation products
 */
function getVariantProducts(catalogId) {
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var productVariants = [];

    var neededCatalog = CatalogMgr.getCatalog(catalogId);
    var products = ProductMgr.queryProductsInCatalog(neededCatalog);

    while (products.hasNext()) {
        var product = products.next();
        if (product.isVariant() && product.isOnline()) {
            productVariants.push(product);
        }
    }

    return productVariants;
}

/**
 * Returns the object with image data based on received image name
 * @param {string} name The name of image
 * @returns {Object} The object with image data
 */
function getDataFromName(name) {
    var params = name.split('_');

    var dataObject = {
        productId: params[1],
        color: params[2]
    };

    return dataObject;
}

/**
 * Start writing XML file
 * @param {dw.io.XMLStreamWriter} xsw XMLStreamWriter to write the XML file
 */
function startDocument(xsw) {
    xsw.writeStartDocument('UTF-8', '1.0');
    xsw.writeRaw(
        '\n<catalog xmlns="http://www.demandware.com/xml/impex/catalog/2006-10-31" catalog-id="toryburch-master-catalog">'
    );
}

/**
 * End writing XML file
 * @param {dw.io.XMLStreamWriter} xsw XMLStreamWriter to write the XML file
 */
function endDocument(xsw) {
    xsw.writeRaw('\n</catalog>');
    xsw.close();
}

/**
 * Start writing the product XML element
 * @param {string} productId Product ID
 * @param {dw.io.XMLStreamWriter} xsw XMLStreamWriter to write the XML file
 */
function startProductElem(productId, xsw) {
    xsw.writeRaw('\n<product product-id="' + productId + '">\n<images>');
}

/**
 * End writing the product XML element
 * @param {dw.io.XMLStreamWriter} xsw XMLStreamWriter to write the XML file
 */
function endProductElem(xsw) {
    xsw.writeRaw('\n</images>\n</product>');
}


/**
 * Downloads image files from generated link based on HTTP
 * @param {string} name Full name of image file to download
 * @param {string} type Type of resolution to select the appropriate folder
 */
function downloadImage(name, type) {
    var HTTPClient = require('dw/net/HTTPClient');
    var File = require('dw/io/File');

    var httpClient = new HTTPClient();
    var imgFile = new File(File.CATALOGS + '/toryburch-master-catalog/default/images/' + type + '/' + name);

    if (!imgFile.exists()) {
        try {
            httpClient.open('GET', 'https://s7d2.scene7.com/is/image/ToryBurch/' + name);
            httpClient.sendAndReceiveToFile(imgFile);
        } catch (e) {
            Logger.error('Error occurred when downloading image | Image name: {0} | Error details: {1}', name, e);
        }
    }
}

/**
 * Creates image group XML elements based on image data
 * @param {string} color Color code of product
 * @param {Array} names Set of string with images name values received from custom attribute of product object
 * @param {dw.io.XMLStreamWriter} xsw XMLStreamWriter to write the XML file
 * @returns {dw.system.Status} Status
 */
function setImageGroups(color, names, xsw) {
    var Site = require('dw/system/Site');
    var imageTypes = ['large', 'medium', 'swatch'];

    try {
        if (empty(imgTypeMap)) { // eslint-disable-line no-undef
            imgTypeMap = JSON.parse(Site.getCurrent().getCustomPreferenceValue('imageTypeMapping'));
        }
    } catch (e) {
        Logger.error('Error occurred when parsing JSON. Error details: {0}', e);
        return new Status(Status.ERROR, 'ERROR', 'JSON is invalid!');
    }

    imageTypes.forEach(function (type) {
        xsw.writeRaw(
            '\n<image-group view-type="' + type + '">' +
            '\n<variation attribute-id="color" value="' + color + '"/>'
        );

        names.forEach(function (name) {
            var imageFullName = name + '.' + imgTypeMap[type].type + '-' + imgTypeMap[type].size + '.jpg';
            xsw.writeRaw('\n<image path="images/' + type + '/' + imageFullName + '"/>');
            downloadImage(imageFullName, type);
        });

        xsw.writeRaw('\n</image-group>');
    });

    return new Status(Status.OK, 'OK');
}

/**
 * Creates the product XML element with all needed data for every received product
 * @param {Array} names Set of string with images name values received from custom attribute of product object
 * @param {dw.io.XMLStreamWriter} xsw XMLStreamWriter to write the XML file
 */
function processImages(names, xsw) {
    var dataObject = getDataFromName(names[0]);

    startProductElem(dataObject.productId, xsw);

    setImageGroups(dataObject.color, names, xsw);

    endProductElem(xsw);
}

module.exports = {
    startDocument: startDocument,
    processImages: processImages,
    endDocument: endDocument,
    getVariantProducts: getVariantProducts
};
