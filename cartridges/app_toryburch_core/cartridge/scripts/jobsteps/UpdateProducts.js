'use strict';

var Status = require('dw/system/Status');
var UpdateProductsHelper = require('~/cartridge/scripts/util/UpdateProductsHelper.js');

/**
 * The main function.
 *
 * @param {Object} args Job parameters
 * @returns {dw.system.Status} The exit status for the job step
 */
function run(args) {
    if (args.IsDisabled) {
        return new Status(Status.OK, 'OK', 'Step disabled, skip it...');
    }
    var object = UpdateProductsHelper.getProductsByRefinement(args.CatalogId);
    UpdateProductsHelper.createFileReaderXML(args.TargetCatalogRead, object.productVariations, object.mapMasterProducts, args.CatalogId);

    return new Status(Status.OK, 'OK');
}

exports.Run = run;
