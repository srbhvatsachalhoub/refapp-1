'use strict';

var Status = require('dw/system/Status');
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var CatalogImageHelper = require('~/cartridge/scripts/util/CatalogImageHelper.js');
var Logger = require('dw/system/Logger'); // can be custom logger

/**
 * The main function.
 *
 * @param {Object} args Job parameters
 * @returns {dw.system.Status} The exit status for the job step
 */
function run(args) {
    if (args.IsDisabled) {
        return new Status(Status.OK, 'OK', 'Step disabled, skip it...');
    }

    var targetFolder = args.TargetFolder;
    var catalogId = args.CatalogId;

    var xmlFile = new File(targetFolder + '/product_images_assigning.xml');
    var fileWriter = new FileWriter(xmlFile);
    var xsw = new XMLStreamWriter(fileWriter);
    CatalogImageHelper.startDocument(xsw, catalogId);

    var products = CatalogImageHelper.getVariantProducts(catalogId);

    products.every(function (product) { // eslint-disable-line
        var imgNames = product.getCustom().s7ImageSetNew;
        if (!empty(imgNames)) { // eslint-disable-line no-undef
            CatalogImageHelper.processImages(imgNames, xsw);
        } else {
            imgNames = product.getCustom().s7ImageSet;
            if (!empty(imgNames)) { // eslint-disable-line no-undef
                CatalogImageHelper.processImages(imgNames, xsw);
            } else {
                Logger.error('The custom attribute s7ImageSet is empty | Product ID - {0}', product.ID);
            }
            Logger.error('The custom attribute s7ImageSetNew is empty | Product ID - {0}', product.ID);
        }
        return true;
    });

    CatalogImageHelper.endDocument(xsw);
    fileWriter.close();

    return new Status(Status.OK, 'OK');
}

exports.Run = run;
