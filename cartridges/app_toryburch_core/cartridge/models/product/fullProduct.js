'use strict';
var base = module.superModule;

var decorators = require('*/cartridge/models/product/decorators/index');

/**
 * Decorate product with full product information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {Object} options - Options passed in from the factory
 *
 * @returns {Object} - Decorated product model
 */
module.exports = function fullProduct(product, apiProduct, options) {
    base.call(this, product, apiProduct, options);
    decorators.masterAttributes(product, apiProduct);

    if (apiProduct.isMaster() && options.variationModel.selectedVariants.length) {
        decorators.images(product, options.variationModel.selectedVariants[0], { types: ['hi-res', 'large', 'small', 'medium'], quantity: 'all' });
    } else if (options.variationModel) {
        decorators.images(product, options.variationModel, { types: ['hi-res', 'large', 'small', 'medium'], quantity: 'all' });
    } else {
        decorators.images(product, apiProduct, { types: ['hi-res', 'large', 'small', 'medium'], quantity: 'all' });
    }

    return product;
};
