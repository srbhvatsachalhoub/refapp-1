'use strict';

var base = module.superModule;

/**
 * returns the object that is filled with gift card/certificate values
 * @param {dw.order.Basket} lineItemContainer - current user basket.
 * @returns {Object} the object that keep gitCardCertificate attributes
 */
function getGiftCardTotals(lineItemContainer) {
    var giftCardTotals = {
        show: false,
        total: null,
        due: null,
        paymentInstruments: []
    };

    if (!lineItemContainer) {
        return giftCardTotals;
    }

    var collections = require('*/cartridge/scripts/util/collections');
    var formatMoney = require('dw/util/StringUtils').formatMoney;
    var Money = require('dw/value/Money');

    var giftCardTotal = new Money(0, lineItemContainer.currencyCode);
    collections.forEach(lineItemContainer.getGiftCertificatePaymentInstruments(), function (giftCertificatePaymentInstrument) {
        var giftCertificatePaymentInstrumentTransaction = giftCertificatePaymentInstrument.getPaymentTransaction();
        if (giftCertificatePaymentInstrumentTransaction) {
            var giftCertificatePaymentInstrumentAmount = giftCertificatePaymentInstrumentTransaction.getAmount();
            giftCardTotals.paymentInstruments.push({
                maskedCode: giftCertificatePaymentInstrument.getMaskedGiftCertificateCode(),
                amount: formatMoney(giftCertificatePaymentInstrumentAmount),
                transactionID: giftCertificatePaymentInstrumentTransaction.getTransactionID()
            });

            giftCardTotal = giftCardTotal.add(giftCertificatePaymentInstrumentAmount);
        }
    });

    if (giftCardTotal.value > 0) {
        // total amount of Gift cards applied
        giftCardTotals.total = {
            value: giftCardTotal.value,
            formatted: formatMoney(giftCardTotal)
        };

        var due = lineItemContainer.totalGrossPrice.subtract(giftCardTotal);
        // total due amount to be paid
        giftCardTotals.due = {
            value: due.value,
            formatted: formatMoney(due)
        };

        giftCardTotals.show = true;
    }

    return giftCardTotals;
}

/**
 * returns the object that is filled not complimentary amount's value and formatted value
 * @param {dw.ordre.Basket} lineItemContainer - current user basket.
 * @returns {Object} the object that have NonComplimentaryPaymentAmount
 */
function getNonComplimentaryPaymentAmount(lineItemContainer) {
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var formatMoney = require('dw/util/StringUtils').formatMoney;

    var nonComplimentaryPaymentAmount = basketCalculationHelpers.getNonComplimentaryPaymentAmount(lineItemContainer);

    return {
        value: nonComplimentaryPaymentAmount.value,
        formatted: formatMoney(nonComplimentaryPaymentAmount)
    };
}

/**
 * totals class that represents the order totals of the current line item container
 * extends with giftCardTotals object that have giftcard/certificate related values
 *
 * @param {dw.order.lineItemContainer} lineItemContainer - The current user's line item container
 */
function totals(lineItemContainer) {
    base.call(this, lineItemContainer);

    // gift card certificate/payment attributes
    this.giftCardTotals = getGiftCardTotals(lineItemContainer);

    // nonComplimentaryPaymentAmount
    this.nonComplimentaryPaymentAmount = getNonComplimentaryPaymentAmount(lineItemContainer);
}

module.exports = totals;
