'use strict';

var server = require('server');

var cache = require('*/cartridge/scripts/middleware/cache');

server.extend(module.superModule);

/*
 * This Controller (Product-Show) is the extention for giftcard functionality
 * Checks the product
 * Checks the current site has the giftcard functionality enabled
 * Sets the template for the virtual gift card product rendering template
 */
server.append('Show', function (req, res, next) {
    var viewData = res.getViewData();
    var product = viewData.product;

    // check if the product is available, if the product is gift card product, if the product is bundle or set
    // if not, return
    if (!product || !product.isGiftCard || product.productType === 'bundle' || product.productType === 'set') {
        return next();
    }

    var Site = require('dw/system/Site');
    var URLUtils = require('dw/web/URLUtils');

    // check if the site is giftcard enabled
    // if not, render the not found page for the gift card product
    if (!Site.getCurrent().getCustomPreferenceValue('isGiftCardEnabled')) {
        res.setStatusCode(410);
        res.render('error/notFound');
        return next();
    }

    // if the product is virtual gift card product
    // then set giftCardProductFieldsTemplate for the virtual gift card product
    if (product.giftCardType === 'virtual') {
        viewData.giftCardProductFieldsTemplate = 'giftCard/product/components/virtualGiftCardProductFields';
    } // else physical

    var giftCardDetailsForm = server.forms.getForm('giftCardDetails');
    giftCardDetailsForm.clear();

    viewData.addToCartUrl = URLUtils.url('GiftCard-AddProduct');
    viewData.giftCardDetailsForm = giftCardDetailsForm;

    res.setViewData(viewData);
    return next();
});

/*
 * This Controller (Product-ShowQuickView) is the extention for giftcard functionality
 * Checks the product
 * Checks the current site has the giftcard functionality enabled
 * Sets the template for the virtual gift card product rendering template
 */
server.append('ShowQuickView', cache.applyPromotionSensitiveCache, function (req, res, next) {
    var ProductFactory = require('*/cartridge/scripts/factories/product');

    var params = req.querystring;
    var product = ProductFactory.get(params);

    // check if the product is available, if the product is gift card product, if the product is bundle or set
    // if not, return
    if (!product || !product.isGiftCard || product.productType === 'bundle' || product.productType === 'set') {
        next();
        return;
    }

    var Site = require('dw/system/Site');
    var URLUtils = require('dw/web/URLUtils');

    // check if the site is giftcard enabled
    // if not, render the not found page for the gift card product
    if (!Site.getCurrent().getCustomPreferenceValue('isGiftCardEnabled')) {
        res.setStatusCode(410);
        res.render('error/notFound');
        next();
        return;
    }

    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    var productHelper = require('*/cartridge/scripts/helpers/productHelpers');

    var viewData = res.getViewData();

    // if the product is virtual gift card product
    // then set giftCardProductFieldsTemplate for the virtual gift card product
    if (product.giftCardType === 'virtual') {
        viewData.giftCardProductFieldsTemplate = 'giftCard/product/components/virtualGiftCardProductFields';
    } // else physical

    var giftCardDetailsForm = server.forms.getForm('giftCardDetails');
    giftCardDetailsForm.clear();

    viewData.product = product;
    viewData.addToCartUrl = URLUtils.url('GiftCard-AddProduct');
    viewData.giftCardDetailsForm = giftCardDetailsForm;
    viewData.resources = productHelper.getResources();

    var renderedTemplate = renderTemplateHelper.getRenderedHtml(viewData, 'product/quickView.isml');

    res.json({
        renderedTemplate: renderedTemplate
    });

    next();
});

module.exports = server.exports();
