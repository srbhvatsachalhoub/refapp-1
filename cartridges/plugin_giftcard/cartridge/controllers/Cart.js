'use strict';

var server = require('server');

server.extend(module.superModule);

/*
 * This Controller (Cart-GetProduct) is the extention for giftcard functionality
 * Checks the product
 * Checks the current site has the giftcard functionality enabled
 * Sets the template for the virtual gift card product rendering template
 */
server.append('GetProduct', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Site = require('dw/system/Site');
    var URLUtils = require('dw/web/URLUtils');
    var collections = require('*/cartridge/scripts/util/collections');

    var viewData = res.getViewData();
    var product = viewData.product;

    // check if the product is available, if the product is gift card product,
    // if the uuid is provided if the product is bundle or set
    // if not, return
    if (!product || !product.isGiftCard || !viewData.uuid || product.productType === 'bundle' || product.productType === 'set') {
        return next();
    }

    // check basket first
    var currentBasket = BasketMgr.getCurrentBasket();
    if (!currentBasket) {
        return next();
    }

    // check if the site is giftcard enabled
    // if not, render the not found page for the gift card product
    if (!Site.getCurrent().getCustomPreferenceValue('isGiftCardEnabled')) {
        return next();
    }

    var pli = collections.find(currentBasket.allProductLineItems, function (item) {
        return item.UUID === viewData.uuid;
    });

    // check if the productlineitem is found
    if (!pli) {
        return next();
    }

    // if the product is virtual gift card product
    // then set giftCardProductFieldsTemplate for the virtual gift card product
    if (product.giftCardType === 'virtual') {
        viewData.giftCardProductFieldsTemplate = 'giftCard/product/components/virtualGiftCardProductFields';
        viewData.giftCardAddMessageChecked = !!pli.custom.giftCardFromName || !!pli.custom.giftCardRecipientName || !!pli.giftMessage;
    } // else physical

    var giftCardDetailsForm = server.forms.getForm('giftCardDetails');
    giftCardDetailsForm.clear();
    giftCardDetailsForm.giftCardPliUUID.value = pli.UUID;
    giftCardDetailsForm.giftCardRecipientEmail.value = pli.custom.giftCardRecipientEmail;
    giftCardDetailsForm.giftCardRecipientEmailConfirm.value = pli.custom.giftCardRecipientEmail;
    giftCardDetailsForm.giftCardFromName.value = pli.custom.giftCardFromName;
    giftCardDetailsForm.giftCardRecipientName.value = pli.custom.giftCardRecipientName;
    giftCardDetailsForm.giftMessage.value = pli.giftMessage;

    viewData.updateCartUrl = URLUtils.url('GiftCard-UpdateProduct');
    viewData.giftCardDetailsForm = giftCardDetailsForm;

    res.setViewData(viewData);
    return next();
});

module.exports = server.exports();
