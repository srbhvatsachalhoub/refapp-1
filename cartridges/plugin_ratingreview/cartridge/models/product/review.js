'use strict';

var Resource = require('dw/web/Resource');
var StringUtils = require('dw/util/StringUtils');

/**
 * Gets nickname from custom object with fallback scenarios
 * @param {custom-object} apiRatingsReviews - RatingsReviews of the product
 * @returns {string} nickname
 */
function getNickName(apiRatingsReviews) {
    var nickName = apiRatingsReviews.custom.customerNickName;
    if (!nickName) {
        if (apiRatingsReviews.custom.customerName) {
            var customerNameArray = apiRatingsReviews.custom.customerName.split(' ');
            if (customerNameArray.length > 1) {
                nickName = StringUtils.format('{0}.{1}.', customerNameArray[0][0], customerNameArray[1][0]);
            } else {
                nickName = StringUtils.format('{0}.', customerNameArray[0][0]);
            }
        } else {
            nickName = '-';
        }
    }
    return nickName;
}

/**
 * Decodes encoded html
 * @param {string} str encoded html string
 * @returns {string} decoded html string
 */
function htmlDecode(str) {
    if (str) {
        return str
            .replace(/&amp;/g, '&')
            .replace(/&quot;/g, '"')
            .replace(/&#39;/g, '\'')
            .replace(/&lt;/g, '<')
            .replace(/&#x2F;/g, '/')
            .replace(/&gt;/g, '>');
    }
    return str;
}

/**
 * Creates a plain object that contains ratings reviews information
 * @param {custom-object} apiRatingsReviews - RatingsReviews of the product
 * @returns {Object} a plain object that contains information about the ratings and reviews of the product
 */
function createReviewObject(apiRatingsReviews) {
    var result;
    if (apiRatingsReviews) {
        var countryCode = apiRatingsReviews.custom.locale.split('_')[1];

        result = {
            id: apiRatingsReviews.custom.id,
            productId: apiRatingsReviews.custom.productId,
            reviewerName: htmlDecode(getNickName(apiRatingsReviews)),
            country: Resource.msg(countryCode, 'country', null),
            date: apiRatingsReviews.custom.date,
            rating: apiRatingsReviews.custom.rating,
            title: htmlDecode(apiRatingsReviews.custom.title),
            message: htmlDecode(apiRatingsReviews.custom.message),
            recommended: apiRatingsReviews.custom.recommended,
            positiveRelevancy: apiRatingsReviews.custom.positiveRelevancy,
            negativeRelevancy: apiRatingsReviews.custom.negativeRelevancy,
            approved: apiRatingsReviews.custom.approved
        };
    } else {
        result = null;
    }
    return result;
}

/**
 * Review class
 * @param {custom-object} apiRatingsReviews - RatingsReviews of the product
 * @constructor
 */
function review(apiRatingsReviews) {
    return createReviewObject(apiRatingsReviews);
}

module.exports = review;
