'use strict';

var server = require('server');

var logger = require('dw/system/Logger').getLogger('payfort.credit');
var Resource = require('dw/web/Resource');

var payfortHelpers = require('*/cartridge/scripts/helpers/payfortHelpers');
var payfortConstants = require('*/cartridge/scripts/util/payfortConstants');

/**
 * Generates the handle process error
 * @return {Object} error object
 */
function handleError() {
    return { fieldErrors: [], serverErrors: [Resource.msg('error.card.information.error', 'creditCard', null)], error: true };
}

/**
 * Verifies that entered credit card information is a valid card. If the information is valid a
 * credit card payment instrument is created
 * @param {dw.order.Basket} basket - Current users's basket
 * @param {Object} paymentInformation - the payment information
 * @return {Object} returns an error object
 */
function Handle(basket, paymentInformation) {
    // check the provided parameters
    if (!basket || !paymentInformation) {
        return handleError();
    }

    try {
        return payfortHelpers.handleCreditCard(basket, paymentInformation);
    } catch (e) {
        logger.error('An error occured while handling basket payment, basket: {0}, message {1}, stack: {2}',
            basket.UUID,
            e.message,
            e.stack);
    }

    return handleError();
}

/**
 * Generates the authorize process error
 * @return {Object} error object
 */
function authorizeError() {
    return { fieldErrors: {}, serverErrors: [Resource.msg('error.technical', 'checkout', null)], error: true };
}

/**
 * Authorizes a payment using a credit card.
 * Customizations for payFort processor and
 * payFort logic to authorize credit card payment.
 * @param {dw.order.Order} order - The order object
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @return {Object} returns an error object
 */
function Authorize(order, orderNumber, paymentInstrument, paymentProcessor) {
    var Transaction = require('dw/system/Transaction');

    // check the provided parameters
    if (!order || !orderNumber || !paymentInstrument || !paymentProcessor) {
        logger.error('An error occured while authorizing order payment, these params are empty: order: {0}, orderNo: {1}, paymentInstrument: {2}, paymentProcessor: {3}',
            !!order, orderNumber, !!paymentInstrument, !!paymentProcessor);
        return authorizeError();
    }
    var creditCardForm = server.forms.getForm('billing').creditCardFields;
    if (!creditCardForm || !order) {
        logger.error('An error occured while authorizing order payment, these are empty: orderNo: {0}, creditCardForm: {1}, order: {2}',
            orderNumber, !!creditCardForm, !!order);
        return authorizeError();
    }

    try {
        Transaction.wrap(function () {
            paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
            paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);
        });

        var paymentInformation = payfortHelpers.preparePaymentInformation(creditCardForm, paymentInstrument);
        return payfortHelpers.authorizeCreditCard(order, paymentInstrument, paymentProcessor, paymentInformation);
    } catch (e) {
        // set last payment status as error
        payfortHelpers.setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, e.message);

        logger.error('An error occured while authorizing order payment, orderNo: {0}, message {1}, stack: {2}',
            orderNumber,
            e.message,
            e.stack);
    }

    return authorizeError();
}

/**
 * Captures the given order's payment instrument
 * @param {dw.order.Order} order - The order to be captured
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be captured
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function Capture(order, paymentInstrument, paymentProcessor) {
    try {
        return payfortHelpers.capturePayment(order, paymentInstrument, paymentProcessor);
    } catch (e) {
        // set last payment status as error
        payfortHelpers.setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, e.message);

        var StringUtils = require('dw/util/StringUtils');

        var message = StringUtils.format(
            'An error occured while capturing order payment, orderNo: {0}, message {1}, stack: {2}',
            order.orderNo,
            e.message,
            e.stack
        );

        logger.error(message);
        return {
            error: true,
            message: message
        };
    }
}

/**
 * Checks the payment status of the given order's payment instrument
 * @param {dw.order.Order} order - The order to be checked
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be checked
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function CheckStatus(order, paymentInstrument, paymentProcessor) {
    try {
        return payfortHelpers.checkPaymentStatus(order, paymentInstrument, paymentProcessor);
    } catch (e) {
        // set last payment status as error
        payfortHelpers.setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, e.message);

        var StringUtils = require('dw/util/StringUtils');

        var message = StringUtils.format(
            'An error occured while checking the order payment status, orderNo: {0}, message {1}, stack: {2}',
            order.orderNo,
            e.message,
            e.stack
        );

        logger.error(message);
        return {
            error: true,
            message: message
        };
    }
}

/**
 * Cancel/Void the given order's payment instrument
 * @param {dw.order.Order} order - The order to be cancelled
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be cancelled
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function Cancel(order, paymentInstrument, paymentProcessor) {
    try {
        return payfortHelpers.cancelPayment(order, paymentInstrument, paymentProcessor);
    } catch (e) {
        // set last payment status as error
        payfortHelpers.setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, e.message);

        var StringUtils = require('dw/util/StringUtils');

        var message = StringUtils.format(
            'An error occured while cancelling order payment, orderNo: {0}, message {1}, stack: {2}',
            order.orderNo,
            e.message,
            e.stack
        );

        logger.error(message);
        return {
            error: true,
            message: message
        };
    }
}

/**
 * Refund the given order's payment instrument
 * @param {dw.order.Order} order - The order to be refunded
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be refunded
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @return {Object} returns an error or success object
 */
function Refund(order, paymentInstrument, paymentProcessor) {
    try {
        return payfortHelpers.refundPayment(order, paymentInstrument, paymentProcessor);
    } catch (e) {
        // set last payment status as error
        payfortHelpers.setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, e.message);

        var StringUtils = require('dw/util/StringUtils');

        var message = StringUtils.format(
            'An error occured while refunding order payment, orderNo: {0}, message {1}, stack: {2}',
            order.orderNo,
            e.message,
            e.stack
        );

        logger.error(message);
        return {
            error: true,
            message: message
        };
    }
}

/**
 * Gets the Payment Processor's custom value for save card disabled or not
 * @returns {boolean} true if saved card functionality is disabled
 */
function IsSaveCardDisabled() {
    var Site = require('dw/system/Site');

    return Site.getCurrent().getCustomPreferenceValue('payfortDisableSaveCard') || false;
}

/**
 * Checks if the saved card locale and target locale have the same service,
 * Different PSP merchants can be used for different country/locale,
 * In order to use saved card functionality, the payment services for savedCard and target locales should be same
 * @param {string} savedCardLocale - the locale for saved card
 * @param {string} targetLocale - the target locale to be checked for saved card is applicable or not
 * @returns {boolean} true if the saved card is applicable for the target locale, false otherwise
 */
function IsSavedCardApplicableForLocale(savedCardLocale, targetLocale) {
    return payfortHelpers.isSavedCardApplicableForLocale(savedCardLocale, targetLocale);
}

/**
 * Process order feedback payment status against order payment status
 * If different then arrange order payment status
 * @param {Object} req - The SFCC request object
 * @param {dw.order.Order} order - The order to be checked for feedback status
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be checked for feedback status
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {dw.object.CustomObject} feedback - The feedback custom object
 * @return {Object} returns an error or success object
 */
function Feedback(req, order, paymentInstrument, paymentProcessor, feedback) {
    try {
        return payfortHelpers.feedbackPayment(req, order, paymentInstrument, paymentProcessor, feedback);
    } catch (e) {
        var StringUtils = require('dw/util/StringUtils');
        var message = StringUtils.format(
            'An error occured while processing PSP feedback, orderNo: {0}, message {1}, stack: {2}',
            order.orderNo,
            e.message,
            e.stack
        );
        logger.error(message);
        return {
            error: true,
            message: message
        };
    }
}

/**
 * Inactivate token on PSP side
 * @param {dw.order.Order} order - The refence order for the payment token
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to be processed for inactivate psp token
 * @return {Object} returns an error or success object
 */
function DeleteToken(order, paymentInstrument) {
    try {
        return payfortHelpers.deleteToken(order, paymentInstrument);
    } catch (e) {
        var StringUtils = require('dw/util/StringUtils');
        var message = StringUtils.format(
            'An error occured while deleting/inactivating PSP token, reference orderNo: {0}, token: {1}, message {2}, stack: {3}',
            order.orderNo,
            paymentInstrument.creditCardToken,
            e.message,
            e.stack
        );
        logger.error(message);
        return {
            error: true,
            message: message
        };
    }
}

/**
 * Gets the Payment Processor's custom value for apple pay is enabled or not
 * @returns {boolean} true if apple pay is enabled
 */
function ApplePay() {
    var Site = require('dw/system/Site');
    var collections = require('*/cartridge/scripts/util/collections');

    var payfortApplePayEnabled = Site.getCurrent().getCustomPreferenceValue('payfortApplePayEnabled') || false;

    // if it is not enabled then no need to check the locales
    // just return false
    if (!payfortApplePayEnabled) {
        return null;
    }

    var ArrayList = require('dw/util/ArrayList');
    var payfortApplePayEnabledLocales = new ArrayList(Site.getCurrent().getCustomPreferenceValue('payfortApplePayEnabledLocales'));

    // check if the current locale is enabled for the apple pay
    payfortApplePayEnabled = collections.some(payfortApplePayEnabledLocales, function (payfortApplePayEnabledLocale) {
        return payfortApplePayEnabledLocale === request.locale; // eslint-disable-line no-undef
    });

    // if it is not enabled for the current locale
    // just return null
    if (!payfortApplePayEnabled) {
        return null;
    }

    return {
        showOnPDP: (Site.getCurrent().getCustomPreferenceValue('payfortApplePayInjectOnPDP') || false),
        showOnCart: (Site.getCurrent().getCustomPreferenceValue('payfortApplePayInjectOnCart') || false)
    };
}

/**
 * Authorizes a payment using a credit card.
 * Customizations for payFort processor and
 * payFort logic to authorize credit card payment.
 * @param {dw.order.Order} order - The order object
 * @param {number} orderNumber - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor - The payment processor of the current payment method
 * @param {Object} applePayData - The applePay JSON object
 * @return {Object} returns an error object
 */
function AuthorizeApplePay(order, orderNumber, paymentInstrument, paymentProcessor, applePayData) {
    var Transaction = require('dw/system/Transaction');

    // check the provided parameters
    if (!order || !paymentInstrument || !paymentProcessor || !applePayData) {
        logger.error('An error occured while authorizing order apple pay payment, these params are empty: orderNo: {0}, paymentInstrument: {1}, paymentProcessor: {2}, applePayData: {3}',
            !!order, !!paymentInstrument, !!paymentProcessor, !!applePayData);
        return authorizeError();
    }

    try {
        Transaction.wrap(function () {
            paymentInstrument.paymentTransaction.setTransactionID(order.orderNo);
            paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);
        });

        return payfortHelpers.authorizeApplePay(order, paymentInstrument, paymentProcessor, applePayData);
    } catch (e) {
        // set last payment status as error
        payfortHelpers.setLastPSPPaymentStatus(order, payfortConstants.pspPaymentStatus.error, e.message);

        logger.error('An error occured while authorizing order apple pay payment, orderNo: {0}, message {1}, stack: {2}',
            order.orderNo,
            e.message,
            e.stack);
    }

    return authorizeError();
}

exports.Handle = Handle;
exports.Authorize = Authorize;
exports.Capture = Capture;
exports.IsSaveCardDisabled = IsSaveCardDisabled;
exports.IsSavedCardApplicableForLocale = IsSavedCardApplicableForLocale;
exports.CheckStatus = CheckStatus;
exports.Cancel = Cancel;
exports.Refund = Refund;
exports.Feedback = Feedback;
exports.DeleteToken = DeleteToken;
exports.ApplePay = ApplePay;
exports.AuthorizeApplePay = AuthorizeApplePay;
