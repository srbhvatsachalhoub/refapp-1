'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./productTile/productTileAddToCart'));
    processInclude(require('brand_core/product/backInStock'));
});
