'use strict';

var base = require('brand_core/newsletterSubscribe/newsletter');

$(document).ready(function () {
    var $subscribeCheckbox = $('.js-subscribe-checkbox');
    $subscribeCheckbox.on('change', function () {
        $('.js-option-list-row input').prop('checked', $(this).is(':checked'));
    });
});

module.exports = base;
