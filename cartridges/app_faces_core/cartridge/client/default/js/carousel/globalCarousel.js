'use strict';
var globalSwatchesOptions = require('./../config/swatchesCarousel.json').options;
var carouselSelector = '.js-global-carousel';

/**
 * Gets scrollbar width
 * @param {jQuery} $element - $element
 * @returns {Object} options
 */
function getOption($element) {
    var options;
    var width = $(window).width();
    var selector = 'carouselOptionsMobile';
    var smallDesktopHasTabletView = $element.data('carouselSmallDesktopAsTablet');
    var desktopMinBP = !smallDesktopHasTabletView ? window.RA_BREAKPOINTS.lg : window.RA_BREAKPOINTS.xl;

    if (width >= window.RA_BREAKPOINTS.md) {
        selector = 'carouselOptionsTablet';
    }
    if (width >= desktopMinBP) {
        selector = 'carouselOptions';
    }
    if ($element.hasClass('js-swatches-carousel')) {
        options = globalSwatchesOptions;
        options.rtl = window.RA_DIRECTION === 'rtl';
        // config for swatches is global, it is processed as const config from .json
    } else {
        // if config depends on screen width it is processed via data attr, should be seated in .isml
        options = $element.data(selector);
    }
    return options || {};
}

/**
* init custom settings for carousels after initialize
* @param {jQuery} $carousel - $carousel
* @param {Object} options - options
*/
function handlePostInitSettings($carousel, options) {
    if (options.carouselDynamicDotsPosition) {
        var $bottomContentItems = $carousel.find('.js-carousel-item-bottom-content');
        var bottomContentHeight = null;
        var bottomContentHeightArray = [];

        if ($bottomContentItems.length > 0) {
            $bottomContentItems.each(function (index, element) {
                bottomContentHeightArray.push($(element).outerHeight());
            });

            bottomContentHeightArray.sort(function (a, b) {
                return a - b;
            });

            bottomContentHeight = bottomContentHeightArray.reverse()[0];

            if (bottomContentHeight) {
                $carousel.find('.slick-dots').css('bottom', bottomContentHeight + 'px');
            }
        }
    } else {
        $carousel.find('.slick-dots').css('bottom', '');
    }
}

/**
* init all carousels
* @param {jQuery} $container - $element
*/
function initAllCarousels($container) {
    $($container || carouselSelector).each(function () {
        var $this = $(this);
        var options = getOption($this);
        if (!options.disable) {
            $this.slick(options);

            handlePostInitSettings($this, options);
        }
    });
}

/**
* init events for carousels
* @param {jQuery} $element - $element
*/
function initEvents() {
    $(window).on('resize orientationchange', function () {
        $(carouselSelector).each(function () {
            var $this = $(this);
            if (this.slick) {
                $this.slick('unslick');
            } else {
                $this.removeClass('slick-initialized');
            }
            var options = getOption($this);
            if (!options.disable) {
                $this.slick(options);

                handlePostInitSettings($this, options);
            }
        });
    });

    // init carousel for new elements, after update DOM on PLP
    $(document).on('search:success', function () {
        $(carouselSelector).not('.slick-initialized').each(function () {
            initAllCarousels($(this));
        });
    });
}


/**
 * init carousel
 */
function init() {
    initAllCarousels();
    initEvents();
}

module.exports = {
    init: init
};
