'use strict';

var noUiSlider = require('nouislider');
var urlHelper = require('brand_core/helpers/urlHelper');
var base = require('brand_core/search/search');
var userAgent = require('brand_core/helpers/userAgent');
var selectpicker = require('../components/selectpicker');
var applyFilter = base.getApplyFilterMethod();
var isPriceSlider = false;
var refinementBarState = {
    expandedRefinement: null
};

/**
 * Exit from the full-screen mode (view more)
 * @return {void}
 */
function goBack() {
    var $modalTitle = $('.js-mobile-modal.open').find('.js-mobile-modal-title');
    var modalTitleText = $modalTitle.data('title');
    var $refinementPrice = $('.js-card-refinement.refinement-price');
    $modalTitle.text(modalTitleText);
    $('.js-full-screen').removeClass('full-screen');
    $('.js-card-refinement').each(function () {
        $(this).show();
        $('.js-extra-item').addClass('d-none');
        $('.js-btn-plp-action-more-fullscreen').removeClass('d-none');
    });
    $('.js-card-refinement.full-screen').removeClass('full-screen');
    if (!$refinementPrice.hasClass('open')) {
        $refinementPrice.addClass('open');
    }

    refinementBarState.expandedRefinement = null;
}

/**
 * Activate 'View all' revinement view
 * @param {Object} $card - $card
 * @return {void}
 */
function activateViewAllRefinements($card) {
    var $modalTitle = $('.js-refinement-container.js-mobile-modal').find('.js-mobile-modal-title');
    var titleValue = $card.data('refinement');
    var refinementId = $card.data('refinementId');

    $('.js-btn-plp-action-more-fullscreen').addClass('d-none');
    $modalTitle.html(titleValue);
    $('.js-card-refinement').each(function () {
        $(this).hide();
    });
    $card.show();
    $card.addClass('full-screen');
    $('.js-extra-item').removeClass('d-none');
    $('.js-btn-back').removeClass('d-none');

    setTimeout(function () {
        $('.js-mobile-modal-body').scrollTop(0);
    }, 0);

    refinementBarState.expandedRefinement = refinementId;
}

base.initPriceSlider = function () {
    var $sliders = $('.js-price-refinement-slider');
    var selectedMinStr = urlHelper.getParameter('pmin');
    var selectedMaxStr = urlHelper.getParameter('pmax');
    var selectedMin = parseFloat((selectedMinStr || '0').replace(/,(.+?)$/, '.$1'));
    var selectedMax = parseFloat((selectedMaxStr || '0').replace(/,(.+?)$/, '.$1'));

    $sliders.each(function () {
        if (this.noUiSlider && this.noUiSlider.destroy) {
            this.noUiSlider.destroy();
        }

        var $slider = $(this);
        var $value = $slider.parent().find('.js-price-refinement-value');
        var url = $slider.data('url');
        var template = $slider.data('template');
        var min = parseFloat($slider.data('min'));
        var max = parseFloat($slider.data('max'));
        var startedMin;
        var startedMax;

        if (selectedMin < min) selectedMin = min;
        if (selectedMax > max) selectedMax = max;

        noUiSlider.create(this, {
            start: [selectedMin || min, selectedMax || max],
            direction: window.RA_DIRECTION,
            connect: true,
            step: 10,
            range: {
                min: min,
                max: max
            }
        });

        this.noUiSlider.on('start', function (values) {
            startedMin = values[0];
            startedMax = values[1];
        });

        this.noUiSlider.on('update', function (values) {
            if (selectedMinStr || selectedMaxStr) {
                $slider.addClass('slider-active');
            } else {
                $slider.removeClass('slider-active');
            }

            $value.html(
                template
                    .replace('{pmin}', Math.floor(values[0]))
                    .replace('{pmax}', Math.ceil(values[1]))
            );
        });

        this.noUiSlider.on('set', function (values, handle) {
            if (handle === 0 && startedMin === values[0]) return;
            if (handle === 1 && startedMax === values[1]) return;
            url = url.replace(/(pmin=)(.+?)(?=&)/, '$1' + values[0]);
            if ((/(pmax=)(.+?)(?=&)/).test(url)) {
                url = url.replace(/(pmax=)(.+?)(?=&)/, '$1' + values[1]);
            } else {
                // if pmax is the last parameter in url
                url = url.replace(/(pmax=)(.+?)(?=$)/, '$1' + values[1]);
            }
            isPriceSlider = true;
            applyFilter(url, 'refinement');
        });
    });

    $(document)
        .off('search:refinement:success', base.initPriceSlider)
        .on('search:refinement:success', base.initPriceSlider);
};

base.sortOption = function () {
    $(document)
        .on('click', '.js-sort-option', function (e) {
            e.preventDefault();
            $('.js-sort-option').removeClass('selected');
            $(this).addClass('selected');
        })
        .on('click', '.js-apply-sort-option-btn', function (e) {
            e.preventDefault();
            var $selected = $('.js-sort-option.selected');
            var $sortSelecbox = $('.js-sort-options-selectbox');
            var newUrl = $selected.attr('href');
            var oldUrl = $sortSelecbox.val();

            if (oldUrl !== newUrl) {
                $sortSelecbox.val(newUrl).trigger('change');
            }
        })
        .on('state:add:open', '.js-sort-container', function () {
            $('body').addClass('disable-scroll');
            var $sortSelecbox = $('.js-sort-options-selectbox');
            var url = $sortSelecbox.val();
            $('.js-sort-option[href="' + url + '"]').trigger('click');
        })
        .on('state:remove:open', '.js-sort-container', function () {
            $('body').removeClass('disable-scroll');
        });
};

base.initSelectpicker = function () {
    selectpicker('.js-search-results select');

    $(document)
        .off('search:refinement:success', base.initSelectpicker)
        .on('search:refinement:success', base.initSelectpicker);
};

base.initOnRefinementSuccess = function () {
    var $previousRefinements = null;

    $(document)
        .on('search:refinement:start', function () {
            var $currentRefinements = $('.js-refinement-container [data-refinement]');
            if ($previousRefinements && $previousRefinements.length) {
                $previousRefinements.each(function () {
                    var $this = $(this);
                    var id = $this.data('refinement');
                    if ($currentRefinements.filter('[data-refinement="' + id + '"]').length === 0) {
                        $currentRefinements.push(this);
                    }
                });
            }
            $previousRefinements = $currentRefinements;
        })
        .on('search:refinement:success', function () {
            var $refinementContainer = $('.js-refinement-container');
            var $openRefinements = $previousRefinements.filter('.open');
            $refinementContainer.find('.open').removeClass('open');
            $refinementContainer.find('[data-refinement]').each(function () {
                var $this = $(this);
                var id = $this.data('refinement');
                if ($openRefinements.filter('[data-refinement="' + id + '"]').length ||
                    $previousRefinements.filter('[data-refinement="' + id + '"]').length === 0) {
                    $this.addClass('open');
                }
            });
        });
};

base.refinements = function () {
    var refinementValue = null;
    var $body = $('body');
    $(document)
        .on('state:add:open', '.js-refinement-container', function () {
            $body.addClass('disable-scroll');
            if (userAgent.isMobileSafari) {
                $body.addClass('mobile-safari');
            }
        })
        .on('state:remove:open', '.js-refinement-container', function () {
            goBack();
            $body.removeClass('disable-scroll');
        })
        .on('search:refinement:start', function (e) {
            refinementValue = $(e.target.activeElement).data('refinement-value');
        })
        .on('search:refinement:success', function () {
            if ($('.js-refinement-container').hasClass('open')) {
                var elPriceSlider = $('.js-price-refinement-slider').get(0);
                var elRefinement = $('a[data-refinement-value="' + refinementValue + '"]').get(0);
                if (elRefinement) {
                    elRefinement.scrollIntoView();
                } else if (isPriceSlider && elPriceSlider) {
                    elPriceSlider.scrollIntoView();
                }
            }
            if ($('.js-no-result-reset-filter-btn').length) {
                $body.removeClass('disable-scroll');
            }
            isPriceSlider = false;
        });
};

base.initContentSearch = function () {
    var $container = $('.js-content-result-container');

    // Don't go further, if there isn't a content result element in the page
    if ($container.length === 0) return;

    // Don't go further, if content search results are empty
    if ($container.find('.js-content-result-empty').length) $container.remove();

    // Show "Related Content" title
    $container.find('.js-content-result-title').removeClass('d-none');

    // Add view more functionality
    $(document).on('click', '.js-content-view-more-btn', function () {
        var $btn = $(this);
        $.spinner().start();
        $.ajax({
            method: 'GET',
            url: $btn.data('url'),
            success: function (response) {
                $btn.closest('.js-content-view-more-container').remove();
                $container.find('.js-content-result-placeholder').append(response);
                $.spinner().stop();
            },
            error: function () {
                $.spinner().stop();
            }
        });
    });

    $(document).on('click', '.js-btn-refine-by', function () {
        var $applyButton = $('.js-apply-link');
        $applyButton.addClass('disabled');
    });

    $(document).on('click', '.js-btn-plp-action-more-fullscreen', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $card = $(this).closest('.js-card-refinement');

        activateViewAllRefinements($card);
    });

    $(document).on('click', '.js-btn-back', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).addClass('d-none');
        goBack();
    });
};

base.setSearchKeyword = function () {
    var $keyword = $('.js-search-keyword');
    if ($keyword.length) {
        var $input = $('.js-sws-input');
        $input.val($keyword.val()).closest('.js-sws-container').addClass('search-focus');
        $('.js-toggle-search-mobile').attr('data-no-focus', true).trigger('click');
    }
};

base.filter =
base.closeRefinements =
base.resize = function () {
    // Intentionally empty. Don't remove this function.
};

base.handleRefinements = function ($results) {
    if (!$results) {
        return;
    }

    $('.refinement.active').each(function () {
        $(this).removeClass('active');
        var activeDiv = $results.find('.' + $(this)[0].className.replace(/ /g, '.'));
        activeDiv.addClass('active');
        activeDiv.find('button.title').attr('aria-expanded', 'true');
    });

    base.updateDom($results, '.refinement-bar');

    if (refinementBarState.expandedRefinement) {
        activateViewAllRefinements($('.js-card-refinement[data-refinement-id=' + refinementBarState.expandedRefinement + ']'));
    }
};

module.exports = base;
