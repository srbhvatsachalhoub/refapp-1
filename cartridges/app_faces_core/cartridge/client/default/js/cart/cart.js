'use strict';

var productCarousel = require('../product/productCarousel');
var quantitySelector = require('../product/quantitySelector');
var countCharacters = require('brand_core/helpers/countCharacters');
var StickySidebar = require('sticky-sidebar/dist/sticky-sidebar');
var throttle = require('lodash/throttle');

/**
 * Initializes sticky sidebar
 * @returns {Object} sticky sidebar object
 */
function initStickySidebar() {
    return new StickySidebar('.js-sticky-sidebar', { // eslint-disable-line no-unused-vars
        topSpacing: 100,
        bottomSpacing: 20,
        containerSelector: '.js-sticky-sidebar-container',
        innerWrapperSelector: '.js-sticky-sidebar-inner'
    });
}

module.exports = function () {
    var $sidebar = $('.js-sticky-sidebar');
    var $sidebarContainer = $('.js-sticky-sidebar-container');
    var $sidebarPlaceholderMobile = $('.js-sidebar-placeholder-mobile');
    var $win = $(window);
    var promoCount = 0;

    // Init quantity selector
    quantitySelector.init();

    // Init char counter
    countCharacters();

    // Init sticky sidebar
    var sidebar = initStickySidebar();

    // Update sticky calculations on accordion collapse/expand actions
    $('.js-acc-container').on('state:toggle:open', function () {
        sidebar.destroy();
        sidebar = initStickySidebar();
    });

    // Toggle gift wrapping textarea
    $('.js-gift-toggle').change(function () {
        if ($(this).is(':checked')) {
            $('.js-gift-form-item').removeClass('d-none');
        } else {
            $('.js-gift-form-item').addClass('d-none');
        }
        sidebar.destroy();
        sidebar = initStickySidebar();
    });

    // Update discount totals on cart update
    $('body').on('cart:updateTotals', function (e, data) {
        $('.js-error-message').empty();
        if (data.totals.orderLevelDiscountTotal.value > 0) {
            $('.sub-total').addClass('strike-through totals-price-main');
            $('.js-discounted-sub-total')
                .removeClass('d-none')
                .html(data.totals.orderLevelDiscountTotal.discountedSubTotal.formatted);
            $('.js-discount-value-line').removeClass('d-none');
            $('.js-discount-value').html(data.totals.orderLevelDiscountTotal.formatted);
        } else {
            $('.js-discounted-sub-total').addClass('d-none');
            $('.js-discount-value-line').addClass('d-none');
            $('.sub-total').removeClass('strike-through totals-price-main');
        }
        // Hide promo applied label after removing a promo code
        var NewPromoCount = $('.coupons-and-promos').children().length;
        if (NewPromoCount < promoCount) {
            $('.js-have-promo-label').addClass('d-none');
            $('.js-not-have-promo-label').removeClass('d-none');
        }
        promoCount = NewPromoCount;
    });

    // Hide promo applied label on error
    $('body').on('cart:errorMessageUpdate', function () {
        $('.js-have-promo-label').addClass('d-none');
        $('.js-not-have-promo-label').removeClass('d-none');
    });

    // Show promo applied label when valid coupon code applied
    $('body').on('cart:promoApplied', function () {
        $('.js-have-promo-label').removeClass('d-none');
        $('.js-not-have-promo-label').addClass('d-none');
    });

    // Reinit free shipping products slider after cart total price update
    $('body').on('cart:updateFreeShippingApproachingProducts', function () {
        productCarousel('.js-free-shipping-approaching-carousel');
    });

    /**
     * Moves sidebar to different places on mobile and desktop views
     */
    function moveSidebar() {
        if ($('input').is(':focus') === false) {
            if ($win.width() < window.RA_BREAKPOINTS.md) {
                $sidebarPlaceholderMobile.append($sidebar);
            } else {
                $sidebarContainer.append($sidebar);
            }
        }
    }

    /**
        * initialize bonus discount item events, supports multiple components
    */
    function initBonusDiscountItems() {
        $('.js-bonus-discount-item').each(function () {
            var $bonusDiscountItem = $(this);
            var action = $bonusDiscountItem.data('action');
            var uuid = $bonusDiscountItem.data('uuid');
            var maxpids = $bonusDiscountItem.data('maxpids');
            if (!action || !uuid || !maxpids) {
                return;
            }

            $bonusDiscountItem.off('click').on('click', '.js-bonus-discount-product', function (e) {
                e.preventDefault();

                var $bonusDiscountProduct = $(this);
                var pid = $bonusDiscountProduct.data('pid');
                if (!pid) {
                    return;
                }
                $bonusDiscountItem.find('.js-bonus-discount-product');
                var allSelectedPids = $bonusDiscountItem.find('.js-bonus-discount-product.selected').map(function () {
                    return $(this).data('pid');
                }).get();
                var selectedPids = [];
                $.each(allSelectedPids, function (i, el) {
                    if ($.inArray(el, selectedPids) === -1) selectedPids.push(el);
                });

                var indexOf = selectedPids.indexOf(pid);
                if (indexOf > -1) {
                    selectedPids.splice(indexOf, 1);
                } else {
                    if (selectedPids.length === maxpids) {
                        selectedPids.pop();
                    }
                    selectedPids.push(pid);
                }

                var urlParams = {
                    uuid: uuid,
                    pids: JSON.stringify(selectedPids)
                };

                $bonusDiscountItem.spinner().start();
                $.ajax({
                    url: action,
                    type: 'post',
                    dataType: 'json',
                    data: urlParams,
                    success: function (data) {
                        if (data && data.success && data.pids) {
                            $bonusDiscountItem.find('.js-bonus-discount-product')
                                .removeClass('selected')
                                .removeClass('btn-primary')
                                .addClass('btn-outline-primary');
                            $bonusDiscountItem.find('.js-bonus-discount-product span').text($bonusDiscountItem.data('labelselect'));
                            data.pids.forEach(function (addedPid) {
                                var $selectedBonus = $bonusDiscountItem.find('.js-bonus-discount-product[data-pid="' + addedPid + '"]');
                                if (!$selectedBonus) {
                                    return;
                                }
                                $selectedBonus.removeClass('btn-outline-primary').addClass('btn-primary').addClass('selected');
                                $selectedBonus.find('span').text($bonusDiscountItem.data('labelselected'));
                            });
                            $('body').trigger('cart:bonusSelected', data);
                        }
                        $.spinner().stop();
                    },
                    error: function (err) {
                        if (err.responseJSON.redirectUrl) {
                            window.location.href = err.responseJSON.redirectUrl;
                        }
                        $.spinner().stop();
                    }
                });
            });
        });
    }

    // Change sidebar location on mobile and desktop versions
    moveSidebar();
    initBonusDiscountItems();
    $win.on('resize', throttle(moveSidebar, 100));
};
