'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./addressBook/addressBook'));
    processInclude(require('brand_core/addressBook/fieldCity'));
    processInclude(require('brand_core/addressBook/fieldAddressId'));
});
