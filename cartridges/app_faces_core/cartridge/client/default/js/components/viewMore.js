'use strict';

module.exports = function () {
    var $viewMoreBtn = $('.js-view-more-btn');

    $viewMoreBtn.each(function () {
        var $btn = $(this);
        var selContainer = $btn.data('item-container');

        if (selContainer !== '.js-product-carousel') {
            var $container = $btn.parent().find(selContainer);
            var initialVisibleIndex = $container.data('initial-item-count') || 1;
            if ($container.children().length <= initialVisibleIndex) {
                $btn.hide().removeClass('d-block');
            }
        }
    });

    $viewMoreBtn.click(function () {
        var $btn = $(this);
        var count = $btn.data('item-count');
        var selContainer = $btn.data('item-container');
        var $container = $btn.parent().find(selContainer);
        var initialVisibleIndex = $container.data('initial-item-count') || 1;
        var lastVisibleIndex = $btn.data('last-visible-item-index') || initialVisibleIndex;
        var nextVisibleIndex = lastVisibleIndex + count;
        var $lastVisibleItem = $container.find('> :nth-child(' + lastVisibleIndex + ')');
        var $nextVisibleItem = $container.find('> :nth-child(' + nextVisibleIndex + ')');

        // Show next page's items
        $lastVisibleItem.addClass('d-inline-block')
            .nextUntil($nextVisibleItem).addClass('d-inline-block');

        // Set last visible index to be able to
        // find which items were shown previously
        $btn.data('last-visible-item-index', nextVisibleIndex);

        // Hide view more button if there is no items left.
        if ($nextVisibleItem.length === 0) {
            $btn.hide().removeClass('d-block');
        }
    });
};
