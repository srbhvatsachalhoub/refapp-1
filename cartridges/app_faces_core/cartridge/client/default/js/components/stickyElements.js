'use strict';

var scrollListener = require('../utils/scrollListener');
var stickyElementsDefaultOptions = {
    initViewMode: [],
    isStuck: false,
    spacer: '.js-sticky-spacer',
    startPoint: 'self',
    stopPoint: null,
    stuckSide: 'top'
};
var options = {};
var cssSelectors = {
    stickyElement: '.js-sticky-element'
};
var $cache = {};
var initialized = false;

/**
 * @function
 * @description Merge sticky elements options with params
 * @param {Object} params - params
 * @returns {void}
 */
function initStickyElementsOptions(params) {
    options = $.extend(true, options, stickyElementsDefaultOptions, params || {});

    if (typeof params === 'object' && 'initViewMode' in params) {
        options.initViewMode = params.initViewMode;
    }
}

/**
 * @function
 * @description Initialize cache
 * @param {string} selector - selector
 * @returns {void}
 */
function initCache(selector) {
    $cache.context = $(selector);
    $cache.window = $(window);
    $cache.document = $(document);
    $cache.html = $('html');
    $cache.page = $('.js-page');
}

/**
 * @function
 * @description Add or remove sticky class for element or HTML tag
 * @param {Object} $element - $element
 * @param {boolean} activate - activate
 */
function updateActiveClasses($element, activate) {
    $element.toggleClass('is-sticky', activate);
}

/**
 * @function
 * @description Keep static height of element
 * @param {Object} $element - $element
 * @param {Object} stickyElementOptions - stickyElementOptions
 * @param {number} height - height of element
 */
function updateSpacer($element, stickyElementOptions, height) {
    var $spacerElement = $element.next(stickyElementOptions.spacer);

    if ($spacerElement.length === 0) {
        return;
    }

    $spacerElement.height(height);
}

/**
 * @function
 * @description Disable Sticky State
 * @param {Object} $element - $element
 * @param {Object} stickyElementOptions - stickyElementOptions
 */
function disableStickyState($element, stickyElementOptions) {
    updateActiveClasses($element, false);

    updateSpacer($element, stickyElementOptions, 0);

    if (stickyElementOptions.stuckBottom) {
        $cache.page.css('padding-bottom', 0);
    }

    stickyElementOptions.isStuck = false; // eslint-disable-line
}


/**
 * @function
 * @description Enable Sticky State
 * @param {Object} $element - $element
 * @param {Object} stickyElementOptions - stickyElementOptions
 * @param {Object} isScrolled - Get window.scrolY and activate sticky state if element riched active position
 */
function enableStickyState($element, stickyElementOptions, isScrolled) {
    if (isScrolled || window.scrollY > stickyElementOptions.startPoint) {
        updateActiveClasses($element, true);

        updateSpacer($element, stickyElementOptions, stickyElementOptions.elementInitialHeight);

        if (stickyElementOptions.stuckBottom) {
            var elementHeight = $element.outerHeight();

            $cache.page.css('padding-bottom', elementHeight);
        }

        stickyElementOptions.isStuck = true; // eslint-disable-line

        if (stickyElementOptions.setDataInWindow) {
            window[stickyElementOptions.setDataInWindow].enabled = true;
        }
    }
}

/**
 * @function
 * @description Update Sticky Element
 * @param {number} scrollY - scrollY
 * @param {Object} $element - element
 * @param {Object} stickyElementOptions - isScrollDirDown
 */
function updateStickyElement(scrollY, $element, stickyElementOptions) {
    var isEnableStickyState = scrollY > stickyElementOptions.startPoint;
    var isDisableStickyState = (scrollY <= stickyElementOptions.startPoint) || (stickyElementOptions.stopPoint && scrollY > stickyElementOptions.stopPoint);

    if (isEnableStickyState) {
        if (!stickyElementOptions.isStuck) {
            enableStickyState($element, stickyElementOptions, true);
        }
    }

    if (isDisableStickyState && stickyElementOptions.isStuck) {
        disableStickyState($element, stickyElementOptions);
    }
}

/**
 * @function
 * @description This handler will be called to recalculate sticky header block on 'scroll' event
 * @param {number} scrollY - scroll top position
 * @param {Object} $element - element
 * @param {Object} stickyElementOptions - stickyElementOptions
 * @param {boolean} isScrollDirDown - isScrollDirDown
 * @param {number} scrollStep - scrollStep
 */
function stickyElementsHandler(scrollY, $element, stickyElementOptions, isScrollDirDown, scrollStep) {
    updateStickyElement(scrollY, $element, stickyElementOptions, isScrollDirDown, scrollStep);
}

/**
 * @function
 * @description Destroy sticky elements
 * @param {Object} $element - jQuery Object
 * @param {Object} stickyElementOptions - options
 */
function unInitStickyElement($element, stickyElementOptions) {
    disableStickyState($element, stickyElementOptions);

    scrollListener.unRegisterHandler(stickyElementsHandler, $element);

    $element.data('sticky-inited', false);
}

/**
 * @function
 * @description Initialize sticky elements
 * @param {Object} $element - jQuery Object
 * @param {Object} params - Object
 * @returns {Void} - Void
 */
function initStickyElement($element, params) {
    if ($element.length === 0) {
        return false;
    }

    var windowWidth = $(window).width();
    var viewMode = windowWidth >= window.RA_BREAKPOINTS.md ? 'desktop' : 'mobile';

    var stickyElementDataOptions = $element.data('sticky') || {};
    var stickyElementOptions = $.extend({}, options, stickyElementDataOptions, params || {});

    if ($element.data('sticky-inited') === true) {
        unInitStickyElement($element, stickyElementOptions);
    }

    if ('initViewMode' in stickyElementDataOptions) {
        stickyElementOptions.initViewMode = stickyElementDataOptions.initViewMode;
    }

    var initViewMode = stickyElementOptions.initViewMode.length === 0 || stickyElementOptions.initViewMode.indexOf(viewMode) !== -1;

    if (!initViewMode) {
        return false;
    }

    if (stickyElementOptions.stuckSide === 'top') {
        stickyElementOptions.stuckTop = true;
    } else if (stickyElementOptions.stuckSide === 'bottom') {
        stickyElementOptions.stuckBottom = true;
    }

    stickyElementOptions.elementInitialHeight = $element.outerHeight();
    stickyElementOptions.stopPointValue = Infinity;

    if (stickyElementOptions.startPoint === 'self') {
        stickyElementOptions.startPoint = $element.offset().top;

        if (window.disableScroll) {
            stickyElementOptions.startPoint += $cache.page.scrollTop();
        }

        if (stickyElementOptions.stuckBottom) {
            stickyElementOptions.startPoint = stickyElementOptions.startPoint + stickyElementOptions.elementInitialHeight; // eslint-disable-line
        }
    }

    if (stickyElementOptions.stopPoint) {
        stickyElementOptions.slideSelfActivated = false;

        if (typeof (stickyElementOptions.stopPoint) === 'number') {
            stickyElementOptions.stopPointValue = stickyElementOptions.stopPoint;
        }
    }

    if (initViewMode) {
        enableStickyState($element, stickyElementOptions);
        scrollListener.registerHandler(stickyElementsHandler, $element, stickyElementOptions);
        $element.data('sticky-inited', true);

        $cache.window.scroll();
    }

    return true;
}

/**
 * @function
 * @description Initialize all sticky elements
 */
function initAllStickyElements() {
    $(cssSelectors.stickyElement).each(function (index, element) {
        initStickyElement($(element));
    });
}

/**
 * @function
 * @description Initialize events for sticky elements
 */
function initEvents() {
    $cache.document
        .on('window.orientationchange', function () {
            setTimeout(function () {
                initAllStickyElements();
            }, 100);
        });
}

var stickyelements = {
    init: function (params) {
        if (initialized) return;

        var parameters = params || {};

        initStickyElementsOptions(parameters);
        initCache(parameters.selector || document);
        initAllStickyElements();
        initEvents();

        initialized = true;
    }
};

module.exports = stickyelements;
