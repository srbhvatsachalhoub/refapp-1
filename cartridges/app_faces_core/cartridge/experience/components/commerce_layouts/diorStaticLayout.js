'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var PageRenderHelper = require('*/cartridge/experience/utilities/PageRenderHelper.js');

/**
 * Render logic for storefront.diorStaticLayout component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;
    var component = context.component;

    model.cssClass = context.content.cssClass || '';
    model.head = content.head || null;
    model.title = content.title || null;
    model.subTitle = content.subTitle || null;
    model.description = content.description || null;
    model.descriptionCenter = content.descriptionCenter || false;
    model.regions = PageRenderHelper.getRegionModelRegistry(component);

    return new Template('experience/components/commerce_layouts/diorStaticLayout').render(model).text;
};
