'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');

/**
 * Render logic for storefront.imageAndText component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */

module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.image = ImageTransformation.getScaledImage(content.image);
    model.imageTablet = content.imageTablet ? ImageTransformation.getScaledImage(content.imageTablet) : null;
    model.imageMobile = content.imageMobile ? ImageTransformation.getScaledImage(content.imageMobile) : null;
    model.alt = content.alt || null;
    model.heading = content.heading || null;
    model.subHeading = content.subHeading || null;
    model.description = content.description || '';
    model.position = content.position || '';
    model.buttonText = content.buttonText || null;
    model.buttonLink = content.buttonLink || null;
    model.carouselItemClassName = content.carouselItemClassName || '';

    return new Template('experience/components/commerce_assets/mainCarouselItem').render(model).text;
};
