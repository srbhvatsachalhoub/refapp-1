'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

/**
 * Render logic for textWithCustomClasses component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */

module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.text = content.text || '';
    model.classes = content.classes || '';

    return new Template('experience/components/commerce_assets/textWithCustomClass').render(model).text;
};
