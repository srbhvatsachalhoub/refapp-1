'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');


/**
 * Render logic for storefront.imageAndText component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */

module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;

    model.heading = content.heading || null;
    model.ITCText = content.ITCText || null;
    model.image = ImageTransformation.getScaledImage(content.image);
    model.link = content.ITCLink || null;
    model.alt = content.alt || null;
    model.imageType = content.ITCType;

    return new Template('experience/components/commerce_assets/imageAndText').render(model).text;
};
