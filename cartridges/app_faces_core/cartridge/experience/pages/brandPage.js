'use strict';
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var PageRenderHelper = require('*/cartridge/experience/utilities/PageRenderHelper.js');
var RegionModelRegistry = require('*/cartridge/experience/utilities/RegionModelRegistry.js');
var brandPageHelper = require('*/cartridge/scripts/helpers/brandPageHelper');
var CatalogMgr = require('dw/catalog/CatalogMgr');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var Locale = require('dw/util/Locale');

/**
 * Render logic for the Brand Page.
 *
 * @param {dw.experience.PageScriptContext} context The page script context object.
 *
 * @returns {string} The template text
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var page = context.page;
    var pageConfiguration = null;
    var pageType = null;
    var brandCategoryID = null;
    var pageCategory = null;

    model.page = page;

    var renderParameters = context.renderParameters;
    if (renderParameters) {
        var params = PageRenderHelper.parseRenderParameters(renderParameters);
        Object.keys(params).forEach(function (key) {
            model[key] = params[key];
        });
    }

    // automatically register configured regions
    var metaDefinition = require('*/cartridge/experience/pages/brandPage.json');
    model.regions = new RegionModelRegistry(page, metaDefinition);

    var currentLocale = Locale.getLocale(request.locale); // eslint-disable-line

    // Determine seo meta data.
    // Used in htmlHead.isml via common/layout/page.isml decorator.
    model.CurrentPageMetaData = PageRenderHelper.getPageMetaData(page);
    model.CurrentPageMetaData.title = page.pageTitle;
    model.CurrentPageMetaData.description = page.pageDescription;
    model.CurrentPageMetaData.keywords = page.pageKeywords;

    if (model.regions.pageConfiguration) {
        pageConfiguration = model.regions.pageConfiguration.region.getVisibleComponents()[0];
        pageType = pageConfiguration.getAttribute('pageConfigurationPageType');
        brandCategoryID = pageConfiguration.getAttribute('pageConfigurationPageCategory');

        if (brandCategoryID) {
            var locale = currentLocale.getCountry();

            try {
                var brandCategoryIDs = JSON.parse(brandCategoryID);

                if (brandCategoryIDs && typeof brandCategoryIDs === 'object' && Object.hasOwnProperty.call(brandCategoryIDs, locale)) {
                    brandCategoryID = brandCategoryIDs[locale];
                }
            } catch (e) {
                //
            }

            pageCategory = CatalogMgr.getCategory(brandCategoryID);
        }
    }

    if (pageCategory) {
        model.uniqueBrandPageData = brandPageHelper.getUniqueBrandPageData(pageCategory);
        model.brandCategoryID = brandCategoryID;

        var breadcrumbs = [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: model.uniqueBrandPageData.brandsRootCategory.displayName,
                url: URLUtils.url('Search-Show', 'cgid', model.uniqueBrandPageData.brandsRootCategory.ID)
            }
        ];

        if (pageType && pageType === 'CDP') {
            breadcrumbs.push(
                {
                    htmlValue: pageCategory.displayName + ' ' + Resource.msg('global.brandmenu.links', 'common', null),
                    url: model.uniqueBrandPageData.brandCategoryUrl
                },
                {
                    htmlValue: page.name
                }
            );
        } else {
            breadcrumbs.push(
                {
                    htmlValue: pageCategory.displayName
                }
            );
        }

        model.breadcrumbs = breadcrumbs;
    }

    if (PageRenderHelper.isInEditMode()) {
        var HookManager = require('dw/system/HookMgr');
        HookManager.callHook('app.experience.editmode', 'editmode');
        model.resetEditPDMode = true;
    }

    // render the page
    return new Template('experience/pages/brandPage').render(model).text;
};
