<isinclude template="/components/modules" sf-toolkit="off" />
<isdecorate template="common/layout/page">
    <isscript>
        var Site = require('dw/system/Site');
        var assets = require('*/cartridge/scripts/assets.js');
        assets.addCss('/css/cart.css');
        assets.addJs('/js/cart.js');
        if (Site.getCurrent().getCustomPreferenceValue('isGiftCardEnabled')) {
            assets.addJs('/js/giftCardDetail.js');
        }
    </isscript>

    <isinclude template="components/breadcrumbs/pageBreadcrumbs"/>

    <isif condition="${pdict.reportingURLs && pdict.reportingURLs.length}">
        <isinclude template="reporting/reportingUrls" />
    </isif>

    <isinclude template="cart/cartEmpty" />
    <isif condition="${pdict.items.length > 0}">
        <div class="container cart cart-page mt-0">
            <div class="row">
                <div class="col">
                    <h2 class="cart-title text-uppercase text-center mb-4">
                        ${Resource.msg('label.your.bag', 'cart', null)}
                        <span class="js-item-count d-none"
                            data-singular-tpl="(${Resource.msg('label.one.item.in.cart', 'cart', null)})"
                            data-plural-tpl="(${Resource.msg('label.x.items.in.cart', 'cart', null)})">
                            (${Resource.msgf(pdict.numItems === 1 ? 'label.one.item.in.cart' : 'label.x.items.in.cart', 'cart', null, pdict.numItems)})
                        </span>
                    </h2>
                </div>
            </div>
            <div class="row cart-page-wrapper">
                <div class="col-xl-8">
                    <div class="cart-error-messaging cart-error">
                        <isif condition="${pdict.valid && pdict.valid.error && pdict.items.length !== 0}">
                            <div class="alert alert-danger alert-dismissible valid-cart-error mb-4 fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                ${pdict.valid.message}
                            </div>
                        </isif>
                    </div>
                    <div class="row cart-header-row text-uppercase d-none d-xl-flex">
                        <div class="col-xl-6">
                            ${Resource.msg('label.products', 'checkout', null)}
                        </div>
                        <div class="col-xl-3">
                            ${Resource.msg('label.quantity', 'checkout', null)}
                        </div>
                        <div class="col-xl-3">
                            ${Resource.msg('label.price', 'checkout', null)}
                        </div>
                    </div>
                    <div class="cart-product-list">
                        <isloop items="${pdict.items}" var="lineItem">
                            <isset name="lineItemErrorCode"
                                value="${pdict.valid.invalidProductMap && pdict.valid.invalidProductMap[lineItem.id]}"
                                scope="page" />
                            <div class="cart-product-wrapper ${lineItemErrorCode ? 'invalid' : ''}"
                                data-error-code="${lineItemErrorCode}">
                                <isif condition="${lineItem.productType === 'bundle'}">
                                    <isinclude template="cart/productCard/cartBundleCard" />
                                <iselse/>
                                    <isif condition="${lineItem.noProduct === true}">
                                        <isinclude template="cart/productCard/uncategorizedCartProductCard" />
                                    <iselse/>
                                        <isinclude template="cart/productCard/cartProductCard" />
                                    </isif>
                                </isif>
                            </div>
                        </isloop>
                    </div>
                    <div class="js-sidebar-placeholder-mobile"></div>
                    <div class="row cart-menu-vertical row">
                        <isif condition="${pdict.bonusDiscountItems && pdict.bonusDiscountItems.length > 0}">
                            <div class="col-12">
                                <div class="cart-section-wrapper js-acc-container open">
                                    <h3 class="cart-section-title" data-toggle-class-open="parent">
                                        <i class="sc-icon sc-icon-samples tab-icon"></i>
                                        ${Resource.msg('label.select.free.samples', 'cart', null)}
                                        <i class="plus sc-icon-plus"></i>
                                        <i class="minus sc-icon-minus"></i>
                                    </h3>
                                    <div class="cart-section-content">
                                        <isinclude template="cart/bonusDiscountLineItems" />
                                    </div>
                                </div>
                            </div>
                        </isif>
                        <isif condition="${!pdict.hasGiftCardProduct && dw.system.Site.getCurrent().getCustomPreferenceValue('enableSendAsGift')}">
                            <isinclude template="cart/components/isGift" />
                        </isif>
                    </div>
                    <div class="row cart-free-shipping-wrapper">
                        <div class="col-12 js-cart-free-shipping-approaching-products">
                            <isinclude template="cart/freeShippingApproachingProducts" />
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 totals js-sticky-sidebar-container">
                    <iscomment>totals, and checkout actions</iscomment>
                    <div class="js-sticky-sidebar">
                        <div class="js-sticky-sidebar-inner">
                            <div class="next-step-box">
                                <div class="row mb-1">
                                    <div class="col-12">
                                        <div class="next-step-box-totals">
                                            <div class="totals-line">
                                                <div class="sub-total-label">
                                                    <span class="sub-total-label-text">${Resource.msg('label.sub.total', 'cart', null)}
                                                    </span>
                                                    <span class="sub-total-label-text">(${Resource.msg('label.vat.included', 'cart', null)})
                                                    </span>
                                                </div>

                                                <div class="totals-line-value order-sub-total price ">
                                                    <span
                                                        class='totals-price-discounted js-discounted-sub-total <isif condition="
                                                        ${!pdict.totals.orderLevelDiscountTotal.discountedSubTotal}">d-none</isif>'>
                                                        ${pdict.totals.orderLevelDiscountTotal.discountedSubTotal ? pdict.totals.orderLevelDiscountTotal.discountedSubTotal.formatted : ''}
                                                    </span>
                                                    <span
                                                        class='totals-price-discounted sub-total <isif condition="
                                                        ${pdict.totals.orderLevelDiscountTotal.discountedSubTotal}">totals-price-main strike-through</isif>'>
                                                        ${pdict.totals.subTotal}
                                                    </span>
                                                </div>
                                            </div>
                                            <div class='totals-line align-items-center js-discount-value-line mt-3 <isif condition="
                                                ${!pdict.totals.orderLevelDiscountTotal.discountedSubTotal}">d-none</isif>'>
                                                <div class="discount-label">
                                                    ${Resource.msg('label.discount', 'checkout', null)}
                                                </div>
                                                <div class="totals-line-value js-discount-value">
                                                    ${pdict.totals.orderLevelDiscountTotal.formatted}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <isinclude template="cart/cartApproachingDiscount" />
                                <isinclude template="cart/cartPromoCode" />
                                <div class="coupons-and-promos">
                                    <isinclude template="cart/cartCouponDisplay" />
                                </div>
                                <div class="row mt-3 d-none d-xl-flex">
                                    <div class="col-12 checkout-continue">
                                        <a href="${URLUtils.https('Checkout-Login')}"
                                            class="checkout-continue-btn btn btn-primary btn-block ${pdict.valid && pdict.valid.error ? 'disabled' : ''}"
                                            role="button" aria-pressed="true">
                                            <i class="sc-icon sc-icon-password mr-1"></i>
                                            ${Resource.msg('button.next.step', 'cart', null)}
                                        </a>
                                    </div>
                                </div>
                                <iscontentasset aid="checkout-supported-payment-methods" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mobile-next-step d-xl-none">
                <a href="${URLUtils.https('Checkout-Login')}"
                    class="checkout-continue-btn btn btn-primary btn-block ${pdict.valid && pdict.valid.error ? 'disabled' : ''}"
                    role="button" aria-pressed="true">
                    <i class="sc-icon sc-icon-password mr-1"></i>
                    ${Resource.msg('button.next.step', 'cart', null)}
                </a>
            </div>

            <isinclude template="cart/cartRemoveProductModal" />
        </div>

        <isinclude template="cart/cartRemoveCouponModal" />
    </isif>
    <div class="container">
        <isslot id="cart-recommendations-m" description="Recommended products" context="global" />
    </div>
</isdecorate>
