<iscomment>
    Logic to get default applied paymentMethod to assign paymentMethod hidden
</iscomment>
<isscript>
    var methodId = 'CREDIT_CARD';
    if (pdict.order.billing.payment.appliedPaymentInstruments && pdict.order.billing.payment.appliedPaymentInstruments.length) {
        for (var i = 0; i < pdict.order.billing.payment.appliedPaymentInstruments.length; i++) {
            var appliedMethod = pdict.order.billing.payment.appliedPaymentInstruments[i];

            if (appliedMethod !== 'GIFT_CERTIFICATE') {
                methodId = appliedMethod;
                break;
            }
        }
    } else if (pdict.order.billing.payment.applicablePaymentMethods && pdict.order.billing.payment.applicablePaymentMethods.length) {
        for (var i = 0; i < pdict.order.billing.payment.applicablePaymentMethods.length; i++) {
            var applicablePaymentMethod = pdict.order.billing.payment.applicablePaymentMethods[i].ID;

            if (applicablePaymentMethod !== 'GIFT_CERTIFICATE') {
                methodId = applicablePaymentMethod;
                break;
            }
        }
    }
</isscript>

<div class="form-nav billing-nav payment-information mb-0"
    data-create-payment-instrument="${ URLUtils.url('CheckoutServices-CreatePaymentInstrument') }"
    data-payment-method-id="${ methodId }"
    data-is-new-payment="${pdict.customer.registeredUser && pdict.customer.customerPaymentInstruments.length > 0 ? false : true}">
    <ul class="payment-options nav" role="tablist">
        <isloop items="${pdict.order.billing.payment.applicablePaymentMethods}" var="paymentOption" status="loopState">
            <isset name="isPaymentMethodActive" value="${
                pdict.order.billing.payment.appliedPaymentInstruments &&
                pdict.order.billing.payment.appliedPaymentInstruments.length > 0 ?
                    (pdict.order.billing.payment.appliedPaymentInstruments.indexOf(paymentOption.ID) > -1) : loopState.first
            }" scope="page" />

            <li class="payment-options-item d-flex align-items-center text-center">
                <iscomment> Selectable payment method items </iscomment>
                <a role="tab"
                    data-toggle="tab"
                    href="${'#ID_' + paymentOption.ID}"
                    data-method-id="${paymentOption.ID}"
                    data-has-payment-payment-instruments="${pdict.customer.registeredUser && pdict.customer.customerPaymentInstruments.length > 0}"
                    class="payment-options-link js-payment-method-tab
                        ${'payment-method-' + paymentOption.ID.toLowerCase()}
                        ${pdict.customer.registeredUser && pdict.customer.customerPaymentInstruments.length > 0 ? 'customer-has-payment-instruments' : '' }
                        ${pdict.customer.registeredUser ? 'customer-is-registered' : '' }
                        ${isPaymentMethodActive ? 'active' : '' }">
                    <i class="payment-options-link-icon sc-icon-${paymentOption.ID === 'CREDIT_CARD' ? 'payment-methods' : paymentOption.ID.toLowerCase()}"></i>
                    <span class="payment-options-link-text text-uppercase">
                        ${paymentOption.ID === 'CREDIT_CARD' ? Resource.msg('credit.card', 'checkout', null) : paymentOption.name}
                        <isif condition="${paymentOption.note}">
                            <span class="nav-link-note">
                                <isprint value="${paymentOption.note}" encoding="off" />
                            </span>
                        </isif>
                    </span>
                </a>
            </li>
            <isif condition="${paymentOption.serviceFee}">
                <span class="label-service-fee ml-auto">
                    +<isprint value="${paymentOption.serviceFee}" encoding="off" />
                </span>
            </isif>
        </isloop>
    </ul>
    <div class="payment-content tab-content">
        <isloop items="${pdict.order.billing.payment.applicablePaymentMethods}" var="paymentOption" status="loopState">
            <isset name="isPaymentMethodActive" value="${
                pdict.order.billing.payment.appliedPaymentInstruments &&
                pdict.order.billing.payment.appliedPaymentInstruments.length > 0 ?
                    (pdict.order.billing.payment.appliedPaymentInstruments.indexOf(paymentOption.ID) > -1) : loopState.first
            }" scope="page" />
            <div role="tabpanel"
                id="${'ID_' + paymentOption.ID}"
                class="payment-content-item tab-pane fade
                    ${'payment-method-' + paymentOption.ID.toLowerCase()}
                    ${pdict.customer.registeredUser && pdict.customer.customerPaymentInstruments.length > 0 ? 'customer-has-payment-instruments' : '' }
                    ${pdict.customer.registeredUser ? 'customer-is-registered' : '' }
                    ${isPaymentMethodActive ? 'show active' : '' }" >

                <iscomment> Apple Pay content </iscomment>
                <isif condition="${paymentOption.ID === 'DW_APPLE_PAY'}">
                    <isinclude template="checkout/billing/paymentOptions/applePayContent" />
                </isif>

                <iscomment> Credit card content </iscomment>
                <isif condition="${paymentOption.ID === 'CREDIT_CARD'}">
                    <isinclude template="checkout/billing/paymentOptions/creditCardContent" />

                    <iscomment> Add new credit card form </iscomment>
                    <isif condition="${pdict.customer.registeredUser && pdict.customer.customerPaymentInstruments.length > 0}">
                        <isloop items="${pdict.order.billing.payment.applicablePaymentMethods}" var="paymentOption" status="loopState">
                            <isif condition="${paymentOption.ID === 'CREDIT_CARD'}">
                                <div class="payment-add-new js-btn-add-new-credit-card" data-method-id="${paymentOption.ID}">
                                    <i></i>
                                    <div class="row">
                                        <div class="col-12 flex-10auto">
                                            <button class="payment-add-new-btn btn btn-link" type="button">
                                                ${Resource.msg('button.add.new.card', 'checkout', null)}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <fieldset class="credit-card-form fieldset-content border-bottom-0 checkout-hidden js-form-add-new-credit-card">
                                    <iscontentasset aid="checkout-supported-credit-cards" />
                                    <isinclude template="checkout/billing/creditCardForm" />
                                </fieldset>
                            </isif>
                        </isloop>
                    </isif>
                </isif>

                <iscomment> Cash on delivery content </iscomment>
                <isif condition="${paymentOption.ID === 'COD'}">
                     <div class="checkout-cash-delivery-content">
                        <div class="checkout-cash-delivery-with-price row">
                            <iscomment> TODO: fix paymentOption.serviceFee </iscomment>
                            <div class="checkout-cash-delivery-text col-8">
                                <isif condition="${ paymentOption.note }">
                                    <isprint value="${ paymentOption.note }" encoding="off"/>
                                </isif>
                            </div>
                            <isif condition="${paymentOption.serviceFee}">
                                <div class="checkout-cash-delivery-price text-right col-4">+<isprint value="${paymentOption.serviceFee}" encoding="off" /></div>
                            </isif>
                        </div>
                    </div>
                    <isinclude template="checkout/billing/paymentOptions/codContent" />
                </isif>
            </div>
        </isloop>
    </div>

    <iscomment>Store paymentMethod as hidden</iscomment>
    <input type="hidden" class="form-control"
            name="${pdict.forms.billingForm.paymentMethod.htmlName}"
            value="${ methodId }" />

    <input type="hidden" name="${pdict.csrf.tokenName}" value="${pdict.csrf.token}"/>
</div>
