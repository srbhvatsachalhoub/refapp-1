'use strict';

var server = require('server');
server.extend(module.superModule);
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');

/**
 * Extended to add breadcrumbs
 */
server.append('Show', function (req, res, next) {
    var viewData = res.getViewData();

    // breadcrumbs
    viewData.breadcrumbs = [
        {
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('title.cart', 'cart', null)
        }
    ];

    res.setViewData(viewData);

    next();
});


module.exports = server.exports();
