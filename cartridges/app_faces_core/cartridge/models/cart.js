'use strict';

var base = module.superModule;
var rrTemplateHelper = require('*/cartridge/scripts/helpers/rrTemplateHelper');

/**
 * Gets the free shipping approaching product tile params
 * @returns {Object} the product tile parameters object
 */
function getFreeShippingApproachingProductTileParams() {
    return {
        pview: 'tile',
        ratings: rrTemplateHelper.ratingsReviewsEnabled(),
        swatches: 'true',
        showBadges: 'true',
        showQuickView: 'false',
        showShopNowButton: 'false',
        showAddToBagButton: 'true',
        showName: 'true',
        showBrand: 'true',
        showWishList: 'true',
        shortDescription: 'false'
    };
}

base.getFreeShippingApproachingProductTileParams = getFreeShippingApproachingProductTileParams;
module.exports = base;
