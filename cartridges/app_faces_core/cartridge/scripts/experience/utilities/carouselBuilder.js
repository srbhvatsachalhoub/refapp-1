'use strict';

/* eslint no-param-reassign: ["error", { "props": true, "ignorePropertyModificationsFor": ["model"] }] */
var PageRenderHelper = require('*/cartridge/experience/utilities/PageRenderHelper.js');
var URLUtils = require('dw/web/URLUtils');

/**
 * Helper to encapsulate common code for building a carousel
 *
 * @param {Object} model - model object for a component
 * @param {Object} context - model object for a component
 * @return {Object} model - prepared model
 */
function init(model, context) {
    model.regions = PageRenderHelper.getRegionModelRegistry(context.component);
    var content = context.content;
    var numberOfSlides = model.regions.slides.region.size;
    var className = 'js-global-carousel carousels';

    model.regions.slides.setClassName(className);
    model.regions.slides.setAttribute('data-class-name', className);
    model.regions.slides.setAttribute('data-carousel-small-desktop-as-tablet', className);
    model.regions.slides.setAttribute('data-carousel-options',
        JSON.stringify({
            autoplay: content.mdCarouselAutoplay === 'true',
            autoplaySpeed: content.mdCarouselAutoplaySpeed,
            slidesToShow: content.mdCarouselSlidesToDisplay,
            slidesToScroll: content.mdCarouselSlidesToDisplay,
            rows: 1,
            dots: context.content.mdCarouselIndicators,
            arrows: context.content.mdCarouselControls
        }));
    model.regions.slides.setAttribute('data-carousel-options-tablet',
        JSON.stringify({
            autoplay: content.smCarouselAutoplay === 'true',
            autoplaySpeed: content.smCarouselAutoplaySpeed,
            slidesToShow: content.smCarouselSlidesToDisplay,
            slidesToScroll: content.smCarouselSlidesToDisplay,
            rows: 1,
            dots: context.content.smCarouselIndicators,
            arrows: context.content.smCarouselControls,
            carouselDynamicDotsPosition: content.smCarouselIndicatorsDynamicPosition
        }));
    model.regions.slides.setAttribute('data-carousel-options-mobile',
        JSON.stringify({
            autoplay: content.xsCarouselAutoplay === 'true',
            autoplaySpeed: content.xsCarouselAutoplaySpeed,
            slidesToShow: content.xsCarouselSlidesToDisplay,
            slidesToScroll: content.xsCarouselSlidesToDisplay,
            rows: 1,
            dots: context.content.xsCarouselIndicators,
            arrows: context.content.xsCarouselControls,
            carouselDynamicDotsPosition: content.xsCarouselIndicatorsDynamicPosition
        }));
    model.regions.slides.setComponentClassName('carousel-item');
    model.regions.slides.setComponentClassName('carousel-item active');

    for (var i = 0; i < numberOfSlides; i++) {
        model.regions.slides.setComponentAttribute('data-position', i, { position: i });
    }

    if (context.component.typeID === 'einstein.einsteinCarousel'
        || context.component.typeID === 'einstein.einsteinCarouselProduct'
        || context.component.typeID === 'einstein.einsteinCarouselCategory') {
        numberOfSlides = context.content.count;
    }

    model.id = 'carousel-' + context.component.getID();

    model.numberOfSlides = model.regions.slides.region.size;
    if (context.component.typeID === 'einstein.einsteinCarousel'
        || context.component.typeID === 'einstein.einsteinCarouselProduct'
        || context.component.typeID === 'einstein.einsteinCarouselCategory') {
        model.numberOfSlides = context.content.count - 1;
    }

    var carouselType = context.content.carouselType;
    var categoryProductIDs = null;
    var isProductCarousel = carouselType && (carouselType === 'Product Carousel' || carouselType === 'Product Category Carousel');

    model.productCarouselItems = null;
    model.title = context.content.textHeadline ? context.content.textHeadline : null;
    model.fullWidth = context.content.fullWidth ? context.content.fullWidth : false;
    model.productCarouselCategoryLimit = context.content.productCarouselCategoryLimit || 20;

    if (carouselType === 'Product Category Carousel' && context.content.productCarouselCategory) {
        var ProductSearchModel = require('dw/catalog/ProductSearchModel');
        var apiProductSearch = new ProductSearchModel();

        apiProductSearch.setCategoryID(context.content.productCarouselCategory.ID);
        apiProductSearch.search();

        var searchResultProducts = apiProductSearch.getRefinementCategory().products;

        var searchResultProductsIter = searchResultProducts.iterator();

        categoryProductIDs = [];

        var iterIndex = 0;

        while (iterIndex < model.productCarouselCategoryLimit && searchResultProductsIter.hasNext()) {
            var searchResultProduct = searchResultProductsIter.next();

            categoryProductIDs.push(searchResultProduct.ID);

            iterIndex++;
        }
    }

    if (isProductCarousel) {
        var configuredProductIDs = context.content.productCarouselItems ? context.content.productCarouselItems.replace(/ /g, '').split(',') : null;

        model.productCarouselItems = categoryProductIDs || configuredProductIDs;
        model.product = true;

        if (context.content.viewAllLinkCategoryID) {
            model.categoryUrl = URLUtils.url('Search-Show', 'cgid', context.content.viewAllLinkCategoryID);
        } else if (context.content.productCarouselCategory) {
            model.categoryUrl = URLUtils.url('Search-Show', 'cgid', context.content.productCarouselCategory.ID);
        }
    }

    model.productTileConfig = {
        productTileDisplayRatings: context.content.productTileDisplayRatings || false,
        productTileShowAddToBagButton: context.content.productTileShowAddToBagButton || false
    };

    model.hasVisibleSlides = model.regions.slides.region.getVisibleComponents().length > 0;
    model.regionRenderSettingsAttributes = model.regions.slides.regionRenderSettings.attributes;
    model.showViewAllLink = !!context.content.showViewAllLink;
    model.showViewAllButton = !!context.content.showViewAllButton;

    return model;
}

module.exports = {
    init: init
};
