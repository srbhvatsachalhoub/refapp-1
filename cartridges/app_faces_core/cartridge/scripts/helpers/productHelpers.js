'use strict';

var base = module.superModule;

var collections = require('*/cartridge/scripts/util/collections');
var ATTRIBUTE_NAME_COLOR = 'color';
var ATTRIBUTE_NAME_SIZE = 'size';


/**
 * Get a specific ProductVariationAttribute from a variationModel.productVariationAttributes collection
 * @param  {dw.util.Collection} productVariationAttributes - attribute value
 * @param  {string} attrId - attribute ID
 * @return {dw.catalog.ProductVariationAttribute} Represents a product variation attribute
 */
function getProductVariationAttribute(productVariationAttributes, attrId) {
    var dwAttr = collections.find(productVariationAttributes, function (item) {
        return item.attributeID === attrId || item.ID === attrId;
    });
    return dwAttr;
}

/**
 * Sets the initial attribute value into the params.variables to have selected attribute value
 * for now, only color
 * first color variation valiuye is being set, if no color is set for the request
 * @param {dw.catalog.Product} apiProduct - Product instance returned from the API
 * @param {Object} params - the query string params
 * @returns {Object} - the default selected params
 */
function setInitialAttrValues(apiProduct, params) {
    if (!apiProduct.master) {
        return params;
    }
    var variationModel = apiProduct.variationModel;
    if (!variationModel) {
        return params;
    }

    var variables = params.variables || {};
    var productVariationAttributes = variationModel.productVariationAttributes;
    if (!productVariationAttributes) {
        return params;
    }

    var lowPriceVariant;
    if (variationModel.master.priceModel && variationModel.master.priceModel.minPrice && variationModel.master.priceModel.minPrice.value) {
        var minPriceValue = variationModel.master.priceModel.minPrice.value;
        lowPriceVariant = collections.find(variationModel.variants, function (variant) {
            if (!variant.priceModel || !variant.priceModel.price || !variant.priceModel.price.value || !variant.getAvailabilityModel() || !variant.getAvailabilityModel().isInStock()) {
                return false;
            }
            return minPriceValue === variant.priceModel.price.value;
        });
        if (lowPriceVariant) {
            var productColorVariationAttribute = getProductVariationAttribute(productVariationAttributes, ATTRIBUTE_NAME_COLOR);
            var productSizeVariationAttribute = getProductVariationAttribute(productVariationAttributes, ATTRIBUTE_NAME_SIZE);
            if (productColorVariationAttribute && !variables[ATTRIBUTE_NAME_COLOR] && !variables[productColorVariationAttribute.ID]) {
                var variationColorValue = variationModel.getVariationValue(lowPriceVariant, productColorVariationAttribute);
                if (variationColorValue) {
                    variables[(productColorVariationAttribute.ID || ATTRIBUTE_NAME_COLOR)] = {
                        id: apiProduct.ID,
                        value: variationColorValue.value
                    };
                }
            } else if (productSizeVariationAttribute && !variables[ATTRIBUTE_NAME_SIZE] && !variables[productSizeVariationAttribute.ID]) {
                var variationSizeValue = variationModel.getVariationValue(lowPriceVariant, productSizeVariationAttribute);
                if (variationSizeValue) {
                    variables[(productSizeVariationAttribute.ID || ATTRIBUTE_NAME_SIZE)] = {
                        id: apiProduct.ID,
                        value: variationSizeValue.value
                    };
                }
            }
        }
        params.variables = variables; // eslint-disable-line no-param-reassign
    }
    return params;
}

base.setInitialAttrValues = setInitialAttrValues;
module.exports = base;
