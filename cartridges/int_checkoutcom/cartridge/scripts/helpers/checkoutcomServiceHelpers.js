'use strict';
/* global request */
/**
 * Class for checkout.com HTTP Service Initialization
 * HTTP Services
 * Service Name: checkoutcom.service
 * Profile Nane: checkoutcom.service.profile
 * Cerendials Name: checkoutcom.service.credentials.{CountryCode}
 */

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var Site = require('dw/system/Site');
var StringUtils = require('dw/util/StringUtils');
var URLUtils = require('dw/web/URLUtils');
var checkoutcomServiceConstants = require('*/cartridge/scripts/util/checkoutcomServiceConstants');
var checkoutcomConstants = require('*/cartridge/scripts/util/checkoutcomConstants');

/**
 * Gets the order locale,
 * if order is not provided then current request's locale object
 * @param {dw.order.Order} order - Current order
 * @returns {dw.util.Locale} the locale object
 */
function getOrderLocale(order) { // eslint-disable-line no-unused-vars
    var Locale = require('dw/util/Locale');

    var locale = order ? order.getCustomerLocaleID() : request.locale;
    return Locale.getLocale(locale);
}

/**
 * Gets the order language (ISO 639 code),
 * if order is not provided then current request's locale object's language
 * @param {dw.order.Order} order - Current order
 * @returns {string} the ISO 639 language code
 */
function getLanguage(order) {
    return getOrderLocale(order).getLanguage();
}

/**
 * Generates the checkoutcom service credential id
 * based on SitePref.checkoutcomServiceCredentialType
 * Single -> just return SitePref.checkoutcomServiceCredentialId
 * Country -> concat SitePref.checkoutcomServiceCredentialId with the current request or order country
 * Locale -> concat SitePref.checkoutcomServiceCredentialId with the current request or order locale or targetLocale
 * @param {dw.order.Order} order - Current order
 * @param {string} targetLocale - the target locale
 * @returns {string} the service credential id of the checkoutcom service
 */
function getServiceCredentialId(order, targetLocale) {
    // if serviceCredentialType is single
    // then just return sitePref.checkoutcomServiceCredentialId
    if (checkoutcomServiceConstants.serviceCredentialType === checkoutcomConstants.serviceCredentialType.single) {
        return checkoutcomServiceConstants.serviceCredentialId;
    }
    var Locale = require('dw/util/Locale');

    var locale = order ? order.getCustomerLocaleID() : (targetLocale || request.locale);
    switch (checkoutcomServiceConstants.serviceCredentialType) {
        case checkoutcomConstants.serviceCredentialType.country:
            // if serviceCredentialType is country
            // then return sitePref.checkoutcomServiceCredentialId + country
            // checkoutcom.service.credentials + '.' + 'ae' -> checkoutcom.service.credentials.ae
            return StringUtils.format('{0}.{1}', checkoutcomServiceConstants.serviceCredentialId, Locale.getLocale(locale).getCountry().toLowerCase());
        case checkoutcomConstants.serviceCredentialType.locale:
            // if serviceCredentialType is locale
            // then return sitePref.checkoutcomServiceCredentialId + locale
            // checkoutcom.service.credentials + '.' + 'en_AE' -> checkoutcom.service.credentials.en_ae
            return StringUtils.format('{0}.{1}', checkoutcomServiceConstants.serviceCredentialId, locale.toLowerCase());
        default:
            // not supoosed to be here, throw an error
            throw new Error(StringUtils.format('SitePref.checkoutcomServiceCredentialType can not recognized, value: {0}', checkoutcomServiceConstants.serviceCredentialType));
    }
}

/**
 * Gets the checkoutcom service url
 * based on action parameter
 * @param {string} action - Current action for the request
 * @param {Object} args - optional list of arguments or a collection, which are included into the result string
 * @returns {string} the url of the service
 */
function getServiceURL(action, args) {
    switch (action) {
        case checkoutcomConstants.action.paymentDetail:
            return StringUtils.format('{0}/{1}', checkoutcomServiceConstants.apiURL, args[0]);
        case checkoutcomConstants.action.capture:
            return StringUtils.format('{0}/{1}/captures', checkoutcomServiceConstants.apiURL, args[0]);
        case checkoutcomConstants.action.refund:
            return StringUtils.format('{0}/{1}/refunds', checkoutcomServiceConstants.apiURL, args[0]);
        case checkoutcomConstants.action.void:
            return StringUtils.format('{0}/{1}/voids', checkoutcomServiceConstants.apiURL, args[0]);
        case checkoutcomConstants.action.token:
            if (args && args[0] === checkoutcomConstants.sourceType.token) {
                return checkoutcomServiceConstants.apiURL;
            }
            return checkoutcomServiceConstants.tokensURL;
        default:
            return checkoutcomServiceConstants.apiURL;
    }
}

/**
 * Calculates the amount value that is send to checkoutcom
 * Before sending the amount value of any transaction,
 * it is needed to multiply the value with the currency decimal code according to ISO code 3.
 * For example: If the amount value was 500 AED; according to ISO code 3,
 * the value should be multiplied with 100 (2 decimal points); so it will be sent in the request as 50000.
 * Another example: If the amount value was 100 JOD; according to ISO code 3,
 * the value should be multiplied with 1000 (3 decimal points); so it will be sent in the request as 100000.
 * @param {dw.value.Money} amount - the amount of the transaction
 * @returns {number} returns the new amount as checkoutcom requested
 */
function calculateAmount(amount) {
    var Currency = require('dw/util/Currency');

    var currency = Currency.getCurrency(amount.currencyCode);
    return Math.round(amount.multiply(Math.pow(10, currency.defaultFractionDigits)).value);
}

/**
 * Prepares the service
 * Sets the credential of the service
 * Sets the url of the service
 * @param {dw.svc.Service} svc - The checkout.com service
 * @param {dw.order.Order} order - The current order
 * @param {string} action - The action -> TOKENIZATION, AUTHORIZATION, PURCHASE, ...
 * @param {Object} args - optional list of arguments or a collection
 */
function prepareService(svc, order, action, args) {
    svc.setCredentialID(getServiceCredentialId(order));
    svc.setURL(getServiceURL(action, args));
    switch (action) {
        case checkoutcomConstants.action.paymentDetail:
            svc.setRequestMethod('GET');
            break;
        default:
            svc.setRequestMethod('POST');
            break;
    }
}

/**
 * Gets the service credential
 * Prepares the service
 * Returns the credential of the service
 * @param {dw.svc.Service} svc - The checkout.com service
 * @param {dw.order.Order} order - The current order
 * @param {string} action - The action -> AUTHORIZATION, PURCHASE, ...
 * @param {Object} args - optional list of arguments or a collection
 * @returns {dw.svc.ServiceCredential} the service credential of the service
 */
function getServiceCredential(svc, order, action, args) {
    prepareService(svc, order, action, args);
    var serviceConfig = svc.getConfiguration();
    return serviceConfig.getCredential();
}

/**
 * Prepares the checkoutcom customer Object for the payload
 * @param {dw.order.Order} order - The current order
 * @returns {Object} the customer object
 */
function getCustomerForPayload(order) {
    var customer = {};
    if (order.customer && order.customer.registered) {
        var profile = order.customer.profile;
        var profileName = StringUtils.format('{0} {1}', profile.firstName, profile.lastName);
        customer = {
            email: profile.email || order.customerEmail, // could be externally registered
            name: profileName
        };
        if (profile.custom.checkoutcomCustomerId) {
            customer.id = profile.custom.checkoutcomCustomerId;
        }
    } else {
        customer = {
            email: order.customerEmail,
            name: order.customerName
        };
    }
    return customer;
}

/**
 * Adds/Extends the common fields for PaymentRequest payload
 * @param {dw.order.Order} order - The current order
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument
 * @param {Object} existingPayload - The existing payload
 * @returns {Object} the extended payload object
 */
function extendPaymentRequestCommonsToPayload(order, paymentInstrument, existingPayload) {
    var billingAddress = order.billingAddress;
    var shippingAddress = order.getDefaultShipment().shippingAddress;
    var payload = existingPayload;

    payload.source.billing_address = {
        address_line1: billingAddress.address1,
        city: billingAddress.city,
        zip: billingAddress.postalCode,
        country: billingAddress.countryCode.value
    };

    if (billingAddress.address2) {
        payload.source.billing_address.address_line2 = billingAddress.address2;
    }

    if (billingAddress.phone) {
        payload.source.phone = {
            // country_code:
            number: billingAddress.phone
        };
    }

    payload.amount = calculateAmount(paymentInstrument.paymentTransaction.amount);
    payload.currency = order.getCurrencyCode();
    payload.payment_type = checkoutcomConstants.paymentType.regular;
    payload.reference = order.orderNo;
    payload.description = order.orderNo;
    payload.capture = checkoutcomServiceConstants.autoCapture;
    payload.billing_descriptor = {
        name: Site.getCurrent().ID,
        city: 'Dubai'
    };
    payload.shipping = {
        address: {
            address_line1: shippingAddress.address1,
            city: shippingAddress.city,
            zip: shippingAddress.postalCode,
            country: shippingAddress.countryCode.value
        }
    };
    payload['3ds'] = {
        enabled: checkoutcomServiceConstants.threeDSecure,
        attempt_n3d: checkoutcomServiceConstants.nonThreeDSecure,
        eci: checkoutcomConstants.eci.ecommerce
    };
    payload.risk = {
        enabled: checkoutcomServiceConstants.risk
    };
    payload.success_url = URLUtils.https('CheckoutcomReturn-AuthorizationSuccess').toString();
    payload.failure_url = URLUtils.https('CheckoutcomReturn-AuthorizationFail').toString();

    if (shippingAddress.address2) {
        payload.shipping.address.address_line2 = shippingAddress.address2;
    }

    if (shippingAddress.phone) {
        payload.shipping.phone = {
            // country_code:
            number: shippingAddress.phone
        };
    }

    if (checkoutcomServiceConstants.autoCaptureTime > 0) {
        var Calendar = require('dw/util/Calendar');

        var currentCalendar = new Calendar();
        currentCalendar.add(Calendar.SECOND, checkoutcomServiceConstants.autoCaptureTime);
        payload.capture_on = currentCalendar.time.toISOString();
    }

    payload.customer = getCustomerForPayload(order);

    if (request.httpRemoteAddress) {
        payload.payment_ip = request.httpRemoteAddress;
    }

    if (paymentInstrument
        && paymentInstrument.creditCardType
        && paymentInstrument.creditCardType.equalsIgnoreCase(checkoutcomConstants.creditCardTypes.mada)) {
        payload['3ds'].enabled = true;
        payload.metadata = {
            udf1: 'mada'
        };
        delete payload.capture;
        delete payload.capture_on;
    }

    return payload;
}


/**
 * Prepares the checkoutcom Service Payload Object
 * According to given action
 * Returns the payload
 * @param {dw.order.Order} order - The current order
 * @param {Object} paymentInformation - The paymentInformation object
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument
 * @param {string} action - The action -> TOKENIZATION, AUTHORIZATION, PURCHASE, ...
 * @param {string} sourceType - The sourceType -> card, token, ...
 * @returns {Object} the payload JSON Request object
 */
function getPayload(order, paymentInformation, paymentInstrument, action, sourceType) {
    var payload = null;

    switch (action) {
        case checkoutcomConstants.action.authorization:
            switch (sourceType) {
                case checkoutcomConstants.sourceType.card:
                    payload = {
                        source: {
                            type: checkoutcomConstants.sourceType.card,
                            number: paymentInformation.cardNumber,
                            expiry_month: paymentInformation.expirationMonth,
                            expiry_year: paymentInformation.expirationYear,
                            name: paymentInformation.cardOwner || '',
                            cvv: paymentInformation.securityCode,
                            stored: paymentInformation.saveCard
                        }
                    };
                    payload = extendPaymentRequestCommonsToPayload(order, paymentInstrument, payload);

                    // pay with card token ?
                    if (paymentInformation.cardToken) {
                        payload.source.type = checkoutcomConstants.sourceType.id;
                        payload.source.id = paymentInformation.cardToken;
                        delete payload.source.number;
                        delete payload.source.expiry_month;
                        delete payload.source.expiry_year;
                    }
                    break;
                default:
                    break;
            }

            break;
        case checkoutcomConstants.action.paymentDetail:
            // no payload object, request will be GET
            break;
        case checkoutcomConstants.action.capture:
        case checkoutcomConstants.action.refund:
            payload = {
                amount: calculateAmount(paymentInstrument.paymentTransaction.amount),
                reference: order.orderNo
            };

            break;
        case checkoutcomConstants.action.void:
            payload = {
                reference: order.orderNo
            };

            break;
        case checkoutcomConstants.action.token:
            switch (sourceType) {
                case checkoutcomConstants.sourceType.applepay:
                    payload = {
                        type: checkoutcomConstants.sourceType.applepay,
                        token_data: {
                            version: paymentInformation.payment.token.paymentData.version,
                            data: paymentInformation.payment.token.paymentData.data,
                            signature: paymentInformation.payment.token.paymentData.signature,
                            header: paymentInformation.payment.token.paymentData.header
                        }
                    };

                    break;
                case checkoutcomConstants.sourceType.token:
                    payload = {
                        source: {
                            type: checkoutcomConstants.sourceType.token,
                            token: paymentInformation.token
                        }
                    };
                    payload = extendPaymentRequestCommonsToPayload(order, paymentInstrument, payload);

                    break;
                default:
                    break;
            }
            break;
        case checkoutcomConstants.action.knet:
            payload = {
                source: {
                    type: checkoutcomConstants.sourceType.knet,
                    language: getLanguage(order),
                    user_defined_field1: order.orderNo
                }
            };
            payload = extendPaymentRequestCommonsToPayload(order, paymentInstrument, payload);

            break;
        default:
            // not supoosed to be here, throw an error
            throw new Error(StringUtils.format('the action can not be recognized, orderNo: {0}, action: {0}', order.orderNo, action));
    }

    return payload;
}

/**
 * Prepares the service
 * Prepares the checkout.com Service Payload Object
 * Returns the payload
 * @param {dw.svc.Service} svc - The checkout.com service
 * @param {dw.order.Order} order - The current order
 * @param {Object} paymentInformation - The paymentInformation object
 * @param {dw.order.PaymentInstrument} paymentInstrument - The payment instrument
 * @param {string} action - The action -> TOKENIZATION, AUTHORIZATION, PURCHASE, ...
 * @param {string} sourceType - The sourceType -> card, token, ...
 * @param {Object} args - optional list of arguments or a collection
 * @returns {Object} the payload JSON Request object
 */
function initService(svc, order, paymentInformation, paymentInstrument, action, sourceType, args) {
    getServiceCredential(svc, order, action, args);
    return getPayload(
        order,
        paymentInformation,
        paymentInstrument,
        action,
        sourceType
    );
}

/**
 * Creates an instance of the checkoutcom.service service.
 * {dw.svc.LocalServiceRegistry} - checkoutcom service object
 */
var checkoutcomService = LocalServiceRegistry.createService('checkoutcom.service', {
    /**
     * checkout.com Service ServiceCallback createRequest function
     * @param {dw.svc.Service} svc - The service
     * @param {Object} payload - the request JSON object
     * @returns {object} the request data object
     */

    createRequest: function (svc, payload) { // eslint-disable-line consistent-return
        svc.setAuthentication('NONE');
        svc.addHeader('Authorization', !payload || !payload.type || payload.type !== checkoutcomConstants.sourceType.applepay ? svc.getConfiguration().getCredential().password : svc.getConfiguration().getCredential().user);
        svc.addHeader('Content-Type', 'application/json');

        if (payload) {
            return JSON.stringify(payload);
        }
    },
    parseResponse: function (svc, client) {
        return JSON.parse(client.text);
    }
});

module.exports = {
    checkoutcomService: checkoutcomService,
    initService: initService,
    getServiceCredential: getServiceCredential,
    getPayload: getPayload,
    getServiceCredentialId: getServiceCredentialId
};
