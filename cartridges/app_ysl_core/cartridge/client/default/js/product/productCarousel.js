'use strict';

var nextArrow = require('../templates/slickArrowNext');
var prevArrow = require('../templates/slickArrowPrev');
var carousel = require('brand_core/components/carousel');

/**
 * Initializes carousel
 * @param {jQuery} $container - Container element that holds carousel items
 */
function initCarousel($container) {
    carousel($container, {
        dots: true,
        infinite: true,
        nextArrow: nextArrow,
        prevArrow: prevArrow
    });
}

module.exports = function (selector) {
    selector = selector || '.js-product-carousel'; // eslint-disable-line

    initCarousel($(selector));

    $(selector + '-async').on('productLoader:loaded', function () {
        initCarousel($(this));
    });

    $('[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(e.target.hash).find(selector + '-async').slick('setPosition');
    });
};
