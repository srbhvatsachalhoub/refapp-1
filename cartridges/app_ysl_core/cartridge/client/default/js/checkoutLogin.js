'use strict';
var formHelpers = require('base/checkout/formErrors');
/**
 * initializeEvents for guest login
 */
function initalizeEvents() {
    var formSelector = '.js-guest-email-form';
    var $guestEmailForm = $(formSelector);
    $guestEmailForm.on('submit', function (e) {
        e.preventDefault();
        var url = $(this).attr('action');
        var method = $(this).attr('method');
        $(this).spinner().start();
        $.ajax({
            url: url,
            method: method,
            data: $(this).serialize(),
            success: function (data) {
                if (data) {
                    if (data.success && data.redirectUrl) {
                        window.location.href = data.redirectUrl;
                    } else if (data.fieldErrors.length) {
                        data.fieldErrors.forEach(function (error) {
                            if (Object.keys(error).length) {
                                formHelpers.loadFormErrors(formSelector, error);
                            }
                        });
                    }
                }
            },
            complete: function () {
                $guestEmailForm.spinner().stop();
            }
        });
        return false;
    });
}


$(document).ready(function () {
    initalizeEvents();
    // assign step numbers
    $('.js-step-number').each(function (index) {
        $(this).prepend(index + 1 + '.');
    });
});
