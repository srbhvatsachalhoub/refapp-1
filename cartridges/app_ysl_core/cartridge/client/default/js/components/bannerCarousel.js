'use strict';

var nextArrow = require('../templates/slickArrowNext');
var prevArrow = require('../templates/slickArrowPrev');
var carousel = require('brand_core/components/carousel');

module.exports = function (selector) {
    $('#slick-play-icon').hide();

    $('#slick-pause-icon').on('click', function () {
        $('.js-banner-carousel').slick('slickPause');
        $('#slick-pause-icon').hide();
        $('#slick-play-icon').show();
    });

    $('#slick-play-icon').on('click', function () {
        $('.js-banner-carousel').slick('slickPlay');
        $('#slick-pause-icon').show();
        $('#slick-play-icon').hide();
    });

    $(selector || '.js-banner-carousel').each(function () {
        carousel($(this), {
            dots: true,
            autoplay: true,
            autoplaySpeed: 3000,
            nextArrow: nextArrow,
            prevArrow: prevArrow
        });
    });
};
