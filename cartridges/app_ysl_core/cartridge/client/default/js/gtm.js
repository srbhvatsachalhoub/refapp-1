'use strict';

$(document).ready(function () {
    var $doc = $(document);
    window.dataLayer = window.dataLayer || [];
    var currencyCode = $('input.js-gtm-site-currencycode').val();

    $doc.on('click', '[data-gtm-enhancedecommerce-onclick]', function () {
        var $gtmOnclickElem = $(this);
        var gtmOnclickObject = $gtmOnclickElem.data('gtm-enhancedecommerce-onclick');
        if (!gtmOnclickObject) {
            return;
        }

        var quantity = $gtmOnclickElem.data('gtm-enhancedecommerce-onclick-quantity');
        var $quantityInput = $gtmOnclickElem.parents('.js-product-info').find('.js-quantity-input');
        if (!quantity
            && $quantityInput.length
            && gtmOnclickObject.ecommerce
            && (gtmOnclickObject.ecommerce.add || gtmOnclickObject.ecommerce.remove)
            && ($gtmOnclickElem.data('uuid') === $quantityInput.data('uuid') || (gtmOnclickObject.ecommerce.add || gtmOnclickObject.ecommerce.remove).products[0].id === $quantityInput.data('uuid'))) {
            quantity = $quantityInput.val();
        }

        if (quantity) {
            (gtmOnclickObject.ecommerce.add || gtmOnclickObject.ecommerce.remove).products[0].quantity = parseInt(quantity, 10);
        }

        if ($gtmOnclickElem.data('toggle') === 'modal') {
            var $target = $($gtmOnclickElem.data('target'));
            if ($target.length) {
                var $cartDeleteConfirmationBtn = $target.find('.cart-delete-confirmation-btn');
                if ($cartDeleteConfirmationBtn.length) {
                    $cartDeleteConfirmationBtn.attr('data-gtm-enhancedecommerce-onclick', '');
                    $cartDeleteConfirmationBtn.data('gtm-enhancedecommerce-onclick', gtmOnclickObject);
                    return;
                }
            }
        }

        window.dataLayer.push(gtmOnclickObject);
    });

    $doc.on('gtm:enhancedEcommercePushPageDataLayer', function (context, data) {
        if (!data.pageDataLayerEnhancedEcommerce) {
            return;
        }

        data.pageDataLayerEnhancedEcommerce.forEach(function (dataLayerEnhancedEcommerce) {
            window.dataLayer.push(dataLayerEnhancedEcommerce);
        });
    });

    $doc.on('gtm:enhancedEcommerceImpressions', function () {
        var productImpressions = [];
        var position = 0;
        $('[data-gtm-enhancedecommerce-impression]').each(function () {
            var $this = $(this);
            var gtmEnhancedEcommerceImpression = $this.data('gtm-enhancedecommerce-impression');
            if (gtmEnhancedEcommerceImpression.position) {
                return;
            }
            gtmEnhancedEcommerceImpression.position = ++position;
            $this.data('gtm-enhancedecommerce-impression', gtmEnhancedEcommerceImpression);
            productImpressions.push(gtmEnhancedEcommerceImpression);
        });
        if (productImpressions.length) {
            window.dataLayer.push({
                event: 'uaevent',
                eventCategory: 'Ecommerce',
                eventAction: 'Product Impressions',
                ecommerce: {
                    currencyCode: currencyCode,
                    impressions: productImpressions
                }
            });
        }
    });
    $doc.trigger('gtm:enhancedEcommerceImpressions', {});
    $doc.on('search:showMore:success', function () {
        $doc.trigger('gtm:enhancedEcommerceImpressions', {});
    });

    $doc.on('gtm:customerLoginEvent', function (context, data) {
        window.dataLayer.push({
            event: 'customerLogin',
            customerID: data.customerID,
            customerEmail: data.customerEmail
        });
    });

    $doc.on('gtm:customerRegisterEvent', function (context, data) {
        window.dataLayer.push({
            event: 'customerRegister',
            customerID: data.customerID,
            customerEmail: data.customerEmail
        });
    });
});
