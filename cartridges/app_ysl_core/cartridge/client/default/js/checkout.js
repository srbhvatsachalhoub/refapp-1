'use strict';
var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./checkout/checkout'));

    if (window.dw &&
        window.dw.applepay &&
        window.ApplePaySession &&
        window.ApplePaySession.canMakePayments()) {
        $('body').addClass('apple-pay-enabled');
    }

    processInclude(require('brand_core/checkout/smsVerification'));
    processInclude(require('brand_core/checkout/giftWrapper'));
    processInclude(require('./checkout/billingCustom')); // if billilng.js include on ysl, move on this billing js file.
});
