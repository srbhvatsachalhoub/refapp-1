'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var PageRenderHelper = require('*/cartridge/experience/utilities/PageRenderHelper.js');
var visibilityMapper = require('*/cartridge/experience/utilities/VisibilityMapper.js');

/**
 * Render logic for the storefront.popularCategories.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    var model = new HashMap();
    var content = context.content;
    var component = context.component;

    model.bgColor = content.bgColor;
    model.preheader = content.preheader || null;
    model.header = content.header || null;
    model.visibility = visibilityMapper(content.visibility);
    model.regions = PageRenderHelper.getRegionModelRegistry(component);

    return new Template('experience/components/commerce_layouts/exploreByCategory').render(model).text;
};
