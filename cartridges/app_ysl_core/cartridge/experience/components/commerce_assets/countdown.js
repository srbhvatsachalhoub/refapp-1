'use strict';
/* request */
var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');
var csrfProtection = require('dw/web/CSRFProtection');
var ImageTransformation = require('*/cartridge/experience/utilities/ImageTransformation.js');
var server = require('server');

/**
 * Render logic for storefront.productCountdown component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @returns {string} The template to be displayed
 */
module.exports.render = function (context) {
    try {
        var content = context.content;
        var params = '';
        var onlineFromDate = '';
        // eslint-disable-next-line no-undef
        if (request.httpParameters.params) {
            // eslint-disable-next-line no-undef
            params = JSON.parse(JSON.parse(request.httpParameters.params[0]).custom);
            onlineFromDate = params.params.onlineFrom;
        }

        var subscribeForm = server.forms.getForm('subscribe');
        subscribeForm.clear();
        var model = new HashMap();
        model.image = ImageTransformation.getScaledImage(content.image);
        model.title = content.title;
        model.subTitle = content.subTitle;
        model.onlineFrom = onlineFromDate;
        model.subscribeForm = subscribeForm;
        var csrf = {
            tokenName: csrfProtection.getTokenName(),
            token: csrfProtection.generateToken()
        };
        model.csrf = csrf;
        return new Template('experience/components/commerce_assets/product/countdown').render(model).text;
    } catch (ex) {
        return new Template('error/notFound').render().text;
    }
};
