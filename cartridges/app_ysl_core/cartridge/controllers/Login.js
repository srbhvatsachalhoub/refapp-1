'use strict';

var server = require('server');
server.extend(module.superModule);


var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var digitHelpers = require('*/cartridge/scripts/helpers/digitHelpers');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var gtmDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmDataLayerHelpers');


/**
 * Gets a localized error message regarding the case
 * @param {string} errorCase Error case
 * @returns {string} error message
 */
function getErrorMessage(errorCase) {
    switch (errorCase) {
        case 'EXISTING_CUSTOMER':
            return Resource.msg('message.error.login.existingCustomer', 'login', null);
        case 'NO_PROFILE':
            return Resource.msg('message.error.login.noProfile', 'login', null);
        default: return Resource.msg('message.error.login.unknown', 'login', null);
    }
}


/**
 * Appends login parameters to view data
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 */
function appendLoginParameters(req, res, next) {
    var Site = require('dw/system/Site');

    var oAuthFacebookProviderID = Site.getCurrent().getCustomPreferenceValue('oAuthFacebookProviderID');
    if (oAuthFacebookProviderID) {
        res.setViewData({
            oAuthFacebookProviderID: oAuthFacebookProviderID
        });
    }

    var oAuthGoogleProviderID = Site.getCurrent().getCustomPreferenceValue('oAuthGoogleProviderID');
    if (oAuthGoogleProviderID) {
        res.setViewData({
            oAuthGoogleProviderID: oAuthGoogleProviderID
        });
    }

    if (req.querystring.error === '1') {
        res.setViewData({
            errorMessage: getErrorMessage(req.querystring.case)
        });
    }

    res.setViewData({
        years: digitHelpers.getYearsAsOptionList(req, 1900)
    });

    next();
}


/**
 * Renders given template for login form
 * @param {Object} req - Request object
 * @param {Object} res - Response object
 * @param {Function} next - Next call in the middleware chain
 * @param {string} template - Template path that needs to be rendered
 */
function includeLoginForm(req, res, next, template) {
    var target = req.querystring.rurl || 1;
    var rememberMe = false;
    var userName = '';
    var actionUrl = URLUtils.url('Account-Login', 'rurl', target);
    var createAccountUrl = URLUtils.url('Account-SubmitRegistration', 'rurl', target).relative().toString();

    if (req.currentCustomer.credentials) {
        rememberMe = true;
        userName = req.currentCustomer.credentials.username;
    }

    var profileForm = server.forms.getForm('profile');
    profileForm.clear();

    res.render(template, {
        rememberMe: rememberMe,
        userName: userName,
        actionUrl: actionUrl,
        rurl: target,
        profileForm: profileForm,
        oAuthReentryEndpoint: 1,
        createAccountUrl: createAccountUrl
    });

    next();
}

server.get(
    'MiniLogin',
    consentTracking.consent,
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        includeLoginForm(req, res, next, 'account/components/miniLogin');
    },
    appendLoginParameters
);


/**
 * Extend viewdata by setting page data layer object
 */
server.append('Show', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    next();
});

module.exports = server.exports();
