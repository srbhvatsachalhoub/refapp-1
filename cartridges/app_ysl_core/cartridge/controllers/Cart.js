'use strict';

var gtmDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmDataLayerHelpers');
var gtmEnhancedEcommerceDataLayerHelpers = require('*/cartridge/scripts/helpers/gtmEnhancedEcommerceDataLayerHelpers');

var server = require('server');
server.extend(module.superModule);

server.append('MiniCart', server.middleware.include, function (req, res, next) {
    var viewData = res.getViewData();
    var template = (req.querystring.view && req.querystring.view === 'mobile') ? 'components/header/components/miniCartIcon' : '/components/header/miniCart';

    res.render(template, viewData);
    next();
});

server.prepend('MiniCartShow', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');

    // need to create a basket for MiniCartShow if basket is not created before for the session
    // in order to show approaching free shipping discount
    BasketMgr.getCurrentOrNewBasket();
    next();
});

/**
 * UpdateNonNestedBonusProducts Controller Action
 * Add choice of bonus(es) to the basket
 */
server.append('UpdateNonNestedBonusProducts', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var OrderModel = require('*/cartridge/models/order');
    var localeHelpers = require('*/cartridge/scripts/helpers/localeHelpers');

    var config = {
        numberOfLineItems: '*'
    };
    var currentBasket = BasketMgr.getCurrentBasket();
    var orderModel = new OrderModel(
        currentBasket,
        { config: config, countryCode: localeHelpers.getCurrentCountryCode(), containerView: 'order' }
    );

    res.setViewData({ order: orderModel });

    return next();
});

/**
 * Calculates the unit and total prices of items
 * that are just added to cart.
 * Extended to disable to provide bonusDiscountLineItem
 */
server.append('AddProduct', function (req, res, next) {
    var viewData = res.getViewData();

    // disable pdp bonus items popup in ysl
    viewData.newBonusDiscountLineItem = {};

    res.setViewData(viewData);

    next();
});

/**
 * Extend viewdata by setting page data layer object
 */
server.append('Show', function (req, res, next) {
    gtmDataLayerHelpers.attachCachablePageDataLayer(req, res);
    gtmEnhancedEcommerceDataLayerHelpers.checkout(req, res);
    next();
});

module.exports = server.exports();
