<isinclude template="/components/modules" sf-toolkit="off" />
<isdecorate template="common/layout/checkoutOnlyLogos">
    <isscript>
        var assets = require('*/cartridge/scripts/assets.js');
        assets.addJs('/js/checkoutLogin.js');
        assets.addCss('/css/checkoutLogin.css');
        assets.addCss('/css/cart.css');
    </isscript>

    <isif condition="${pdict.reportingURLs && pdict.reportingURLs.length}">
        <isinclude template="reporting/reportingUrls" />
    </isif>
    <div class="checkout-login-container">
        <div class="container">
            <div class="row">
                <div class="col-12 px-0">
                    <h4 class="checkout-section-title">
                        <isprint value="${Resource.msg('label.checkout', 'checkout', null)}" encoding="off" />
                    </h4>
                </div>
                <div class="col-lg-8">
                    <div class="row step-wrapper step-one p-0">
                        <div class="col-12 px-0 px-lg-3">
                            <div class="step-title d-flex justify-content-start">
                                <h5 class="js-step-number">
                                    <isprint value="${Resource.msg('label.checkout.step.sign.in', 'checkout', null)}" encoding="off" />
                                </h5>
                            </div>
                        </div>
                        <div class="col-lg-12 d-none d-lg-block">
                            <div class="row">
                                <div class="col-6">
                                    <isinclude template="account/components/loginForm" />
                                    <isinclude template="account/password/requestPasswordResetModal" />
                                </div>
                                <div class="col-6">
                                    <isif condition="${!dw.system.Site.getCurrent().getCustomPreferenceValue('disableGuestCheckout')}">
                                        <isinclude template="account/components/guestCheckoutForm.isml" />
                                    </isif>
                                    <iscontentasset aid="create-account-checkout" />
                                </div>
                            </div>
                            <div class="col-12 px-0">
                                <div class="border-bottom pb-3 mb-3"></div>
                            </div>
                        </div>
                        <div class="checkout-account-container">
                            <div class="checkout-tab d-block d-lg-none">
                                <div class="container p-0">
                                    <div class="row">
                                        <div class="d-block col-12">
                                            <div class="checkout-account-section" data-toggle-class-open="parent">
                                                <p class="col-6 p-0">${Resource.msg('label.registered.members', 'login', null)}</p>
                                                <p class="col-6 p-0 d-flex justify-content-end align-items-center">
                                                    <i class="icon-minus"></i>
                                                    <i class="icon-plus"></i>
                                                </p>
                                            </div>
                                            <div class="checkout-tab-content">
                                                <isinclude template="account/components/loginForm" />
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="checkout-account-section" data-toggle-class-open="parent">
                                                <p class="col-6 p-0">${Resource.msg('label.checkout.as.guest', 'checkout', null)}</p>
                                                <div class="col-6 p-0 d-flex justify-content-end align-items-center">
                                                    <i class="icon-minus"></i>
                                                    <i class="icon-plus"></i>
                                                </div>
                                            </div>
                                            <div class="checkout-tab-content">
                                                <isif condition="${!dw.system.Site.getCurrent().getCustomPreferenceValue('disableGuestCheckout')}">
                                                    <isinclude template="account/components/guestCheckoutForm.isml" />
                                                </isif>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="checkout-account-section" data-toggle-class-open="parent">
                                                <p class="col-6 p-0">${Resource.msg('link.create.an.account', 'login', null)}</p>
                                                <div class="col-6 p-0 d-flex justify-content-end align-items-center">
                                                    <i class="icon-minus"></i>
                                                    <i class="icon-plus"></i>
                                                </div>
                                            </div>
                                            <div class="checkout-tab-content">
                                                <iscontentasset aid="create-account-message" />
                                                <a href="${URLUtils.url('Login-Show', 'action', 'register', 'rurl', pdict.rurl)}" 
                                                   class="btn btn-primary btn-block mt-3">
                                                    ${Resource.msg('link.create.an.account', 'login', null)}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <isif condition="${pdict.hasSample}">
                        <div class="row step-wrapper px-0">
                            <div class="col-12 px-0 px-lg-3">
                                <div class="step-title">
                                    <h5 class="js-step-number">
                                        <isprint value="${Resource.msg('label.checkout.step.select.samples', 'checkout', null)}" encoding="off" />
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </isif>
                    <div class="row step-wrapper px-0">
                        <div class="col-12 px-0 px-lg-3">
                            <div class="step-title">
                                <h5 class="js-step-number">
                                    <isprint value="${Resource.msg('label.checkout.step.shipping', 'checkout', null)}" encoding="off" />
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="row step-wrapper px-0">
                        <div class="col-12 px-0 px-lg-3">
                            <div class="step-title">
                                <h5 class="js-step-number">
                                    <isprint value="${Resource.msg('label.checkout.step.review', 'checkout', null)}" encoding="off" />
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 px-0 px-lg-3">
                    <div class="next-step-box">
                        <isinclude template="checkout/components/orderSummaryCart" />
                        <div class="row">
                            <div class="col-12">
                                <div class="summary-payment-description">
                                    <iscontentasset aid="summary-payment-description" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <isif condition="${pdict.order}">
                        <isinclude template="checkout/orderProductSummary" />
                    </isif>
                    <div class="checkout-customer-service d-none d-lg-block">
                        <iscontentasset aid="cart-customer-service" />
                    </div>
                    <div class="d-lg-none">
                        <iscontentasset aid="checkout-supported-payment-methods" />
                    </div>
                </div>
                <iscomment> <div class="col-12 px-3 d-flex justify-content-between">
                    <h2 class="checkout-title">
                        ${Resource.msg('label.your.bag', 'cart', null)}
                    </h2>
                    <h5>
                        ${Resource.msg('label.edit.cart', 'cart', null)}
                    </h5>
                </div>
                <div class="col-12 d-flex justify-content-center">
                    <isloop items="${pdict.basketModel.items}" var="lineItem">
                        <isif condition="${lineItem.productType === 'bundle'}">
                            <isinclude template="cart/productCard/cartBundleCard" />
                        <iselse/>
                            <isif condition="${lineItem.noProduct === true}">
                                <isinclude template="cart/productCard/uncategorizedCartProductCard" />
                            <iselse/>
                                <isinclude template="cart/productCard/cartProductCard" />
                            </isif>
                        </isif>
                    </isloop>
                </div> </iscomment>
            </div>
        </div>
    </div>
</isdecorate>
