<isinclude template="/components/modules" sf-toolkit="off" />
<isdecorate template="common/layout/page">
    <isscript>
        var assets = require('*/cartridge/scripts/assets.js');
        if (pdict.googleMapsApi) {
            assets.addJs(pdict.googleMapsApi);
        }
        assets.addJs('/js/storeLocator.js');
        assets.addCss('/css/storeLocator.css');
    </isscript>

    <isinclude template="/components/breadcrumbs/pageBreadcrumbs" />

    <div class="container pt-4 store-locator-wrapper px-0-lg-down max-width-100-lg-down">
        <iscontainertitle containertitle_value="${Resource.msg('title.hero.text', 'storeLocator', null)}" />

        <div class="container store-detail-container px-0-lg-down max-width-100-lg-down">
            <div class="flex-column-reverse flex-xl-row row">
                <div class="col-xl-5">
                    <isset name="store" value="${pdict.store}" scope="page" />
                    <div id="${store.ID}">
                        <isinclude template="/storeLocator/components/detailStoreDetail" />
                    </div>
                </div>
                <div class="col-xl-7 px-0-lg-down">
                    <div class="row d-block d-xl-none mx-0-lg-down">
                        <div class="col">
                            <h2 class="store-detail-title mb-3 text-center">${store.name}</h2>
                        </div>
                    </div>
                    <div class="map-canvas" data-has-google-api="${pdict.googleMapsApi ? true : false}"
                        data-gmap-marker-image="https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld={0}|000|FFF"
                        data-locations="${pdict.locations}">
                        <div class="container">
                            <p class="store-locator-no-apiKey alert alert-danger">
                                ${Resource.msg('error.no.api.key', 'storeLocator', null)}</p>
                        </div>
                    </div>
                    <div class="row d-flex d-xl-none mb-4 mx-0-lg-down">
                        <div class="col-12 px-0-lg-down">
                            <div class="border-bottom">
                                <div class="row py-2 mx-0-lg-down">
                                    <div class="col-6 store-directions-column">
                                        <a class="js-get-directions get-directions-btn d-flex" target='_blank'
                                            href="https://maps.google.com/?daddr=${store.latitude},${store.longitude}">
                                            <i class="sc-icon-store-directions"></i>
                                            ${Resource.msg('link.getdirections', 'storeLocator', null)}
                                        </a>
                                    </div>
                                    <div class="col-6">
                                        <isif condition="${store.phone}">
                                            <div class="store-phone-mobile">
                                                <i class="sc-icon-store-phone mr-3"></i>
                                                <p class="store-detail-phone">
                                                    <a class="storelocator-phone" href="tel:${store.phone}">
                                                        ${store.phone}</a>
                                                </p>
                                            </div>
                                        </isif>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <iscontentasset aid="store-locator-services" />
</isdecorate>
