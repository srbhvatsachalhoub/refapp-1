'use strict';

var base = module.superModule;

/**
 * Gets currentBasket's uuid
 * Calls base createOrder
 * If success removes the abandoned cart object that belongs to the basket
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {dw.order.Order} The order object created from the current basket
 */
function createOrder(currentBasket) {
    var basketUUID = currentBasket.UUID;
    var order = base.createOrder(currentBasket);
    if (!order) {
        return order;
    }

    var abandonedCartHelpers = require('*/cartridge/scripts/helpers/abandonedCartHelpers');
    abandonedCartHelpers.remove(basketUUID);

    return order;
}

Object.keys(base || {}).filter(function (baseExport) {
    return baseExport !== 'createOrder';
}).forEach(function (baseExport) {
    module.exports[baseExport] = base[baseExport];
});

module.exports.createOrder = createOrder;

