'use strict';
/* global request */

var Site = require('dw/system/Site');
var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
var logger = require('dw/system/Logger').getLogger('abandonedcartjob');
var XMLStreamWriter = require('dw/io/XMLStreamWriter');
var StringWriter = require('dw/io/StringWriter');

/**
 * Prepares abandonedcart e-mail and sends it.
 * @param {dw.object.CustomObject} abandonedCart - AbandonedCart Object
 * @returns {boolean} status of operation.
 */
function triggerEmail(abandonedCart) {
    // check the custom object locale to send the email in correct language
    if (request.locale !== abandonedCart.custom.customerLocale) {
        request.setLocale(abandonedCart.custom.customerLocale);
    }
    var sw = new StringWriter();
    var xsw = new XMLStreamWriter(sw);
    /* eslint-disable */
    xsw.writeStartDocument();
    xsw.writeStartElement("order");
        xsw.writeAttribute("order-no", abandonedCart.custom.basketID);
        xsw.writeStartElement("currency");
            xsw.writeCharacters(abandonedCart.custom.currencyCode);
        xsw.writeEndElement();
        xsw.writeStartElement("customer");
            xsw.writeStartElement("customer-name");
                xsw.writeCharacters(abandonedCart.custom.customerName);
            xsw.writeEndElement();
        xsw.writeEndElement();
        xsw.writeStartElement('product-lineitems');
        var lineItems = JSON.parse(abandonedCart.custom.lineItems);
        for (var i = 0; i < lineItems.length; i++) {
            var lineItem = lineItems[i];
            xsw.writeStartElement('product-lineitem');
                xsw.writeStartElement('gross-price');
                    xsw.writeCharacters(lineItem.productGrossPrice);
                xsw.writeEndElement();
                xsw.writeStartElement('base-price');
                    xsw.writeCharacters(lineItem.productBasePrice);
                xsw.writeEndElement();
                xsw.writeStartElement('product-id');
                    xsw.writeCharacters(lineItem.productId);
                xsw.writeEndElement();
                xsw.writeStartElement('product-name');
                    xsw.writeCharacters(lineItem.productName);
                xsw.writeEndElement();
                xsw.writeStartElement('quantity');
                    xsw.writeAttribute("unit", "");
                    xsw.writeCharacters(lineItem.productQuantity);
                xsw.writeEndElement();
            xsw.writeEndElement();
        }
        xsw.writeEndElement();
    xsw.writeStartElement('totals');
        xsw.writeStartElement('merchandize-total');
            xsw.writeStartElement('gross-price');
                xsw.writeCharacters(abandonedCart.custom.adjustedTotalPrice);
            xsw.writeEndElement();
        xsw.writeEndElement();
    xsw.writeEndElement();
    xsw.writeEndElement();
    xsw.writeEndDocument();
    xsw.flush();
    var orderAsXML = sw.toString();
    xsw.close();
    var emailObj = {
        to: abandonedCart.custom.customerEmail,
        subject: 'subject',
        from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
        type: emailHelpers.emailTypes.abandonedCart
    };

    var abandonedCartMailObj = {
        email: abandonedCart.custom.customerEmail,
        OrderAsXML: orderAsXML,
        Locale: abandonedCart.custom.customerLocale
    };

    try {
        emailHelpers.sendEmail(emailObj, 'abandonedCart/abandonedCartEmail', { AbandonedCart: abandonedCartMailObj });
        return true;
    } catch (e) {
        logger.error('While firing abandonedcart e-mail for {0}, an unexpected error occured! {1}', abandonedCart.UUID, e.message);
        return false;
    }
}

/**
 * Iterates abandoned cart objects and fires abandoned cart e-mails.
 * @param {dw.util.HashMap} args - inputs from the BM
 * @returns {dw.system.Status} status object
 */
function execute(args) {
    var Calendar = require('dw/util/Calendar');
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var Transaction = require('dw/system/Transaction');
    var Status = require('dw/system/Status');

    var delayInMinutes = args.DelayInMinutes || 30;

    var calendar = Site.current.calendar;
    calendar.add(Calendar.MINUTE, -1 * delayInMinutes);

    var abandonedCartsIterator = CustomObjectMgr.queryCustomObjects(
        'AbandonedCart',
        'lastModified <= {0} AND NOT(custom.isSubmitted = true)',
        null,
        calendar.time
    );

    if (!abandonedCartsIterator.count) {
        logger.info('There is no abandoned carts to submit to SFMC');
        return new Status(Status.OK);
    }

    while (abandonedCartsIterator.hasNext()) {
        var abandonedCart = abandonedCartsIterator.next();
        try {
            triggerEmail(abandonedCart);
            Transaction.wrap(function () { // eslint-disable-line no-loop-func
                // we do not need to remove custom object which has 7 days retention, will be removed automatically. Meanwhile we will have inspect chance for objects within 7 days.
                abandonedCart.custom.isSubmitted = true;
                abandonedCart.custom.submissionDate = new Date();
            });
        } catch (e) {
            logger.error('While processing abandoned cart : {0} an error occured {1}', abandonedCart.custom.basketID, e.message);
        }
    }

    return new Status(Status.OK);
}

module.exports = exports = {
    execute: execute
};
